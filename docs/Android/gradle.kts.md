# build.gradle.kts

```kotlin
val kotlinVersion: String by extra

plugins {
    id("com.android.application")
    id("kotlin-android")
    id("kotlin-android-extensions")
    id("kotlin-kapt")
    id("androidx.navigation.safeargs.kotlin")
}
apply {
    plugin("kotlin-android")
}

android {
    compileSdkVersion(30)
    defaultConfig {
        applicationId = "com.lxb.weike"
        minSdkVersion(21)
        targetSdkVersion(30)
        versionCode = 1
        versionName = "1.1"
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        vectorDrawables.useSupportLibrary = true
        ndk {
            abiFilters.addAll(arrayOf("armeabi", "armeabi-v7a", "x86", "arm64-v8a", "x86_64"))
        }

//        manifestPlaceholders = [
//                JPUSH_PKGNAME: "com.lxb.weike",
//                JPUSH_APPKEY : "eafd534bafcc72cdf53c902c", //JPush上注册的包名对应的appkey.
//                JPUSH_CHANNEL: "developer-default", //暂时填写默认值即可.
//        ]
        multiDexEnabled = true

        javaCompileOptions {
            annotationProcessorOptions {
                argument("room.schemaLocation", "$projectDir/schemas")
                argument("room.incremental", "true")
                argument("room.expandProjection", "true")
            }
        }
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")
        }
        getByName("debug") {
//            isMinifyEnabled = true
            resValue("string", "PORT_NUMBER", "8081")
        }
    }

    dexOptions {
        javaMaxHeapSize = "4g"
    }
    productFlavors {
    }

    android.applicationVariants.all {
        val buildTypeName = this.buildType.name
        outputs.all {
//            outputFileName = "weike_v${defaultConfig.versionName}_${variant.buildType.name}.apk"
            if (this is com.android.build.gradle.internal.api.ApkVariantOutputImpl) {
                this.outputFileName = "weike_v${defaultConfig.versionName}_${buildTypeName}.apk"
            }
        }
    }

    compileOptions {
        // Flag to enable support for the new language APIs
        isCoreLibraryDesugaringEnabled = true
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    kotlinOptions {
        jvmTarget = "1.8"
    }

    buildToolsVersion = "30.0.2"
    ndkVersion = "21.1.6352462"

//    fun listSubFile(): ArrayList<String> {
//        val resFolder = "src/main/res/layouts"
//        val files = file(resFolder).listFiles()
//        val folders = ArrayList<String>()
//        files?.let {
//            it.forEach { file ->
//                folders.add(file.absolutePath)
//            }
//        }
//        folders.add(file(resFolder).parentFile.absolutePath)
//        return folders
//    }
//
//    sourceSets {
//        getByName("main") {
//            res.srcDirs("src/main/res", "src/main/res/layouts", listSubFile())
//        }
//    }
}

//compileKotlin {
//    sourceCompatibility = JavaVersion.VERSION_1_8
//    targetCompatibility = JavaVersion.VERSION_1_8
//
//    kotlinOptions {
//        jvmTarget = '1.8'
//        apiVersion = '1.1'
//        languageVersion = '1.1'
//    }
//}

//static def buildTime() {
//    def date = new Date()
//    def formattedDate = date.format('yyyyMMdd')
//    return formattedDate
//}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))
//    implementation("androidx.legacy:legacy-support-v4:1.0.0")

    coreLibraryDesugaring("com.android.tools:desugar_jdk_libs:1.1.0")

    implementation("androidx.multidex:multidex:2.0.1")
    implementation("androidx.appcompat:appcompat:1.3.0-alpha02")
    implementation("androidx.appcompat:appcompat-resources:1.3.0-alpha02")
    implementation("androidx.core:core-ktx:1.5.0-alpha04")
    implementation("androidx.annotation:annotation:1.2.0-alpha01")
    implementation("androidx.constraintlayout:constraintlayout:2.0.4")

    implementation("androidx.viewpager2:viewpager2:1.0.0")
    implementation("androidx.recyclerview:recyclerview:1.2.0-alpha06")
    implementation("androidx.vectordrawable:vectordrawable:1.2.0-alpha02")

    implementation("androidx.activity:activity-ktx:1.2.0-beta01")
    implementation("androidx.fragment:fragment-ktx:1.3.0-beta01")
    debugImplementation("androidx.fragment:fragment-testing:1.3.0-beta01")

    implementation("androidx.navigation:navigation-fragment-ktx:2.3.1")
    implementation("androidx.navigation:navigation-ui-ktx:2.3.1")

    testImplementation("junit:junit:4.13.1")
    androidTestImplementation("androidx.test:runner:1.3.0")
    androidTestImplementation("androidx.test.espresso:espresso-core:3.3.0")

    implementation(project(":baseadapter"))
    implementation(project(":chatinputview"))
    implementation(project(":panel-androidx"))
//    implementation("com.effective.android:panelSwitchHelper-androidx:1.3.10")

    implementation("com.blankj:utilcodex:1.30.4")

    implementation("com.google.android.material:material:1.2.1")
    implementation("com.google.code.gson:gson:2.8.6")

    implementation("com.github.promeg:tinypinyin:2.0.3")// TinyPinyin核心包，约80KB
    implementation("com.github.promeg:tinypinyin-lexicons-android-cncity:2.0.3")
    // 可选，适用于Android的中国地区词典

    implementation("com.scwang.smart:refresh-layout-kernel:2.0.1")      //核心必须依赖
    implementation("com.scwang.smart:refresh-header-classics:2.0.1")    //经典刷新头
//    implementation("com.scwang.smart:refresh-header-radar:2.0.1")       //雷达刷新头
//    implementation("com.scwang.smart:refresh-header-falsify:2.0.1")     //虚拟刷新头
//    implementation("com.scwang.smart:refresh-header-material:2.0.1")    //谷歌刷新头
//    implementation("com.scwang.smart:refresh-header-two-level:2.0.1")   //二级刷新头
//    implementation("com.scwang.smart:refresh-footer-ball:2.0.1")        //球脉冲加载
    implementation("com.scwang.smart:refresh-footer-classics:2.0.1")    //经典加载

    implementation("com.github.bumptech.glide:glide:4.11.0")
    kapt("com.github.bumptech.glide:compiler:4.11.0")

    implementation("io.reactivex.rxjava2:rxjava:2.2.20")
    implementation("io.reactivex.rxjava2:rxandroid:2.1.1")

    val autodisposeVersion = "1.4.0"
    implementation("com.uber.autodispose:autodispose:$autodisposeVersion")
    api("com.uber.autodispose:autodispose-android:$autodisposeVersion")
    api("com.uber.autodispose:autodispose-android-archcomponents:$autodisposeVersion")

    val okhttpVersion = "4.9.0"
    implementation("com.squareup.okhttp3:okhttp:$okhttpVersion")
    implementation("com.squareup.okhttp3:logging-interceptor:$okhttpVersion")

    val retrofit2Version = "2.9.0"
    implementation("com.squareup.retrofit2:retrofit:$retrofit2Version")
    implementation("com.squareup.retrofit2:converter-gson:$retrofit2Version")
    implementation("com.squareup.retrofit2:adapter-rxjava2:$retrofit2Version")

    implementation("com.haibin:calendarview:3.6.9")
    implementation("org.greenrobot:eventbus:3.2.0")

    implementation("me.zhanghai.android.materialratingbar:library:1.4.0")
    implementation("com.github.yuzhiqiang1993:zxing:2.2.8")
    implementation("com.github.PhilJay:MPAndroidChart:v3.1.0")

    implementation("com.google.android:flexbox:2.0.1")
    implementation("com.afollestad.material-dialogs:core:3.3.0")
    implementation("com.afollestad.material-dialogs:input:3.3.0")
    implementation("com.github.LuckSiege.PictureSelector:picture_library:v2.5.9")
    implementation("app.futured.donut:library:2.0.0")
    implementation("com.github.gabriel-TheCode:AestheticDialogs:1.3.3")

    implementation("com.gyf.immersionbar:immersionbar:3.0.0")
    implementation("com.alibaba.android:vlayout:1.2.36@aar") {
        isTransitive = true
    }

    implementation("com.amap.api:location:5.2.0")
    implementation("com.amap.api:search:7.7.0")
    implementation("com.amap.api:3dmap:7.7.0")

//    implementation "cn.jiguang.sdk:jpush:3.1.3"  // 此处以JPush 3.1.1 版本为例。
//    implementation "cn.jiguang.sdk:jcore:1.2.1"  // 此处以JCore 1.1.9 版本为例。

    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:1.4.10")

    val lifecycleVersion = "2.3.0-beta01"
    implementation("androidx.lifecycle:lifecycle-viewmodel-ktx:$lifecycleVersion")
    implementation("androidx.lifecycle:lifecycle-livedata-ktx:$lifecycleVersion")
    implementation("androidx.lifecycle:lifecycle-common-java8:$lifecycleVersion")

    val workVersion = "2.4.0"
    implementation("androidx.work:work-runtime-ktx:$workVersion")
    implementation("androidx.work:work-rxjava2:$workVersion")
    androidTestImplementation("androidx.work:work-testing:$workVersion")

    val roomVersion = "2.2.5"
    implementation("androidx.room:room-runtime:$roomVersion")
    implementation("androidx.room:room-rxjava2:$roomVersion")
    kapt("androidx.room:room-compiler:$roomVersion")
    implementation("androidx.room:room-ktx:$roomVersion")
    testImplementation("androidx.room:room-testing:$roomVersion")

    val pagingVersion = "2.1.2"
    implementation("androidx.paging:paging-runtime-ktx:$pagingVersion") // use -ktx for Kotlin
    implementation("androidx.paging:paging-rxjava2-ktx:$pagingVersion") // use -ktx for Kotlin
    testImplementation("androidx.paging:paging-common-ktx:$pagingVersion") // use -ktx for Kotlin

    // https://github.com/amitshekhariitbhu/Android-Debug-Database
    debugImplementation("com.amitshekhar.android:debug-db:1.0.6")
}

androidExtensions {
    isExperimental = true
}
repositories {
    mavenCentral()
}
```
