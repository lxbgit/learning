# ViewModel

## `ViewModel`是什么？

> ViewModel 类旨在以注重生命周期的方式存储和管理界面相关的数据.

这里面有两个信息：

- 注重生命周期的方式。

由于ViewModel的生命周期是作用于整个Activity的，所以就节省了一些关于状态维护的工作，最明显的就是对于屏幕旋转这种情况，以前对数据进行保存读取，而ViewModel则不需要，他可以自动保留数据。

其次，由于ViewModel在生命周期内会保持局部单例，所以可以更方便Activity的多个Fragment之间通信，因为他们能获取到同一个ViewModel实例，也就是数据状态可以共享了。

- 存储和管理界面相关的数据。

ViewModel层的根本职责，就是负责维护界面上UI的状态，其实就是维护对应的数据，因为数据会最终体现到UI界面上。所以ViewModel层其实就是对界面相关的数据进行管理，存储等操作。

## ViewModel 为什么被设计出来，解决了什么问题？

1）不会因为屏幕旋转而销毁，减少了维护状态的工作
2）由于在作用域内单一实例的特性，使得多个fragment之间可以方便通信，并且维护同一个数据状态。
3）完善了MVVM架构，使得解耦更加纯粹。

问： 在ViewModel组件被设计出来之前，MVVM又是怎么实现ViewModel这一层级的呢？
答： 其实就是自己编写类，然后通过接口，内部依赖实现View和数据的双向绑定

## 说说ViewModel原理

- ViewModel的创建与销毁

创建：

`ViewModel`的初始化很简单，通过`ViewModelProvider`就可以获取到`ViewModel`实例

销毁：

可以看到`ViewModel`中有`clear()`方法和`onCleared()`方法。通过跟踪方法的调用可以知道`ViewModel`的销毁过程。最终在`ViewModelStore`类中找到了`clear()`方法。

Activity中的销毁:

> 在ComponentActivity的构造方法中，可以看到通过`Lifecycle`在`ON_DESTROY`事件中销毁`ViewModel`。

```java
getLifecycle().addObserver(new LifecycleEventObserver() {
            @Override
            public void onStateChanged(@NonNull LifecycleOwner source,
                    @NonNull Lifecycle.Event event) {
                if (event == Lifecycle.Event.ON_DESTROY) {
                    if (!isChangingConfigurations()) {
                        // 销毁ViewModel
                        getViewModelStore().clear();
                    }
                }
            }
        });
```

Fragment中的销毁:

首先通过代码跟踪到`ViewModelStore`的`clear()`方法调用的地方，在`FragmentManagerViewModel`类的`clearNonConfigState()`方法中找到了ViewModel的销毁逻辑

```java
void clearNonConfigState(@NonNull Fragment f) {
    ...
    // Clear and remove the Fragment's ViewModelStore
    ViewModelStore viewModelStore = mViewModelStores.get(f.mWho);
    if (viewModelStore != null) {
        // 销毁ViewModel
        viewModelStore.clear();
        mViewModelStores.remove(f.mWho);
    }
}
```

如果继续跟踪代码可以看到代码的调用栈是 `FragmentStateManager::destroy()` -> (Fragment状态切换)->`FragmentManager::dispatchDestroy()`->`FragmentActivity::onDestory()`。

> `ViewModel`的销毁在`onDestory()`中，是晚于`onDestoryView()`的，所以要注意在使用`ViewModel`做操作时会不会触发组件更新。不然的话可能造成空指针异常

- ViewModel的生命周期如何与组件生命周期绑定？

> 主要还是通过Lifecycle和组件的生命周期方法来进行回调管理。

- 为什么Activity重建时ViewModel的数据没有丢失？

按照上面的逻辑，在Activity重建时会执行destory生命周期事件，那么为什么ViewModel没有销毁呢？

还是直接在代码里找答案，通过对ComponentActivity的getViewModelStore()方法进行分析。可以找到这个问题的答案。

```java
public ViewModelStore getViewModelStore() {
    	...
        if (mViewModelStore == null) {
            NonConfigurationInstances nc =
                    (NonConfigurationInstances) getLastNonConfigurationInstance();
            if (nc != null) {
                // Restore the ViewModelStore from NonConfigurationInstances
                mViewModelStore = nc.viewModelStore;
            }
            if (mViewModelStore == null) {
                mViewModelStore = new ViewModelStore();
            }
        }
        return mViewModelStore;
    }
```

可以看`mViewModelStore`变量如果是null的话，会从`NonConfigurationInstances`实例中取。那么我们就分析`NonConfigurationInstances`实例的来源。

从`NonConfigurationInstances`的名称可以大致判断这个类与配置无关。

```java
public final Object onRetainNonConfigurationInstance() {
        Object custom = onRetainCustomNonConfigurationInstance();

        ViewModelStore viewModelStore = mViewModelStore;
        if (viewModelStore == null) {
            // No one called getViewModelStore(), so see if there was an existing
            // ViewModelStore from our last NonConfigurationInstance
            NonConfigurationInstances nc =
                    (NonConfigurationInstances) getLastNonConfigurationInstance();
            if (nc != null) {
                viewModelStore = nc.viewModelStore;
            }
        }

        if (viewModelStore == null && custom == null) {
            return null;
        }

        NonConfigurationInstances nci = new NonConfigurationInstances();
        nci.custom = custom;
        nci.viewModelStore = viewModelStore;
        return nci;
    }
```

继续分析下去，可以看到在`onRetainNonConfigurationInstance`中会存储`ViewModelStore`实例，这也就是为什么ViewModel不会在Activity重建时被销毁的原因。

- Fragment之前如何共享ViewModel？

共享的是`Activity`的`ViewModel`.
