# 更换 APP logo

## 配置 `AndroidManifest.xml`

```xml

<activity
    android:name=".ui.LoginActivity"
    android:enabled="false"
    android:exported="true"
    android:icon="@mipmap/app_logo"
    android:label="@string/app_name"
    android:roundIcon="@mipmap/app_logo"
    android:theme="@style/Theme.Composeapp.NoActionBar"
    android:windowSoftInputMode="adjustResize">
    <intent-filter>
        <action android:name="android.intent.action.MAIN" />

        <category android:name="android.intent.category.LAUNCHER" />
    </intent-filter>
</activity>

<activity-alias
    android:name="DefaultLogoLoginActivity"
    android:enabled="true"
    android:exported="true"
    android:icon="@mipmap/app_logo"
    android:label="@string/app_name"
    android:roundIcon="@mipmap/app_logo"
    android:targetActivity=".ui.LoginActivity"
    android:theme="@style/Theme.Composeapp.NoActionBar">
    <intent-filter>
        <action android:name="android.intent.action.MAIN" />

        <category android:name="android.intent.category.LAUNCHER" />
    </intent-filter>
</activity-alias>

<activity-alias
    android:name="DefaultLogo1LoginActivity"
    android:enabled="false"
    android:exported="true"
    android:icon="@mipmap/app_logo1"
    android:label="@string/app_name"
    android:roundIcon="@mipmap/app_logo1"
    android:targetActivity=".ui.LoginActivity"
    android:theme="@style/Theme.Composeapp.NoActionBar">
    <intent-filter>
        <action android:name="android.intent.action.MAIN" />

        <category android:name="android.intent.category.LAUNCHER" />
    </intent-filter>
</activity-alias>
```

## 代码启用禁用

```kotlin

val pm = ContextHolder.appContext.packageManager

# 启用
pm.setComponentEnabledSetting(
    ComponentName(ContextHolder.appContext, logo.componentName),
    PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
    PackageManager.DONT_KILL_APP
)

# 禁用
pm.setComponentEnabledSetting(
    ComponentName(ContextHolder.appContext, logo.componentName),
    PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
    PackageManager.DONT_KILL_APP
)

```
