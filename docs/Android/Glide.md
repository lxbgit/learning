# `Glide`

- `Glide` 有几级缓存

Glide三级缓存，内存-磁盘-网络

- 默认的磁盘缓存 `DiskLruCache`

LRU(最近最少使用)

- Glide磁盘缓存的一大特点是可以双重缓存：就是资源的原始大小的缓存以及每次裁剪后的最终资源的缓存，这样一来当请求相同规则的资源时可以直接从磁盘缓存获取，而不需要加载出原始文件进行裁剪，也就是以空间换时间；这些缓存策略可以设置(DiskCacheStrategy)；

- glide 流程简介
  
  1. `Glide.with(context/activity/fragment)`拿到界面绑定的唯一`RequestManager`，使请求与声明周期绑定

     - 非主线时: 创建了一个 `ApplicationLifecycle` ( 模拟生命周期 / 全局生命周期 ),放进一个全局唯一 `applicationManager` ( `RequestManager` 实例 ) 中

     - 主线程 + Activity: 往 Activity 插入一个没界面的 `RequestManagerFragment`,创建了一个 `ActivityFragmentLifecycle` 实例, 在 `RequestManagerFragment` 生命周期函数被调用时调用

     - 主线程 + Fragment 时: 往 Fragment 插入一个没界面的`SupportRequestManagerFragment`,创建了一个 `ActivityFragmentLifecycle` 实例, 在 `SupportRequestManagerFragment` 生命周期函数被调用时调用

  2. `requestManager.load(url)`，根据请求参数的类型找到相应的ModelLoader等组件，构建`LoadProvider`，并创建出`DrawableRequestBuilder`对象

  3. into(ImageView)，会根据设置或者ImageView的scaleType设置transformation，并根据transcode类型构建相应的Target对象，开始请求

  4. 请求时先将view上原有的request和资源清空(或回收或释放)，然后根据设置的裁剪尺寸或view自身的尺寸开始请求流程

  5. 先依次从两级内存缓存中查找(根据策略)，命中则回调通知，否则先到磁盘缓存里找

  6. 磁盘缓存依次寻找result和source资源(根据策略)，命中后通过MainHandler回调到主线程，进行callback回调，没有命中则从数据源请求

  7. 通过modelLoader从数据源进行请求，并完成磁盘缓存，最终生成Resouce资源，通过MainHandler回调到主线程，进行callback回调

  8. ImageView的Target作为回调拿到Resource后，将Drawable设置到ImageView上完成加载

