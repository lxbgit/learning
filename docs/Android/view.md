# View

[参考](http://blog.csdn.net/lixpjita39/article/details/75303856)

## TabLayout问题

1 . `TabItem` 明明设置的小写，但是运行出来确是大写，`Tablayout`有个属性叫`app:tabTextAppearance`，可以设置`TabItem`显示的大小写：

![QQ图片20170814093042.png](http://upload-images.jianshu.io/upload_images/1800520-7b4bab00d9328f69.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

```xml
<style name="TabLayoutTextAppearance" parent="TextAppearance.AppCompat.Widget.ActionBar.Title.Inverse">
    <item name="android:textSize">16sp</item>
    <!-- 在这设置 -->
    <item name="android:textAllCaps">false</item>
</style>

// 设置
app:tabTextAppearance="@style/TabLayoutTextAppearance"
```

设置之后：

![QQ图片20170814093103.png](http://upload-images.jianshu.io/upload_images/1800520-6fb9dc2294f6bcc6.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

> 建议在重写style之前，先看一些系统自带的样式，然后根据需求在重写

## GridLayout属性介绍

### 本身属性

- **android:alignmentMode**
> 说明：当设置alignMargins，使视图的外边界之间进行校准。可以取以下值: 1. `alignBounds` – 对齐子视图边界。 2. `alignMargins` – 对齐子视距内容。

- **android:columnCount**
> 说明：GridLayout的最大列数

- **android:rowCount**
> 说明：GridLayout的最大行数

- **android:columnOrderPreserved**
> 说明：当设置为true，使列边界显示的顺序和列索引的顺序相同。默认是true。

- **android:orientation**
> 说明：GridLayout中子元素的布局方向。有以下取值：`horizontal` – 水平布 / `vertical` – 竖直布局。

- **android:rowOrderPreserved**
> 说明：当设置为true，使行边界显示的顺序和行索引的顺序相同。默认是true。

- **android:useDefaultMargins**
> 说明： 当设置ture，当没有指定视图的布局参数时，告诉GridLayout使用默认的边距。默认值是false。

## 子元素属性

- **android:layout_column**

> 说明：显示该子控件的列，例如`android:layout_column=”0”`,表示当前子控件显示在第1列，`android:layout_column=”1”`,表示当前子控件显示在第2列。

- **android:layout_columnSpan**

> 说明：该控件所占的列数，例如`android:layout_columnSpan=”2”`,表示当前子控件占两列。

- **android:layout_row**

> 说明：显示该子控件的行，例如`android:layout_row=”0”`,表示当前子控件显示在第1行，`android:layout_row=”1”`,表示当前子控件显示在第2行。

- **android:layout_rowSpan**

> 说明：该控件所占的列数，例如`android:layout_rowSpan=”2”`,表示当前子控件占两行。

- **android:layout_columnWeight**

> 说明：该控件的列权重，与`android:layout_weight`类似，例如有GridLayout上两列，都设置`android:layout_columnWeight = “1”`,则两列各占GridLayout宽度的一半

- **android:layout_rowWeight**

> 说明：该控件的行权重，原理同`android:layout_columnWeight`。


## RecyclerViewDemo

> recyclerview常用的功能，根据不同的要求，为每一种要求简单写一个 demo（主要是练习，不喜勿喷！！）
最近读了[鸿洋大神的博客](http://blog.csdn.net/lmj623565791/article/details/51118836/)后，对RecyclerView非常感兴趣，根据自己的理解，以及大神的博客再加上一些APP需求，写出一下几个Demo;
最后，项目中对常用的Adapter做了一步封装，主要就是添加header、footer、loadMore

- 为recyclerView 添加header 和 footer

![headerAndFooter.gif](http://upload-images.jianshu.io/upload_images/1800520-f72201094b364310.gif?imageMogr2/auto-orient/strip)

- 将recyclerView的Header下拉过度放大，松开恢复

当RecyclerView下拉过度时，动态改变ImageView的高度
![scaleHeader.gif](http://upload-images.jianshu.io/upload_images/1800520-fabebd2171da82ed.gif?imageMogr2/auto-orient/strip)

- 为recyclerView 添加 sticky section header

主要使用ItemDecoration实现
![sectionDeco.gif](http://upload-images.jianshu.io/upload_images/1800520-65e62207850ca915.gif?imageMogr2/auto-orient/strip)

- 为recyclerView 添加 section header

使用FrameLayout+headerView的方式实现
![sticky.gif](http://upload-images.jianshu.io/upload_images/1800520-7667292d206e3c31.gif?imageMogr2/auto-orient/strip)

[github地址](https://github.com/dreamerlxb/RecyclerViewDemo)

## AMap定位

- 初始化定位监听

```java
   public AMapLocationClient mLocationClient = null;
   public AMapLocationListener mLocationListener = new AMapLocationListener() {

       @Override
       public void onLocationChanged(AMapLocation aMapLocation) {
           if (aMapLocation != null) {
               if (aMapLocation.getErrorCode() == 0) {
                    //可在其中解析amapLocation获取相应内容。
                    aMapLocation.getLocationType();//获取当前定位结果来源，如网络定位结果，详见定位类型表

                    //aMapLocation.getLatitude();//获取纬度
                    Log.i("lat", aMapLocation.getLatitude() + "");
                    Log.i("lng", aMapLocation.getLongitude() + "");

                    //aMapLocation.getLongitude();//获取经度
                    aMapLocation.getAccuracy();//获取精度信息
                    aMapLocation.getAddress();//地址，如果option中设置isNeedAddress为false，则没有此结果，网络定位结果中会有地址信息，GPS定位不返回地址信息。
                    aMapLocation.getCountry();//国家信息
                    aMapLocation.getProvince();//省信息
                    aMapLocation.getCity();//城市信息
                    aMapLocation.getDistrict();//城区信息
                    aMapLocation.getStreet();//街道信息
                    aMapLocation.getStreetNum();//街道门牌号信息
                    aMapLocation.getCityCode();//城市编码
                    aMapLocation.getAdCode();//地区编码
                    aMapLocation.getAoiName();//获取当前定位点的AOI信息
                    aMapLocation.getBuildingId();//获取当前室内定位的建筑物Id
                    aMapLocation.getFloor();//获取当前室内定位的楼层
                    aMapLocation.getGpsAccuracyStatus();//获取GPS的当前状态

                    //获取定位时间
                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Date date = new Date(aMapLocation.getTime());
                    df.format(date);
               } else {
                   //定位失败时，可通过ErrCode（错误码）信息来确定失败的原因，errInfo是错误信息，详见错误码表。
                   Log.e("AmapError","location Error, ErrCode:"
                           + aMapLocation.getErrorCode() + ", errInfo:"
                           + aMapLocation.getErrorInfo());
               }
           }
       }
   };
```

- 初始化地图代码

```java
private void initMapLocation() {
    //初始化定位服务
    mLocationClient = new AMapLocationClient(getApplicationContext());

    AMapLocationClientOption mLocationOption = new AMapLocationClientOption();
    //选择定位模式
    mLocationOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Hight_Accuracy);

    //低功耗定位模式
    mLocationOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Battery_Saving);

    //设置定位模式为AMapLocationMode.Device_Sensors，仅设备模式。
    mLocationOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Device_Sensors);

    //获取一次定位结果：
    //该方法默认为false。
    mLocationOption.setOnceLocation(true);

    //获取最近3s内精度最高的一次定位结果：
    //设置setOnceLocationLatest(boolean b)接口为true，启动定位时SDK会返回最近3s内精度最高的一次定位结果。如果设置其为true，setOnceLocation(boolean b)接口也会被设置为true，反之不会，默认为false。
    mLocationOption.setOnceLocationLatest(true);

    //设置定位间隔,单位毫秒,默认为2000ms，最低1000ms。
    mLocationOption.setInterval(1000);

    //设置是否返回地址信息（默认返回地址信息）
    mLocationOption.setNeedAddress(true);

    //单位是毫秒，默认30000毫秒，建议超时时间不要低于8000毫秒。
    mLocationOption.setHttpTimeOut(20000);

    //关闭缓存机制
    mLocationOption.setLocationCacheEnable(false);

    //设置定位回调监听
    mLocationClient.setLocationListener(mLocationListener);

    //给定位客户端对象设置定位参数
    mLocationClient.setLocationOption(mLocationOption);


    //启动定位
    mLocationClient.startLocation();
}
```
