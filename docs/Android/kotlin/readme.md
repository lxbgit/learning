# Android

- Kotlin/Java
- Gradle
- 四大组件(`Activity`/`Service`/`Broadcast`/`Provider`)
- Fragment
- Application
- View/ViewGroup
- Netty（通讯）
- Lifecycle （声明周期）
- MVVM（开发模式）
- 协程
- [Navigation 导航](https://developer.android.google.cn/jetpack/androidx/releases/navigation)
- Room 数据库
- [`Compose`](https://developer.android.google.cn/jetpack/compose)

## kotlin 跟Java相比，有哪些你觉得比较好用的功能

- 协程
- 方法扩展
- data class
- value class
- 高阶函数
- 空安全（?:）
