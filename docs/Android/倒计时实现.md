# 倒计时实现

- 第一种

```kotlin
   private var timer: CountDownTimer? = null

   /**
    * 修改发送按钮显示信息
    */
   private fun changeSendBtnInfo() {
       send_btn.isEnabled = false

       /** 倒计时60秒，一次1秒 */
       timer = object: CountDownTimer(60 * 1000, 1000) {
           override fun onTick(millisUntilFinished: Long) {
               send_btn.text = getString(R.string.phone_validation_code_count_down_tip, millisUntilFinished/1000)//"还剩${millisUntilFinished/1000}秒"
           }
           override fun onFinish() {
               send_btn.isEnabled = true
               send_btn.setText(R.string.send)
           }
       }

       timer?.start()
   }

   override fun onDestroyView() {
       timer?.cancel()
       super.onDestroyView()
   }
```

- 第二种

```kotlin
val handler = Handler()
var secondsLeft = 60

val runnable = object : Runnable {
    override fun run() {
        if (secondsLeft > 0) {
            secondsLeft--
            // 更新 UI，例如显示剩余秒数
            handler.postDelayed(this, 1000)
        } else {
            // 倒计时结束时执行的操作
            // 更新 UI，例如显示 "倒计时结束"
        }
    }
}
handler.postDelayed(runnable, 1000)

```

- 第三种

```kotlin
GlobalScope.launch(Dispatchers.Main) {
    var secondsLeft = 60
    while (secondsLeft > 0) {
        // 更新 UI，例如显示剩余秒数
        delay(1000)
        secondsLeft--
    }
    // 倒计时结束时执行的操作
    // 更新 UI，例如显示 "倒计时结束"
}

```
