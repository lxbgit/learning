# 查看Key Store

```shell
keytool -list -v -keystore /Users/lili/MyDocuments/AndroidKeyStore/doorplate-pad/ks.jks
```

> JKS 密钥库使用专用格式。建议使用 "keytool -importkeystore -srckeystore /Users/lili/MyDocuments/AndroidKeyStore/bjhlb-app/ks.jks -destkeystore /Userents/AndroidKeyStore/bjhlb-app/ks.jks -deststoretype pkcs12" 迁移到行业标准格式 PKCS12。

```shell
keytool -importkeystore -srckeystore /Users/lili/MyDocuments/AndroidKeyStore/bjhlb-app/ks.jks -destkeystore /Users/lili/MyDocuments/AndroidKeyStore/bjhlb-app/ks.jks -deststoretype pkcs12

```

## Android 默认获取sha1

```shell
keytool -list -v -keystore ~/.android/debug.keystore -alias androiddebugkey -storepass android -keypass android
```

## windows 下获取默认sha1

```shell
keytool -list -v -keystore C:\Users\Administrator\.android\debug.keystore -alias androiddebugkey -storepass android -keypass android
```

## 更新

```shell
# 获取sha1
/Users/apple/.sdkman/candidates/java/11.0.2-open/bin/keytool -list -v -keystore /Users/apple/AndroidStudioProjects/crypto-app/sig.jks
```

> 注意生成jks时，使用的java版本跟获取sha1时使用的java版本必须一致

// 以下获取MD5有问题
[//]: # (java8 以上 `keytool` 生成证书后， `keytool` 查看证书没有MD5)

[//]: # (java8以上获取MD5：)

[//]: # (```shell)

[//]: # (/Users/apple/.sdkman/candidates/java/11.0.2-open/bin/keytool -exportcert -keystore /Users/apple/AndroidStudioProjects/crypto-app/sig.jks | openssl dgst -md5)

[//]: # (```)

[//]: # (MD5: `58:e6:60:67:88:b7:33:d9:b3:d4:fe:76:55:11:24:4e`)
SHA1: `A0:04:8F:30:7C:C9:EA:5C:C8:5C:C5:76:B1:2C:F5:09:C2:2F:58:1E`
SHA256: `9A:EE:7C:8C:24:34:FB:E7:00:70:83:01:3F:1B:72:76:EA:C2:D7:2B:CC:CF:B1:EC:FD:7A:9F:C9:3E:72:5F:EC`


## 问题

查看sha1时，报错：`keytool 错误: java.io.IOException: Invalid keystore format`，可能的原因就是java版本不一致


# 获取MD5、SHA1

1. 在gradle配置 `signingConfigs`

2. 执行 `./gradlew signingReport`
