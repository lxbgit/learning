# gradle 配置

- 使用指定版本

```kotlin
allprojects {
//    repositories {
//        mavenCentral()
//        maven (url = "https://maven.aliyun.com/nexus/content/groups/public/")
//        maven(url = "https://jitpack.io")
//        maven( url = "https://developer.huawei.com/repo/")
//        google()
//    }

    //Support @JvmDefault
    tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile>(org.jetbrains.kotlin.gradle.tasks.KotlinCompile::class.java) {
        kotlinOptions {
            freeCompilerArgs = listOf("-Xjvm-default=enable")
            jvmTarget = "11"
        }
    }

    configurations {
        all {
            resolutionStrategy.force("androidx.compose.animation:animation:${Libraries.Compose.version}")
            resolutionStrategy.eachDependency {
                if (requested.group == "androidx.compose.animation"
                    || requested.group == "androidx.compose.runtime"
                    || requested.group == "androidx.compose.ui"
                    || requested.group == "androidx.compose.foundation") {
                    useVersion(Libraries.Compose.version)
                }
            }
        }
    }
}
```

## 使用gradle获取MD5、SHA1

- 在gradle中配置 `signingConfigs`

```kotlin
android {
    // ...
     signingConfigs {
            getByName("debug") {
                storeFile =
                    file(rootProject.ext["storeFile"].toString()) //File("/Users/apple/AndroidStudioProjects/crypto-app/sig.jks")
                storePassword = rootProject.ext["storePassword"].toString()
                keyAlias = rootProject.ext["keyAlias"].toString()
                keyPassword = rootProject.ext["keyPassword"].toString()
            }

            create("release") {
                storeFile =
                    file(rootProject.ext["storeFile"].toString()) //File("/Users/apple/AndroidStudioProjects/crypto-app/sig.jks")
                storePassword = rootProject.ext["storePassword"].toString()
                keyAlias = rootProject.ext["keyAlias"].toString()
                keyPassword = rootProject.ext["keyPassword"].toString()
            }
        }
}
```

- 执行 `./gradlew signingReport`

## 修改打包apk文件名

```kotlin
android {
    // ...

     applicationVariants.all {
        val buildTypeName = this.buildType.name
//        val f = this.flavorName // _${f}

//        println("================")
//        println(buildDir.path)
//        packageApplicationProvider.configure {
//            outputDirectory.dir(buildDir.parent + "/${buildTypeName}")
//        }
        outputs.all {
            if (this is com.android.build.gradle.internal.api.ApkVariantOutputImpl) {
//                packageApplicationProvider.get().outputDirectory.file(buildDir.parent + "/${buildTypeName}")
                this.outputFileName = "保密盒子_v${versionName}_${buildTypeName}.apk"
            }
        }
    }
}
```
