# LiveData

> LiveData 是一种可观察的数据存储器类。与常规的可观察类不同，LiveData 具有生命周期感知能力，意指它遵循其他应用组件（如 Activity、Fragment 或 Service）的生命周期。这种感知能力可确保 LiveData 仅更新处于活跃生命周期状态的应用组件观察者。

> 主要思想就是用到了观察者模式思想，让观察者和被观察者解耦，同时还能感知到数据的变化，所以一般被用到ViewModel中，ViewModel负责触发数据的更新，更新会通知到`LiveData`，然后`LiveData`再通知活跃状态的观察者。

`LiveData`还能无缝衔接到MVVM架构中，主要体现在其可以感知到Activity等生命周期，这样就带来了很多好处：

- 不会发生内存泄漏 观察者会绑定到 Lifecycle对象，并在其关联的生命周期遭到销毁后进行自我清理。

- 不会因 Activity 停止而导致崩溃 如果观察者的生命周期处于非活跃状态（如返回栈中的 Activity），则它不会接收任何 LiveData 事件。

- 自动判断生命周期并回调方法 如果观察者的生命周期处于 STARTED或 RESUMED状态，则 LiveData 会认为该观察者处于活跃状态，就会调用onActive方法，否则，如果 LiveData 对象没有任何活跃观察者时，会调用 onInactive()方法。

## LiveData原理

> 订阅方法，也就是`observe`方法。通过该方法把订阅者和被观察者关联起来，形成观察者模式
