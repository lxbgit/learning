# SMS

## `SmsManager.sendTextMessage()` 方法用于向指定的目的地地址发送文本短信。以下是该方法的参数说明：

- destinationAddress：短信的目的地地址，即接收方的手机号码。
- scAddress：短信服务中心的地址，一般不需要设置，传入 null 即可。
- text：短信的内容。
- sentIntent：发送短信后，用于接收发送状态的 PendingIntent 对象。
- deliveryIntent：接收方接收到短信后，用于接收状态报告的 PendingIntent 对象。


## `PendingIntent.getBroadcast()` 方法用于创建一个广播类型的 `PendingIntent` 对象，该对象用于接收广播事件。该方法的四个参数含义如下：

- context：当前上下文。
- requestCode：请求代码，用于标识该 PendingIntent 对象。
- intent：要发送的广播事件。
- flags：PendingIntent 的标志位，一般传入 0 即可。

在上面的代码中，我们使用 `PendingIntent.getBroadcast()` 方法创建了一个 `PendingIntent` 对象，用于接收发送状态。mContext 是上下文对象，`sentIntent` 是一个发送状态的广播事件，0 是请求代码，0 是 `PendingIntent` 的标志位。创建好 `PendingIntent` 对象后，我们可以将其作为参数传递给 SmsManager.`sendTextMessage()` 方法，以便在短信发送后接收发送状态。当发送状态发生改变时，系统将会发送一个广播事件，该事件包含 `PendingIntent` 对象的信息，系统将 `PendingIntent` 对象交给应用程序处理，从而实现发送状态的回调。
