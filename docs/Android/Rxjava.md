# `Rxjava3`

- RxJava中map、flatMap/concatMap的区别，你还用过其他哪些操作符?
  - map是通过原始数据类型返回另外一种数据类型，而flatMap是通过原始数据类型返回另外一种被观察者
  - 总之一句一话,他们的区别在于：concatMap是有序的，flatMap是无序的，concatMap最终输出的顺序与原序列保持一致，而flatMap则不一定，有可能出现交错。

- Observer处理完onComplete后会还能onNext吗?
  - onComplete是用来控制不能发送数据的，也就是不能onNext了，包括onError也是不能再发送onNext数据了，该方法中也是调用了dispose方法。

- RxJava怎么通过被订阅者传给订阅者的过程是什么样的?

  - 订阅是从下游的Observer向上游的Observable发送订阅，然后在订阅的过程中，给下游的Observer发送订阅监听，并且给上游的被观察者添加订阅

  - 发送主要通过上游的被观察者通知发射器，然后发射器会发送给下游的observer。

- Maybe、Single、Flowable、Completable几种观察者的区别，以及他们在什么场景用？
  - `Maybe`: 没有onNext方法，也就是说不能发多条数据，如果回调到onSuccess再不能发消息了，如果直接回调onComplete相当于没发数据，也就是说Maybe可能不发送数据，如果发送数据只会发送单条数据
  - `Observer`: 这个不用多说了，它是能发送多条数据的，直到发送onError或onComplete才不会再发送数据了，当然它也是可以不发送数据的，直接发送onError或onComplete。
  - `Single`: single也是发送单条数据，单是它要么成功要么失败
  - `Flowable`: Flowable没有FlowableObserver接口，它是由FlowableSubscriber代表观察者，Flowable在后面被压的时候讲，我们只要知道它是被压策略的一个被观察者
  - `Completable`: Completable不发送数据，只会发送成功或失败的事件，当然这个用得很少。

> 从上面各个对应的observer接口来看，如果只想发一条数据，或者不发数据就用`Maybe`，如果想法多条数据或者不发数据就用`Observable`，如果只发一条数据或者失败就用`Single`，如果想用背压策略使用`Flowable`，如果不发数据就用`Completable`。

- RxJava切换线程是怎么回事?

  - RxJava切换线程使用subscribeOn指定被观察者的在哪个线程执行，使用observeOn指定观察者在哪个线程执行，通常我们写法如下:

    ```java
    subscribeOn(Schedulers.io())
    .observeOn(AndroidSchedulers.mainThread())
    ```

  > subscribeOn实际是创建了ObservableSubscribeOn的Observable，它的订阅方法里面创建了SubscribeOnObserver，通过线程池执行Runnable来达到上游Observable的订阅在子线程中执行，这就是为什么subscribeOn能控制observable在哪个线程中执行的原因

  > observeOn实际是创建了ObservableObserveOn的Observable，它的订阅方法里面创建了ObserveOnObserver，而ObserveOnObserver是实现了Runnable接口，把它包装成message给主线程的Handler发送一条消息，而ObserveOnObserver的run方法中会给下游的Observer发送数据。所以这就是observeOn能让observer在哪个线程中执行

- RxJava的subscribeOn只有第一次生效?

> 如果你理解了订阅的过程，其实该问题很好理解，subscribeOn是规定上游的observable在哪个线程中执行，如果我们执行多次的subscribeOn的话，从下游的observer到上游的observable的订阅过程，最开始调用的subscribeOn返回的observable会把后面执行的subscribeOn返回的observable给覆盖了，因此我们感官的是只有第一次的subscribeOn能生效。

- RxJava的observeOn多次调用哪个有效?

> 上面分析了observeOn是指定下游的observer在哪个线程中执行，所以这个更好理解，看observeOn下一个observer是哪一个，所以多次调用observeOn肯定是最后一个observeOn控制有效

- RxJava中背压是怎么回事？
