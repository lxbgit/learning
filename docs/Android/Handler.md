# `Handler`

Handler是Android SDK来处理异步消息的核心类。
子线程与主线程通过Handler来进行通信。子线程可以通过Handler来通知主线程进行UI更新

## Looper 如何在子线程中创建

> 要在哪个线程启动消息循环，就需要在该线程执行`Looper.prepare()` & `Looper.loop()`。只有调用`Looper.loop()`之后，消息循环才算真正运转起来了。具体来说，启动消息循环的分为两种情况：主线程消息循环 & 子线程消息循环，前者由 Framework 启动，而后者需要我们自己启动：

> 创建 `Handler` 的代码需要放在`Looper.prepare();` & `Looper.loop();`中间执行，这是因为创建 `Handler` 对象时需要聚合 `Looper` 对象（默认使用的是当前线程的 `Looper`），而只有执行`Looper.prepare();`之后，才会创建该线程私有的 `Looper` 对象，否则创建 `Handler` 会抛异常。

## 说一下 `Looper`、`handler`、`线程`间的关系。例如一个线程可以对应几个 Looper、几个Handler？

每个线程只允许调用一次`Looper.prepare()`，否则会抛异常。这样设计是因为一个 `Looper` 对应了一个消息循环，而一个线程进行多个消息循环是没有意义的（一个线程不可能同时进行两个死循环）。那么，Handler 是如何保证 Looper 线程唯一的呢？

答：首先，`Handler` 主要利用了 `ThreadLocal` 在每个线程单独存储副本的特性，保证了一个`ThreadLocal<Looper>`在不同线程存取的`Looper`对象相互独立；其次，`ThreadLocal` 是 `Looper` 的一个`static final`变量，这样就保证了整个进程中 `sThreadLocal`对象不可变；第三，`Looper.prepare()`判断在一个线程里重复调用，则会抛出异常。

## `Handler#post(Runnable)` 是如何执行的？

消息发送的 API 非常多，最终它们都会调用到`Handler#sendMessageAtTime(Message msg, long uptimeMillis)`，内部会交给`MessageQueue#enqueueMessage(Message msg, long when)`处理

- 每个消息的处理时间（when）不一样（SystemClock.uptimeMillis() + delayMill）
- 消息入队时，根据消息的处理时间（when）做插入排序，队头的消息就是最需要执行的消息
- 当消息队列为空时（无消息时线程会阻塞），消息入队需要唤醒线程
- 当消息队列不为空时（一般不需要唤醒），只有当开启同步屏障后第一个异步消息需要唤醒（开启同步屏障时会在队首插入一个占位消息，此时消息队列不为空，但是线程可能是阻塞的）

## Looper 死循环为什么不阻塞主线程？ / Looper 死循环为什么不会 ANR?

```
消息队列中无消息怎么处理 block
nativePollOnce值为-1表示无限等待，让出cpu时间片给其线程，本线程等待
0表示无须等待直接返回
nativePollOnce -> epoll(linux) ->linux层的messagequeue
```

```
Handler内存泄漏的原因 :
MessageQueue持有Message，Message持有activity
delay多久，message就会持有activity多久
方法：静态内部类、弱引用
```