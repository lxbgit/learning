import{_ as e,e as t}from"./app.4176ed4c.js";const n={},o=t(`<ul><li>member\uFF0C\u6DFB\u52A0\u6210\u5458\u8DEF\u7531\uFF0C\u53EA\u9700\u5728 resource \u5757\u4E2D\u6DFB\u52A0 member \u5757\uFF1A</li></ul><div class="language-ruby ext-rb line-numbers-mode"><pre class="language-ruby"><code>resources <span class="token symbol">:nodes</span> <span class="token keyword">do</span>
    member <span class="token keyword">do</span>
      post <span class="token symbol">:block</span>
    <span class="token keyword">end</span>
  <span class="token keyword">end</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><blockquote><p>Rails \u8DEF\u7531\u80FD\u591F\u8BC6\u522B /nodes/1/block \u8DEF\u5F84\u4E0A\u7684 POST \u8BF7\u6C42</p></blockquote><ul><li>collection</li></ul><div class="language-ruby ext-rb line-numbers-mode"><pre class="language-ruby"><code>resource <span class="token symbol">:uers</span><span class="token punctuation">,</span> <span class="token symbol">controller</span><span class="token operator">:</span> <span class="token string-literal"><span class="token string">&#39;uers&#39;</span></span> <span class="token keyword">do</span>
    collection <span class="token keyword">do</span>
      get <span class="token symbol">:login</span>
    <span class="token keyword">end</span>
 <span class="token keyword">end</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><blockquote><p>Rails \u8DEF\u7531\u80FD\u591F\u8BC6\u522B /uers/login \u8DEF\u5F84\u4E0A\u7684 GET \u8BF7\u6C42</p></blockquote><ul><li>namespace \u4E0E scope <code>namespace</code>\u58F0\u660E\u8DEF\u7531\u4FE1\u606F\uFF1A</li></ul><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>namespace :blog do
   resources :contexts
end
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><p>\u751F\u6210\u7684\u8DEF\u7531\u4FE1\u606F\u5982\u4E0B\uFF1A</p><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code> blog_contexts GET    /blog/contexts(.:format)          {:action=&gt;&quot;index&quot;, :controller=&gt;&quot;blog/contexts&quot;}
                  POST   /blog/contexts(.:format)          {:action=&gt;&quot;create&quot;, :controller=&gt;&quot;blog/contexts&quot;}
 new_blog_context GET    /blog/contexts/new(.:format)      {:action=&gt;&quot;new&quot;, :controller=&gt;&quot;blog/contexts&quot;}
edit_blog_context GET    /blog/contexts/:id/edit(.:format) {:action=&gt;&quot;edit&quot;, :controller=&gt;&quot;blog/contexts&quot;}
     blog_context GET    /blog/contexts/:id(.:format)      {:action=&gt;&quot;show&quot;, :controller=&gt;&quot;blog/contexts&quot;}
                  PUT    /blog/contexts/:id(.:format)      {:action=&gt;&quot;update&quot;, :controller=&gt;&quot;blog/contexts&quot;}
                  DELETE /blog/contexts/:id(.:format)      {:action=&gt;&quot;destroy&quot;, :controller=&gt;&quot;blog/contexts&quot;}
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><p>\u800Cscope\u5219\u5982\u4E0B\uFF1A</p><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>scope :module =&gt; &#39;blog&#39; do
   resources :contexts
end
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><p>\u751F\u6210\u7684\u8DEF\u7531\u5982\u4E0B\uFF1A</p><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code> contexts GET    /contexts(.:format)           {:action=&gt;&quot;index&quot;, :controller=&gt;&quot;blog/contexts&quot;}
              POST   /contexts(.:format)           {:action=&gt;&quot;create&quot;, :controller=&gt;&quot;blog/contexts&quot;}
  new_context GET    /contexts/new(.:format)       {:action=&gt;&quot;new&quot;, :controller=&gt;&quot;blog/contexts&quot;}
 edit_context GET    /contexts/:id/edit(.:format)  {:action=&gt;&quot;edit&quot;, :controller=&gt;&quot;blog/contexts&quot;}
      context GET    /contexts/:id(.:format)       {:action=&gt;&quot;show&quot;, :controller=&gt;&quot;blog/contexts&quot;}
              PUT    /contexts/:id(.:format)       {:action=&gt;&quot;update&quot;, :controller=&gt;&quot;blog/contexts&quot;}
              DELETE /contexts/:id(.:format)       {:action=&gt;&quot;destroy&quot;, :controller=&gt;&quot;blog/contexts&quot;}
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div>`,14);function s(l,i){return o}var c=e(n,[["render",s],["__file","ruby-on-rails(\u4E03)-\u8DEF\u7531.html.vue"]]);export{c as default};
