import{_ as e,r as t,o as p,c as o,a as s,b as l,F as i,e as c,d as n}from"./app.4176ed4c.js";const r={},u=c(`<h1 id="redis\u5E38\u7528\u547D\u4EE4" tabindex="-1"><a class="header-anchor" href="#redis\u5E38\u7528\u547D\u4EE4" aria-hidden="true">#</a> redis\u5E38\u7528\u547D\u4EE4</h1><ul><li>\u8FDE\u63A5\u8FDC\u7A0BRedis\u670D\u52A1\uFF1A</li></ul><div class="language-bash ext-sh line-numbers-mode"><pre class="language-bash"><code>redis-cli -h <span class="token function">host</span> -p port -a password
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div></div></div><ul><li>\u5E38\u7528\u547D\u4EE4\u6F14\u793A\uFF1A</li></ul><div class="language-bash ext-sh line-numbers-mode"><pre class="language-bash"><code>\u279C  ~ /Users/lion/Desktop/lxb/redis/redis-4.0.2
\u279C  redis-4.0.2 src/redis-cli
<span class="token number">127.0</span>.0.1:637<span class="token operator"><span class="token file-descriptor important">9</span>&gt;</span> <span class="token builtin class-name">set</span> foo bar
OK
<span class="token number">127.0</span>.0.1:637<span class="token operator"><span class="token file-descriptor important">9</span>&gt;</span> get foo
<span class="token string">&quot;bar&quot;</span>
<span class="token number">127.0</span>.0.1:637<span class="token operator"><span class="token file-descriptor important">9</span>&gt;</span> <span class="token builtin class-name">set</span> user1 lxb
OK
<span class="token number">127.0</span>.0.1:637<span class="token operator"><span class="token file-descriptor important">9</span>&gt;</span> get user1
<span class="token string">&quot;lxb&quot;</span>
<span class="token number">127.0</span>.0.1:637<span class="token operator"><span class="token file-descriptor important">9</span>&gt;</span> hmset user:1 username lxb password <span class="token number">123456</span>
OK
<span class="token number">127.0</span>.0.1:637<span class="token operator"><span class="token file-descriptor important">9</span>&gt;</span> hgetall user:1
<span class="token number">1</span><span class="token punctuation">)</span> <span class="token string">&quot;username&quot;</span>
<span class="token number">2</span><span class="token punctuation">)</span> <span class="token string">&quot;lxb&quot;</span>
<span class="token number">3</span><span class="token punctuation">)</span> <span class="token string">&quot;password&quot;</span>
<span class="token number">4</span><span class="token punctuation">)</span> <span class="token string">&quot;123456&quot;</span>
<span class="token number">127.0</span>.0.1:637<span class="token operator"><span class="token file-descriptor important">9</span>&gt;</span> lpush lang js
<span class="token punctuation">(</span>integer<span class="token punctuation">)</span> <span class="token number">1</span>
<span class="token number">127.0</span>.0.1:637<span class="token operator"><span class="token file-descriptor important">9</span>&gt;</span> lpush lang java
<span class="token punctuation">(</span>integer<span class="token punctuation">)</span> <span class="token number">2</span>
<span class="token number">127.0</span>.0.1:637<span class="token operator"><span class="token file-descriptor important">9</span>&gt;</span> lpush lang swift
<span class="token punctuation">(</span>integer<span class="token punctuation">)</span> <span class="token number">3</span>
<span class="token number">127.0</span>.0.1:637<span class="token operator"><span class="token file-descriptor important">9</span>&gt;</span> lpush lang c
<span class="token punctuation">(</span>integer<span class="token punctuation">)</span> <span class="token number">4</span>
<span class="token number">127.0</span>.0.1:637<span class="token operator"><span class="token file-descriptor important">9</span>&gt;</span> lpush lang c++
<span class="token punctuation">(</span>integer<span class="token punctuation">)</span> <span class="token number">5</span>
<span class="token number">127.0</span>.0.1:637<span class="token operator"><span class="token file-descriptor important">9</span>&gt;</span> lrange lang <span class="token number">0</span> <span class="token number">10</span>
<span class="token number">1</span><span class="token punctuation">)</span> <span class="token string">&quot;c++&quot;</span>
<span class="token number">2</span><span class="token punctuation">)</span> <span class="token string">&quot;c&quot;</span>
<span class="token number">3</span><span class="token punctuation">)</span> <span class="token string">&quot;swift&quot;</span>
<span class="token number">4</span><span class="token punctuation">)</span> <span class="token string">&quot;java&quot;</span>
<span class="token number">5</span><span class="token punctuation">)</span> <span class="token string">&quot;js&quot;</span>
<span class="token number">127.0</span>.0.1:637<span class="token operator"><span class="token file-descriptor important">9</span>&gt;</span> SADD testset values1
<span class="token punctuation">(</span>integer<span class="token punctuation">)</span> <span class="token number">1</span>
<span class="token number">127.0</span>.0.1:637<span class="token operator"><span class="token file-descriptor important">9</span>&gt;</span> SADD testset values2
<span class="token punctuation">(</span>integer<span class="token punctuation">)</span> <span class="token number">1</span>
<span class="token number">127.0</span>.0.1:637<span class="token operator"><span class="token file-descriptor important">9</span>&gt;</span> SADD testset values3
<span class="token punctuation">(</span>integer<span class="token punctuation">)</span> <span class="token number">1</span>
<span class="token number">127.0</span>.0.1:637<span class="token operator"><span class="token file-descriptor important">9</span>&gt;</span> SADD testset values4
<span class="token punctuation">(</span>integer<span class="token punctuation">)</span> <span class="token number">1</span>
<span class="token number">127.0</span>.0.1:637<span class="token operator"><span class="token file-descriptor important">9</span>&gt;</span> SADD testset values5
<span class="token punctuation">(</span>integer<span class="token punctuation">)</span> <span class="token number">1</span>
<span class="token number">127.0</span>.0.1:637<span class="token operator"><span class="token file-descriptor important">9</span>&gt;</span> SMEMBERS testset
<span class="token number">1</span><span class="token punctuation">)</span> <span class="token string">&quot;values4&quot;</span>
<span class="token number">2</span><span class="token punctuation">)</span> <span class="token string">&quot;values3&quot;</span>
<span class="token number">3</span><span class="token punctuation">)</span> <span class="token string">&quot;values2&quot;</span>
<span class="token number">4</span><span class="token punctuation">)</span> <span class="token string">&quot;values1&quot;</span>
<span class="token number">5</span><span class="token punctuation">)</span> <span class="token string">&quot;values5&quot;</span>
<span class="token number">127.0</span>.0.1:637<span class="token operator"><span class="token file-descriptor important">9</span>&gt;</span> PING
PONG
<span class="token number">127.0</span>.0.1:637<span class="token operator"><span class="token file-descriptor important">9</span>&gt;</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><blockquote><p><code>PING</code> \u547D\u4EE4\uFF0C\u7528\u4E8E\u68C0\u6D4B <code>redis</code> \u670D\u52A1\u662F\u5426\u542F\u52A8\u3002</p></blockquote>`,6),d=n("\u53EF\u4EE5\u53C2\u8003"),k={href:"http://www.runoob.com/redis/redis-commands.html",target:"_blank",rel:"noopener noreferrer"},m=n("redis\u547D\u4EE4");function v(b,g){const a=t("ExternalLinkIcon");return p(),o(i,null,[u,s("p",null,[d,s("a",k,[m,l(a)])])],64)}var q=e(r,[["render",v],["__file","\u5E38\u7528\u547D\u4EE4.html.vue"]]);export{q as default};
