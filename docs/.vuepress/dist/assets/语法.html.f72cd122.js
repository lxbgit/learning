import{_ as e,e as a}from"./app.4176ed4c.js";const n={},s=a(`<h1 id="thymeleaf-\u8BED\u6CD5" tabindex="-1"><a class="header-anchor" href="#thymeleaf-\u8BED\u6CD5" aria-hidden="true">#</a> thymeleaf \u8BED\u6CD5</h1><ul><li><code>th:fragment</code></li></ul><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>th:insert  \uFF1A\u4FDD\u7559\u81EA\u5DF1\u7684\u4E3B\u6807\u7B7E\uFF0C\u4FDD\u7559th:fragment\u7684\u4E3B\u6807\u7B7E\u3002
th:replace \uFF1A\u4E0D\u8981\u81EA\u5DF1\u7684\u4E3B\u6807\u7B7E\uFF0C\u4FDD\u7559th:fragment\u7684\u4E3B\u6807\u7B7E\u3002
th:include \uFF1A\u4FDD\u7559\u81EA\u5DF1\u7684\u4E3B\u6807\u7B7E\uFF0C\u4E0D\u8981th:fragment\u7684\u4E3B\u6807\u7B7E\u3002\uFF08\u5B98\u65B93.0\u540E\u4E0D\u63A8\u8350\uFF09
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><p>\u4F8B\u5B50(\u5B9A\u4E49<code>fragment</code>)\uFF1A</p><div class="language-html ext-html line-numbers-mode"><pre class="language-html"><code><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name"><span class="token namespace">xmlns:</span>th</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">&quot;</span>http://www.thymeleaf.org<span class="token punctuation">&quot;</span></span> <span class="token attr-name"><span class="token namespace">th:</span>fragment</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">&quot;</span>header(title)<span class="token punctuation">&quot;</span></span><span class="token punctuation">&gt;</span></span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div></div></div><p>\u4F7F\u7528 <code>&lt;th:block th:insert=&quot;forum/taglib :: adminHead(&#39;\u9879\u76EE\u7BA1\u7406|Issue&#39;)&quot;/&gt;</code></p><ul><li>\u6587\u672C\u66FF\u6362 |...|</li></ul><div class="language-html ext-html line-numbers-mode"><pre class="language-html"><code><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>li</span> <span class="token attr-name"><span class="token namespace">th:</span>class</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">&quot;</span>|layui-nav-item \${index == 2 ? <span class="token punctuation">&#39;</span>layui-this<span class="token punctuation">&#39;</span> : <span class="token punctuation">&#39;</span><span class="token punctuation">&#39;</span>}|<span class="token punctuation">&quot;</span></span><span class="token punctuation">&gt;</span></span>
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span class="token attr-name"><span class="token namespace">th:</span>href</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">&quot;</span>@{/projects}<span class="token punctuation">&quot;</span></span><span class="token punctuation">&gt;</span></span>\u9879\u76EE<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>a</span><span class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>li</span><span class="token punctuation">&gt;</span></span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ul><li><code>th:each=&quot;e,eState : \${emps}&quot;</code></li></ul><blockquote><p>\u5176\u4E2De\u4E3A\u5FAA\u73AF\u7684\u6BCF\u4E00\u9879\uFF0CeState\u662F\u4E0B\u6807\u5C5E\u6027(\u53EF\u7701\u7565)\uFF0CeState\u5C5E\u6027\u5305\u62EC</p></blockquote><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>index\uFF1A\u5217\u8868\u72B6\u6001\u7684\u5E8F\u53F7\uFF0C\u4ECE0\u5F00\u59CB\uFF1B
count\uFF1A\u5217\u8868\u72B6\u6001\u7684\u5E8F\u53F7\uFF0C\u4ECE1\u5F00\u59CB\uFF1B
size\uFF1A\u5217\u8868\u72B6\u6001\uFF0C\u5217\u8868\u6570\u636E\u6761\u6570\uFF1B
current\uFF1A\u5217\u8868\u72B6\u6001\uFF0C\u5F53\u524D\u6570\u636E\u5BF9\u8C61
even\uFF1A\u5217\u8868\u72B6\u6001\uFF0C\u662F\u5426\u4E3A\u5947\u6570\uFF0Cboolean\u7C7B\u578B
odd\uFF1A\u5217\u8868\u72B6\u6001\uFF0C\u662F\u5426\u4E3A\u5076\u6570\uFF0Cboolean\u7C7B\u578B
first\uFF1A\u5217\u8868\u72B6\u6001\uFF0C\u662F\u5426\u4E3A\u7B2C\u4E00\u6761\uFF0Cboolean\u7C7B\u578B
last\uFF1A\u5217\u8868\u72B6\u6001\uFF0C\u662F\u5426\u4E3A\u6700\u540E\u4E00\u6761\uFF0Cboolean\u7C7B\u578B
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ul><li><code>th:if</code></li></ul><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>&lt;!--th:if \u6761\u4EF6\u5224\u65AD\uFF0C\u7C7B\u4F3C\u7684\u6709th:switch\uFF0Cth:case\uFF0C\u4F18\u5148\u7EA7\u4EC5\u6B21\u4E8Eth:each,--&gt;
&lt;p th:text=&quot;\${map.Boss.name}&quot; th:if=&quot;\${map.Boss.age gt 20}&quot;&gt;&lt;/p&gt;
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div></div></div><p>\u8FD0\u7B97\u5173\u7CFB</p><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>gt\uFF1Agreat than\uFF08\u5927\u4E8E\uFF09
ge\uFF1Agreat equal\uFF08\u5927\u4E8E\u7B49\u4E8E\uFF09
eq\uFF1Aequal\uFF08\u7B49\u4E8E\uFF09
lt\uFF1Aless than\uFF08\u5C0F\u4E8E\uFF09
le\uFF1Aless equal\uFF08\u5C0F\u4E8E\u7B49\u4E8E\uFF09
ne\uFF1Anot equal\uFF08\u4E0D\u7B49\u4E8E\uFF09
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>[[\u2026]]
\u8868\u793Ath:text
\u4F1A\u8F6C\u4E49\u7279\u6B8A\u5B57\u7B26

[(\u2026)]
\u8868\u793Ath:utext
\u4E0D\u4F1A\u8F6C\u4E49\u7279\u6B8A\u5B57\u7B26
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h2 id="thymeleaf-tags" tabindex="-1"><a class="header-anchor" href="#thymeleaf-tags" aria-hidden="true">#</a> thymeleaf tags</h2><p><code>th:remove</code>\u7684\u503C\u5982\u4E0B:</p><ul><li><code>all</code>:\u5220\u9664\u5305\u542B\u6807\u7B7E\u548C\u6240\u6709\u7684\u5B69\u5B50\u3002</li><li><code>body</code>:\u4E0D\u5305\u542B\u6807\u8BB0\u5220\u9664,\u4F46\u5220\u9664\u5176\u6240\u6709\u7684\u5B69\u5B50\u3002</li><li><code>tag</code>:\u5305\u542B\u6807\u8BB0\u7684\u5220\u9664,\u4F46\u4E0D\u5220\u9664\u5B83\u7684\u5B69\u5B50\u3002</li><li><code>all-but-first</code>:\u5220\u9664\u6240\u6709\u5305\u542B\u6807\u7B7E\u7684\u5B69\u5B50,\u9664\u4E86\u7B2C\u4E00\u4E2A\u3002</li><li><code>none</code>:\u4EC0\u4E48\u4E5F\u4E0D\u505A\u3002\u8FD9\u4E2A\u503C\u662F\u6709\u7528\u7684\u52A8\u6001\u8BC4\u4F30\u3002</li></ul><p><code>th:insert</code> \uFF1A\u4FDD\u7559\u81EA\u5DF1\u7684\u4E3B\u6807\u7B7E\uFF0C\u4FDD\u7559<code>th:fragment</code>\u7684\u4E3B\u6807\u7B7E\u3002 <code>th:replace</code> \uFF1A\u4E0D\u8981\u81EA\u5DF1\u7684\u4E3B\u6807\u7B7E\uFF0C\u4FDD\u7559<code>th:fragment</code>\u7684\u4E3B\u6807\u7B7E\u3002 <code>th:include</code> \uFF1A\u4FDD\u7559\u81EA\u5DF1\u7684\u4E3B\u6807\u7B7E\uFF0C\u4E0D\u8981<code>th:fragment</code>\u7684\u4E3B\u6807\u7B7E\u3002\uFF08\u5B98\u65B93.0\u540E\u4E0D\u63A8\u8350\uFF09</p><ul><li><code>#dates</code>:<code>java.util.Date</code> \u5BF9\u8C61\u7684\u5B9E\u7528\u65B9\u6CD5\u3002</li><li><code>#calendars</code>:\u548C<code>dates</code>\u7C7B\u4F3C, \u4F46\u662F<code>java.util.Calendar</code>\u5BF9\u8C61.</li><li><code>#numbers</code>: \u683C\u5F0F\u5316\u6570\u5B57\u5BF9\u8C61\u7684\u5B9E\u7528\u65B9\u6CD5\u3002</li><li><code>#strings</code>: \u5B57\u7B26\u521B\u5BF9\u8C61\u7684\u5B9E\u7528\u65B9\u6CD5\uFF1A <code>contains</code>, <code>startsWith</code>, <code>prepending</code>/<code>appending</code>\u7B49.</li><li><code>#objects</code>: \u5BF9<code>objects</code>\u64CD\u4F5C\u7684\u5B9E\u7528\u65B9\u6CD5\u3002</li><li><code>#bools</code>: \u5BF9\u5E03\u5C14\u503C\u6C42\u503C\u7684\u5B9E\u7528\u65B9\u6CD5\u3002</li><li><code>#arrays</code>: \u6570\u7EC4\u7684\u5B9E\u7528\u65B9\u6CD5\u3002</li><li><code>#lists</code>:<code>list</code>\u7684\u5B9E\u7528\u65B9\u6CD5\u3002</li><li><code>#sets</code>: <code>set</code>\u7684\u5B9E\u7528\u65B9\u6CD5\u3002</li><li><code>#maps</code>: <code>map</code>\u7684\u5B9E\u7528\u65B9\u6CD5\u3002</li><li><code>#aggregates</code>: \u5BF9\u6570\u7EC4\u6216\u96C6\u5408\u521B\u5EFA\u805A\u5408\u7684\u5B9E\u7528\u65B9\u6CD5\u3002</li><li><code>#messages</code>: \u5728\u8868\u8FBE\u5F0F\u4E2D\u83B7\u53D6\u5916\u90E8\u4FE1\u606F\u7684\u5B9E\u7528\u65B9\u6CD5\u3002</li><li><code>#ids</code>: \u5904\u7406\u53EF\u80FD\u91CD\u590D\u7684id\u5C5E\u6027\u7684\u5B9E\u7528\u65B9\u6CD5 (\u6BD4\u5982\uFF1A\u8FED\u4EE3\u7684\u7ED3\u679C)\u3002</li></ul><h2 id="thymeleaf-\u65E5\u671F\u683C\u5F0F\u5316" tabindex="-1"><a class="header-anchor" href="#thymeleaf-\u65E5\u671F\u683C\u5F0F\u5316" aria-hidden="true">#</a> thymeleaf \u65E5\u671F\u683C\u5F0F\u5316</h2><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>/*
 * Format date with the standard locale format
 * Also works with arrays, lists or sets
 */
\${#temporals.format(temporal)}
\${#temporals.arrayFormat(temporalsArray)}
\${#temporals.listFormat(temporalsList)}
\${#temporals.setFormat(temporalsSet)}

/*
 * Format date with the standard format for the provided locale
 * Also works with arrays, lists or sets
 */
\${#temporals.format(temporal, locale)}
\${#temporals.arrayFormat(temporalsArray, locale)}
\${#temporals.listFormat(temporalsList, locale)}
\${#temporals.setFormat(temporalsSet, locale)}

/*
 * Format date with the specified pattern
 * SHORT, MEDIUM, LONG and FULL can also be specified to used the default java.time.format.FormatStyle patterns
 * Also works with arrays, lists or sets
 */
\${#temporals.format(temporal, &#39;dd/MMM/yyyy HH:mm&#39;)}
\${#temporals.arrayFormat(temporalsArray, &#39;dd/MMM/yyyy HH:mm&#39;)}
\${#temporals.listFormat(temporalsList, &#39;dd/MMM/yyyy HH:mm&#39;)}
\${#temporals.setFormat(temporalsSet, &#39;dd/MMM/yyyy HH:mm&#39;)}

/*
 * Format date with the specified pattern and locale
 * Also works with arrays, lists or sets
 */
\${#temporals.format(temporal, &#39;dd/MMM/yyyy HH:mm&#39;, locale)}
\${#temporals.arrayFormat(temporalsArray, &#39;dd/MMM/yyyy HH:mm&#39;, locale)}
\${#temporals.listFormat(temporalsList, &#39;dd/MMM/yyyy HH:mm&#39;, locale)}
\${#temporals.setFormat(temporalsSet, &#39;dd/MMM/yyyy HH:mm&#39;, locale)}

/*
 * Format date with ISO-8601 format
 * Also works with arrays, lists or sets
 */
\${#temporals.formatISO(temporal)}
\${#temporals.arrayFormatISO(temporalsArray)}
\${#temporals.listFormatISO(temporalsList)}
\${#temporals.setFormatISO(temporalsSet)}

/*
 * Obtain date properties
 * Also works with arrays, lists or sets
 */
\${#temporals.day(temporal)}                    // also arrayDay(...), listDay(...), etc.
\${#temporals.month(temporal)}                  // also arrayMonth(...), listMonth(...), etc.
\${#temporals.monthName(temporal)}              // also arrayMonthName(...), listMonthName(...), etc.
\${#temporals.monthNameShort(temporal)}         // also arrayMonthNameShort(...), listMonthNameShort(...), etc.
\${#temporals.year(temporal)}                   // also arrayYear(...), listYear(...), etc.
\${#temporals.dayOfWeek(temporal)}              // also arrayDayOfWeek(...), listDayOfWeek(...), etc.
\${#temporals.dayOfWeekName(temporal)}          // also arrayDayOfWeekName(...), listDayOfWeekName(...), etc.
\${#temporals.dayOfWeekNameShort(temporal)}     // also arrayDayOfWeekNameShort(...), listDayOfWeekNameShort(...), etc.
\${#temporals.hour(temporal)}                   // also arrayHour(...), listHour(...), etc.
\${#temporals.minute(temporal)}                 // also arrayMinute(...), listMinute(...), etc.
\${#temporals.second(temporal)}                 // also arraySecond(...), listSecond(...), etc.
\${#temporals.nanosecond(temporal)}             // also arrayNanosecond(...), listNanosecond(...), etc.

/*
 * Create temporal (java.time.Temporal) objects from its components
 */
\${#temporals.create(year,month,day)}                                // return a instance of java.time.LocalDate
\${#temporals.create(year,month,day,hour,minute)}                    // return a instance of java.time.LocalDateTime
\${#temporals.create(year,month,day,hour,minute,second)}             // return a instance of java.time.LocalDateTime
\${#temporals.create(year,month,day,hour,minute,second,nanosecond)}  // return a instance of java.time.LocalDateTime

/*
 * Create a temporal (java.time.Temporal) object for the current date and time
 */
\${#temporals.createNow()}                      // return a instance of java.time.LocalDateTime
\${#temporals.createNowForTimeZone(zoneId)}     // return a instance of java.time.ZonedDateTime
\${#temporals.createToday()}                    // return a instance of java.time.LocalDate
\${#temporals.createTodayForTimeZone(zoneId)}   // return a instance of java.time.LocalDate

/*
 * Create a temporal (java.time.Temporal) object for the provided date
 */
\${#temporals.createDate(isoDate)}              // return a instance of java.time.LocalDate
\${#temporals.createDateTime(isoDate)}          // return a instance of java.time.LocalDateTime
\${#temporals.createDate(isoDate, pattern)}     // return a instance of java.time.LocalDate
\${#temporals.createDateTime(isoDate, pattern)} // return a instance of java.time.LocalDateTime
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div>`,23);function t(i,l){return s}var d=e(n,[["render",t],["__file","\u8BED\u6CD5.html.vue"]]);export{d as default};
