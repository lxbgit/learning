import{_ as s,e as n}from"./app.4176ed4c.js";const a={},e=n(`<h1 id="lsof-\u547D\u4EE4" tabindex="-1"><a class="header-anchor" href="#lsof-\u547D\u4EE4" aria-hidden="true">#</a> <code>lsof</code> \u547D\u4EE4</h1><blockquote><p>lsof(list open files)\u662F\u4E00\u4E2A\u67E5\u770B\u8FDB\u7A0B\u6253\u5F00\u7684\u6587\u4EF6\u7684\u5DE5\u5177\u3002</p></blockquote><div class="language-bash ext-sh line-numbers-mode"><pre class="language-bash"><code>-a \u6307\u793A\u5176\u5B83\u9009\u9879\u4E4B\u95F4\u4E3A\u4E0E\u7684\u5173\u7CFB
-c <span class="token operator">&lt;</span>\u8FDB\u7A0B\u540D<span class="token operator">&gt;</span> \u8F93\u51FA\u6307\u5B9A\u8FDB\u7A0B\u6240\u6253\u5F00\u7684\u6587\u4EF6
-d <span class="token operator">&lt;</span>\u6587\u4EF6\u63CF\u8FF0\u7B26<span class="token operator">&gt;</span> \u5217\u51FA\u5360\u7528\u8BE5\u6587\u4EF6\u53F7\u7684\u8FDB\u7A0B
+d <span class="token operator">&lt;</span>\u76EE\u5F55<span class="token operator">&gt;</span>  \u8F93\u51FA\u76EE\u5F55\u53CA\u76EE\u5F55\u4E0B\u88AB\u6253\u5F00\u7684\u6587\u4EF6\u548C\u76EE\u5F55<span class="token punctuation">(</span>\u4E0D\u9012\u5F52<span class="token punctuation">)</span>
+D <span class="token operator">&lt;</span>\u76EE\u5F55<span class="token operator">&gt;</span>  \u9012\u5F52\u8F93\u51FA\u53CA\u76EE\u5F55\u4E0B\u88AB\u6253\u5F00\u7684\u6587\u4EF6\u548C\u76EE\u5F55
-i <span class="token operator">&lt;</span>\u6761\u4EF6<span class="token operator">&gt;</span>  \u8F93\u51FA\u7B26\u5408\u6761\u4EF6\u4E0E\u7F51\u7EDC\u76F8\u5173\u7684\u6587\u4EF6
-n \u4E0D\u89E3\u6790\u4E3B\u673A\u540D
-p <span class="token operator">&lt;</span>\u8FDB\u7A0B\u53F7<span class="token operator">&gt;</span> \u8F93\u51FA\u6307\u5B9A PID \u7684\u8FDB\u7A0B\u6240\u6253\u5F00\u7684\u6587\u4EF6
-P \u4E0D\u89E3\u6790\u7AEF\u53E3\u53F7
-t \u53EA\u8F93\u51FA PID
-u \u8F93\u51FA\u6307\u5B9A\u7528\u6237\u6253\u5F00\u7684\u6587\u4EF6
-U \u8F93\u51FA\u6253\u5F00\u7684 UNIX domain socket \u6587\u4EF6
-h \u663E\u793A\u5E2E\u52A9\u4FE1\u606F
-v \u663E\u793A\u7248\u672C\u4FE1\u606F
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ul><li>\u67E5\u770B\u5360\u7528\u7AEF\u53E3\u8FDB\u7A0B\u7684PID</li></ul><div class="language-bash ext-sh line-numbers-mode"><pre class="language-bash"><code><span class="token function">lsof</span> -i:<span class="token punctuation">{</span>\u7AEF\u53E3\u53F7<span class="token punctuation">}</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div></div></div>`,5);function l(i,o){return e}var r=s(a,[["render",l],["__file","lsof.html.vue"]]);export{r as default};
