import{_ as e,e as n}from"./app.4176ed4c.js";const s={},a=n(`<p>create:</p><div class="language-ruby ext-rb line-numbers-mode"><pre class="language-ruby"><code>before_validation

after_validation

before_save

around_save

before_create

around_create

after_create

after_save

after_commit<span class="token operator">/</span>after_rollback
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ul><li>\u6761\u4EF6\u56DE\u8C03</li></ul><p>\u4F7F\u7528<code>:if</code> \u6216\u8005 <code>:unless</code>\u9009\u9879, \u4F8B\u5982:</p><div class="language-ruby ext-rb line-numbers-mode"><pre class="language-ruby"><code>before_update <span class="token symbol">:del_team_activities</span><span class="token punctuation">,</span> <span class="token symbol">if</span><span class="token operator">:</span> <span class="token class-name">Proc</span><span class="token punctuation">.</span><span class="token keyword">new</span> <span class="token punctuation">{</span> <span class="token operator">|</span>a<span class="token operator">|</span> a<span class="token punctuation">.</span>team_ids<span class="token punctuation">.</span>present<span class="token operator">?</span> <span class="token punctuation">}</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div></div></div>`,5);function i(l,r){return a}var d=e(s,[["render",i],["__file","Active-Record-\u56DE\u8C03.html.vue"]]);export{d as default};
