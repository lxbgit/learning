import{_ as a,r as i,o as t,c as l,a as n,b as c,F as o,d as s,e as p}from"./app.4176ed4c.js";const d={},u=n("h1",{id:"nginx",tabindex:"-1"},[n("a",{class:"header-anchor",href:"#nginx","aria-hidden":"true"},"#"),s(" Nginx")],-1),r={href:"https://nginx.org/en/docs/http/ngx_http_core_module.html#client_max_body_size",target:"_blank",rel:"noopener noreferrer"},m=s("\u5B98\u65B9\u6587\u6863"),v=p(`<ul><li>\u8BBE\u7F6Enginx\u670D\u52A1\u5141\u8BB8\u7528\u6237\u6700\u5927\u4E0A\u4F20\u6570\u636E\u5927\u5C0F</li></ul><div class="language-nginx ext-nginx line-numbers-mode"><pre class="language-nginx"><code><span class="token directive"><span class="token keyword">client_max_body_size</span> <span class="token number">8m</span></span><span class="token punctuation">;</span>
<span class="token comment"># \u53C2\u6570\u8BED\u6CD5 client_max_body_size \u5177\u4F53\u7684\u5927\u5C0F\u503C\uFF0C\u9ED8\u8BA41m</span>
<span class="token comment"># \u653E\u7F6E\u4F4D\u7F6E http\uFF0Cserver\uFF0Clocation</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ul><li>\u914D\u7F6E\u89E3\u91CA</li></ul><div class="language-bash ext-sh line-numbers-mode"><pre class="language-bash"><code><span class="token punctuation">[</span>root@web01 ~<span class="token punctuation">]</span><span class="token comment"># cat /application/nginx/conf/nginx.conf</span>
worker_processes  <span class="token number">2</span><span class="token punctuation">;</span>
worker_cpu_affinity 0101 <span class="token number">1010</span><span class="token punctuation">;</span>
error_log logs/error.log<span class="token punctuation">;</span>
 
<span class="token comment">#\u914D\u7F6ENginx worker\u8FDB\u7A0B\u6700\u5927\u6253\u5F00\u6587\u4EF6\u6570</span>
worker_rlimit_nofile <span class="token number">65535</span><span class="token punctuation">;</span>
 
user www www<span class="token punctuation">;</span>
events <span class="token punctuation">{</span>
    <span class="token comment">#\u5355\u4E2A\u8FDB\u7A0B\u5141\u8BB8\u7684\u5BA2\u6237\u7AEF\u6700\u5927\u8FDE\u63A5\u6570</span>
    worker_connections  <span class="token number">20480</span><span class="token punctuation">;</span>
    <span class="token comment">#\u4F7F\u7528epoll\u6A21\u578B</span>
    use epoll<span class="token punctuation">;</span>
<span class="token punctuation">}</span>
http <span class="token punctuation">{</span>
    include       mime.types<span class="token punctuation">;</span>
    default_type  application/octet-stream<span class="token punctuation">;</span>
    <span class="token comment">#sendfile        on;</span>
    <span class="token comment">#keepalive_timeout  65;</span>
    <span class="token comment">#\u8BBF\u95EE\u65E5\u5FD7\u914D\u7F6E</span>
    log_format  main  <span class="token string">&#39;$remote_addr - $remote_user [$time_local] &quot;$request&quot; &#39;</span>
                      <span class="token string">&#39;$status $body_bytes_sent &quot;$http_referer&quot; &#39;</span>
                      <span class="token string">&#39;&quot;$http_user_agent&quot; &quot;$http_x_forwarded_for&quot;&#39;</span><span class="token punctuation">;</span>
 
 
    <span class="token comment">#\u865A\u62DF\u4E3B\u673A</span>
    include /application/nginx/conf/extra/www.conf<span class="token punctuation">;</span>
    include /application/nginx/conf/extra/blog.conf<span class="token punctuation">;</span>
    include /application/nginx/conf/extra/bbs.conf<span class="token punctuation">;</span>
    include /application/nginx/conf/extra/edu.conf<span class="token punctuation">;</span>
    include /application/nginx/conf/extra/phpmyadmin.conf<span class="token punctuation">;</span>
    include /application/nginx/conf/extra/status.conf<span class="token punctuation">;</span>
 
    <span class="token comment">#nginx\u4F18\u5316----------------------</span>
    <span class="token comment">#\u9690\u85CF\u7248\u672C\u53F7</span>
    server_tokens on<span class="token punctuation">;</span>
 
    <span class="token comment">#\u4F18\u5316\u670D\u52A1\u5668\u57DF\u540D\u7684\u6563\u5217\u8868\u5927\u5C0F </span>
    server_names_hash_bucket_size <span class="token number">64</span><span class="token punctuation">;</span>
    server_names_hash_max_size <span class="token number">2048</span><span class="token punctuation">;</span>
 
    <span class="token comment">#\u5F00\u542F\u9AD8\u6548\u6587\u4EF6\u4F20\u8F93\u6A21\u5F0F</span>
    sendfile on<span class="token punctuation">;</span>
    <span class="token comment">#\u51CF\u5C11\u7F51\u7EDC\u62A5\u6587\u6BB5\u6570\u91CF</span>
    <span class="token comment">#tcp_nopush on;</span>
    <span class="token comment">#\u63D0\u9AD8I/O\u6027\u80FD</span>
    tcp_nodelay on<span class="token punctuation">;</span>
 
    <span class="token comment">#\u8FDE\u63A5\u8D85\u65F6 \u65F6\u95F4\u5B9A\u4E49 \u9ED8\u8BA4\u79D2 \u9ED8\u8BA465\u79D2</span>
    keepalive_timeout <span class="token number">60</span><span class="token punctuation">;</span>
    
    <span class="token comment">#\u8BFB\u53D6\u5BA2\u6237\u7AEF\u8BF7\u6C42\u5934\u6570\u636E\u7684\u8D85\u65F6\u65F6\u95F4 \u9ED8\u8BA4\u79D2 \u9ED8\u8BA460\u79D2</span>
    client_header_timeout <span class="token number">15</span><span class="token punctuation">;</span>
    
    <span class="token comment">#\u8BFB\u53D6\u5BA2\u6237\u7AEF\u8BF7\u6C42\u4E3B\u4F53\u7684\u8D85\u65F6\u65F6\u95F4 \u9ED8\u8BA4\u79D2 \u9ED8\u8BA460\u79D2</span>
    client_body_timeout <span class="token number">15</span><span class="token punctuation">;</span>
    
    <span class="token comment">#\u54CD\u5E94\u5BA2\u6237\u7AEF\u7684\u8D85\u65F6\u65F6\u95F4 \u9ED8\u8BA4\u79D2 \u9ED8\u8BA460\u79D2</span>
    send_timeout <span class="token number">25</span><span class="token punctuation">;</span>
 
    <span class="token comment">#\u4E0A\u4F20\u6587\u4EF6\u7684\u5927\u5C0F\u9650\u5236  \u9ED8\u8BA41m</span>
    client_max_body_size 8m<span class="token punctuation">;</span>
<span class="token punctuation">}</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div>`,4);function b(k,_){const e=i("ExternalLinkIcon");return t(),l(o,null,[u,n("p",null,[n("a",r,[m,c(e)])]),v],64)}var g=a(d,[["render",b],["__file","index.html.vue"]]);export{g as default};
