import{_ as i,e as n}from"./app.4176ed4c.js";const e={},s=n(`<h1 id="mosquitto-\u914D\u7F6E\u4FE1\u606F" tabindex="-1"><a class="header-anchor" href="#mosquitto-\u914D\u7F6E\u4FE1\u606F" aria-hidden="true">#</a> mosquitto \u914D\u7F6E\u4FE1\u606F</h1><div class="language-conf ext-conf line-numbers-mode"><pre class="language-conf"><code># =================================================================  
# General configuration  
# =================================================================  

# \u5BA2\u6237\u7AEF\u5FC3\u8DF3\u7684\u95F4\u9694\u65F6\u95F4  
#retry_interval 20  

# \u7CFB\u7EDF\u72B6\u6001\u7684\u5237\u65B0\u65F6\u95F4  
#sys_interval 10  

# \u7CFB\u7EDF\u8D44\u6E90\u7684\u56DE\u6536\u65F6\u95F4\uFF0C0\u8868\u793A\u5C3D\u5FEB\u5904\u7406  
#store_clean_interval 10  

# \u670D\u52A1\u8FDB\u7A0B\u7684PID  
#pid_file /var/run/mosquitto.pid  

# \u670D\u52A1\u8FDB\u7A0B\u7684\u7CFB\u7EDF\u7528\u6237  
#user mosquitto  

# \u5BA2\u6237\u7AEF\u5FC3\u8DF3\u6D88\u606F\u7684\u6700\u5927\u5E76\u53D1\u6570  
#max_inflight_messages 10  

# \u5BA2\u6237\u7AEF\u5FC3\u8DF3\u6D88\u606F\u7F13\u5B58\u961F\u5217  
#max_queued_messages 100  

# \u7528\u4E8E\u8BBE\u7F6E\u5BA2\u6237\u7AEF\u957F\u8FDE\u63A5\u7684\u8FC7\u671F\u65F6\u95F4\uFF0C\u9ED8\u8BA4\u6C38\u4E0D\u8FC7\u671F  
#persistent_client_expiration  

# =================================================================  
# Default listener  
# =================================================================  

# \u670D\u52A1\u7ED1\u5B9A\u7684IP\u5730\u5740  
#bind_address  

# \u670D\u52A1\u7ED1\u5B9A\u7684\u7AEF\u53E3\u53F7  
#port 1883  

# \u5141\u8BB8\u7684\u6700\u5927\u8FDE\u63A5\u6570\uFF0C-1\u8868\u793A\u6CA1\u6709\u9650\u5236  
#max_connections -1  

# cafile\uFF1ACA\u8BC1\u4E66\u6587\u4EF6  
# capath\uFF1ACA\u8BC1\u4E66\u76EE\u5F55  
# certfile\uFF1APEM\u8BC1\u4E66\u6587\u4EF6  
# keyfile\uFF1APEM\u5BC6\u94A5\u6587\u4EF6  
#cafile  
#capath  
#certfile  
#keyfile  

# \u5FC5\u987B\u63D0\u4F9B\u8BC1\u4E66\u4EE5\u4FDD\u8BC1\u6570\u636E\u5B89\u5168\u6027  
#require_certificate false  

# \u82E5require_certificate\u503C\u4E3Atrue\uFF0Cuse_identity_as_username\u4E5F\u5FC5\u987B\u4E3Atrue  
#use_identity_as_username false  

# \u542F\u7528PSK\uFF08Pre-shared-key\uFF09\u652F\u6301  
#psk_hint  

# SSL/TSL\u52A0\u5BC6\u7B97\u6CD5\uFF0C\u53EF\u4EE5\u4F7F\u7528\u201Copenssl ciphers\u201D\u547D\u4EE4\u83B7\u53D6  
# as the output of that command.  
#ciphers  

# =================================================================  
# Persistence  
# =================================================================  

# \u6D88\u606F\u81EA\u52A8\u4FDD\u5B58\u7684\u95F4\u9694\u65F6\u95F4  
#autosave_interval 1800  

# \u6D88\u606F\u81EA\u52A8\u4FDD\u5B58\u529F\u80FD\u7684\u5F00\u5173  
#autosave_on_changes false  

# \u6301\u4E45\u5316\u529F\u80FD\u7684\u5F00\u5173  
persistence true  

# \u6301\u4E45\u5316DB\u6587\u4EF6  
#persistence_file mosquitto.db  

# \u6301\u4E45\u5316DB\u6587\u4EF6\u76EE\u5F55  
#persistence_location /var/lib/mosquitto/  

# =================================================================  
# Logging  
# =================================================================  

# 4\u79CD\u65E5\u5FD7\u6A21\u5F0F\uFF1Astdout\u3001stderr\u3001syslog\u3001topic  
# none \u5219\u8868\u793A\u4E0D\u8BB0\u65E5\u5FD7\uFF0C\u6B64\u914D\u7F6E\u53EF\u4EE5\u63D0\u5347\u4E9B\u8BB8\u6027\u80FD  
log_dest none  

# \u9009\u62E9\u65E5\u5FD7\u7684\u7EA7\u522B\uFF08\u53EF\u8BBE\u7F6E\u591A\u9879\uFF09  
#log_type error  
#log_type warning  
#log_type notice  
#log_type information  

# \u662F\u5426\u8BB0\u5F55\u5BA2\u6237\u7AEF\u8FDE\u63A5\u4FE1\u606F  
#connection_messages true  

# \u662F\u5426\u8BB0\u5F55\u65E5\u5FD7\u65F6\u95F4  
#log_timestamp true  

# =================================================================  
# Security  
# =================================================================  

# \u5BA2\u6237\u7AEFID\u7684\u524D\u7F00\u9650\u5236\uFF0C\u53EF\u7528\u4E8E\u4FDD\u8BC1\u5B89\u5168\u6027  
#clientid_prefixes  

# \u5141\u8BB8\u533F\u540D\u7528\u6237 , \u662F\u5426\u5F00\u542F\u533F\u540D\u7528\u6237\u767B\u5F55\uFF0C\u9ED8\u8BA4\u662Ftrue
#allow_anonymous true  

# \u7528\u6237/\u5BC6\u7801\u6587\u4EF6\uFF0C\u9ED8\u8BA4\u683C\u5F0F\uFF1Ausername:password  
# \u8FD9\u4E2A\u8282\u70B9\u662F\u544A\u8BC9\u670D\u52A1\u5668\u4F60\u8981\u914D\u7F6E\u7684\u7528\u6237\u5C06\u5B58\u653E\u5728\u54EA\u91CC\u3002\u6253\u5F00\u6B64\u914D\u7F6E\u5E76\u6307\u5B9Apwfile.example\u6587\u4EF6\u8DEF\u52B2\uFF08\u6CE8\u610F\u662F\u7EDD\u5BF9\u8DEF\u52B2\uFF09
#password_file  /etc/mosquitto/pwfile.example \uFF08\u8FD9\u91CC\u7684\u5730\u5740\u6839\u636E\u81EA\u5DF1\u6587\u4EF6\u5B9E\u9645\u4F4D\u7F6E\u586B\u5199)

# PSK\u683C\u5F0F\u5BC6\u7801\u6587\u4EF6\uFF0C\u9ED8\u8BA4\u683C\u5F0F\uFF1Aidentity:key  
#psk_file  

# pattern write sensor/%u/data  
# ACL\u6743\u9650\u914D\u7F6E\uFF0C\u5E38\u7528\u8BED\u6CD5\u5982\u4E0B\uFF1A  
# \u7528\u6237\u9650\u5236\uFF1Auser &lt;username&gt;  
# \u8BDD\u9898\u9650\u5236\uFF1Atopic [read|write] &lt;topic&gt;  
# \u6B63\u5219\u9650\u5236\uFF1Apattern write sensor/%u/data  
#acl_file  

# =================================================================  
# Bridges  
# =================================================================  

# \u5141\u8BB8\u670D\u52A1\u4E4B\u95F4\u4F7F\u7528\u201C\u6865\u63A5\u201D\u6A21\u5F0F\uFF08\u53EF\u7528\u4E8E\u5206\u5E03\u5F0F\u90E8\u7F72\uFF09  
#connection &lt;name&gt;  
#address &lt;host&gt;[:&lt;port&gt;]  
#topic &lt;topic&gt; [[[out | in | both] qos-level] local-prefix remote-prefix]  

# \u8BBE\u7F6E\u6865\u63A5\u7684\u5BA2\u6237\u7AEFID  
#clientid  

# \u6865\u63A5\u65AD\u5F00\u65F6\uFF0C\u662F\u5426\u6E05\u9664\u8FDC\u7A0B\u670D\u52A1\u5668\u4E2D\u7684\u6D88\u606F  
#cleansession false  

# \u662F\u5426\u53D1\u5E03\u6865\u63A5\u7684\u72B6\u6001\u4FE1\u606F  
#notifications true  

# \u8BBE\u7F6E\u6865\u63A5\u6A21\u5F0F\u4E0B\uFF0C\u6D88\u606F\u5C06\u4F1A\u53D1\u5E03\u5230\u7684\u8BDD\u9898\u5730\u5740  
# $SYS/broker/connection/&lt;clientid&gt;/state  
#notification_topic  

# \u8BBE\u7F6E\u6865\u63A5\u7684keepalive\u6570\u503C  
#keepalive_interval 60  

# \u6865\u63A5\u6A21\u5F0F\uFF0C\u76EE\u524D\u6709\u4E09\u79CD\uFF1Aautomatic\u3001lazy\u3001once  
#start_type automatic  

# \u6865\u63A5\u6A21\u5F0Fautomatic\u7684\u8D85\u65F6\u65F6\u95F4  
#restart_timeout 30  

# \u6865\u63A5\u6A21\u5F0Flazy\u7684\u8D85\u65F6\u65F6\u95F4  
#idle_timeout 60  

# \u6865\u63A5\u5BA2\u6237\u7AEF\u7684\u7528\u6237\u540D  
#username  

# \u6865\u63A5\u5BA2\u6237\u7AEF\u7684\u5BC6\u7801  
#password  

# bridge_cafile\uFF1A\u6865\u63A5\u5BA2\u6237\u7AEF\u7684CA\u8BC1\u4E66\u6587\u4EF6  
# bridge_capath\uFF1A\u6865\u63A5\u5BA2\u6237\u7AEF\u7684CA\u8BC1\u4E66\u76EE\u5F55  
# bridge_certfile\uFF1A\u6865\u63A5\u5BA2\u6237\u7AEF\u7684PEM\u8BC1\u4E66\u6587\u4EF6  
# bridge_keyfile\uFF1A\u6865\u63A5\u5BA2\u6237\u7AEF\u7684PEM\u5BC6\u94A5\u6587\u4EF6  
#bridge_cafile  
#bridge_capath  
#bridge_certfile  
#bridge_keyfile
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div>`,2);function l(d,v){return s}var r=i(e,[["render",l],["__file","mosquitto\u914D\u7F6E.html.vue"]]);export{r as default};
