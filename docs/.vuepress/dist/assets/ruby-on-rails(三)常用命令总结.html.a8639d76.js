import{_ as e,e as i}from"./app.4176ed4c.js";const n={},s=i(`<p>\u4F7F\u7528<code>rails new appname</code>\u751F\u6210Rails\u5E94\u7528\u540E\uFF0C\u6211\u4EEC\u53EF\u4EE5\u901A\u8FC7<code>tree</code>\u547D\u4EE4\u6765\u67E5\u770BRails\u5E94\u7528\u7684\u76EE\u5F55\u7ED3\u6784\uFF1A</p><h3 id="\u76EE\u5F55\u7ED3\u6784" tabindex="-1"><a class="header-anchor" href="#\u76EE\u5F55\u7ED3\u6784" aria-hidden="true">#</a> \u76EE\u5F55\u7ED3\u6784</h3><p>\u5E94\u7528\u7A0B\u5E8F\u76EE\u5F55\u4E0B\u4F1A\u6709app\u3001bin\u3001config\u3001db\u3001lib\u3001log\u3001public\u3001test\u3001tmp\u548Cvendor\u7B4911\u4E2A\u76EE\u5F55\u548Cconfig.ru\u3001Gemfile\u3001Gemfile.lock\u3001Rakefile\u3001README.md\u7B495\u4E2A\u6587\u4EF6\u3002 \u76EE\u5F55\u5728\u7A0D\u540E\u4F1A\u4E00\u4E00\u89E3\u91CA\uFF0C\u5148\u770B\u4E00\u4E0Bapp\u76EE\u5F55\u4E0B\u7684\u6587\u4EF6\uFF1A</p><ul><li>config.ru \u7528\u6765\u542F\u52A8Rails\u7A0B\u5E8F\u7684Rack\u8BBE\u7F6E\u6587\u4EF6</li></ul><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>require ::File.expand_path(&#39;../config/environment&#39;, __FILE__)
run Myapps::Application
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div></div></div><ul><li>Gemfile\u8BBE\u7F6ERails\u7A0B\u5E8F\u6240\u4F9D\u8D56\u7684Gems (\u4E00\u65E6\u7528bundle install\u5B89\u88C5\u540E\uFF0C\u4F1A\u751F\u6210Gemfile.lock)</li></ul><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>source &#39;https://ruby.taobao.org/&#39;
gem &#39;rails&#39;, &#39;3.2.1&#39;
gem &#39;sqlite3&#39;
group :assets do
 gem &#39;sass-rails&#39;,  &#39;~&gt; 3.2.3&#39;
 gem &#39;coffee-rails&#39;, &#39;~&gt; 3.2.1&#39;
 gem &#39;uglifier&#39;, &#39;&gt;= 1.0.3&#39;
end
gem &#39;jquery-rails&#39;
gem ... ...
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ul><li>Rakefile \u7528\u6765\u8F7D\u5165\u53EF\u4EE5\u88AB\u7EC8\u7AEF\u6267\u884C\u7684Rake\u4EFB\u52A1</li></ul><hr><p>\u4E0B\u9762\u662F\u7528tree\u547D\u4EE4\u67E5\u770B\uFF0C\u6240\u663E\u793A\u7684\u76EE\u5F55\u548C\u6587\u4EF6\u7ED3\u6784\uFF1A</p><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>.
\u251C\u2500\u2500 app
\u2502  \u251C\u2500\u2500 assets
\u2502  \u2502  \u251C\u2500\u2500 images
\u2502  \u2502  \u2502  \u2514\u2500\u2500 rails.png
\u2502  \u2502  \u251C\u2500\u2500 javascripts
\u2502  \u2502  \u2502  \u2514\u2500\u2500 application.js
\u2502  \u2502  \u2514\u2500\u2500 stylesheets
\u2502  \u2502    \u2514\u2500\u2500 application.css
\u2502  \u251C\u2500\u2500 controllers
\u2502  \u2502  \u2514\u2500\u2500 application_controller.rb
\u2502  \u251C\u2500\u2500 helpers
\u2502  \u2502  \u2514\u2500\u2500 application_helper.rb
\u2502  \u251C\u2500\u2500 mailers
\u2502  \u251C\u2500\u2500 models
\u2502  \u2514\u2500\u2500 views
\u2502    \u2514\u2500\u2500 layouts
\u2502      \u2514\u2500\u2500 application.html.erb
\u251C\u2500\u2500 config
\u2502  \u251C\u2500\u2500 application.rb
\u2502  \u251C\u2500\u2500 boot.rb
\u2502  \u251C\u2500\u2500 database.yml
\u2502  \u251C\u2500\u2500 environment.rb
\u2502  \u251C\u2500\u2500 environments
\u2502  \u2502  \u251C\u2500\u2500 development.rb
\u2502  \u2502  \u251C\u2500\u2500 production.rb
\u2502  \u2502  \u2514\u2500\u2500 test.rb
\u2502  \u251C\u2500\u2500 initializers
\u2502  \u2502  \u251C\u2500\u2500 backtrace_silencers.rb
\u2502  \u2502  \u251C\u2500\u2500 inflections.rb
\u2502  \u2502  \u251C\u2500\u2500 mime_types.rb
\u2502  \u2502  \u251C\u2500\u2500 secret_token.rb
\u2502  \u2502  \u251C\u2500\u2500 session_store.rb
\u2502  \u2502  \u2514\u2500\u2500 wrap_parameters.rb
\u2502  \u251C\u2500\u2500 locales
\u2502  \u2502  \u2514\u2500\u2500 en.yml
\u2502  \u2514\u2500\u2500 routes.rb
\u251C\u2500\u2500 config.ru
\u251C\u2500\u2500 db
\u2502  \u2514\u2500\u2500 seeds.rb
\u251C\u2500\u2500 doc
\u2502  \u2514\u2500\u2500 README_FOR_APP
\u251C\u2500\u2500 Gemfile
\u251C\u2500\u2500 lib
\u2502  \u251C\u2500\u2500 assets
\u2502  \u2514\u2500\u2500 tasks
\u251C\u2500\u2500 log
\u251C\u2500\u2500 public
\u2502  \u251C\u2500\u2500 404.html
\u2502  \u251C\u2500\u2500 422.html
\u2502  \u251C\u2500\u2500 500.html
\u2502  \u251C\u2500\u2500 favicon.ico
\u2502  \u251C\u2500\u2500 index.html
\u2502  \u2514\u2500\u2500 robots.txt
\u251C\u2500\u2500 Rakefile
\u251C\u2500\u2500 README.rdoc
\u251C\u2500\u2500 script
\u2502  \u2514\u2500\u2500 rails
\u251C\u2500\u2500 test
\u2502  \u251C\u2500\u2500 fixtures
\u2502  \u251C\u2500\u2500 functional
\u2502  \u251C\u2500\u2500 integration
\u2502  \u251C\u2500\u2500 performance
\u2502  \u2502  \u2514\u2500\u2500 browsing_test.rb
\u2502  \u251C\u2500\u2500 test_helper.rb
\u2502  \u2514\u2500\u2500 unit
\u251C\u2500\u2500 tmp
\u2502  \u2514\u2500\u2500 cache
\u2502    \u2514\u2500\u2500 assets
\u2514\u2500\u2500 vendor
  \u251C\u2500\u2500 assets
  \u2502  \u251C\u2500\u2500 javascripts
  \u2502  \u2514\u2500\u2500 stylesheets
  \u2514\u2500\u2500 plugins
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h2 id="\u5E94\u7528\u76EE\u5F55-app" tabindex="-1"><a class="header-anchor" href="#\u5E94\u7528\u76EE\u5F55-app" aria-hidden="true">#</a> \u5E94\u7528\u76EE\u5F55(app/)</h2><p>app\u76EE\u5F55\u662FRails\u7A0B\u5E8F\u7684\u4E3B\u76EE\u5F55\uFF0C\u4E0D\u540C\u5B50\u76EE\u5F55\u5206\u522B\u5B58\u653E\u4E86\u6A21\u578B models (M)\u3001\u63A7\u5236\u5668 controller (C)\u3001\u89C6\u56FE views (V)\u53CAmailers\u3001helpers\u3001channels\u3001 jobs\u548Cassets\u7B49\u6587\u6863\u3002</p><h4 id="\u6A21\u578B-\u63A7\u5236\u5668-\u89C6\u56FE" tabindex="-1"><a class="header-anchor" href="#\u6A21\u578B-\u63A7\u5236\u5668-\u89C6\u56FE" aria-hidden="true">#</a> \u6A21\u578B\uFF0D\u63A7\u5236\u5668\uFF0D\u89C6\u56FE</h4><p>\u5206\u522B\u5B58\u653E\u6A21\u578B\u3001\u63A7\u5236\u5668\u548C\u89C6\u56FE\u3002\u5176\u4E2D\uFF0C\u6A21\u578B\u7EDF\u4E00\u5B58\u653E\u5728app/models\u76EE\u5F55\u4E0B\uFF0C\u63A7\u5236\u5668\u7EDF\u4E00\u5B58\u653E\u5728app/controllers\u76EE\u5F55\u4E0B(\u53EF\u4EE5\u4F7F\u7528\u76EE\u5F55\u8FDB\u4E00\u6B65\u7EC4\u7EC7\u63A7\u5236\u5668\uFF0C\u4F8B\u5982admin\u76EE\u5F55\u4E0B\u7528\u4E8E\u5B58\u653E\u7BA1\u7406\u540E\u53F0\u76F8\u5173\u7684\u63A7\u5236\u5668)\uFF0C\u89C6\u56FE\u5B58\u653E\u5728app/views\u76EE\u5F55\u4E0B\uFF0C\u89C6\u56FE\u6A21\u578B\u5B58\u653E\u5728app/view/layouts\u76EE\u5F55\u4E0B\uFF0C\u9ED8\u8BA4\u4E3Aapplicaiton.html.erb\u3002</p><h4 id="assets\u9759\u6001\u6587\u4EF6" tabindex="-1"><a class="header-anchor" href="#assets\u9759\u6001\u6587\u4EF6" aria-hidden="true">#</a> assets\u9759\u6001\u6587\u4EF6</h4><p>assets\u9759\u6001\u6587\u4EF6\u5B58\u653E\u5728app/assets\u76EE\u5F55\u4E0B\uFF0C\u5206\u522B\u4E3Aapp/assets/images\u3001app/assets/stylesheets\u3001app/assets/javascripts\u76EE\u5F55\u3002</p><h4 id="helpers" tabindex="-1"><a class="header-anchor" href="#helpers" aria-hidden="true">#</a> helpers</h4><p>helpers\u662F\u4E00\u4E9B\u5728\u89C6\u56FE\u4E2D\u53EF\u4EE5\u4F7F\u7528\u7684\u5C0F\u65B9\u6CD5\uFF0C\u7528\u6765\u4EA7\u751F\u8F83\u590D\u6742\u7684HTML\u3002\u9884\u8BBE\u7684Helper\u6587\u4EF6\u540D\u79F0\u5BF9\u5E94\u63A7\u5236\u5668\uFF0C\u4F46\u4E0D\u5F3A\u5236\u8981\u6C42\uFF0C\u5728\u4EFB\u610F\u4E00\u4E2AHelper\u6587\u4EF6\u4E2D\u5B9A\u4E49\u7684\u65B9\u6CD5\uFF0C\u90FD\u53EF\u4EE5\u5728\u4EFB\u4F55\u89C6\u56FE\u4E2D\u4F7F\u7528\u3002 ......</p><h2 id="\u914D\u7F6E\u6587\u4EF6\u76EE\u5F55-config" tabindex="-1"><a class="header-anchor" href="#\u914D\u7F6E\u6587\u4EF6\u76EE\u5F55-config" aria-hidden="true">#</a> \u914D\u7F6E\u6587\u4EF6\u76EE\u5F55(config/)</h2><p>\u867D\u7136Rails\u9075\u5FAA\u201C\u7EA6\u5B9A\u4F18\u4E8E\u914D\u7F6E\u201D\u7684\u539F\u5219\uFF0C\u4F46\u4ECD\u6709\u4E00\u4E9B\u9700\u8981\u8BBE\u5B9A\u7684\u5730\u65B9\u3002\u5728\u914D\u7F6E\u6587\u4EF6\u76EE\u5F55\u4E0B\uFF0C\u4F1A\u5B58\u653E\u5E94\u7528\u7A0B\u5E8F\u8BBE\u7F6E\u6587\u4EF6application.rb\u3001\u6570\u636E\u5E93\u8BBE\u7F6E\u6587\u4EF6database.yml\u3001\u8DEF\u7531\u8BBE\u7F6E\u6587\u4EF6routes.rb\u3001\u591A\u91CD\u73AF\u5883\u8BBE\u7F6Econfig/environments\u76EE\u5F55\u3001\u5176\u5B83\u521D\u59CB\u8BBE\u7F6E\u6587\u4EF6config/initializers\u3002</p><h4 id="rails\u542F\u52A8\u5E94\u7528\u7A0B\u5E8F\u8BBE\u7F6E" tabindex="-1"><a class="header-anchor" href="#rails\u542F\u52A8\u5E94\u7528\u7A0B\u5E8F\u8BBE\u7F6E" aria-hidden="true">#</a> Rails\u542F\u52A8\u5E94\u7528\u7A0B\u5E8F\u8BBE\u7F6E</h4><p>\u542F\u52A8Rails\u7A0B\u5E8F(\u4F8B\u5982rails console\u6216rails server)\uFF0C\u4F1A\u6267\u884C\u4EE5\u4E0B\u4E09\u4E2A\u6587\u6863 boot.rb \u8F7D\u5165Bundler\u73AF\u5883\uFF0C\u8FD9\u4E2A\u6587\u4EF6\u7531Rails\u81EA\u52A8\u4EA7\u751F\uFF0C\u4E0D\u9700\u8981\u4FEE\u6539\uFF1B</p><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>require &#39;rubygems&#39;

# Set up gems listed in the Gemfile.
ENV[&#39;BUNDLE_GEMFILE&#39;] ||= File.expand_path(&#39;../../Gemfile&#39;, __FILE__)

require &#39;bundler/setup&#39; if File.exists?(ENV[&#39;BUNDLE_GEMFILE&#39;])
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><p>application.rb \u8F7D\u5165Rails gems\u548C\u4F9D\u8D56\u7684\u5176\u5B83gems\uFF0C\u63A5\u7740\u8BBE\u5B9ARails\u7A0B\u5E8F\uFF1B</p><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>require File.expand_path(&#39;../boot&#39;, __FILE__)
require &#39;rails/all&#39;

if defined?(Bundler)
 # If you precompile assets before deploying to production, use this line
 Bundler.require(*Rails.groups(:assets =&gt; %w(development test)))
 # If you want your assets lazily compiled in production, use this line
 # Bundler.require(:default, :assets, Rails.env)
end

module Myapps
 class Application &lt; Rails::Application
  # Settings in config/environments/* take precedence over those specified here.
  # Application configuration should go into files in config/initializers
  # ... ...

  # Configure the default encoding used in templates for Ruby 1.9.
  config.encoding = &quot;utf-8&quot;

  # Configure sensitive parameters which will be filtered from the log file.
  config.filter_parameters += [:password]

  # ... ...

  # Enable the asset pipeline
  config.assets.enabled = true

  # Version of your assets, change this if you want to expire all your assets
  config.assets.version = &#39;1.0&#39;
 end
end
environment.rb \u6267\u884C\u6240\u6709\u542F\u52A8\u7A0B\u5E8F(initializers)\uFF0C\u8FD9\u4E2A\u6587\u4EF6\u540C\u6837\u7531Rails\u4EA7\u751F\uFF0C\u4E0D\u9700\u8981\u4FEE\u6539\u3002
# Load the rails application
require File.expand_path(&#39;../application&#39;, __FILE__)

# Initialize the rails application
Myapps::Application.initialize!
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h4 id="\u521D\u59CB\u8BBE\u7F6E\u6587\u4EF6-initializers" tabindex="-1"><a class="header-anchor" href="#\u521D\u59CB\u8BBE\u7F6E\u6587\u4EF6-initializers" aria-hidden="true">#</a> \u521D\u59CB\u8BBE\u7F6E\u6587\u4EF6(initializers)</h4><p>\u7531environment.rb\u8C03\u7528\uFF0C\u7CFB\u7EDF\u9ED8\u8BA4\u7684\u521D\u59CB\u8BBE\u7F6E\u6587\u4EF6\u6709backtrace_silencers.rb\u3001inflections.rb\u3001mime_types.rb\u3001secret_token.rb\u3001session_store.rb\u548Cwrap_parameters.rb\u7B496\u4E2A\uFF0C\u5206\u522B\u5BF9\u5E94\u7684\u7528\u9014\u662F\uFF1A\u9009\u62E9\u6027\u79FB\u52A8\u5F02\u5E38\u8FFD\u8E2A\u3001\u5355\u590D\u6570\u8F6C\u6362\u3001mime_types\u3001\u52A0\u5BC6cookies\u4FE1\u606F\u7684token\u3001\u9ED8\u8BA4session\u50A8\u5B58\u4EE5\u53CA\u53C2\u6570\u5C01\u88C5\u7B49\u3002</p><h2 id="\u6570\u636E\u5E93\u5B58\u50A8\u76EE\u5F55-db" tabindex="-1"><a class="header-anchor" href="#\u6570\u636E\u5E93\u5B58\u50A8\u76EE\u5F55-db" aria-hidden="true">#</a> \u6570\u636E\u5E93\u5B58\u50A8\u76EE\u5F55(db/)</h2><ul><li>migrate\u6587\u4EF6\u5939 \u6570\u636E\u5E93\u8FC1\u79FB\u6587\u4EF6</li><li>schema.rb</li><li>seeds.rb \u521D\u59CB\u5316\u6570\u636E\u5E93</li></ul><h2 id="\u5171\u4EAB\u7C7B\u6216\u6A21\u5757\u6587\u4EF6-lib" tabindex="-1"><a class="header-anchor" href="#\u5171\u4EAB\u7C7B\u6216\u6A21\u5757\u6587\u4EF6-lib" aria-hidden="true">#</a> \u5171\u4EAB\u7C7B\u6216\u6A21\u5757\u6587\u4EF6(lib/)</h2><p>\u4E00\u4E9B\u5171\u4EAB\u7684\u7C7B\u6216\u6A21\u5757\u53EF\u4EE5\u5B58\u653E\u5728\u8BE5\u76EE\u5F55\u3002\u53E6\u5916\uFF0CRake\u7684\u4EFB\u52A1\uFF0C\u53EF\u5B58\u653E\u5728lib/tasks\u76EE\u5F55\u4E0B\u3002</p><p>##\u65E5\u5FD7\u76EE\u5F55(log/)</p><ul><li>development.log \u5F00\u53D1\u73AF\u5883\u4E0B\u65E5\u5FD7\u6253\u5370</li></ul><h2 id="\u516C\u5171\u6587\u4EF6\u76EE\u5F55-public" tabindex="-1"><a class="header-anchor" href="#\u516C\u5171\u6587\u4EF6\u76EE\u5F55-public" aria-hidden="true">#</a> \u516C\u5171\u6587\u4EF6\u76EE\u5F55(public/)</h2><p>\u5BF9\u4E8Eweb\u670D\u52A1\u5668\u6765\u8BF4\uFF0C\u53EF\u4EE5\u76F4\u63A5\u8BBF\u95EE\u7684\u6587\u4EF6\u76EE\u5F55\u3002\u53EF\u4EE5\u7528\u4E8E\u5B58\u653E\u901A\u7528\u7684images\u3001stylesheets\u548Cjavascripts (Rails 2.x)\u3002</p><h2 id="\u6D4B\u8BD5\u6587\u4EF6\u76EE\u5F55-test" tabindex="-1"><a class="header-anchor" href="#\u6D4B\u8BD5\u6587\u4EF6\u76EE\u5F55-test" aria-hidden="true">#</a> \u6D4B\u8BD5\u6587\u4EF6\u76EE\u5F55(test/)</h2><p>\u7528\u4E8E\u5B58\u653E\u5355\u5143\u6D4B\u8BD5\u3001\u529F\u80FD\u6D4B\u8BD5\u53CA\u6574\u5408\u6D4B\u8BD5\u6587\u4EF6\u3002</p><h2 id="\u4E34\u65F6\u6587\u4EF6\u76EE\u5F55-tmp" tabindex="-1"><a class="header-anchor" href="#\u4E34\u65F6\u6587\u4EF6\u76EE\u5F55-tmp" aria-hidden="true">#</a> \u4E34\u65F6\u6587\u4EF6\u76EE\u5F55(tmp/)</h2><p>\u5B58\u653E\u4E00\u4E9B\u4E34\u65F6\u6587\u4EF6</p><h2 id="\u7B2C\u4E09\u65B9\u63D2\u4EF6\u76EE\u5F55-vendor" tabindex="-1"><a class="header-anchor" href="#\u7B2C\u4E09\u65B9\u63D2\u4EF6\u76EE\u5F55-vendor" aria-hidden="true">#</a> \u7B2C\u4E09\u65B9\u63D2\u4EF6\u76EE\u5F55(vendor/)</h2><p>\u5728\u4F7F\u7528bundler\u5B89\u88C5gems\u63D2\u4EF6\u65F6\uFF0C\u4E5F\u53EF\u4EE5\u9009\u62E9\u5B89\u88C5\u5728\u8BE5\u76EE\u5F55\u4E0B\u3002 \u4F8B\u5982<code>bundle install --path vendor/bundle</code></p>`,42);function l(a,d){return s}var v=e(n,[["render",l],["__file","ruby-on-rails(\u4E09)\u5E38\u7528\u547D\u4EE4\u603B\u7ED3.html.vue"]]);export{v as default};
