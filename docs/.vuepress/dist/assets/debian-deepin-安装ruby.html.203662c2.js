import{_ as n,e as s}from"./app.4176ed4c.js";const e={},a=s(`<p>Checking requirements for ubuntu. \u51FA\u9519</p><div class="language-bash ext-sh line-numbers-mode"><pre class="language-bash"><code><span class="token function">sudo</span> <span class="token function">apt-get</span> update

<span class="token function">sudo</span> <span class="token function">apt-get</span> <span class="token function">install</span> git-core <span class="token function">curl</span> zlib1g-dev build-essential libssl-dev libreadline-dev libyaml-dev libsqlite3-dev sqlite3 libxml2-dev libxslt1-dev libcurl4-openssl-dev python-software-properties

<span class="token function">sudo</span> <span class="token function">apt-get</span> <span class="token function">install</span> libgdbm-dev libncurses5-dev automake libtool bison libffi-dev

<span class="token function">curl</span> -L <span class="token punctuation">[</span>https://get.rvm.io<span class="token punctuation">]</span><span class="token punctuation">(</span>https://get.rvm.io/<span class="token punctuation">)</span> <span class="token operator">|</span> <span class="token function">bash</span> -s stable

<span class="token builtin class-name">source</span> ~/.rvm/scripts/rvm

<span class="token builtin class-name">echo</span> <span class="token string">&quot;source ~/.rvm/scripts/rvm&quot;</span> <span class="token operator">&gt;&gt;</span> ~/.bashrc

rvm <span class="token function">install</span> <span class="token number">2.4</span>.1

rvm use <span class="token number">2.4</span>.1 --default

ruby -v
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div>`,2);function i(l,t){return a}var o=n(e,[["render",i],["__file","debian-deepin-\u5B89\u88C5ruby.html.vue"]]);export{o as default};
