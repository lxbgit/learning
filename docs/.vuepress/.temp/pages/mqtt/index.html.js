import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/mqtt/index.html.vue"
const data = JSON.parse("{\"path\":\"/mqtt/\",\"title\":\"MQTT\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"服务器搭建\",\"slug\":\"服务器搭建\",\"link\":\"#服务器搭建\",\"children\":[{\"level\":3,\"title\":\"mosquitto\",\"slug\":\"mosquitto\",\"link\":\"#mosquitto\",\"children\":[]},{\"level\":3,\"title\":\"mosquitto_sub命令\",\"slug\":\"mosquitto-sub命令\",\"link\":\"#mosquitto-sub命令\",\"children\":[]},{\"level\":3,\"title\":\"其他\",\"slug\":\"其他\",\"link\":\"#其他\",\"children\":[]}]}],\"git\":{\"updatedTime\":1629253375000,\"contributors\":[{\"name\":\"xuebiaoli\",\"username\":\"xuebiaoli\",\"email\":\"lixuebiao1115@gmail.com\",\"commits\":2},{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":3}]},\"filePathRelative\":\"mqtt/readme.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
