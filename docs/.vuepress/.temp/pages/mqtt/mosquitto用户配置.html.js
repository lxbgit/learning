import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/mqtt/mosquitto用户配置.html.vue"
const data = JSON.parse("{\"path\":\"/mqtt/mosquitto%E7%94%A8%E6%88%B7%E9%85%8D%E7%BD%AE.html\",\"title\":\"mosquitto 用户配置\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"首先我们来新增两个用户 1： admin/admin 2: mosquitto/mosquitto 具体步骤\",\"slug\":\"首先我们来新增两个用户-1-admin-admin-2-mosquitto-mosquitto-具体步骤\",\"link\":\"#首先我们来新增两个用户-1-admin-admin-2-mosquitto-mosquitto-具体步骤\",\"children\":[]},{\"level\":2,\"title\":\"增加权限配置\",\"slug\":\"增加权限配置\",\"link\":\"#增加权限配置\",\"children\":[]}],\"git\":{\"updatedTime\":1595209285000,\"contributors\":[{\"name\":\"xuebiaoli\",\"username\":\"xuebiaoli\",\"email\":\"lixuebiao1115@gmail.com\",\"commits\":3},{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":1}]},\"filePathRelative\":\"mqtt/mosquitto用户配置.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
