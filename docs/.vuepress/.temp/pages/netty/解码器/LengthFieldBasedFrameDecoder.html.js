import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/netty/解码器/LengthFieldBasedFrameDecoder.html.vue"
const data = JSON.parse("{\"path\":\"/netty/%E8%A7%A3%E7%A0%81%E5%99%A8/LengthFieldBasedFrameDecoder.html\",\"title\":\"LengthFieldBasedFrameDecoder\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"重要参数\",\"slug\":\"重要参数\",\"link\":\"#重要参数\",\"children\":[]},{\"level\":2,\"title\":\"lengthFieldLength 长度域字节数\",\"slug\":\"lengthfieldlength-长度域字节数\",\"link\":\"#lengthfieldlength-长度域字节数\",\"children\":[]},{\"level\":2,\"title\":\"initialBytesToStrip\",\"slug\":\"initialbytestostrip\",\"link\":\"#initialbytestostrip\",\"children\":[]},{\"level\":2,\"title\":\"lengthAdjustment\",\"slug\":\"lengthadjustment\",\"link\":\"#lengthadjustment\",\"children\":[]},{\"level\":2,\"title\":\"其他情况\",\"slug\":\"其他情况\",\"link\":\"#其他情况\",\"children\":[]}],\"git\":{\"updatedTime\":1627358201000,\"contributors\":[{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":1}]},\"filePathRelative\":\"netty/解码器/LengthFieldBasedFrameDecoder.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
