import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/maven/index.html.vue"
const data = JSON.parse("{\"path\":\"/maven/\",\"title\":\"maven 常用命令\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"maven项目install时忽略执行test的几种方法\",\"slug\":\"maven项目install时忽略执行test的几种方法\",\"link\":\"#maven项目install时忽略执行test的几种方法\",\"children\":[]},{\"level\":2,\"title\":\"springboot项目中，在pom.xml文件的中添加如下配置：\",\"slug\":\"springboot项目中-在pom-xml文件的中添加如下配置\",\"link\":\"#springboot项目中-在pom-xml文件的中添加如下配置\",\"children\":[]},{\"level\":2,\"title\":\"maven项目的pom.xml文件的中添加如下配置：\",\"slug\":\"maven项目的pom-xml文件的中添加如下配置\",\"link\":\"#maven项目的pom-xml文件的中添加如下配置\",\"children\":[]},{\"level\":2,\"title\":\"maven项目install时忽略执行test的几种方法\",\"slug\":\"maven项目install时忽略执行test的几种方法-1\",\"link\":\"#maven项目install时忽略执行test的几种方法-1\",\"children\":[]},{\"level\":2,\"title\":\"springboot项目中，在pom.xml文件的中添加如下配置：\",\"slug\":\"springboot项目中-在pom-xml文件的中添加如下配置-1\",\"link\":\"#springboot项目中-在pom-xml文件的中添加如下配置-1\",\"children\":[]},{\"level\":2,\"title\":\"maven项目的pom.xml文件的中添加如下配置：\",\"slug\":\"maven项目的pom-xml文件的中添加如下配置-1\",\"link\":\"#maven项目的pom-xml文件的中添加如下配置-1\",\"children\":[]},{\"level\":2,\"title\":\"打包时忽略某些文件\",\"slug\":\"打包时忽略某些文件\",\"link\":\"#打包时忽略某些文件\",\"children\":[]},{\"level\":2,\"title\":\"SpringBoot 使用maven打包-引入外部jar包\",\"slug\":\"springboot-使用maven打包-引入外部jar包\",\"link\":\"#springboot-使用maven打包-引入外部jar包\",\"children\":[]}],\"git\":{\"updatedTime\":1670315493000,\"contributors\":[{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":7}]},\"filePathRelative\":\"maven/readme.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
