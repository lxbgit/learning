import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/nginx/Nginx的root和alias.html.vue"
const data = JSON.parse("{\"path\":\"/nginx/Nginx%E7%9A%84root%E5%92%8Calias.html\",\"title\":\"Nginx root和alias\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"介绍\",\"slug\":\"介绍\",\"link\":\"#介绍\",\"children\":[]},{\"level\":2,\"title\":\"举例说明\",\"slug\":\"举例说明\",\"link\":\"#举例说明\",\"children\":[]},{\"level\":2,\"title\":\"案例\",\"slug\":\"案例\",\"link\":\"#案例\",\"children\":[]}],\"git\":{\"updatedTime\":1670316263000,\"contributors\":[{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":3}]},\"filePathRelative\":\"nginx/Nginx的root和alias.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
