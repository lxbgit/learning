import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/java/时间API.html.vue"
const data = JSON.parse("{\"path\":\"/java/%E6%97%B6%E9%97%B4API.html\",\"title\":\"时间API\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"java.util.Date\",\"slug\":\"java-util-date\",\"link\":\"#java-util-date\",\"children\":[]},{\"level\":2,\"title\":\"java.time\",\"slug\":\"java-time\",\"link\":\"#java-time\",\"children\":[]}],\"git\":{\"updatedTime\":1644997255000,\"contributors\":[{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":1}]},\"filePathRelative\":\"java/时间API.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
