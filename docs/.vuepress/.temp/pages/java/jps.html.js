import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/java/jps.html.vue"
const data = JSON.parse("{\"path\":\"/java/jps.html\",\"title\":\"JPS\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"jps\",\"slug\":\"jps-1\",\"link\":\"#jps-1\",\"children\":[{\"level\":3,\"title\":\"语法\",\"slug\":\"语法\",\"link\":\"#语法\",\"children\":[]},{\"level\":3,\"title\":\"用法\",\"slug\":\"用法\",\"link\":\"#用法\",\"children\":[]}]}],\"git\":{\"updatedTime\":1669023754000,\"contributors\":[{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":1}]},\"filePathRelative\":\"java/jps.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
