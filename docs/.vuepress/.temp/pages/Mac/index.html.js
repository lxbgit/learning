import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/Mac/index.html.vue"
const data = JSON.parse("{\"path\":\"/Mac/\",\"title\":\"Mac 工具\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"Homebrew\",\"slug\":\"homebrew\",\"link\":\"#homebrew\",\"children\":[{\"level\":3,\"title\":\"Homebrew官网\",\"slug\":\"homebrew官网\",\"link\":\"#homebrew官网\",\"children\":[]},{\"level\":3,\"title\":\"使用brew安装wget\",\"slug\":\"使用brew安装wget\",\"link\":\"#使用brew安装wget\",\"children\":[]}]},{\"level\":2,\"title\":\"MacPort\",\"slug\":\"macport\",\"link\":\"#macport\",\"children\":[]},{\"level\":2,\"title\":\"SDKMAN\",\"slug\":\"sdkman\",\"link\":\"#sdkman\",\"children\":[{\"level\":3,\"title\":\"安装\",\"slug\":\"安装\",\"link\":\"#安装\",\"children\":[]},{\"level\":3,\"title\":\"使用\",\"slug\":\"使用\",\"link\":\"#使用\",\"children\":[]}]},{\"level\":2,\"title\":\"Xcode\",\"slug\":\"xcode\",\"link\":\"#xcode\",\"children\":[]},{\"level\":2,\"title\":\"mac 配置JAVA_HOME\",\"slug\":\"mac-配置java-home\",\"link\":\"#mac-配置java-home\",\"children\":[]},{\"level\":2,\"title\":\"终端命令行用sublime打开文件或者目录\",\"slug\":\"终端命令行用sublime打开文件或者目录\",\"link\":\"#终端命令行用sublime打开文件或者目录\",\"children\":[]},{\"level\":2,\"title\":\"Mac下使用brew安装redis\",\"slug\":\"mac下使用brew安装redis\",\"link\":\"#mac下使用brew安装redis\",\"children\":[]},{\"level\":2,\"title\":\"其他\",\"slug\":\"其他\",\"link\":\"#其他\",\"children\":[{\"level\":3,\"title\":\"brew 开启服务\",\"slug\":\"brew-开启服务\",\"link\":\"#brew-开启服务\",\"children\":[]},{\"level\":3,\"title\":\"Mac 常用快捷键\",\"slug\":\"mac-常用快捷键\",\"link\":\"#mac-常用快捷键\",\"children\":[]}]}],\"git\":{\"updatedTime\":1630897540000,\"contributors\":[{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":3}]},\"filePathRelative\":\"Mac/readme.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
