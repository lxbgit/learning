import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/gradle/providedCompile 与compile区别.html.vue"
const data = JSON.parse("{\"path\":\"/gradle/providedCompile%20%E4%B8%8Ecompile%E5%8C%BA%E5%88%AB.html\",\"title\":\"gradle providedCompile 与compile区别\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"Gradle compile\",\"slug\":\"gradle-compile\",\"link\":\"#gradle-compile\",\"children\":[]},{\"level\":2,\"title\":\"Gradle providedCompile\",\"slug\":\"gradle-providedcompile\",\"link\":\"#gradle-providedcompile\",\"children\":[]}],\"git\":{\"updatedTime\":1670315493000,\"contributors\":[{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":3}]},\"filePathRelative\":\"gradle/providedCompile 与compile区别.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
