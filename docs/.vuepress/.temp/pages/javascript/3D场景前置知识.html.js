import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/javascript/3D场景前置知识.html.vue"
const data = JSON.parse("{\"path\":\"/javascript/3D%E5%9C%BA%E6%99%AF%E5%89%8D%E7%BD%AE%E7%9F%A5%E8%AF%86.html\",\"title\":\"3D 场景前置知识\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"基础\",\"slug\":\"基础\",\"link\":\"#基础\",\"children\":[]},{\"level\":2,\"title\":\"相机\",\"slug\":\"相机\",\"link\":\"#相机\",\"children\":[{\"level\":3,\"title\":\"THREE.PerspectiveCamera(fov,aspect,near,far)\",\"slug\":\"three-perspectivecamera-fov-aspect-near-far\",\"link\":\"#three-perspectivecamera-fov-aspect-near-far\",\"children\":[]}]},{\"level\":2,\"title\":\"灯光\",\"slug\":\"灯光\",\"link\":\"#灯光\",\"children\":[{\"level\":3,\"title\":\"光源分类\",\"slug\":\"光源分类\",\"link\":\"#光源分类\",\"children\":[]}]},{\"level\":2,\"title\":\"Mesh\",\"slug\":\"mesh\",\"link\":\"#mesh\",\"children\":[{\"level\":3,\"title\":\"材质分类\",\"slug\":\"材质分类\",\"link\":\"#材质分类\",\"children\":[]},{\"level\":3,\"title\":\"几何图形\",\"slug\":\"几何图形\",\"link\":\"#几何图形\",\"children\":[]}]},{\"level\":2,\"title\":\"加载外部模型\",\"slug\":\"加载外部模型\",\"link\":\"#加载外部模型\",\"children\":[{\"level\":3,\"title\":\"支持的格式\",\"slug\":\"支持的格式\",\"link\":\"#支持的格式\",\"children\":[]}]},{\"level\":2,\"title\":\"粒子\",\"slug\":\"粒子\",\"link\":\"#粒子\",\"children\":[]},{\"level\":2,\"title\":\"场景交互\",\"slug\":\"场景交互\",\"link\":\"#场景交互\",\"children\":[]},{\"level\":2,\"title\":\"动画\",\"slug\":\"动画\",\"link\":\"#动画\",\"children\":[]}],\"git\":{\"updatedTime\":1610935431000,\"contributors\":[{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":1}]},\"filePathRelative\":\"javascript/3D场景前置知识.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
