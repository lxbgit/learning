import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/javascript/underscore常用的方法.html.vue"
const data = JSON.parse("{\"path\":\"/javascript/underscore%E5%B8%B8%E7%94%A8%E7%9A%84%E6%96%B9%E6%B3%95.html\",\"title\":\"underscore\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"collections\",\"slug\":\"collections\",\"link\":\"#collections\",\"children\":[]},{\"level\":2,\"title\":\"Arrays\",\"slug\":\"arrays\",\"link\":\"#arrays\",\"children\":[]}],\"git\":{\"updatedTime\":1597286492000,\"contributors\":[{\"name\":\"dreamerlxb\",\"username\":\"dreamerlxb\",\"email\":\"1558485399@qq.com\",\"commits\":2},{\"name\":\"xuebiaoli\",\"username\":\"xuebiaoli\",\"email\":\"lixuebiao1115@gmail.com\",\"commits\":1},{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":2}]},\"filePathRelative\":\"javascript/underscore常用的方法.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
