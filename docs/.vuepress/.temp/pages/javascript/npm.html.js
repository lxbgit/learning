import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/javascript/npm.html.vue"
const data = JSON.parse("{\"path\":\"/javascript/npm.html\",\"title\":\"npm 命令\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"通过改变地址来使用淘宝镜像\",\"slug\":\"通过改变地址来使用淘宝镜像\",\"link\":\"#通过改变地址来使用淘宝镜像\",\"children\":[]},{\"level\":2,\"title\":\"开发的npm registry 管理工具 nrm, 能够查看和切换当前使用的registry;\",\"slug\":\"开发的npm-registry-管理工具-nrm-能够查看和切换当前使用的registry\",\"link\":\"#开发的npm-registry-管理工具-nrm-能够查看和切换当前使用的registry\",\"children\":[]},{\"level\":2,\"title\":\"其他命令\",\"slug\":\"其他命令\",\"link\":\"#其他命令\",\"children\":[]},{\"level\":2,\"title\":\"npm下载缓慢解决方法\",\"slug\":\"npm下载缓慢解决方法\",\"link\":\"#npm下载缓慢解决方法\",\"children\":[]},{\"level\":2,\"title\":\"方法1\",\"slug\":\"方法1\",\"link\":\"#方法1\",\"children\":[]},{\"level\":2,\"title\":\"方法2 安装cnpm\",\"slug\":\"方法2-安装cnpm\",\"link\":\"#方法2-安装cnpm\",\"children\":[]},{\"level\":2,\"title\":\"n模块\",\"slug\":\"n模块\",\"link\":\"#n模块\",\"children\":[]}],\"git\":{\"updatedTime\":1622776666000,\"contributors\":[{\"name\":\"xuebiaoli\",\"username\":\"xuebiaoli\",\"email\":\"lixuebiao1115@gmail.com\",\"commits\":1},{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":4}]},\"filePathRelative\":\"javascript/npm.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
