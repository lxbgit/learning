import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/javascript/OpenLayer/OpenLayer.html.vue"
const data = JSON.parse("{\"path\":\"/javascript/OpenLayer/OpenLayer.html\",\"title\":\"openlayer4\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"ol.layer.Tile\",\"slug\":\"ol-layer-tile\",\"link\":\"#ol-layer-tile\",\"children\":[{\"level\":3,\"title\":\"事件\",\"slug\":\"事件\",\"link\":\"#事件\",\"children\":[]},{\"level\":3,\"title\":\"方法\",\"slug\":\"方法\",\"link\":\"#方法\",\"children\":[]}]},{\"level\":2,\"title\":\"展示GIF 图片 案例\",\"slug\":\"展示gif-图片-案例\",\"link\":\"#展示gif-图片-案例\",\"children\":[]}],\"git\":{\"updatedTime\":1608522231000,\"contributors\":[{\"name\":\"dreamerlxb\",\"username\":\"dreamerlxb\",\"email\":\"1558485399@qq.com\",\"commits\":2},{\"name\":\"xuebiaoli\",\"username\":\"xuebiaoli\",\"email\":\"lixuebiao1115@gmail.com\",\"commits\":2},{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":2}]},\"filePathRelative\":\"javascript/OpenLayer/OpenLayer.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
