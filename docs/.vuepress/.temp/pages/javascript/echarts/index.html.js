import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/javascript/echarts/index.html.vue"
const data = JSON.parse("{\"path\":\"/javascript/echarts/\",\"title\":\"ECharts color属性设置（RGB，透明度，线性渐变，径向渐变，纹理填充）\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[],\"git\":{\"updatedTime\":1622776666000,\"contributors\":[{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":1}]},\"filePathRelative\":\"javascript/echarts/readme.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
