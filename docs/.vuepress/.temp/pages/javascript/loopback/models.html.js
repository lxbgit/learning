import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/javascript/loopback/models.html.vue"
const data = JSON.parse("{\"path\":\"/javascript/loopback/models.html\",\"title\":\"在server/models或者common/models下定义model\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[],\"git\":{\"updatedTime\":1597286492000,\"contributors\":[{\"name\":\"dreamerlxb\",\"username\":\"dreamerlxb\",\"email\":\"1558485399@qq.com\",\"commits\":1},{\"name\":\"xuebiaoli\",\"username\":\"xuebiaoli\",\"email\":\"lixuebiao1115@gmail.com\",\"commits\":1},{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":3}]},\"filePathRelative\":\"javascript/loopback/models.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
