import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/javascript/loopback/middleware.html.vue"
const data = JSON.parse("{\"path\":\"/javascript/loopback/middleware.html\",\"title\":\"middleware配置属性\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"中间件的定义\",\"slug\":\"中间件的定义\",\"link\":\"#中间件的定义\",\"children\":[]},{\"level\":2,\"title\":\"demo\",\"slug\":\"demo\",\"link\":\"#demo\",\"children\":[]}],\"git\":{\"updatedTime\":1595407762000,\"contributors\":[{\"name\":\"dreamerlxb\",\"username\":\"dreamerlxb\",\"email\":\"1558485399@qq.com\",\"commits\":2},{\"name\":\"xuebiaoli\",\"username\":\"xuebiaoli\",\"email\":\"lixuebiao1115@gmail.com\",\"commits\":1},{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":2}]},\"filePathRelative\":\"javascript/loopback/middleware.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
