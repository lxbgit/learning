import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/Spring/i18n/index.html.vue"
const data = JSON.parse("{\"path\":\"/Spring/i18n/\",\"title\":\"国际化\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"Spring Security 国际化配置\",\"slug\":\"spring-security-国际化配置\",\"link\":\"#spring-security-国际化配置\",\"children\":[{\"level\":3,\"title\":\"重要的几个类\",\"slug\":\"重要的几个类\",\"link\":\"#重要的几个类\",\"children\":[]},{\"level\":3,\"title\":\"逐个解释\",\"slug\":\"逐个解释\",\"link\":\"#逐个解释\",\"children\":[]}]}],\"git\":{\"updatedTime\":1627358702000,\"contributors\":[{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":3}]},\"filePathRelative\":\"Spring/i18n/readme.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
