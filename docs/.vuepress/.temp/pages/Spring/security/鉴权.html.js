import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/Spring/security/鉴权.html.vue"
const data = JSON.parse("{\"path\":\"/Spring/security/%E9%89%B4%E6%9D%83.html\",\"title\":\"Authentication\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"AbstractAuthenticationProcessingFilter 介绍\",\"slug\":\"abstractauthenticationprocessingfilter-介绍\",\"link\":\"#abstractauthenticationprocessingfilter-介绍\",\"children\":[]},{\"level\":2,\"title\":\"AbstractAuthenticationProcessingFilter 由以下组件完成\",\"slug\":\"abstractauthenticationprocessingfilter-由以下组件完成\",\"link\":\"#abstractauthenticationprocessingfilter-由以下组件完成\",\"children\":[]},{\"level\":2,\"title\":\"鉴权\",\"slug\":\"鉴权\",\"link\":\"#鉴权\",\"children\":[{\"level\":3,\"title\":\"@Secured\",\"slug\":\"secured\",\"link\":\"#secured\",\"children\":[]},{\"level\":3,\"title\":\"jsr250\",\"slug\":\"jsr250\",\"link\":\"#jsr250\",\"children\":[]},{\"level\":3,\"title\":\"PrePost\",\"slug\":\"prepost\",\"link\":\"#prepost\",\"children\":[]}]}],\"git\":{\"updatedTime\":1653365614000,\"contributors\":[{\"name\":\"xuebiaoli\",\"username\":\"xuebiaoli\",\"email\":\"lixuebiao1115@gmail.com\",\"commits\":3},{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":3}]},\"filePathRelative\":\"Spring/security/鉴权.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
