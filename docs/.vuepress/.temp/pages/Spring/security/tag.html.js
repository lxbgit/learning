import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/Spring/security/tag.html.vue"
const data = JSON.parse("{\"path\":\"/Spring/security/tag.html\",\"title\":\"Spring Security（18）——Jsp标签\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"接下来就可以在页面上自由的使用Spring Security的标签库提供的标签了。\",\"slug\":\"接下来就可以在页面上自由的使用spring-security的标签库提供的标签了。\",\"link\":\"#接下来就可以在页面上自由的使用spring-security的标签库提供的标签了。\",\"children\":[]}],\"git\":{\"updatedTime\":1595557533000,\"contributors\":[{\"name\":\"xuebiaoli\",\"username\":\"xuebiaoli\",\"email\":\"lixuebiao1115@gmail.com\",\"commits\":2},{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":3}]},\"filePathRelative\":\"Spring/security/tag.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
