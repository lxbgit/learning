import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/Spring/security/认证.html.vue"
const data = JSON.parse("{\"path\":\"/Spring/security/%E8%AE%A4%E8%AF%81.html\",\"title\":\"访问控制\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"AccessDecisionManager\",\"slug\":\"accessdecisionmanager\",\"link\":\"#accessdecisionmanager\",\"children\":[]}],\"git\":{\"updatedTime\":1653365614000,\"contributors\":[{\"name\":\"xuebiaoli\",\"username\":\"xuebiaoli\",\"email\":\"lixuebiao1115@gmail.com\",\"commits\":2},{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":4}]},\"filePathRelative\":\"Spring/security/认证.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
