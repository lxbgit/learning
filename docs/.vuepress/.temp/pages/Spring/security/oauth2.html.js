import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/Spring/security/oauth2.html.vue"
const data = JSON.parse("{\"path\":\"/Spring/security/oauth2.html\",\"title\":\"OAuth2\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"基本概念\",\"slug\":\"基本概念\",\"link\":\"#基本概念\",\"children\":[]},{\"level\":2,\"title\":\"授权认证服务\",\"slug\":\"授权认证服务\",\"link\":\"#授权认证服务\",\"children\":[{\"level\":3,\"title\":\"端点接入-endpoints\",\"slug\":\"端点接入-endpoints\",\"link\":\"#端点接入-endpoints\",\"children\":[]}]}],\"git\":{\"updatedTime\":1622776666000,\"contributors\":[{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":2}]},\"filePathRelative\":\"Spring/security/oauth2.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
