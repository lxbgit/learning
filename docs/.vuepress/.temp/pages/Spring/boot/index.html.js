import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/Spring/boot/index.html.vue"
const data = JSON.parse("{\"path\":\"/Spring/boot/\",\"title\":\"Spring Boot\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"@Configuration\",\"slug\":\"configuration\",\"link\":\"#configuration\",\"children\":[{\"level\":3,\"title\":\"注解的配置类有如下要求\",\"slug\":\"注解的配置类有如下要求\",\"link\":\"#注解的配置类有如下要求\",\"children\":[]},{\"level\":3,\"title\":\"@Configuation 总结\",\"slug\":\"configuation-总结\",\"link\":\"#configuation-总结\",\"children\":[]}]},{\"level\":2,\"title\":\"GlobalMethodSecurity\",\"slug\":\"globalmethodsecurity\",\"link\":\"#globalmethodsecurity\",\"children\":[]},{\"level\":2,\"title\":\"异常处理\",\"slug\":\"异常处理\",\"link\":\"#异常处理\",\"children\":[]},{\"level\":2,\"title\":\"SpringBoot 项目在centos 下部署\",\"slug\":\"springboot-项目在centos-下部署\",\"link\":\"#springboot-项目在centos-下部署\",\"children\":[]}],\"git\":{\"updatedTime\":1616220774000,\"contributors\":[{\"name\":\"xuebiaoli\",\"username\":\"xuebiaoli\",\"email\":\"lixuebiao1115@gmail.com\",\"commits\":3},{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":5}]},\"filePathRelative\":\"Spring/boot/readme.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
