import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/Spring/部署/centos7下部署.html.vue"
const data = JSON.parse("{\"path\":\"/Spring/%E9%83%A8%E7%BD%B2/centos7%E4%B8%8B%E9%83%A8%E7%BD%B2.html\",\"title\":\"SpringBoot 项目在centos 下部署\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"打jar包，不啰嗦\",\"slug\":\"打jar包-不啰嗦\",\"link\":\"#打jar包-不啰嗦\",\"children\":[]},{\"level\":2,\"title\":\"创建linux下服务\",\"slug\":\"创建linux下服务\",\"link\":\"#创建linux下服务\",\"children\":[]},{\"level\":2,\"title\":\"在部署服务器上执行以下命令\",\"slug\":\"在部署服务器上执行以下命令\",\"link\":\"#在部署服务器上执行以下命令\",\"children\":[]}],\"git\":{\"updatedTime\":1608102766000,\"contributors\":[{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":4}]},\"filePathRelative\":\"Spring/部署/centos7下部署.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
