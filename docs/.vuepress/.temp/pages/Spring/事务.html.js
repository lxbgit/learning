import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/Spring/事务.html.vue"
const data = JSON.parse("{\"path\":\"/Spring/%E4%BA%8B%E5%8A%A1.html\",\"title\":\"Spring事务介绍\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"Transactional注解的常用属性表：\",\"slug\":\"transactional注解的常用属性表\",\"link\":\"#transactional注解的常用属性表\",\"children\":[]},{\"level\":2,\"title\":\"TransactionDefinition传播行为的常量：\",\"slug\":\"transactiondefinition传播行为的常量\",\"link\":\"#transactiondefinition传播行为的常量\",\"children\":[]},{\"level\":2,\"title\":\"@Transactional spring 配置事务 注意事项\",\"slug\":\"transactional-spring-配置事务-注意事项\",\"link\":\"#transactional-spring-配置事务-注意事项\",\"children\":[]},{\"level\":2,\"title\":\"数据库事务\",\"slug\":\"数据库事务\",\"link\":\"#数据库事务\",\"children\":[]}],\"git\":{\"updatedTime\":1595558888000,\"contributors\":[{\"name\":\"xuebiaoli\",\"username\":\"xuebiaoli\",\"email\":\"lixuebiao1115@gmail.com\",\"commits\":2},{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":3}]},\"filePathRelative\":\"Spring/事务.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
