import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/Spring/mvc/servlet.html.vue"
const data = JSON.parse("{\"path\":\"/Spring/mvc/servlet.html\",\"title\":\"Servlet 和 Filter\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"调用流程\",\"slug\":\"调用流程\",\"link\":\"#调用流程\",\"children\":[]},{\"level\":2,\"title\":\"注意点\",\"slug\":\"注意点\",\"link\":\"#注意点\",\"children\":[]}],\"git\":{\"updatedTime\":1644978354000,\"contributors\":[{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":2}]},\"filePathRelative\":\"Spring/mvc/servlet.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
