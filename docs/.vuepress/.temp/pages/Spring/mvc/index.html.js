import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/Spring/mvc/index.html.vue"
const data = JSON.parse("{\"path\":\"/Spring/mvc/\",\"title\":\"Spring MVC\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"过滤器、拦截器、控制器\",\"slug\":\"过滤器、拦截器、控制器\",\"link\":\"#过滤器、拦截器、控制器\",\"children\":[]},{\"level\":2,\"title\":\"@RequestMapping\",\"slug\":\"requestmapping\",\"link\":\"#requestmapping\",\"children\":[]},{\"level\":2,\"title\":\"@ModelAttribute\",\"slug\":\"modelattribute\",\"link\":\"#modelattribute\",\"children\":[{\"level\":3,\"title\":\"@ModelAttribute用于注解方法\",\"slug\":\"modelattribute用于注解方法\",\"link\":\"#modelattribute用于注解方法\",\"children\":[]},{\"level\":3,\"title\":\"@ModelAttribute注解方法入参\",\"slug\":\"modelattribute注解方法入参\",\"link\":\"#modelattribute注解方法入参\",\"children\":[]}]}],\"git\":{\"updatedTime\":1627358702000,\"contributors\":[{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":3}]},\"filePathRelative\":\"Spring/mvc/readme.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
