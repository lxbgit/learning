import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/Spring/v5.0/changelog.html.vue"
const data = JSON.parse("{\"path\":\"/Spring/v5.0/changelog.html\",\"title\":\"Spring5变化\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"基准升级\",\"slug\":\"基准升级\",\"link\":\"#基准升级\",\"children\":[]},{\"level\":2,\"title\":\"兼容 JDK9 运行时\",\"slug\":\"兼容-jdk9-运行时\",\"link\":\"#兼容-jdk9-运行时\",\"children\":[]},{\"level\":2,\"title\":\"使用 JDK8 特性\",\"slug\":\"使用-jdk8-特性\",\"link\":\"#使用-jdk8-特性\",\"children\":[]},{\"level\":2,\"title\":\"响应式编程支持\",\"slug\":\"响应式编程支持\",\"link\":\"#响应式编程支持\",\"children\":[]},{\"level\":2,\"title\":\"函数式web框架\",\"slug\":\"函数式web框架\",\"link\":\"#函数式web框架\",\"children\":[]},{\"level\":2,\"title\":\"Kotlin支持\",\"slug\":\"kotlin支持\",\"link\":\"#kotlin支持\",\"children\":[]},{\"level\":2,\"title\":\"移除的特性\",\"slug\":\"移除的特性\",\"link\":\"#移除的特性\",\"children\":[]},{\"level\":2,\"title\":\"增强\",\"slug\":\"增强\",\"link\":\"#增强\",\"children\":[]}],\"git\":{\"updatedTime\":1627293767000,\"contributors\":[{\"name\":\"dreamerlxb\",\"username\":\"dreamerlxb\",\"email\":\"1558485399@qq.com\",\"commits\":2},{\"name\":\"xuebiaoli\",\"username\":\"xuebiaoli\",\"email\":\"lixuebiao1115@gmail.com\",\"commits\":3},{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":3}]},\"filePathRelative\":\"Spring/v5.0/changelog.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
