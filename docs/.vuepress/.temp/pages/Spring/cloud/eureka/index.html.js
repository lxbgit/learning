import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/Spring/cloud/eureka/index.html.vue"
const data = JSON.parse("{\"path\":\"/Spring/cloud/eureka/\",\"title\":\"服务注册中心\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"eureka\",\"slug\":\"eureka\",\"link\":\"#eureka\",\"children\":[]}],\"git\":{\"updatedTime\":1616492473000,\"contributors\":[{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":2}]},\"filePathRelative\":\"Spring/cloud/eureka/readme.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
