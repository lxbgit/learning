export const data = {
  "key": "v-16a01b20",
  "path": "/build-tools/maven/",
  "title": "maven 常用命令",
  "lang": "zh-CN",
  "frontmatter": {},
  "excerpt": "",
  "headers": [
    {
      "level": 2,
      "title": "maven项目install时忽略执行test的几种方法",
      "slug": "maven项目install时忽略执行test的几种方法",
      "children": []
    },
    {
      "level": 2,
      "title": "springboot项目中，在pom.xml文件的中添加如下配置：",
      "slug": "springboot项目中-在pom-xml文件的中添加如下配置",
      "children": []
    },
    {
      "level": 2,
      "title": "maven项目的pom.xml文件的中添加如下配置：",
      "slug": "maven项目的pom-xml文件的中添加如下配置",
      "children": []
    },
    {
      "level": 2,
      "title": "maven项目install时忽略执行test的几种方法",
      "slug": "maven项目install时忽略执行test的几种方法-1",
      "children": []
    },
    {
      "level": 2,
      "title": "springboot项目中，在pom.xml文件的中添加如下配置：",
      "slug": "springboot项目中-在pom-xml文件的中添加如下配置-1",
      "children": []
    },
    {
      "level": 2,
      "title": "maven项目的pom.xml文件的中添加如下配置：",
      "slug": "maven项目的pom-xml文件的中添加如下配置-1",
      "children": []
    },
    {
      "level": 2,
      "title": "打包时忽略某些文件",
      "slug": "打包时忽略某些文件",
      "children": []
    },
    {
      "level": 2,
      "title": "SpringBoot 使用maven打包-引入外部jar包",
      "slug": "springboot-使用maven打包-引入外部jar包",
      "children": []
    }
  ],
  "git": {
    "updatedTime": 1608522231000,
    "contributors": [
      {
        "name": "apple",
        "email": "lixuebiao1115@126.com",
        "commits": 3
      }
    ]
  },
  "filePathRelative": "build-tools/maven/readme.md"
}
