export const data = {
  "key": "v-ab2ca022",
  "path": "/build-tools/maven/maven%E7%88%B6%E5%AD%90%E9%A1%B9%E7%9B%AE%E6%89%93%E5%8C%85%E5%BC%82%E5%B8%B8.html",
  "title": "maven父子工程---子模块相互依赖打包时所遇到的问题:依赖的程序包找不到",
  "lang": "zh-CN",
  "frontmatter": {},
  "excerpt": "",
  "headers": [
    {
      "level": 2,
      "title": "打war包时",
      "slug": "打war包时",
      "children": []
    },
    {
      "level": 2,
      "title": "如果是以jar包形式提供那么pom修改为",
      "slug": "如果是以jar包形式提供那么pom修改为",
      "children": []
    },
    {
      "level": 2,
      "title": "原因",
      "slug": "原因",
      "children": []
    }
  ],
  "git": {
    "updatedTime": 1617867591000,
    "contributors": [
      {
        "name": "apple",
        "email": "lixuebiao1115@126.com",
        "commits": 4
      }
    ]
  },
  "filePathRelative": "build-tools/maven/maven父子项目打包异常.md"
}
