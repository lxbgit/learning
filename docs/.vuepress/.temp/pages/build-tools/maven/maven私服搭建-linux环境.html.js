export const data = {
  "key": "v-76cd5f52",
  "path": "/build-tools/maven/maven%E7%A7%81%E6%9C%8D%E6%90%AD%E5%BB%BA-linux%E7%8E%AF%E5%A2%83.html",
  "title": "Nexus 在CentOS下安装",
  "lang": "zh-CN",
  "frontmatter": {},
  "excerpt": "",
  "headers": [
    {
      "level": 2,
      "title": "nexus 下载并解压",
      "slug": "nexus-下载并解压",
      "children": []
    },
    {
      "level": 2,
      "title": "检查环境",
      "slug": "检查环境",
      "children": []
    },
    {
      "level": 2,
      "title": "配置 nexus 环境变量",
      "slug": "配置-nexus-环境变量",
      "children": []
    },
    {
      "level": 2,
      "title": "启动nexus",
      "slug": "启动nexus",
      "children": []
    },
    {
      "level": 2,
      "title": "其他命令",
      "slug": "其他命令",
      "children": []
    },
    {
      "level": 2,
      "title": "默认",
      "slug": "默认",
      "children": []
    },
    {
      "level": 2,
      "title": "问题",
      "slug": "问题",
      "children": []
    }
  ],
  "git": {
    "updatedTime": 1616481666000,
    "contributors": [
      {
        "name": "apple",
        "email": "lixuebiao1115@126.com",
        "commits": 4
      }
    ]
  },
  "filePathRelative": "build-tools/maven/maven私服搭建-linux环境.md"
}
