export const data = {
  "key": "v-302ca0d6",
  "path": "/build-tools/gradle/providedCompile%20%E4%B8%8Ecompile%E5%8C%BA%E5%88%AB.html",
  "title": "gradle providedCompile 与compile区别",
  "lang": "zh-CN",
  "frontmatter": {},
  "excerpt": "",
  "headers": [
    {
      "level": 2,
      "title": "Gradle compile",
      "slug": "gradle-compile",
      "children": []
    },
    {
      "level": 2,
      "title": "Gradle providedCompile",
      "slug": "gradle-providedcompile",
      "children": []
    }
  ],
  "git": {
    "updatedTime": 1617867591000,
    "contributors": [
      {
        "name": "apple",
        "email": "lixuebiao1115@126.com",
        "commits": 2
      }
    ]
  },
  "filePathRelative": "build-tools/gradle/providedCompile 与compile区别.md"
}
