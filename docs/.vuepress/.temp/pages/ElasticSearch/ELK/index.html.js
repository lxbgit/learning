import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/ElasticSearch/ELK/index.html.vue"
const data = JSON.parse("{\"path\":\"/ElasticSearch/ELK/\",\"title\":\"ELK\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"ELK简介\",\"slug\":\"elk简介\",\"link\":\"#elk简介\",\"children\":[]},{\"level\":2,\"title\":\"版本说明\",\"slug\":\"版本说明\",\"link\":\"#版本说明\",\"children\":[]},{\"level\":2,\"title\":\"安装\",\"slug\":\"安装\",\"link\":\"#安装\",\"children\":[{\"level\":3,\"title\":\"环境检查\",\"slug\":\"环境检查\",\"link\":\"#环境检查\",\"children\":[]},{\"level\":3,\"title\":\"部署安装\",\"slug\":\"部署安装\",\"link\":\"#部署安装\",\"children\":[]}]}],\"git\":{\"updatedTime\":1642062740000,\"contributors\":[{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":3}]},\"filePathRelative\":\"ElasticSearch/ELK/readme.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
