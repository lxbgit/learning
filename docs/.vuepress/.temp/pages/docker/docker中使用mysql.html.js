import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/docker/docker中使用mysql.html.vue"
const data = JSON.parse("{\"path\":\"/docker/docker%E4%B8%AD%E4%BD%BF%E7%94%A8mysql.html\",\"title\":\"docker 中使用mysql\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"拉取mysql\",\"slug\":\"拉取mysql\",\"link\":\"#拉取mysql\",\"children\":[]},{\"level\":2,\"title\":\"启动\",\"slug\":\"启动\",\"link\":\"#启动\",\"children\":[]},{\"level\":2,\"title\":\"启动mysql demo\",\"slug\":\"启动mysql-demo\",\"link\":\"#启动mysql-demo\",\"children\":[]},{\"level\":2,\"title\":\"进入容器bash\",\"slug\":\"进入容器bash\",\"link\":\"#进入容器bash\",\"children\":[]},{\"level\":2,\"title\":\"启动mysql\",\"slug\":\"启动mysql\",\"link\":\"#启动mysql\",\"children\":[{\"level\":3,\"title\":\"目录结构\",\"slug\":\"目录结构\",\"link\":\"#目录结构\",\"children\":[]},{\"level\":3,\"title\":\"5.7\",\"slug\":\"_5-7\",\"link\":\"#_5-7\",\"children\":[]},{\"level\":3,\"title\":\"8.x\",\"slug\":\"_8-x\",\"link\":\"#_8-x\",\"children\":[]},{\"level\":3,\"title\":\"参数解释\",\"slug\":\"参数解释\",\"link\":\"#参数解释\",\"children\":[]}]},{\"level\":2,\"title\":\"注意点1\",\"slug\":\"注意点1\",\"link\":\"#注意点1\",\"children\":[]},{\"level\":2,\"title\":\"注意点2\",\"slug\":\"注意点2\",\"link\":\"#注意点2\",\"children\":[]}],\"git\":{\"updatedTime\":1610935431000,\"contributors\":[{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":3}]},\"filePathRelative\":\"docker/docker中使用mysql.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
