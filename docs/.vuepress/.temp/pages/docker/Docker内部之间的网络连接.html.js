import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/docker/Docker内部之间的网络连接.html.vue"
const data = JSON.parse("{\"path\":\"/docker/Docker%E5%86%85%E9%83%A8%E4%B9%8B%E9%97%B4%E7%9A%84%E7%BD%91%E7%BB%9C%E8%BF%9E%E6%8E%A5.html\",\"title\":\"Docker 内部之间的网络连接\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"demo\",\"slug\":\"demo\",\"link\":\"#demo\",\"children\":[]},{\"level\":2,\"title\":\"Docker 创建bridge 网络\",\"slug\":\"docker-创建bridge-网络\",\"link\":\"#docker-创建bridge-网络\",\"children\":[]}],\"git\":{\"updatedTime\":1629274359000,\"contributors\":[{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":4}]},\"filePathRelative\":\"docker/Docker内部之间的网络连接.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
