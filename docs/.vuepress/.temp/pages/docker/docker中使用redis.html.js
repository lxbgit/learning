import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/docker/docker中使用redis.html.vue"
const data = JSON.parse("{\"path\":\"/docker/docker%E4%B8%AD%E4%BD%BF%E7%94%A8redis.html\",\"title\":\"docker 中使用redis\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"搜索镜像\",\"slug\":\"搜索镜像\",\"link\":\"#搜索镜像\",\"children\":[]},{\"level\":2,\"title\":\"下载\",\"slug\":\"下载\",\"link\":\"#下载\",\"children\":[]},{\"level\":2,\"title\":\"查看\",\"slug\":\"查看\",\"link\":\"#查看\",\"children\":[]},{\"level\":2,\"title\":\"启用daemon\",\"slug\":\"启用daemon\",\"link\":\"#启用daemon\",\"children\":[]},{\"level\":2,\"title\":\"自定义redis.conf启动\",\"slug\":\"自定义redis-conf启动\",\"link\":\"#自定义redis-conf启动\",\"children\":[]},{\"level\":2,\"title\":\"查看运行状态\",\"slug\":\"查看运行状态\",\"link\":\"#查看运行状态\",\"children\":[]},{\"level\":2,\"title\":\"启用redis-cli，即redis客户端\",\"slug\":\"启用redis-cli-即redis客户端\",\"link\":\"#启用redis-cli-即redis客户端\",\"children\":[]}],\"git\":{\"updatedTime\":1608535451000,\"contributors\":[{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":1}]},\"filePathRelative\":\"docker/docker中使用redis.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
