import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/docker/使用dockerfile创建镜像.html.vue"
const data = JSON.parse("{\"path\":\"/docker/%E4%BD%BF%E7%94%A8dockerfile%E5%88%9B%E5%BB%BA%E9%95%9C%E5%83%8F.html\",\"title\":\"Docker Dockerfile\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"docker 部署 SpringBoot jar\",\"slug\":\"docker-部署-springboot-jar\",\"link\":\"#docker-部署-springboot-jar\",\"children\":[]},{\"level\":2,\"title\":\"根据dockerfile 创建镜像\",\"slug\":\"根据dockerfile-创建镜像\",\"link\":\"#根据dockerfile-创建镜像\",\"children\":[]},{\"level\":2,\"title\":\"运行镜像\",\"slug\":\"运行镜像\",\"link\":\"#运行镜像\",\"children\":[]},{\"level\":2,\"title\":\"查看镜像是否运行\",\"slug\":\"查看镜像是否运行\",\"link\":\"#查看镜像是否运行\",\"children\":[]}],\"git\":{\"updatedTime\":1608700363000,\"contributors\":[{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":3}]},\"filePathRelative\":\"docker/使用dockerfile创建镜像.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
