import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/docker/docker使用nginx.html.vue"
const data = JSON.parse("{\"path\":\"/docker/docker%E4%BD%BF%E7%94%A8nginx.html\",\"title\":\"docker 中使用nginx\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"使用docker 下载nginx 镜像\",\"slug\":\"使用docker-下载nginx-镜像\",\"link\":\"#使用docker-下载nginx-镜像\",\"children\":[]},{\"level\":2,\"title\":\"启动nginx\",\"slug\":\"启动nginx\",\"link\":\"#启动nginx\",\"children\":[]},{\"level\":2,\"title\":\"这样不是很方便，还有第二种方式，挂载配置文件\",\"slug\":\"这样不是很方便-还有第二种方式-挂载配置文件\",\"link\":\"#这样不是很方便-还有第二种方式-挂载配置文件\",\"children\":[]},{\"level\":2,\"title\":\"Nginx之location中反向代理proxy_pass配置\",\"slug\":\"nginx之location中反向代理proxy-pass配置\",\"link\":\"#nginx之location中反向代理proxy-pass配置\",\"children\":[]}],\"git\":{\"updatedTime\":1629274359000,\"contributors\":[{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":4}]},\"filePathRelative\":\"docker/docker使用nginx.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
