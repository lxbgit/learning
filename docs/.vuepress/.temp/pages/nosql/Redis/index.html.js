import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/nosql/Redis/index.html.vue"
const data = JSON.parse("{\"path\":\"/nosql/Redis/\",\"title\":\"Redis\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"Mac 下安装 Redis\",\"slug\":\"mac-下安装-redis\",\"link\":\"#mac-下安装-redis\",\"children\":[]},{\"level\":2,\"title\":\"Linux下安装\",\"slug\":\"linux下安装\",\"link\":\"#linux下安装\",\"children\":[]},{\"level\":2,\"title\":\"配置文件解析\",\"slug\":\"配置文件解析\",\"link\":\"#配置文件解析\",\"children\":[]},{\"level\":2,\"title\":\"命令\",\"slug\":\"命令\",\"link\":\"#命令\",\"children\":[]}],\"git\":{\"updatedTime\":1628562497000,\"contributors\":[{\"name\":\"xuebiaoli\",\"username\":\"xuebiaoli\",\"email\":\"lixuebiao1115@gmail.com\",\"commits\":1},{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":4}]},\"filePathRelative\":\"nosql/Redis/readme.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
