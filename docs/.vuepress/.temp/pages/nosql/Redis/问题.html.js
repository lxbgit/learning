import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/nosql/Redis/问题.html.vue"
const data = JSON.parse("{\"path\":\"/nosql/Redis/%E9%97%AE%E9%A2%98.html\",\"title\":\"Redis 问题\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"缓存雪崩\",\"slug\":\"缓存雪崩\",\"link\":\"#缓存雪崩\",\"children\":[]},{\"level\":2,\"title\":\"缓存穿透\",\"slug\":\"缓存穿透\",\"link\":\"#缓存穿透\",\"children\":[]},{\"level\":2,\"title\":\"缓存击穿\",\"slug\":\"缓存击穿\",\"link\":\"#缓存击穿\",\"children\":[]}],\"git\":{\"updatedTime\":1628562497000,\"contributors\":[{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":2}]},\"filePathRelative\":\"nosql/Redis/问题.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
