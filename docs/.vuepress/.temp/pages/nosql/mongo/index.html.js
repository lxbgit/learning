import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/nosql/mongo/index.html.vue"
const data = JSON.parse("{\"path\":\"/nosql/mongo/\",\"title\":\"MongoDB\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"安装\",\"slug\":\"安装\",\"link\":\"#安装\",\"children\":[]},{\"level\":2,\"title\":\"更新\",\"slug\":\"更新\",\"link\":\"#更新\",\"children\":[]},{\"level\":2,\"title\":\"成功\",\"slug\":\"成功\",\"link\":\"#成功\",\"children\":[]},{\"level\":2,\"title\":\"开启服务\",\"slug\":\"开启服务\",\"link\":\"#开启服务\",\"children\":[]}],\"git\":{\"updatedTime\":1616469100000,\"contributors\":[{\"name\":\"xuebiaoli\",\"username\":\"xuebiaoli\",\"email\":\"lixuebiao1115@gmail.com\",\"commits\":3},{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":3}]},\"filePathRelative\":\"nosql/mongo/readme.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
