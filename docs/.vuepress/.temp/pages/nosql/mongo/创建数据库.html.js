import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/nosql/mongo/创建数据库.html.vue"
const data = JSON.parse("{\"path\":\"/nosql/mongo/%E5%88%9B%E5%BB%BA%E6%95%B0%E6%8D%AE%E5%BA%93.html\",\"title\":\"MongoDB 创建数据库\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"语法\",\"slug\":\"语法\",\"link\":\"#语法\",\"children\":[]},{\"level\":2,\"title\":\"实例\",\"slug\":\"实例\",\"link\":\"#实例\",\"children\":[]}],\"git\":{\"updatedTime\":1595209285000,\"contributors\":[{\"name\":\"xuebiaoli\",\"username\":\"xuebiaoli\",\"email\":\"lixuebiao1115@gmail.com\",\"commits\":1},{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":1}]},\"filePathRelative\":\"nosql/mongo/创建数据库.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
