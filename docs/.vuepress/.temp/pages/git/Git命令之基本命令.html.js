import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/Git/Git命令之基本命令.html.vue"
const data = JSON.parse("{\"path\":\"/Git/Git%E5%91%BD%E4%BB%A4%E4%B9%8B%E5%9F%BA%E6%9C%AC%E5%91%BD%E4%BB%A4.html\",\"title\":\"Git 常用命令\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"常用命令\",\"slug\":\"常用命令\",\"link\":\"#常用命令\",\"children\":[]},{\"level\":2,\"title\":\"分支常用命令\",\"slug\":\"分支常用命令\",\"link\":\"#分支常用命令\",\"children\":[]}],\"git\":{\"updatedTime\":1628560846000,\"contributors\":[{\"name\":\"李雪标\",\"username\":\"李雪标\",\"email\":\"lixuebiao1115@163.com\",\"commits\":1}]},\"filePathRelative\":\"Git/Git命令之基本命令.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
