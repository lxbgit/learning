import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/Git/Git命令之版本回退.html.vue"
const data = JSON.parse("{\"path\":\"/Git/Git%E5%91%BD%E4%BB%A4%E4%B9%8B%E7%89%88%E6%9C%AC%E5%9B%9E%E9%80%80.html\",\"title\":\"Git 常用命令2\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"git fetch\",\"slug\":\"git-fetch\",\"link\":\"#git-fetch\",\"children\":[]},{\"level\":2,\"title\":\"git pull\",\"slug\":\"git-pull\",\"link\":\"#git-pull\",\"children\":[]},{\"level\":2,\"title\":\"回退版本\",\"slug\":\"回退版本\",\"link\":\"#回退版本\",\"children\":[{\"level\":3,\"title\":\"git reset\",\"slug\":\"git-reset\",\"link\":\"#git-reset\",\"children\":[]},{\"level\":3,\"title\":\"注意：谨慎使用 –hard 参数，它会删除回退点之前的所有信息。\",\"slug\":\"注意-谨慎使用-–hard-参数-它会删除回退点之前的所有信息。\",\"link\":\"#注意-谨慎使用-–hard-参数-它会删除回退点之前的所有信息。\",\"children\":[]},{\"level\":3,\"title\":\"git revert\",\"slug\":\"git-revert\",\"link\":\"#git-revert\",\"children\":[]}]}],\"git\":{\"updatedTime\":1628560922000,\"contributors\":[{\"name\":\"李雪标\",\"username\":\"李雪标\",\"email\":\"lixuebiao1115@163.com\",\"commits\":1}]},\"filePathRelative\":\"Git/Git命令之版本回退.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
