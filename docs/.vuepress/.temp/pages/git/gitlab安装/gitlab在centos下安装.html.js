import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/Git/gitlab安装/gitlab在centos下安装.html.vue"
const data = JSON.parse("{\"path\":\"/Git/gitlab%E5%AE%89%E8%A3%85/gitlab%E5%9C%A8centos%E4%B8%8B%E5%AE%89%E8%A3%85.html\",\"title\":\"git 安装\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"gitlab\",\"slug\":\"gitlab\",\"link\":\"#gitlab\",\"children\":[{\"level\":3,\"title\":\"新建 /etc/yum.repos.d/gitlab_gitlab-ce.repo，内容为\",\"slug\":\"新建-etc-yum-repos-d-gitlab-gitlab-ce-repo-内容为\",\"link\":\"#新建-etc-yum-repos-d-gitlab-gitlab-ce-repo-内容为\",\"children\":[]},{\"level\":3,\"title\":\"安装依赖\",\"slug\":\"安装依赖\",\"link\":\"#安装依赖\",\"children\":[]},{\"level\":3,\"title\":\"GitLab使用postfix发送邮件\",\"slug\":\"gitlab使用postfix发送邮件\",\"link\":\"#gitlab使用postfix发送邮件\",\"children\":[]},{\"level\":3,\"title\":\"下载\",\"slug\":\"下载\",\"link\":\"#下载\",\"children\":[]},{\"level\":3,\"title\":\"安装\",\"slug\":\"安装\",\"link\":\"#安装\",\"children\":[]},{\"level\":3,\"title\":\"执行\",\"slug\":\"执行\",\"link\":\"#执行\",\"children\":[]},{\"level\":3,\"title\":\"修改密码\",\"slug\":\"修改密码\",\"link\":\"#修改密码\",\"children\":[]},{\"level\":3,\"title\":\"修改gitlab配置文件指定服务器ip和自定义端口\",\"slug\":\"修改gitlab配置文件指定服务器ip和自定义端口\",\"link\":\"#修改gitlab配置文件指定服务器ip和自定义端口\",\"children\":[]},{\"level\":3,\"title\":\"使配置生效\",\"slug\":\"使配置生效\",\"link\":\"#使配置生效\",\"children\":[]},{\"level\":3,\"title\":\"gitlab命令\",\"slug\":\"gitlab命令\",\"link\":\"#gitlab命令\",\"children\":[]},{\"level\":3,\"title\":\"问题\",\"slug\":\"问题\",\"link\":\"#问题\",\"children\":[]}]}],\"git\":{},\"filePathRelative\":\"Git/gitlab安装/gitlab在centos下安装.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
