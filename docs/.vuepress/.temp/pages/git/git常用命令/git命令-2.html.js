import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/Git/git常用命令/git命令-2.html.vue"
const data = JSON.parse("{\"path\":\"/Git/git%E5%B8%B8%E7%94%A8%E5%91%BD%E4%BB%A4/git%E5%91%BD%E4%BB%A4-2.html\",\"title\":\"Git 常用命令2\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"git fetch\",\"slug\":\"git-fetch\",\"link\":\"#git-fetch\",\"children\":[]},{\"level\":2,\"title\":\"git pull\",\"slug\":\"git-pull\",\"link\":\"#git-pull\",\"children\":[]},{\"level\":2,\"title\":\"回退版本\",\"slug\":\"回退版本\",\"link\":\"#回退版本\",\"children\":[{\"level\":3,\"title\":\"git reset\",\"slug\":\"git-reset\",\"link\":\"#git-reset\",\"children\":[]},{\"level\":3,\"title\":\"注意：谨慎使用 –hard 参数，它会删除回退点之前的所有信息。\",\"slug\":\"注意-谨慎使用-–hard-参数-它会删除回退点之前的所有信息。\",\"link\":\"#注意-谨慎使用-–hard-参数-它会删除回退点之前的所有信息。\",\"children\":[]},{\"level\":3,\"title\":\"git revert\",\"slug\":\"git-revert\",\"link\":\"#git-revert\",\"children\":[]}]}],\"git\":{},\"filePathRelative\":\"Git/git常用命令/git命令-2.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
