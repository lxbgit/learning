import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/Git/index.html.vue"
const data = JSON.parse("{\"path\":\"/Git/\",\"title\":\"Git简介\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"简介\",\"slug\":\"简介\",\"link\":\"#简介\",\"children\":[]},{\"level\":2,\"title\":\"Git 与 SVN 区别\",\"slug\":\"git-与-svn-区别\",\"link\":\"#git-与-svn-区别\",\"children\":[]},{\"level\":2,\"title\":\"Git 与 SVN 区别点：\",\"slug\":\"git-与-svn-区别点\",\"link\":\"#git-与-svn-区别点\",\"children\":[]}],\"git\":{},\"filePathRelative\":\"Git/readme.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
