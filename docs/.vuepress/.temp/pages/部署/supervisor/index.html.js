import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/部署/supervisor/index.html.vue"
const data = JSON.parse("{\"path\":\"/%E9%83%A8%E7%BD%B2/supervisor/\",\"title\":\"进程管理（supervisor）\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"查看所有子进程的状态\",\"slug\":\"查看所有子进程的状态\",\"link\":\"#查看所有子进程的状态\",\"children\":[]},{\"level\":2,\"title\":\"查看单个任务状态\",\"slug\":\"查看单个任务状态\",\"link\":\"#查看单个任务状态\",\"children\":[]},{\"level\":2,\"title\":\"关闭任务\",\"slug\":\"关闭任务\",\"link\":\"#关闭任务\",\"children\":[]},{\"level\":2,\"title\":\"关闭所有进程\",\"slug\":\"关闭所有进程\",\"link\":\"#关闭所有进程\",\"children\":[]},{\"level\":2,\"title\":\"启动任务\",\"slug\":\"启动任务\",\"link\":\"#启动任务\",\"children\":[]},{\"level\":2,\"title\":\"重启任务\",\"slug\":\"重启任务\",\"link\":\"#重启任务\",\"children\":[]}],\"git\":{\"updatedTime\":1616916137000,\"contributors\":[{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":1}]},\"filePathRelative\":\"部署/supervisor/readme.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
