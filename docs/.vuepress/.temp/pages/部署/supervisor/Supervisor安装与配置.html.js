import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/部署/supervisor/Supervisor安装与配置.html.vue"
const data = JSON.parse("{\"path\":\"/%E9%83%A8%E7%BD%B2/supervisor/Supervisor%E5%AE%89%E8%A3%85%E4%B8%8E%E9%85%8D%E7%BD%AE.html\",\"title\":\"Supervisor安装与配置\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"安装Python包管理工具 easy_install\",\"slug\":\"安装python包管理工具-easy-install\",\"link\":\"#安装python包管理工具-easy-install\",\"children\":[]},{\"level\":2,\"title\":\"安装Supervisor\",\"slug\":\"安装supervisor\",\"link\":\"#安装supervisor\",\"children\":[]},{\"level\":2,\"title\":\"配置Supervisor应用守护\",\"slug\":\"配置supervisor应用守护\",\"link\":\"#配置supervisor应用守护\",\"children\":[]},{\"level\":2,\"title\":\"配置Supervisor开机启动\",\"slug\":\"配置supervisor开机启动\",\"link\":\"#配置supervisor开机启动\",\"children\":[]},{\"level\":2,\"title\":\"常用的相关管理命令\",\"slug\":\"常用的相关管理命令\",\"link\":\"#常用的相关管理命令\",\"children\":[]}],\"git\":{\"updatedTime\":1616916137000,\"contributors\":[{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":1}]},\"filePathRelative\":\"部署/supervisor/Supervisor安装与配置.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
