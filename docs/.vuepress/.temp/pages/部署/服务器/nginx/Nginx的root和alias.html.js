export const data = {
  "key": "v-53ad3f8a",
  "path": "/%E9%83%A8%E7%BD%B2/%E6%9C%8D%E5%8A%A1%E5%99%A8/nginx/Nginx%E7%9A%84root%E5%92%8Calias.html",
  "title": "Nginx root和alias",
  "lang": "zh-CN",
  "frontmatter": {},
  "excerpt": "",
  "headers": [
    {
      "level": 2,
      "title": "介绍",
      "slug": "介绍",
      "children": []
    },
    {
      "level": 2,
      "title": "举例说明",
      "slug": "举例说明",
      "children": []
    },
    {
      "level": 2,
      "title": "案例",
      "slug": "案例",
      "children": []
    }
  ],
  "git": {
    "updatedTime": 1616221814000,
    "contributors": [
      {
        "name": "apple",
        "email": "lixuebiao1115@126.com",
        "commits": 1
      }
    ]
  },
  "filePathRelative": "部署/服务器/nginx/Nginx的root和alias.md"
}
