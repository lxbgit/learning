import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/部署/携程Apollo统一配置中心/docker中安装使用.html.vue"
const data = JSON.parse("{\"path\":\"/%E9%83%A8%E7%BD%B2/%E6%90%BA%E7%A8%8BApollo%E7%BB%9F%E4%B8%80%E9%85%8D%E7%BD%AE%E4%B8%AD%E5%BF%83/docker%E4%B8%AD%E5%AE%89%E8%A3%85%E4%BD%BF%E7%94%A8.html\",\"title\":\"docker 中使用apollo\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"安装 apollo-configservice\",\"slug\":\"安装-apollo-configservice\",\"link\":\"#安装-apollo-configservice\",\"children\":[]},{\"level\":2,\"title\":\"安装 apollo-adminservice\",\"slug\":\"安装-apollo-adminservice\",\"link\":\"#安装-apollo-adminservice\",\"children\":[]},{\"level\":2,\"title\":\"安装 apollo-portal\",\"slug\":\"安装-apollo-portal\",\"link\":\"#安装-apollo-portal\",\"children\":[]},{\"level\":2,\"title\":\"注意点1\",\"slug\":\"注意点1\",\"link\":\"#注意点1\",\"children\":[]},{\"level\":2,\"title\":\"安装过程中遇到的问题\",\"slug\":\"安装过程中遇到的问题\",\"link\":\"#安装过程中遇到的问题\",\"children\":[]}],\"git\":{\"updatedTime\":1616492473000,\"contributors\":[{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":1}]},\"filePathRelative\":\"部署/携程Apollo统一配置中心/docker中安装使用.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
