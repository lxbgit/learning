import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/Linux/index.html.vue"
const data = JSON.parse("{\"path\":\"/Linux/\",\"title\":\"linux下常用的工具\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"ls命令\",\"slug\":\"ls命令\",\"link\":\"#ls命令\",\"children\":[]},{\"level\":2,\"title\":\"linux 下一些命令1\",\"slug\":\"linux-下一些命令1\",\"link\":\"#linux-下一些命令1\",\"children\":[]},{\"level\":2,\"title\":\"linux 常用命令2\",\"slug\":\"linux-常用命令2\",\"link\":\"#linux-常用命令2\",\"children\":[]},{\"level\":2,\"title\":\"Ubuntu\",\"slug\":\"ubuntu\",\"link\":\"#ubuntu\",\"children\":[]},{\"level\":2,\"title\":\"sdkman\",\"slug\":\"sdkman\",\"link\":\"#sdkman\",\"children\":[]},{\"level\":2,\"title\":\"linux下安装 node\",\"slug\":\"linux下安装-node\",\"link\":\"#linux下安装-node\",\"children\":[]}],\"git\":{\"updatedTime\":1670315592000,\"contributors\":[{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":5}]},\"filePathRelative\":\"Linux/readme.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
