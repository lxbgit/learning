import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/Linux/文件解压.html.vue"
const data = JSON.parse("{\"path\":\"/Linux/%E6%96%87%E4%BB%B6%E8%A7%A3%E5%8E%8B.html\",\"title\":\"linux下文件压缩或解压\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"zip命令解析\",\"slug\":\"zip命令解析\",\"link\":\"#zip命令解析\",\"children\":[{\"level\":3,\"title\":\"zip 例子\",\"slug\":\"zip-例子\",\"link\":\"#zip-例子\",\"children\":[]}]},{\"level\":2,\"title\":\"tar 命令解析\",\"slug\":\"tar-命令解析\",\"link\":\"#tar-命令解析\",\"children\":[{\"level\":3,\"title\":\"独立的命令\",\"slug\":\"独立的命令\",\"link\":\"#独立的命令\",\"children\":[]},{\"level\":3,\"title\":\"下面的参数是根据需要在压缩或解压档案时可选的\",\"slug\":\"下面的参数是根据需要在压缩或解压档案时可选的\",\"link\":\"#下面的参数是根据需要在压缩或解压档案时可选的\",\"children\":[]},{\"level\":3,\"title\":\"下面的参数-f是必须的\",\"slug\":\"下面的参数-f是必须的\",\"link\":\"#下面的参数-f是必须的\",\"children\":[]},{\"level\":3,\"title\":\"tar 例子\",\"slug\":\"tar-例子\",\"link\":\"#tar-例子\",\"children\":[]}]},{\"level\":2,\"title\":\"解压总结\",\"slug\":\"解压总结\",\"link\":\"#解压总结\",\"children\":[]}],\"git\":{\"updatedTime\":1595209285000,\"contributors\":[{\"name\":\"xuebiaoli\",\"username\":\"xuebiaoli\",\"email\":\"lixuebiao1115@gmail.com\",\"commits\":3},{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":1}]},\"filePathRelative\":\"Linux/文件解压.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
