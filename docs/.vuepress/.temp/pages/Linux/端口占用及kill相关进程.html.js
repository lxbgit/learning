import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/Linux/端口占用及kill相关进程.html.vue"
const data = JSON.parse("{\"path\":\"/Linux/%E7%AB%AF%E5%8F%A3%E5%8D%A0%E7%94%A8%E5%8F%8Akill%E7%9B%B8%E5%85%B3%E8%BF%9B%E7%A8%8B.html\",\"title\":\"Linux端口占用及kill相关进程\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"方式一：lsof命令\",\"slug\":\"方式一-lsof命令\",\"link\":\"#方式一-lsof命令\",\"children\":[]},{\"level\":2,\"title\":\"方式二：netstat命令\",\"slug\":\"方式二-netstat命令\",\"link\":\"#方式二-netstat命令\",\"children\":[]},{\"level\":2,\"title\":\"补充：根据程序名查看对应的PID\",\"slug\":\"补充-根据程序名查看对应的pid\",\"link\":\"#补充-根据程序名查看对应的pid\",\"children\":[]},{\"level\":2,\"title\":\"netstat\",\"slug\":\"netstat\",\"link\":\"#netstat\",\"children\":[]},{\"level\":2,\"title\":\"其他\",\"slug\":\"其他\",\"link\":\"#其他\",\"children\":[]}],\"git\":{\"updatedTime\":1629250682000,\"contributors\":[{\"name\":\"xuebiaoli\",\"username\":\"xuebiaoli\",\"email\":\"lixuebiao1115@gmail.com\",\"commits\":1},{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":4}]},\"filePathRelative\":\"Linux/端口占用及kill相关进程.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
