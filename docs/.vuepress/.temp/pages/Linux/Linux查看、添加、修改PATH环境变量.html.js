import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/Linux/Linux查看、添加、修改PATH环境变量.html.vue"
const data = JSON.parse("{\"path\":\"/Linux/Linux%E6%9F%A5%E7%9C%8B%E3%80%81%E6%B7%BB%E5%8A%A0%E3%80%81%E4%BF%AE%E6%94%B9PATH%E7%8E%AF%E5%A2%83%E5%8F%98%E9%87%8F.html\",\"title\":\"Linux 查看、添加、修改PATH 环境变量\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"添加环境变量\",\"slug\":\"添加环境变量\",\"link\":\"#添加环境变量\",\"children\":[]},{\"level\":2,\"title\":\"环境变量改名\",\"slug\":\"环境变量改名\",\"link\":\"#环境变量改名\",\"children\":[]},{\"level\":2,\"title\":\"环境变量删除\",\"slug\":\"环境变量删除\",\"link\":\"#环境变量删除\",\"children\":[]},{\"level\":2,\"title\":\"例子\",\"slug\":\"例子\",\"link\":\"#例子\",\"children\":[]}],\"git\":{\"updatedTime\":1629252099000,\"contributors\":[{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":1}]},\"filePathRelative\":\"Linux/Linux查看、添加、修改PATH环境变量.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
