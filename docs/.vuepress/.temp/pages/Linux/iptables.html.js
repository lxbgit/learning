import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/Linux/iptables.html.vue"
const data = JSON.parse("{\"path\":\"/Linux/iptables.html\",\"title\":\"iptables\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"防止SYN攻击 轻量级预防\",\"slug\":\"防止syn攻击-轻量级预防\",\"link\":\"#防止syn攻击-轻量级预防\",\"children\":[]},{\"level\":2,\"title\":\"防止DOS太多连接进来,可以允许外网网卡每个IP最多15个初始连接,超过的丢弃\",\"slug\":\"防止dos太多连接进来-可以允许外网网卡每个ip最多15个初始连接-超过的丢弃\",\"link\":\"#防止dos太多连接进来-可以允许外网网卡每个ip最多15个初始连接-超过的丢弃\",\"children\":[]},{\"level\":2,\"title\":\"用Iptables抵御DDOS (参数与上相同)\",\"slug\":\"用iptables抵御ddos-参数与上相同\",\"link\":\"#用iptables抵御ddos-参数与上相同\",\"children\":[]}],\"git\":{\"updatedTime\":1628561373000,\"contributors\":[{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":3}]},\"filePathRelative\":\"Linux/iptables.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
