import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/Linux/ssh/index.html.vue"
const data = JSON.parse("{\"path\":\"/Linux/ssh/\",\"title\":\"常用SSH命令\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"连接远程服务器\",\"slug\":\"连接远程服务器\",\"link\":\"#连接远程服务器\",\"children\":[]},{\"level\":2,\"title\":\"从服务器下载整个目录\",\"slug\":\"从服务器下载整个目录\",\"link\":\"#从服务器下载整个目录\",\"children\":[]},{\"level\":2,\"title\":\"上传目录到服务器\",\"slug\":\"上传目录到服务器\",\"link\":\"#上传目录到服务器\",\"children\":[]},{\"level\":2,\"title\":\"上传本地文件到服务器\",\"slug\":\"上传本地文件到服务器\",\"link\":\"#上传本地文件到服务器\",\"children\":[]}],\"git\":{\"updatedTime\":1595209285000,\"contributors\":[{\"name\":\"xuebiaoli\",\"username\":\"xuebiaoli\",\"email\":\"lixuebiao1115@gmail.com\",\"commits\":2},{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":1}]},\"filePathRelative\":\"Linux/ssh/README.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
