import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/Linux/centos/index.html.vue"
const data = JSON.parse("{\"path\":\"/Linux/centos/\",\"title\":\"CentOS\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"CentOS命令\",\"slug\":\"centos命令\",\"link\":\"#centos命令\",\"children\":[]},{\"level\":2,\"title\":\"CentOS中防火墙的使用\",\"slug\":\"centos中防火墙的使用\",\"link\":\"#centos中防火墙的使用\",\"children\":[{\"level\":3,\"title\":\"问题\",\"slug\":\"问题\",\"link\":\"#问题\",\"children\":[]}]}],\"git\":{\"updatedTime\":1616492473000,\"contributors\":[{\"name\":\"xuebiaoli\",\"username\":\"xuebiaoli\",\"email\":\"lixuebiao1115@gmail.com\",\"commits\":1},{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":4}]},\"filePathRelative\":\"Linux/centos/readme.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
