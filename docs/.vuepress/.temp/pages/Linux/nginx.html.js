import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/Linux/nginx.html.vue"
const data = JSON.parse("{\"path\":\"/Linux/nginx.html\",\"title\":\"Nginx 在CentOS下安装\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"下载nginx\",\"slug\":\"下载nginx\",\"link\":\"#下载nginx\",\"children\":[]},{\"level\":2,\"title\":\"安装\",\"slug\":\"安装\",\"link\":\"#安装\",\"children\":[]},{\"level\":2,\"title\":\"配置\",\"slug\":\"配置\",\"link\":\"#配置\",\"children\":[]},{\"level\":2,\"title\":\"编译安装\",\"slug\":\"编译安装\",\"link\":\"#编译安装\",\"children\":[]},{\"level\":2,\"title\":\"查看安装\",\"slug\":\"查看安装\",\"link\":\"#查看安装\",\"children\":[]},{\"level\":2,\"title\":\"命令\",\"slug\":\"命令\",\"link\":\"#命令\",\"children\":[]},{\"level\":2,\"title\":\"查询nginx进程\",\"slug\":\"查询nginx进程\",\"link\":\"#查询nginx进程\",\"children\":[]},{\"level\":2,\"title\":\"重启 nginx\",\"slug\":\"重启-nginx\",\"link\":\"#重启-nginx\",\"children\":[]},{\"level\":2,\"title\":\"开机自启动\",\"slug\":\"开机自启动\",\"link\":\"#开机自启动\",\"children\":[]}],\"git\":{\"updatedTime\":1608102766000,\"contributors\":[{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":1}]},\"filePathRelative\":\"Linux/nginx.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
