import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/Linux/常用命令.html.vue"
const data = JSON.parse("{\"path\":\"/Linux/%E5%B8%B8%E7%94%A8%E5%91%BD%E4%BB%A4.html\",\"title\":\"linux下命令\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"find\",\"slug\":\"find\",\"link\":\"#find\",\"children\":[{\"level\":3,\"title\":\"基本格式\",\"slug\":\"基本格式\",\"link\":\"#基本格式\",\"children\":[]},{\"level\":3,\"title\":\"grep\",\"slug\":\"grep\",\"link\":\"#grep\",\"children\":[]}]},{\"level\":2,\"title\":\"vim常用命令.md\",\"slug\":\"vim常用命令-md\",\"link\":\"#vim常用命令-md\",\"children\":[{\"level\":3,\"title\":\"搜索\",\"slug\":\"搜索\",\"link\":\"#搜索\",\"children\":[]}]},{\"level\":2,\"title\":\"服务\",\"slug\":\"服务\",\"link\":\"#服务\",\"children\":[{\"level\":3,\"title\":\"启动一个服务\",\"slug\":\"启动一个服务\",\"link\":\"#启动一个服务\",\"children\":[]},{\"level\":3,\"title\":\"查看端口是否被占用\",\"slug\":\"查看端口是否被占用\",\"link\":\"#查看端口是否被占用\",\"children\":[]},{\"level\":3,\"title\":\"Ubuntu\",\"slug\":\"ubuntu\",\"link\":\"#ubuntu\",\"children\":[]}]}],\"git\":{\"updatedTime\":1670315493000,\"contributors\":[{\"name\":\"xuebiaoli\",\"username\":\"xuebiaoli\",\"email\":\"lixuebiao1115@gmail.com\",\"commits\":1},{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":3}]},\"filePathRelative\":\"Linux/常用命令.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
