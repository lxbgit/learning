import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/Linux/grep.html.vue"
const data = JSON.parse("{\"path\":\"/Linux/grep.html\",\"title\":\"grep 命令\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"基本格式：\",\"slug\":\"基本格式\",\"link\":\"#基本格式\",\"children\":[]},{\"level\":2,\"title\":\"命令功能：\",\"slug\":\"命令功能\",\"link\":\"#命令功能\",\"children\":[]},{\"level\":2,\"title\":\"命令参数：\",\"slug\":\"命令参数\",\"link\":\"#命令参数\",\"children\":[]},{\"level\":2,\"title\":\"规则表达式：\",\"slug\":\"规则表达式\",\"link\":\"#规则表达式\",\"children\":[{\"level\":3,\"title\":\"grep的规则表达式\",\"slug\":\"grep的规则表达式\",\"link\":\"#grep的规则表达式\",\"children\":[]},{\"level\":3,\"title\":\"POSIX字符\",\"slug\":\"posix字符\",\"link\":\"#posix字符\",\"children\":[]}]},{\"level\":2,\"title\":\"使用实例：\",\"slug\":\"使用实例\",\"link\":\"#使用实例\",\"children\":[]}],\"git\":{\"updatedTime\":1595209285000,\"contributors\":[{\"name\":\"xuebiaoli\",\"username\":\"xuebiaoli\",\"email\":\"lixuebiao1115@gmail.com\",\"commits\":1},{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":1}]},\"filePathRelative\":\"Linux/grep.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
