import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/templates/thymeleaf/语法.html.vue"
const data = JSON.parse("{\"path\":\"/templates/thymeleaf/%E8%AF%AD%E6%B3%95.html\",\"title\":\"thymeleaf 语法\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"thymeleaf tags\",\"slug\":\"thymeleaf-tags\",\"link\":\"#thymeleaf-tags\",\"children\":[]},{\"level\":2,\"title\":\"thymeleaf 日期格式化\",\"slug\":\"thymeleaf-日期格式化\",\"link\":\"#thymeleaf-日期格式化\",\"children\":[]}],\"git\":{\"updatedTime\":1627358702000,\"contributors\":[{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":6}]},\"filePathRelative\":\"templates/thymeleaf/语法.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
