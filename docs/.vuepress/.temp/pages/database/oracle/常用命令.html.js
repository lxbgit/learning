import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/database/oracle/常用命令.html.vue"
const data = JSON.parse("{\"path\":\"/database/oracle/%E5%B8%B8%E7%94%A8%E5%91%BD%E4%BB%A4.html\",\"title\":\"常用命令\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"创建表空间\",\"slug\":\"创建表空间\",\"link\":\"#创建表空间\",\"children\":[]},{\"level\":2,\"title\":\"导入导出\",\"slug\":\"导入导出\",\"link\":\"#导入导出\",\"children\":[]}],\"git\":{\"updatedTime\":1595209285000,\"contributors\":[{\"name\":\"xuebiaoli\",\"username\":\"xuebiaoli\",\"email\":\"lixuebiao1115@gmail.com\",\"commits\":3},{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":1}]},\"filePathRelative\":\"database/oracle/常用命令.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
