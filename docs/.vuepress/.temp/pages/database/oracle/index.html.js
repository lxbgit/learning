import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/database/oracle/index.html.vue"
const data = JSON.parse("{\"path\":\"/database/oracle/\",\"title\":\"Oracle 问题\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"使用sys登录时\",\"slug\":\"使用sys登录时\",\"link\":\"#使用sys登录时\",\"children\":[]},{\"level\":2,\"title\":\"LRM-00109: could not open parameter file '/data/oracle/product/11.2.0/db_1/dbs/initorcl.ora'\",\"slug\":\"lrm-00109-could-not-open-parameter-file-data-oracle-product-11-2-0-db-1-dbs-initorcl-ora\",\"link\":\"#lrm-00109-could-not-open-parameter-file-data-oracle-product-11-2-0-db-1-dbs-initorcl-ora\",\"children\":[]},{\"level\":2,\"title\":\"ORA-01102: cannot mount database in EXCLUSIVE mode\",\"slug\":\"ora-01102-cannot-mount-database-in-exclusive-mode\",\"link\":\"#ora-01102-cannot-mount-database-in-exclusive-mode\",\"children\":[]},{\"level\":2,\"title\":\"ORA-00845: MEMORY_TARGET not supported on this system\",\"slug\":\"ora-00845-memory-target-not-supported-on-this-system\",\"link\":\"#ora-00845-memory-target-not-supported-on-this-system\",\"children\":[]},{\"level\":2,\"title\":\"ora-01950:对表空间XXX无权限\",\"slug\":\"ora-01950-对表空间xxx无权限\",\"link\":\"#ora-01950-对表空间xxx无权限\",\"children\":[]}],\"git\":{\"updatedTime\":1595209285000,\"contributors\":[{\"name\":\"xuebiaoli\",\"username\":\"xuebiaoli\",\"email\":\"lixuebiao1115@gmail.com\",\"commits\":1},{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":2}]},\"filePathRelative\":\"database/oracle/readme.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
