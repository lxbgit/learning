import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/database/mybatis/index.html.vue"
const data = JSON.parse("{\"path\":\"/database/mybatis/\",\"title\":\"Mybatis配置\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"mybatis别名\",\"slug\":\"mybatis别名\",\"link\":\"#mybatis别名\",\"children\":[]},{\"level\":2,\"title\":\"类型处理器(TypeHandlers)\",\"slug\":\"类型处理器-typehandlers\",\"link\":\"#类型处理器-typehandlers\",\"children\":[]},{\"level\":2,\"title\":\"mybatis注解\",\"slug\":\"mybatis注解\",\"link\":\"#mybatis注解\",\"children\":[]},{\"level\":2,\"title\":\"设置(settings)\",\"slug\":\"设置-settings\",\"link\":\"#设置-settings\",\"children\":[{\"level\":3,\"title\":\"一个配置例子\",\"slug\":\"一个配置例子\",\"link\":\"#一个配置例子\",\"children\":[]}]}],\"git\":{\"updatedTime\":1595409532000,\"contributors\":[{\"name\":\"xuebiaoli\",\"username\":\"xuebiaoli\",\"email\":\"lixuebiao1115@gmail.com\",\"commits\":1},{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":2}]},\"filePathRelative\":\"database/mybatis/readme.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
