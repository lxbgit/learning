import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/database/mysql/centos安装mysql8.html.vue"
const data = JSON.parse("{\"path\":\"/database/mysql/centos%E5%AE%89%E8%A3%85mysql8.html\",\"title\":\"Centos7 安装MySQL8\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"添加MySQL8的本地源\",\"slug\":\"添加mysql8的本地源\",\"link\":\"#添加mysql8的本地源\",\"children\":[]},{\"level\":2,\"title\":\"安装MySQL服务器\",\"slug\":\"安装mysql服务器\",\"link\":\"#安装mysql服务器\",\"children\":[]},{\"level\":2,\"title\":\"启动MySQL\",\"slug\":\"启动mysql\",\"link\":\"#启动mysql\",\"children\":[]},{\"level\":2,\"title\":\"MySQL ROOT账号权限配置和远程链接\",\"slug\":\"mysql-root账号权限配置和远程链接\",\"link\":\"#mysql-root账号权限配置和远程链接\",\"children\":[{\"level\":3,\"title\":\"登陆MySQL本地Shell客户端，并修改root初始密码\",\"slug\":\"登陆mysql本地shell客户端-并修改root初始密码\",\"link\":\"#登陆mysql本地shell客户端-并修改root初始密码\",\"children\":[]},{\"level\":3,\"title\":\"修改root账号远程访问权限\",\"slug\":\"修改root账号远程访问权限\",\"link\":\"#修改root账号远程访问权限\",\"children\":[]},{\"level\":3,\"title\":\"需要注意的几个问题\",\"slug\":\"需要注意的几个问题\",\"link\":\"#需要注意的几个问题\",\"children\":[]},{\"level\":3,\"title\":\"引用\",\"slug\":\"引用\",\"link\":\"#引用\",\"children\":[]}]}],\"git\":{\"updatedTime\":1616481666000,\"contributors\":[{\"name\":\"xuebiaoli\",\"username\":\"xuebiaoli\",\"email\":\"lixuebiao1115@gmail.com\",\"commits\":1},{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":3}]},\"filePathRelative\":\"database/mysql/centos安装mysql8.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
