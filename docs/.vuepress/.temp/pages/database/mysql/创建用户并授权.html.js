import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/database/mysql/创建用户并授权.html.vue"
const data = JSON.parse("{\"path\":\"/database/mysql/%E5%88%9B%E5%BB%BA%E7%94%A8%E6%88%B7%E5%B9%B6%E6%8E%88%E6%9D%83.html\",\"title\":\"mysql8.x 创建用户并授权\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"创建用户\",\"slug\":\"创建用户\",\"link\":\"#创建用户\",\"children\":[{\"level\":3,\"title\":\"命令\",\"slug\":\"命令\",\"link\":\"#命令\",\"children\":[]},{\"level\":3,\"title\":\"说明\",\"slug\":\"说明\",\"link\":\"#说明\",\"children\":[]},{\"level\":3,\"title\":\"例子\",\"slug\":\"例子\",\"link\":\"#例子\",\"children\":[]},{\"level\":3,\"title\":\"注意\",\"slug\":\"注意\",\"link\":\"#注意\",\"children\":[]}]},{\"level\":2,\"title\":\"授权\",\"slug\":\"授权\",\"link\":\"#授权\",\"children\":[{\"level\":3,\"title\":\"命令\",\"slug\":\"命令-1\",\"link\":\"#命令-1\",\"children\":[]},{\"level\":3,\"title\":\"说明:\",\"slug\":\"说明-1\",\"link\":\"#说明-1\",\"children\":[]},{\"level\":3,\"title\":\"例子\",\"slug\":\"例子-1\",\"link\":\"#例子-1\",\"children\":[]},{\"level\":3,\"title\":\"注意:\",\"slug\":\"注意-1\",\"link\":\"#注意-1\",\"children\":[]}]},{\"level\":2,\"title\":\"设置与更改用户密码\",\"slug\":\"设置与更改用户密码\",\"link\":\"#设置与更改用户密码\",\"children\":[{\"level\":3,\"title\":\"命令:\",\"slug\":\"命令-2\",\"link\":\"#命令-2\",\"children\":[]},{\"level\":3,\"title\":\"如果是当前登陆用户:\",\"slug\":\"如果是当前登陆用户\",\"link\":\"#如果是当前登陆用户\",\"children\":[]},{\"level\":3,\"title\":\"例子\",\"slug\":\"例子-2\",\"link\":\"#例子-2\",\"children\":[]}]},{\"level\":2,\"title\":\"撤销用户权限\",\"slug\":\"撤销用户权限\",\"link\":\"#撤销用户权限\",\"children\":[{\"level\":3,\"title\":\"命令\",\"slug\":\"命令-3\",\"link\":\"#命令-3\",\"children\":[]},{\"level\":3,\"title\":\"说明\",\"slug\":\"说明-2\",\"link\":\"#说明-2\",\"children\":[]},{\"level\":3,\"title\":\"例子\",\"slug\":\"例子-3\",\"link\":\"#例子-3\",\"children\":[]},{\"level\":3,\"title\":\"注意\",\"slug\":\"注意-2\",\"link\":\"#注意-2\",\"children\":[]}]},{\"level\":2,\"title\":\"删除用户\",\"slug\":\"删除用户\",\"link\":\"#删除用户\",\"children\":[]},{\"level\":2,\"title\":\"注意\",\"slug\":\"注意-3\",\"link\":\"#注意-3\",\"children\":[]}],\"git\":{\"updatedTime\":1616481666000,\"contributors\":[{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":5}]},\"filePathRelative\":\"database/mysql/创建用户并授权.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
