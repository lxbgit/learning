import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/database/mysql/index.html.vue"
const data = JSON.parse("{\"path\":\"/database/mysql/\",\"title\":\"mysql\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"mysql 导入sql\",\"slug\":\"mysql-导入sql\",\"link\":\"#mysql-导入sql\",\"children\":[]},{\"level\":2,\"title\":\"v5.7 修改root默认密码\",\"slug\":\"v5-7-修改root默认密码\",\"link\":\"#v5-7-修改root默认密码\",\"children\":[]},{\"level\":2,\"title\":\"问题1\",\"slug\":\"问题1\",\"link\":\"#问题1\",\"children\":[]},{\"level\":2,\"title\":\"mysql 使用过程中问题2\",\"slug\":\"mysql-使用过程中问题2\",\"link\":\"#mysql-使用过程中问题2\",\"children\":[]},{\"level\":2,\"title\":\"表名忽略大小写\",\"slug\":\"表名忽略大小写\",\"link\":\"#表名忽略大小写\",\"children\":[]}],\"git\":{\"updatedTime\":1626423014000,\"contributors\":[{\"name\":\"xuebiaoli\",\"username\":\"xuebiaoli\",\"email\":\"lixuebiao1115@gmail.com\",\"commits\":2},{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":4}]},\"filePathRelative\":\"database/mysql/readme.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
