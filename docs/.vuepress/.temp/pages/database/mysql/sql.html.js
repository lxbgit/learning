import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/database/mysql/sql.html.vue"
const data = JSON.parse("{\"path\":\"/database/mysql/sql.html\",\"title\":\"mysql\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"数据库设计规范\",\"slug\":\"数据库设计规范\",\"link\":\"#数据库设计规范\",\"children\":[{\"level\":3,\"title\":\"库名\",\"slug\":\"库名\",\"link\":\"#库名\",\"children\":[]},{\"level\":3,\"title\":\"字段\",\"slug\":\"字段\",\"link\":\"#字段\",\"children\":[]}]}],\"git\":{\"updatedTime\":1627358702000,\"contributors\":[{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":6}]},\"filePathRelative\":\"database/mysql/sql.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
