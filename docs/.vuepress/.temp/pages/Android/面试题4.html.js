import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/Android/面试题4.html.vue"
const data = JSON.parse("{\"path\":\"/Android/%E9%9D%A2%E8%AF%95%E9%A2%984.html\",\"title\":\"Java 面试\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"类加载过程\",\"slug\":\"类加载过程\",\"link\":\"#类加载过程\",\"children\":[]},{\"level\":2,\"title\":\"类加载器大致分为3类：启动类加载器、扩展类加载器、应用程序类加载器\",\"slug\":\"类加载器大致分为3类-启动类加载器、扩展类加载器、应用程序类加载器\",\"link\":\"#类加载器大致分为3类-启动类加载器、扩展类加载器、应用程序类加载器\",\"children\":[]},{\"level\":2,\"title\":\"双亲委派模型\",\"slug\":\"双亲委派模型\",\"link\":\"#双亲委派模型\",\"children\":[{\"level\":3,\"title\":\"\\\"双亲委派模型\\\"有什么作用呢？\",\"slug\":\"双亲委派模型-有什么作用呢\",\"link\":\"#双亲委派模型-有什么作用呢\",\"children\":[]}]},{\"level\":2,\"title\":\"设计模式\",\"slug\":\"设计模式\",\"link\":\"#设计模式\",\"children\":[]}],\"git\":{\"updatedTime\":1679891169000,\"contributors\":[{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":5}]},\"filePathRelative\":\"Android/面试题4.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
