import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/Android/Handler.html.vue"
const data = JSON.parse("{\"path\":\"/Android/Handler.html\",\"title\":\"Handler\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"Looper 如何在子线程中创建\",\"slug\":\"looper-如何在子线程中创建\",\"link\":\"#looper-如何在子线程中创建\",\"children\":[]},{\"level\":2,\"title\":\"说一下 Looper、handler、线程间的关系。例如一个线程可以对应几个 Looper、几个Handler？\",\"slug\":\"说一下-looper、handler、线程间的关系。例如一个线程可以对应几个-looper、几个handler\",\"link\":\"#说一下-looper、handler、线程间的关系。例如一个线程可以对应几个-looper、几个handler\",\"children\":[]},{\"level\":2,\"title\":\"Handler#post(Runnable) 是如何执行的？\",\"slug\":\"handler-post-runnable-是如何执行的\",\"link\":\"#handler-post-runnable-是如何执行的\",\"children\":[]},{\"level\":2,\"title\":\"Looper 死循环为什么不阻塞主线程？ / Looper 死循环为什么不会 ANR?\",\"slug\":\"looper-死循环为什么不阻塞主线程-looper-死循环为什么不会-anr\",\"link\":\"#looper-死循环为什么不阻塞主线程-looper-死循环为什么不会-anr\",\"children\":[]}],\"git\":{\"updatedTime\":1645419115000,\"contributors\":[{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":2}]},\"filePathRelative\":\"Android/Handler.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
