import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/Android/sms.html.vue"
const data = JSON.parse("{\"path\":\"/Android/sms.html\",\"title\":\"SMS\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"SmsManager.sendTextMessage() 方法用于向指定的目的地地址发送文本短信。以下是该方法的参数说明：\",\"slug\":\"smsmanager-sendtextmessage-方法用于向指定的目的地地址发送文本短信。以下是该方法的参数说明\",\"link\":\"#smsmanager-sendtextmessage-方法用于向指定的目的地地址发送文本短信。以下是该方法的参数说明\",\"children\":[]},{\"level\":2,\"title\":\"PendingIntent.getBroadcast() 方法用于创建一个广播类型的 PendingIntent 对象，该对象用于接收广播事件。该方法的四个参数含义如下：\",\"slug\":\"pendingintent-getbroadcast-方法用于创建一个广播类型的-pendingintent-对象-该对象用于接收广播事件。该方法的四个参数含义如下\",\"link\":\"#pendingintent-getbroadcast-方法用于创建一个广播类型的-pendingintent-对象-该对象用于接收广播事件。该方法的四个参数含义如下\",\"children\":[]}],\"git\":{\"updatedTime\":1679890065000,\"contributors\":[{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":1}]},\"filePathRelative\":\"Android/sms.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
