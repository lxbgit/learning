import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/Android/ViewModel.html.vue"
const data = JSON.parse("{\"path\":\"/Android/ViewModel.html\",\"title\":\"ViewModel\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"ViewModel是什么？\",\"slug\":\"viewmodel是什么\",\"link\":\"#viewmodel是什么\",\"children\":[]},{\"level\":2,\"title\":\"ViewModel 为什么被设计出来，解决了什么问题？\",\"slug\":\"viewmodel-为什么被设计出来-解决了什么问题\",\"link\":\"#viewmodel-为什么被设计出来-解决了什么问题\",\"children\":[]},{\"level\":2,\"title\":\"说说ViewModel原理\",\"slug\":\"说说viewmodel原理\",\"link\":\"#说说viewmodel原理\",\"children\":[]}],\"git\":{\"updatedTime\":1645418345000,\"contributors\":[{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":1}]},\"filePathRelative\":\"Android/ViewModel.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
