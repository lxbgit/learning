export const data = {
  "key": "v-ba3c348e",
  "path": "/Android/",
  "title": "Android 学习笔记",
  "lang": "zh-CN",
  "frontmatter": {},
  "excerpt": "",
  "headers": [
    {
      "level": 2,
      "title": "Android 获取sha1",
      "slug": "android-获取sha1",
      "children": []
    },
    {
      "level": 2,
      "title": "Intent",
      "slug": "intent",
      "children": [
        {
          "level": 3,
          "title": "传值大小限值",
          "slug": "传值大小限值",
          "children": []
        }
      ]
    },
    {
      "level": 2,
      "title": "Activity一共有以下四种launchMode",
      "slug": "activity一共有以下四种launchmode",
      "children": []
    },
    {
      "level": 2,
      "title": "关于View的x, translationX, left 与 MotionEvent的x, rawX",
      "slug": "关于view的x-translationx-left-与-motionevent的x-rawx",
      "children": []
    },
    {
      "level": 2,
      "title": "本地通知",
      "slug": "本地通知",
      "children": []
    }
  ],
  "git": {
    "updatedTime": 1652506921000,
    "contributors": [
      {
        "name": "apple",
        "email": "lixuebiao1115@126.com",
        "commits": 5
      }
    ]
  },
  "filePathRelative": "Android/readme.md"
}
