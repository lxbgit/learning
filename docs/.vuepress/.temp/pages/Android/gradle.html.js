import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/Android/gradle.html.vue"
const data = JSON.parse("{\"path\":\"/Android/gradle.html\",\"title\":\"gradle 配置\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"使用gradle获取MD5、SHA1\",\"slug\":\"使用gradle获取md5、sha1\",\"link\":\"#使用gradle获取md5、sha1\",\"children\":[]},{\"level\":2,\"title\":\"修改打包apk文件名\",\"slug\":\"修改打包apk文件名\",\"link\":\"#修改打包apk文件名\",\"children\":[]}],\"git\":{\"updatedTime\":1679552589000,\"contributors\":[{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":1}]},\"filePathRelative\":\"Android/gradle.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
