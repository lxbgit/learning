import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/Android/存储变化.html.vue"
const data = JSON.parse("{\"path\":\"/Android/%E5%AD%98%E5%82%A8%E5%8F%98%E5%8C%96.html\",\"title\":\"Android 存储\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"APP专属路径\",\"slug\":\"app专属路径\",\"link\":\"#app专属路径\",\"children\":[{\"level\":3,\"title\":\"external storage 外部存储\",\"slug\":\"external-storage-外部存储\",\"link\":\"#external-storage-外部存储\",\"children\":[]},{\"level\":3,\"title\":\"internal storage 内部存储\",\"slug\":\"internal-storage-内部存储\",\"link\":\"#internal-storage-内部存储\",\"children\":[]}]},{\"level\":2,\"title\":\"手机共享路径\",\"slug\":\"手机共享路径\",\"link\":\"#手机共享路径\",\"children\":[]},{\"level\":2,\"title\":\"Downloads文件夹\",\"slug\":\"downloads文件夹\",\"link\":\"#downloads文件夹\",\"children\":[]}],\"git\":{\"updatedTime\":1679283950000,\"contributors\":[{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":5}]},\"filePathRelative\":\"Android/存储变化.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
