import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/Android/网络请求.html.vue"
const data = JSON.parse("{\"path\":\"/Android/%E7%BD%91%E7%BB%9C%E8%AF%B7%E6%B1%82.html\",\"title\":\"网络请求\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"OKhttp\",\"slug\":\"okhttp\",\"link\":\"#okhttp\",\"children\":[]},{\"level\":2,\"title\":\"Retrofit\",\"slug\":\"retrofit\",\"link\":\"#retrofit\",\"children\":[{\"level\":3,\"title\":\"涉及到的开发模式\",\"slug\":\"涉及到的开发模式\",\"link\":\"#涉及到的开发模式\",\"children\":[]},{\"level\":3,\"title\":\"代理\",\"slug\":\"代理\",\"link\":\"#代理\",\"children\":[]},{\"level\":3,\"title\":\"callAdapterFactory / callFactory\",\"slug\":\"calladapterfactory-callfactory\",\"link\":\"#calladapterfactory-callfactory\",\"children\":[]},{\"level\":3,\"title\":\"ConverterFactory\",\"slug\":\"converterfactory\",\"link\":\"#converterfactory\",\"children\":[]}]}],\"git\":{\"updatedTime\":1679890065000,\"contributors\":[{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":3}]},\"filePathRelative\":\"Android/网络请求.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
