import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/Android/面试题3.html.vue"
const data = JSON.parse("{\"path\":\"/Android/%E9%9D%A2%E8%AF%95%E9%A2%983.html\",\"title\":\"面试3\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"Android 获取sha1\",\"slug\":\"android-获取sha1\",\"link\":\"#android-获取sha1\",\"children\":[]},{\"level\":2,\"title\":\"Intent\",\"slug\":\"intent\",\"link\":\"#intent\",\"children\":[{\"level\":3,\"title\":\"Intent传值大小限值\",\"slug\":\"intent传值大小限值\",\"link\":\"#intent传值大小限值\",\"children\":[]}]},{\"level\":2,\"title\":\"关于View的x, translationX, left 与 MotionEvent的x, rawX\",\"slug\":\"关于view的x-translationx-left-与-motionevent的x-rawx\",\"link\":\"#关于view的x-translationx-left-与-motionevent的x-rawx\",\"children\":[]},{\"level\":2,\"title\":\"本地通知\",\"slug\":\"本地通知\",\"link\":\"#本地通知\",\"children\":[{\"level\":3,\"title\":\"支付相关\",\"slug\":\"支付相关\",\"link\":\"#支付相关\",\"children\":[]}]}],\"git\":{\"updatedTime\":1679895725000,\"contributors\":[{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":8}]},\"filePathRelative\":\"Android/面试题3.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
