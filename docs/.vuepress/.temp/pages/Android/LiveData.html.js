import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/Android/LiveData.html.vue"
const data = JSON.parse("{\"path\":\"/Android/LiveData.html\",\"title\":\"LiveData\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"LiveData原理\",\"slug\":\"livedata原理\",\"link\":\"#livedata原理\",\"children\":[]}],\"git\":{\"updatedTime\":1645418345000,\"contributors\":[{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":1}]},\"filePathRelative\":\"Android/LiveData.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
