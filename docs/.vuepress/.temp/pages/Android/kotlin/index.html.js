import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/Android/kotlin/index.html.vue"
const data = JSON.parse("{\"path\":\"/Android/kotlin/\",\"title\":\"Android\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"kotlin 跟Java相比，有哪些你觉得比较好用的功能\",\"slug\":\"kotlin-跟java相比-有哪些你觉得比较好用的功能\",\"link\":\"#kotlin-跟java相比-有哪些你觉得比较好用的功能\",\"children\":[]}],\"git\":{\"updatedTime\":1652507694000,\"contributors\":[{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":3}]},\"filePathRelative\":\"Android/kotlin/readme.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
