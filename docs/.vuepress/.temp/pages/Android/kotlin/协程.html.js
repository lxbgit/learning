import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/Android/kotlin/协程.html.vue"
const data = JSON.parse("{\"path\":\"/Android/kotlin/%E5%8D%8F%E7%A8%8B.html\",\"title\":\"协程\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"协程是啥？\",\"slug\":\"协程是啥\",\"link\":\"#协程是啥\",\"children\":[]},{\"level\":2,\"title\":\"CoroutineScope\",\"slug\":\"coroutinescope\",\"link\":\"#coroutinescope\",\"children\":[]},{\"level\":2,\"title\":\"协程的目的是\",\"slug\":\"协程的目的是\",\"link\":\"#协程的目的是\",\"children\":[]}],\"git\":{\"updatedTime\":1645496228000,\"contributors\":[{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":2}]},\"filePathRelative\":\"Android/kotlin/协程.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
