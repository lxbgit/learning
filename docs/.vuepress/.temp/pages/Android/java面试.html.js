export const data = {
  "key": "v-115139ae",
  "path": "/Android/java%E9%9D%A2%E8%AF%95.html",
  "title": "Java 面试",
  "lang": "zh-CN",
  "frontmatter": {},
  "excerpt": "",
  "headers": [
    {
      "level": 2,
      "title": "类加载过程",
      "slug": "类加载过程",
      "children": []
    },
    {
      "level": 2,
      "title": "类加载器大致分为3类：启动类加载器、扩展类加载器、应用程序类加载器。",
      "slug": "类加载器大致分为3类-启动类加载器、扩展类加载器、应用程序类加载器。",
      "children": []
    },
    {
      "level": 2,
      "title": "双亲委派模型",
      "slug": "双亲委派模型",
      "children": [
        {
          "level": 3,
          "title": "\"双亲委派模型\"有什么作用呢？",
          "slug": "双亲委派模型-有什么作用呢",
          "children": []
        }
      ]
    },
    {
      "level": 2,
      "title": "设计模式",
      "slug": "设计模式",
      "children": []
    }
  ],
  "git": {
    "updatedTime": 1652507694000,
    "contributors": [
      {
        "name": "apple",
        "email": "lixuebiao1115@126.com",
        "commits": 3
      }
    ]
  },
  "filePathRelative": "Android/java面试.md"
}
