import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/Android/view.html.vue"
const data = JSON.parse("{\"path\":\"/Android/view.html\",\"title\":\"View\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"TabLayout问题\",\"slug\":\"tablayout问题\",\"link\":\"#tablayout问题\",\"children\":[]},{\"level\":2,\"title\":\"GridLayout属性介绍\",\"slug\":\"gridlayout属性介绍\",\"link\":\"#gridlayout属性介绍\",\"children\":[{\"level\":3,\"title\":\"本身属性\",\"slug\":\"本身属性\",\"link\":\"#本身属性\",\"children\":[]}]},{\"level\":2,\"title\":\"子元素属性\",\"slug\":\"子元素属性\",\"link\":\"#子元素属性\",\"children\":[]},{\"level\":2,\"title\":\"RecyclerViewDemo\",\"slug\":\"recyclerviewdemo\",\"link\":\"#recyclerviewdemo\",\"children\":[]},{\"level\":2,\"title\":\"AMap定位\",\"slug\":\"amap定位\",\"link\":\"#amap定位\",\"children\":[]}],\"git\":{\"updatedTime\":1608522231000,\"contributors\":[{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":3}]},\"filePathRelative\":\"Android/view.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
