import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/Android/屏幕适配.html.vue"
const data = JSON.parse("{\"path\":\"/Android/%E5%B1%8F%E5%B9%95%E9%80%82%E9%85%8D.html\",\"title\":\"Android 屏幕适配\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"针对不同的密度创建备用可绘制位图资源\",\"slug\":\"针对不同的密度创建备用可绘制位图资源\",\"link\":\"#针对不同的密度创建备用可绘制位图资源\",\"children\":[]},{\"level\":2,\"title\":\"方式\",\"slug\":\"方式\",\"link\":\"#方式\",\"children\":[]},{\"level\":2,\"title\":\"在compose中使用\",\"slug\":\"在compose中使用\",\"link\":\"#在compose中使用\",\"children\":[]}],\"git\":{\"updatedTime\":1679552589000,\"contributors\":[{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":3}]},\"filePathRelative\":\"Android/屏幕适配.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
