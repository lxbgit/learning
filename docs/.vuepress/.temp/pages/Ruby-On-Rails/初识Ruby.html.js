import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/Ruby-On-Rails/初识Ruby.html.vue"
const data = JSON.parse("{\"path\":\"/Ruby-On-Rails/%E5%88%9D%E8%AF%86Ruby.html\",\"title\":\"\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"ruby简介\",\"slug\":\"ruby简介\",\"link\":\"#ruby简介\",\"children\":[{\"level\":3,\"title\":\"Ruby判断真值时\",\"slug\":\"ruby判断真值时\",\"link\":\"#ruby判断真值时\",\"children\":[]},{\"level\":3,\"title\":\"Ruby 方法\",\"slug\":\"ruby-方法\",\"link\":\"#ruby-方法\",\"children\":[]},{\"level\":3,\"title\":\"Ruby 中的==和equals\",\"slug\":\"ruby-中的-和equals\",\"link\":\"#ruby-中的-和equals\",\"children\":[]},{\"level\":3,\"title\":\"Ruby 中定义类\",\"slug\":\"ruby-中定义类\",\"link\":\"#ruby-中定义类\",\"children\":[]},{\"level\":3,\"title\":\"new 方法创建对象\",\"slug\":\"new-方法创建对象\",\"link\":\"#new-方法创建对象\",\"children\":[]},{\"level\":3,\"title\":\"自定义方法来创建 Ruby 对象\",\"slug\":\"自定义方法来创建-ruby-对象\",\"link\":\"#自定义方法来创建-ruby-对象\",\"children\":[]},{\"level\":3,\"title\":\"self\",\"slug\":\"self\",\"link\":\"#self\",\"children\":[]},{\"level\":3,\"title\":\"类的继承\",\"slug\":\"类的继承\",\"link\":\"#类的继承\",\"children\":[]},{\"level\":3,\"title\":\"Ruby 模块（Module）\",\"slug\":\"ruby-模块-module\",\"link\":\"#ruby-模块-module\",\"children\":[]},{\"level\":3,\"title\":\"Ruby 迭代器\",\"slug\":\"ruby-迭代器\",\"link\":\"#ruby-迭代器\",\"children\":[]},{\"level\":3,\"title\":\"Ruby 块\",\"slug\":\"ruby-块\",\"link\":\"#ruby-块\",\"children\":[]}]}],\"git\":{\"updatedTime\":1595209285000,\"contributors\":[{\"name\":\"dreamerlxb\",\"username\":\"dreamerlxb\",\"email\":\"1558485399@qq.com\",\"commits\":1},{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":1}]},\"filePathRelative\":\"Ruby-On-Rails/初识Ruby.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
