import comp from "/Users/apple/Downloads/work/projects/learning/docs/.vuepress/.temp/pages/Ruby-On-Rails/ruby-on-rails(三)常用命令总结.html.vue"
const data = JSON.parse("{\"path\":\"/Ruby-On-Rails/ruby-on-rails(%E4%B8%89)%E5%B8%B8%E7%94%A8%E5%91%BD%E4%BB%A4%E6%80%BB%E7%BB%93.html\",\"title\":\"\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":3,\"title\":\"目录结构\",\"slug\":\"目录结构\",\"link\":\"#目录结构\",\"children\":[]},{\"level\":2,\"title\":\"应用目录(app/)\",\"slug\":\"应用目录-app\",\"link\":\"#应用目录-app\",\"children\":[]},{\"level\":2,\"title\":\"配置文件目录(config/)\",\"slug\":\"配置文件目录-config\",\"link\":\"#配置文件目录-config\",\"children\":[]},{\"level\":2,\"title\":\"数据库存储目录(db/)\",\"slug\":\"数据库存储目录-db\",\"link\":\"#数据库存储目录-db\",\"children\":[]},{\"level\":2,\"title\":\"共享类或模块文件(lib/)\",\"slug\":\"共享类或模块文件-lib\",\"link\":\"#共享类或模块文件-lib\",\"children\":[]},{\"level\":2,\"title\":\"公共文件目录(public/)\",\"slug\":\"公共文件目录-public\",\"link\":\"#公共文件目录-public\",\"children\":[]},{\"level\":2,\"title\":\"测试文件目录(test/)\",\"slug\":\"测试文件目录-test\",\"link\":\"#测试文件目录-test\",\"children\":[]},{\"level\":2,\"title\":\"临时文件目录(tmp/)\",\"slug\":\"临时文件目录-tmp\",\"link\":\"#临时文件目录-tmp\",\"children\":[]},{\"level\":2,\"title\":\"第三方插件目录(vendor/)\",\"slug\":\"第三方插件目录-vendor\",\"link\":\"#第三方插件目录-vendor\",\"children\":[]}],\"git\":{\"updatedTime\":1595209285000,\"contributors\":[{\"name\":\"dreamerlxb\",\"username\":\"dreamerlxb\",\"email\":\"1558485399@qq.com\",\"commits\":1},{\"name\":\"apple\",\"username\":\"apple\",\"email\":\"lixuebiao1115@126.com\",\"commits\":1}]},\"filePathRelative\":\"Ruby-On-Rails/ruby-on-rails(三)常用命令总结.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
