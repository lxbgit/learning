export const siteData = JSON.parse("{\"base\":\"/learning/\",\"lang\":\"zh-CN\",\"title\":\"学习笔记\",\"description\":\"学习笔记，文档记录，收集学习当中遇到的问题\",\"head\":[],\"locales\":{}}")

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updateSiteData) {
    __VUE_HMR_RUNTIME__.updateSiteData(siteData)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ siteData }) => {
    __VUE_HMR_RUNTIME__.updateSiteData(siteData)
  })
}
