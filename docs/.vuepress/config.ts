import { viteBundler } from '@vuepress/bundler-vite'
import { defaultTheme } from '@vuepress/theme-default'
import { defineUserConfig } from 'vuepress'


export default defineUserConfig({
    bundler: viteBundler(),
//    theme: defaultTheme(),
    title: '学习笔记',
    description: '学习笔记，文档记录，收集学习当中遇到的问题',
    lang: 'zh-CN',
    base: '/learning/',
    theme: defaultTheme({
        navbar: [
            {text: '首页', link: '/'},
            {text: 'Spring', link: '/Spring/'},
            {text: 'Android', link: '/Android/'},
            {
                text: '其他',
                children: [
                    {text: 'Mac', link: '/Mac/'},
                    {text: 'Linux', link: '/Linux/'},
                    {text: 'git', link: '/git/'},
                    // { text: 'Redis', link: '/redis/' },
                    {text: 'maven', link: '/build-tools/maven/'}
                ]
            },
            {text: 'VuePress', link: 'https://v2.vuepress.vuejs.org/zh/guide/'},
        ],
        sidebar: {
            '/Linux/': [
                {
                    text: '搜索',
                    link: '/Linux/grep'
                },
                {
                    text: '常用命令',
                    link: '/Linux/常用命令.md'
                },
                {
                    text: 'SSH',
                    link: '/Linux/ssh/'
                },
                {
                    text: '文件解压',
                    link: '/Linux/文件解压'
                },
                {
                    text: 'netstat命令',
                    link: '/Linux/linux查看外网访问IP.md'
                },
                {
                    text: 'lsof命令',
                    link: '/Linux/端口占用及kill相关进程.md'
                },
                {
                    text: 'linux查看磁盘空间命令',
                    link: '/Linux/linux查看磁盘空间命令.md'
                },
                {
                    text: 'nginx在CentOS7中的操作',
                    link: '/Linux/nginx'
                }
            ],
            '/Android/': [
                {
                    text: '介绍',
                    link: '/Android/'
                },
                {
                    text: '试图',
                    link: '/Android/view'
                }
            ],
            '/Spring/': [
                {
                    text: 'Spring Framework',
                    link: '/Spring/'
                },
                {
                    text: 'Spring Security',
                    link: '/Spring/security/'
                },
                {
                    text: 'Spring Boot',
                    link: '/Spring/boot/'
                },
            ],
            '/': [
                {
                    text: '首页',   // 必要的
                    link: '/',      // 可选的, 标题的跳转链接，应为绝对路径且必须存在
                    // collapsable: false, // 可选的, 默认值是 true,
                    // sidebarDepth: 1,    // 可选的, 默认值是 1
                    children: [
                        {
                            text: 'mybatis',
                            link: '/database/mybatis/'
                        },
                        {
                            text: 'mysql',
                            link: '/database/mysql/',
                            children: [
                                {
                                    text: 'Sql',
                                    link: '/database/mysql/sql',
                                },
                                {
                                    text: '创建用户',
                                    link: '/database/mysql/创建用户并授权',
                                },
                                {
                                    text: 'mysqldump 命令',
                                    link: '/database/mysql/Mysqldump参数',
                                }
                            ]
                        }
                    ]
                },
                {
                    text: 'Javascript',
                    // path: '/javascript/',
                    children: [
                        {
                            text: 'npm',
                            link: '/javascript/npm'
                        },
                        {
                            text: 'ckeditor',
                            link: '/javascript/ckeditor/ckeditor4'
                        }
                    ]
                },
                {
                    text: '模板',
                    children: [
                        {
                            text: 'thymeleaf',
                            children: [
                                {
                                    text: '内联-inline',
                                    link: '/templates/thymeleaf/thymeleaf_inline'
                                },
                                {
                                    text: '片段-fragment',
                                    link: '/templates/thymeleaf/thymeleaf_fragment'
                                },
                                {
                                    text: '语法',
                                    link: '/templates/thymeleaf/语法'
                                }
                            ]
                        },
                        {
                            text: 'freemarker',
                            link: '/templates/freemarker/'
                        }
                    ]
                }
            ]
        },
        lastUpdated: true, // string | boolean
    })
})
