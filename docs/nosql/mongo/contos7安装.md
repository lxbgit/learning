# CentOS 安装Mongodb

下载[地址](https://www.mongodb.com/try/download)

安装[参考1](https://www.runoob.com/mongodb/mongodb-linux-install.html)

安装[参考2](https://docs.mongodb.com/guides/server/install/)

## 开启服务

```shell
 /usr/local/mongodb5/bin/mongod --dbpath /var/lib/mongo --logpath /var/log/mongodb/mongod.log --fork --config /etc/mongod.conf --bind_ip=0.0.0.0
```

### 创建数据库目录

默认情况下 MongoDB 启动后会初始化以下两个目录：

1. 数据存储目录：/var/lib/mongodb
2. 日志文件目录：/var/log/mongodb

我们在启动前可以先创建这两个目录并设置当前用户有读写权限：

```shell
sudo mkdir -p /var/lib/mongo
sudo mkdir -p /var/log/mongodb
sudo chown `whoami` /var/lib/mongo     # 设置权限
sudo chown `whoami` /var/log/mongodb   # 设置权限
```

接下来启动 Mongodb 服务

```shell
mongod --dbpath /var/lib/mongo --logpath /var/log/mongodb/mongod.log --fork
```

打开 /var/log/mongodb/mongod.log 文件看到以下信息，说明启动成功。

```shell
# tail -10f /var/log/mongodb/mongod.log
2020-07-09T12:20:17.391+0800 I  NETWORK  [listener] Listening on /tmp/mongodb-27017.sock
2020-07-09T12:20:17.392+0800 I  NETWORK  [listener] Listening on 127.0.0.1
2020-07-09T12:20:17.392+0800 I  NETWORK  [listener] waiting for connections on port 27017
```

## 停止服务

```shell
./mongod --shutdown --dbpath /var/lib/mongo
```

也可以在 mongo 的命令出口中实现：

```shell
> use admin
switched to db admin
> db.shutdownServer()
```
