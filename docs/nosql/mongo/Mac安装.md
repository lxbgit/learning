# mongodb 安装

## 安装

- `sudo brew install mongodb`

## 更新

- `brew upgrade mongodb`

## 成功

```shell
==> mongodb
To have launchd start mongodb now and restart at login:
  brew services start mongodb
Or, if you don't want/need a background service you can just run:
  mongod --config /usr/local/etc/mongod.conf
```

## 开启服务

`brew services start mongodb`

or

`mongod --config /usr/local/etc/mongod.conf`
