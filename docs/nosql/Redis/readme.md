# Redis

[官网](https://redis.io/)  [参考](http://www.redis.cn/)

- [安装](./Redis在Mac下安装.md)

- [常用命令](./常用命令.md)

- [下载](http://download.redis.io/releases/)
使用`wget`下载

```shell
wget http://download.redis.io/releases/redis-5.0.9.tar.gz
```

## Mac 下安装 Redis

[redis官网](https://redis.io/download)

安装：

```shell
wget http://download.redis.io/releases/redis-4.0.2.tar.gz
tar xzf redis-4.0.2.tar.gz
cd redis-4.0.2
make
```

测试是否安装成功：

```shell
make test
```

使用：

```shell
src/redis-server  # 运行redis默认服务
```

另外打开一个terminal，测试存储信息:

```shell
src/redis-cli
redis> set foo bar
OK
redis> get foo
"bar"
```

## Linux下安装

[文档](./linux下安装.md)

## 配置文件解析

[文档](./配置文件.md)

## 命令

[文档](./常用命令.md)
