# [redis官网](https://redis.io/download)

[下载](http://download.redis.io/releases/)
使用`wget`下载

```shell
wget http://download.redis.io/releases/redis-5.0.9.tar.gz
```

## 安装

- 执行

```shell
wget http://download.redis.io/releases/redis-4.0.2.tar.gz
tar xzf redis-4.0.2.tar.gz
cd redis-4.0.2
make
```

- 测试是否安装成功：

```shell
make test
```

- 错误

> linux下安装时，当执行到`make test`可能会有以下错误

```shell
cd src && make test
make[1]: 进入目录“/home/downloads/redis-5.0.9/src”
    CC Makefile.dep
make[1]: 离开目录“/home/downloads/redis-5.0.9/src”
make[1]: 进入目录“/home/downloads/redis-5.0.9/src”
You need tcl 8.5 or newer in order to run the Redis test
make[1]: *** [test] 错误 1
make[1]: 离开目录“/home/downloads/redis-5.0.9/src”
make: *** [test] 错误 2
```

解决办法如下

```shell
wget http://downloads.sourceforge.net/tcl/tcl8.6.1-src.tar.gz  
sudo tar xzvf tcl8.6.1-src.tar.gz  -C /usr/local/  
cd  /usr/local/tcl8.6.1/unix/  
sudo ./configure  
sudo make  
sudo make install 
```

[参考](https://blog.csdn.net/zhangshu123321/article/details/51440106)

## 使用

- 方法1

```shell
src/redis-server  # 运行redis默认服务
```

另外打开一个terminal，测试存储信息:

```shell
src/redis-cli
redis> set foo bar
OK
redis> get foo
"bar"
```

> 上述方法 redis启动成功，但是这种启动方式需要一直打开窗口，不能进行其他操作，不太方便

- 方法 - 修改redis.conf文件

> 将 `daemonize no` 修改为 `daemonize yes`,(/var/run/redis.pid)

- 指定`redis.conf`文件启动

```shell
./redis-server /usr/local/redis-4.0.6/redis.conf
```

- 关闭redis进程
  1. 首先使用`ps -aux | grep redis`查看redis进程
  2. 使用`kill`命令杀死进程

## 设置Redis 开机自启

[参考文档1](https://www.cnblogs.com/zuidongfeng/p/8032505.html)
[参考文档2](https://blog.csdn.net/zc474235918/article/details/50974483)

当出现问题`/var/redis/run/redis_6379.pid exists, process is already running or crashed`
时，参考 [问题](https://blog.csdn.net/luozhonghua2014/article/details/54649295)
