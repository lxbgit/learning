# 时间API

## `java.util.Date`

## `java.time`

- `LocalDate` -> `long`

```java
// LocalDate -> long
LocalDate value = LocalDate.now();
long v = value.atStartOfDay(ZoneId.systemDefault()).toInstant().toEpochMilli();

```

- `long` -> `LocalDate`

```java
// long -> LocalDate 
long value = 0L;
LocalDate v = LocalDateTime.ofInstant(Instant.ofEpochMilli(value), ZoneId.systemDefault()).toLocalDate();
```

- `LocalDateTime` -> `long`

```java
// LocalDateTime -> Long
LocalDateTime value = LocalDateTime.now();
long v = value.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();

```

- `long` -> `LocalDateTime`

```java
long value = 0L;
Instant instant = Instant.ofEpochMilli(value);
LocalDateTime v = LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
```
