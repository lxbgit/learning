# java 执行文件

```shell
  java [-options] class [args...]
  java [-options] -jar jarfile [args...]
  options：jvm参数 -D开头，使用System.getProperty()获取
  args：程序参数，main方法args获取
```
