# JDK SPI

SPI 全称为 Service Provider Interface，是一种服务发现机制。SPI 的本质是将接口实现类的全限定名配置在文件中，并由服务加载器读取配置文件，加载实现类。这样可以在运行时，动态为接口替换实现类。正因此特性，我们可以很容易的通过 SPI 机制为我们的程序提供拓展功能。

引入了 SPI 机制后，服务接口与服务实现就会达成分离的状态，可以实现 解耦以及程序可扩展机制ß

 例如： `main/resources/META-INF/services/java.sql.Driver`，文件内容为接口实现类
服务调用方通过`ServiceLoader.load`加载服务接口的实现类实例
服务提供方实现服务接口后， 在自己Jar包的`META-INF/services`目录下新建一个接口名全名的文件， 并将具体实现类全名写入

```java

public static void main(String[] args) {
        ServiceLoader<Driver> serviceLoader = ServiceLoader.load(Driver.class);
        for (Driver driver: serviceLoader){
            System.out.println(driver.getName());
        }
    }
```
