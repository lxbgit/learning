# 首页介绍

## 学习笔记

[Android](./Android/readme.md)
[Spring](./Spring/readme.md)
[Git](./git/readme.md)
...

## 特殊符号

> 大于 gt
> 小于 lt
> 大于等于 gte
> 小于等于 lte

## 参考

- [正则表达式](https://juejin.im/post/5edd89936fb9a047970688a8)
- [正则表达式](https://any86.github.io/any-rule/)

- [mysql](https://www.cnblogs.com/virde/p/centos-mysql8-install.html)

- [freemarker](http://freemarker.foofun.cn/)

- [thymeleaf](https://www.thymeleaf.org/)

- [thymeleaf-github](https://github.com/thymeleaf/thymeleaf)

- [Spring](https://spring.io/)

- [mybatis](https://mybatis.org/mybatis-3/zh/index.html)

- [mybatis-github](https://github.com/mybatis)

- [极光产品问题](https://docs.jiguang.cn/jverification/FAQ/prod_faq)

-[中国移动文档](https://dev.10086.cn/docInside?contentId=10000067541479)

- 短信删除
    - [google](https://android-developers.googleblog.com/2013/10/getting-your-sms-apps-ready-for-kitkat.html)
    - [参考文档](https://blog.csdn.net/dodod2012/article/details/78327509)

