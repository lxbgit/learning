# docker 中使用mysql

## 拉取mysql

```shell
# 拉去mysql 5.7版本数据库
docker pull mysql:5.7   # 拉取 mysql:指定版本 | 不加拉取最新
```

## 启动

```shell
docker run -itd --name goMysql -p 3306:3306 -e MYSQL_ROOT_PASSWORD=123456 mysql
# -itd
# -i: 以交互模式运行容器，通常与 -t 同时使用
# -t: 为容器重新分配一个伪输入终端，通常与 -i 同时使用
# -d: 后台运行容器，并返回容器ID
# -name 容器名
# 3306:3306 主机端口：容器端口
# MYSQL_ROOT_PASSWORD=mysql root密码
# mysql 镜像
```

## 启动mysql demo

```shell script
docker run -itd --name mysql-test --network testnet --network-alias mysql-test -p 3306:3306 -e MYSQL_ROOT_PASSWORD=12345678 mysql
```

## 进入容器bash

```shell
docker exec -it goMysql bash
# -i: 交互式操作
# -t: 终端
```

## 启动mysql

### 目录结构

docker
———mysql57
———mysql8x
———mysql
——————conf
—————————my.cnf
——————data

#### my.cnf添加如下内容

```shell
[mysqld]
log_timestamps=SYSTEM
user=mysql
character-set-server=utf8
default_authentication_plugin=mysql_native_password
lower_case_table_names=1
[client]
default-character-set=utf8
[mysql]
default-character-set=utf8
```

#### 解释

log_timestamps=SYSTEM：日志记录使用UTC时区，需要修改成系统时区
character-set-server=utf8：设置字符集为utf8
default_authentication_plugin：mysql8.0后的默认认证插件是caching_sha2_password，会导致我们用Navicat连接不上的问题，避免麻烦直接改成以前的认证方式
lower_case_table_names=1：
因为windows默认是1，linux默认是0，Mac OS X默认是 2，开发中很容易因为本地数据库和线上数据库配置不统一，而导致出问题（windows和mac不能设置为0，最多改成1和2）
下面比较的意思是指：在代码里sql用到表名的地方，用大写表名能否查出数据库里的小写表名。
0：创建的时候区分大小写，查询比较的时候也区分大小写
1：代表创建表是一律小写的，但比较的时候不区分大小写（输入大写表名也能查到）
2：代表创建表区分大小写，但比较的时候是小写的（应该是不区分的意思，mac系统没用过也不懂，以后估计也用得少就不管它了）。

### 5.7

```shell
docker run -d -p 3506:3306 --name mysql57 --privileged=true -v /docker/mysql/conf:/etc/mysql/conf.d -v /docker/mysql/data57:/var/lib/mysql  -v /etc/localtime:/etc/localtime -e MYSQL_ROOT_PASSWORD=123456 mysql:5.7.25 
```

### 8.x

```shell
docker run -d -p 3806:3306 --name mysql8x --privileged=true -v /docker/mysql/conf:/etc/mysql/conf.d -v /docker/mysql/logs:/logs -v /docker/mysql/data8x:/var/lib/mysql -v /etc/localtime:/etc/localtime -e MYSQL_ROOT_PASSWORD=123456 mysql:latest

```

### 参数解释

-p 3806:3306：把容器内的3306端口映射到本机的3806端口，我们远程连接的时候也是连3806

–privileged=true：大约在0.6版，privileged被引入docker。
使用该参数，container内的root拥有真正的root权限。否则，container内的root只是外部的一个普通用户权限。privileged启动的容器，可以看到很多host上的设备，并且可以执行mount。甚至允许你在docker容器中启动docker容器。

-v /docker/mysql/conf/my.cnf:/etc/my.cnf：映射配置文件

-v /docker/mysql/data:/var/lib/mysql：映射数据目录

-v /etc/localtime:/etc/localtime：映射linux时间文件（为了让容器时间和主机时间同步）

设置环境变量参数说明，这里我为了方便只设置了root密码：
-e MYSQL_USER=“woniu” ：添加woniu用户
-e MYSQL_PASSWORD=“123456”：设置添加的用户密码
-e MYSQL_ROOT_PASSWORD=“123456”：设置root用户密码

## 注意点1

- 当在docker中创建多个mysql容器时， 需要注意 容器名不能相同，端口号需要做映射，例如如下：

```shell
# mysql 1, 容器名为 mysql-test, 端口映射到 3306
# 外部访问时使用 ip:3306
# 容器内部访问 mysql-test:13306
docker run -itd --name mysql-test --network testnet --network-alias mysql-test -p 3306:3306 -e MYSQL_ROOT_PASSWORD=12345678 mysql

# mysql 2, 容器名为 mysql-test-2, 端口映射到 13306
# 外部访问时使用 ip:13306
# 容器内部访问 mysql-test:13306
docker run -itd --name mysql-test-2 --network testnet --network-alias mysql-test -p 13306:3306 -e MYSQL_ROOT_PASSWORD=12345678 mysql:5.7
```

## 注意点2

> Mysql5.7 only_full_group_by 默认开启，会导致 group by 严格效验， 很不好使，用以下方法关闭。

```shell
# 先进入 mysql 的container 
docker exec -it mysql /bin/bash

# 找到配置文件
/etc/mysql/mysql.conf.d/mysqld.cnf
```

在文件最后加上：

```shell
sql_mode=STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION
```

重启容器
