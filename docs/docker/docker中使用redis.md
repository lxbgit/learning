# docker 中使用redis

## 搜索镜像

```shell
docker search redis
```

## 下载

```shell
docker pull redis:lastest
```

## 查看

```shell
docker images
```

## 启用daemon

```shell
docker run --name kris-redis -p 6380:6379 -d redis --requirepass "123456"
```

## 自定义redis.conf启动

```shell
docker run -p 6379:6379 --name kris-redis -v /root/docker/redis/redis.conf:/etc/redis/redis.conf  -v /root/docker/redis/data:/data -d redis redis-server /etc/redis/redis.conf --appendonly yes

# -p 6379:6379:把容器内的6379端口映射到宿主机6379端口
# -v /root/docker/redis/redis.conf:/etc/redis/redis.conf：把宿主机配置好的redis.conf放到容器内的这个位置中
# -v /root/docker/redis/data:/data：把redis持久化的数据在宿主机内显示，做数据备份
# redis-server /etc/redis/redis.conf：这个是关键配置，让redis不是无配置启动，而是按照这个redis.conf的配置启动
# -appendonly yes：redis启动后数据持久化
```

## 查看运行状态

```shell
docker ps -a
```

## 启用redis-cli，即redis客户端

```shell
docker exec -it kris-redis redis-cli
```
