# Docker Dockerfile

> Dockerfile 是一个用来构建镜像的文本文件，文本内容包含了一条条构建镜像所需的指令和说明

## docker 部署 SpringBoot jar

```dockerfile
# FROM,表示使用JDK8环境为基础镜像，如果镜像不是本地会从DockerHub进行下载
FROM openjdk:8
# 作者
MAINTAINER lxb <lixuebiao1115@126.com>

# 定义匿名数据卷。在启动容器时忘记挂载数据卷，会自动挂载到匿名卷。
VOLUME /tmp
# ADD,拷贝文件并且重命名
ADD weike-config/doc/jars/weike-admin-2.4.4.jar app.jar

RUN bash -c 'touch /app.jar'
# ENTRYPOINT,为了缩短Tomcat启动时间，添加java.security.egd的系统属性指向/dev/urandom作为ENTRYPOINT
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]

```

## 根据dockerfile 创建镜像

```shell
docker build -f Dockerfile -t  [res]:[tag]  .

# 其中 -f 后面是参数引用的文件 -t 是生成的镜像  . 表示当前目录

# res代表镜像  tag代表标记  注意：不要忘记了 “ . ”
```

## 运行镜像

- 简单方式

```shell
docker run -p port:port -d [res]:[tag]
```

- 指定网络

```shell
docker run --name weike-mgt --network testnet -p 8083:8083 weike-mgt:v2.4.4

# --name : 指定容器名
# --network : 运行容器连接到testnet网络
# 可以使用 --network-alias mysql-test 指定网络别名，在同一个网络下，可以使用 [别名:port] 访问
# 
# -p 端口映射

```

> -d 表示后台运行 -p 端口

## 查看镜像是否运行

```shell
docker ps
```
