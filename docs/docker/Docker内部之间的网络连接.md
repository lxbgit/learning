# Docker 内部之间的网络连接

一、简介
　　内部网络连接的2中方式： Docker NetWorking （1.9版本之后推荐使用这个）和 Docker link（1.9 版本之前都使用这个）

　　推荐使用docker networking 的原因：

　　1.1 Docker networking 可以将容器连接到不同的宿主机器上。

　　1.2 通过 docker networking 连接的容器可以在无需更新连接的情况下，对停止、启动或者重启容器。而使用docker link 可能需要修改一些配置，或者重启相应的容器来维护docker之间的连接。

　　1.3 使用 docker networking ,不必事先创建容器再去创建它，同样，也不要关注容器的运行顺序。

　　这里主要讲述使用docker networking连接内部网路

二、创建docker网络

 1. 创建网络 neto01 网络名称

```shell
docker network create net01
```

 2.查看网络

```shell
docker network inspect net01
```

三、在docker 容器中创建nginx

```shell
#使用我自己创建的docker镜像 --net 添加到net01的网络
docker run --name container1 -itd -p 8086:80 --net=net01 --name nginx02 centos_nginx
docker run --name container2 -itd -p 8087:80 --net=net01 --name nginx03 centos_nginx
```

## demo

```shell
# 创建一个网络，可以通过ifconfig查看
docker network create tms
# 容器启动指定网络
docker run --network tms --network-alias test1 nginx
# network指定网络，network-alias指定容器的别名

# 断开容器的网络连接
docker network disconnet tms 容器ID/name
# 删除网络
docker network rm tms

# 测试
ping test1 # 可以直接使用别名代替ip
```

## Docker 创建bridge 网络

> 容器间互访

- 启动redis

```shell script
docker run -itd --name redis-test --network testnet --network-alias redis-test -p 6379:6379 redis
```

- 启动mysql

```shell script
docker run -itd --name mysql-test --network testnet --network-alias mysql-test -p 3306:3306 -e MYSQL_ROOT_PASSWORD=12345678 mysql
```

- 启动项目

```shell script
docker run --network testnet -p 8083:8083 weike-mgt:v2.4.4
```

- 运行容器连接到testnet网络

使用方法：

```shell script
docker run -it --name <容器名> ---network <bridge> --network-alias <网络别名> <镜像名>
```
