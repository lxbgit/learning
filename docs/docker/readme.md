# docker

## 查看镜像

`docker images`  或者 `docker image ls`

## 搜索镜像

```shell
docker search <镜像名称>

# 例子
docker search xuxueli/xxl-job-admin
```

[搜索](https://hub.docker.com/)
[例子](https://hub.docker.com/r/xuxueli/xxl-job-admin/tags?page=1&ordering=last_updated)

## 下载镜像

```shell
docker pull <镜像名称>
# 例子
docker pull xuxueli/xxl-job-admin:2.3.0
```

## 查看所有的容器

`docker ps -a`

## 启动容器

- 参数
  - -i: 交互式操作。
  - -t: 终端。

- 例子

```shell
docker run -it ubuntu /bin/bash
```

## 启动一个已停止的容器

```shell
docker start b750bbbcfd88 
```

## 停止一个容器

```shell
docker stop <容器 ID>
```

## 停止的容器可以通过 docker restart 重启

```shell
docker restart <容器 ID>
```

## 创建bridge网络

### 查看Docker 网络

```shell
docker network ls
```

### 新建网络

```shell
docker network create -d bridge test-net
```

> 容器间互访

- 启动redis

```shell script
docker run -itd --name redis-test --network testnet --network-alias redis-test -p 6379:6379 redis
```

- 启动mysql

```shell script
docker run -itd --name mysql-test --network testnet --network-alias mysql-test -p 3306:3306 -e MYSQL_ROOT_PASSWORD=12345678 mysql
```

- 启动项目

```shell script
docker run --network testnet -p 8083:8083 weike-mgt:v2.4.4
```

- 运行容器连接到testnet网络

使用方法：

```shell
docker run -it --name <容器名> ---network <bridge> --network-alias <网络别名> <镜像名>
```
