# 安装 docker-compose

[文档](https://docs.docker.com/compose/install/)

## 安装

[参考](https://docs.docker.com/compose/install/#install-compose-on-linux-systems)

## 命令

- 查看版本信息

```shell
docker-compose --version
```

## 升级

[参考](https://docs.docker.com/compose/install/#upgrading)

## 卸载

[参考](https://docs.docker.com/compose/install/#uninstallation)
