# CentOS7 下安装docker

[参考](https://www.runoob.com/docker/centos-docker-install.html)

[官网](https://docs.docker.com/engine/install/centos/)

## 安装

- 方法一

[rpm安装](https://docs.docker.com/engine/install/centos/#install-from-a-package)

```shell
curl -fsSL https://get.docker.com | bash -s docker --mirror aliyun
```

- 方法二(使用存储库安装)

  - 设置存储库

    ```shell
    sudo yum install -y yum-utils
    sudo yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo
    ```

  - 安装

    ```shell
    sudo yum install docker-ce docker-ce-cli containerd.io
    ```

## 启动 Docker

```shell
sudo systemctl start docker
```