# docker 中使用nginx

## 使用docker 下载nginx 镜像

`docker pull nginx`

## 启动nginx

```shell
docker run --name nginx -p 80:80 -d nginx
```

这样就简单的把nginx启动了，但是我们想要改变配置文件nginx.conf ，进入容器,命令：

```shell
docker exec -it nginx bash
```

`nginx.conf`配置文件在 `/etc/nginx/`  下面，但是你使用`vim nginx.conf` 或者`vi nginx.conf`

会发现vi或者vim命令没有用，解决办法：`apt-get  update`  完成之后 `apt-get install vim`

此时你就可以自己定制`nginx.conf`文件了，改好配置文件之后重启容器，步骤，先把容器停了

`docker stop nginx`  然后重启 `docker start nginx`

## 这样不是很方便，还有第二种方式，挂载配置文件

> 就是把装有docker宿主机上面的`nginx.conf`配置文件映射到启动的nginx容器里面，这需要你首先准备好`nginx.conf`配置文件,如果你应经准备好了，下一步是启动nginx

命令：

```shell
docker run --name nginx -p 80:80 -v /home/docker-nginx/nginx.conf:/etc/nginx/nginx.conf -v /home/docker-nginx/log:/var/log/nginx -v /home/docker-nginx/conf.d/default.conf:/etc/nginx/conf.d/default.conf -d nginx
```

解释下上面的命令：

`--name`  给启动的容器起个名字，以后可以使用这个名字启动或者停止容器

`-p` 映射端口，将docker宿主机的80端口和容器的80端口进行绑定

`-v` 挂载文件用的，第一个`-v` 表示将你本地的`nginx.conf`覆盖你要起启动的容器的`nginx.conf`文件，第二个表示将日志文件进行挂载，就是把nginx服务器的日志写到你docker宿主机的`/home/docker-nginx/log/`下面

第三个`-v` 表示的和第一个`-v`意思一样的。

`-d` 表示启动的是哪个镜像

## Nginx之location中反向代理proxy_pass配置

> 项目中要使用nginx作为前端服务的负载均衡，在进行nginx配置过程中，对于在负载均衡配置反向代理proxy_pass参数，proxy_pass配置url参数，在url后面有加"/"或者没有加"/"，在经过反向代理后，代理路径结果是不一样的。示例说明：

假如前端访问地址：`http://172.16.10.110/nginx/index.html`

在location配置如下：

- 加`/`

```nginx
location ^~/nginx/{
      proxy_pass:http://172.16.10.120:8080/;
}
```

经过nginx反向代理后，后台实际访问的服务路径是 `http://172.16.10.120:8080/index.html`，也就是说在有加"/"的情况下，代理路径是不会把location匹配的部分路径加到代理路径后面。

- 不加`/`

```nginx
location ^~/nginx/{
      proxy_pass:http://172.16.10.120:8080;
}
```

经过nginx反向代理后，后台实际访问的服务路径是 `http://172.16.10.120:8080/nginx/index.html`，也就是说在没有加"/"的情况下，代理路径会把location匹配的部分路径加到代理路径后面。
