# docker network

> 容器与容器之间的通信都是通过网络中的IP地址来完成的，这种方式显然是不合理的，因为这个IP地址可能会在启动容器时发生变化，而且也比较难记。
> 那么解决这一问题的方法就是使用网络别名，容器在网络是是允许有别名的，且这个别名在所在网络中都可以直接访问，这就类似局域网在各物理机的主机名。

## 启动容器指定网络及网络别名

```shell
docker run -d -it --name c4 --network test-net --network-alias c4-alias centos /bin/bash -c "while true; do echo hello; sleep 1; done"
e51b0ccd566709595c136d3ec41a72652843880184052324e5702bbdc82e0c22
```

说明：使用`--network`来指定网络，使用`--network-alias`来指定网络别名，也就是说`c4这个容器`在`test-net`这个网络中的别名是`c4-alias`，在这个网络中的其他容器可以通过这个别名来访问该容器。

> 网络别名在整个网络中都是有效的，即在同网络中的任意容器都可通过网络别名访问对应容器。

## docker network 命令

- `docker network connect`  将容器连接到网络

- `docker network create`  创建一个网络

- `docker network ls`  列出网络

## docker network connect命令

> 将某个容器连接到一个docker网络

- 使用

```shell
docker network connect [OPTIONS] NETWORK CONTAINER
```

- 选项

`--alias` : 为容器添加网络范围的别名
`--ip` : 指定IP地址
`--ip6` : 指定IPv6地址
`--link` : 添加链接到另一个容器
`--link-local-ip` : 添加容器的链接本地地址

demo:

```shell
# 将运行中的容器ctn1 连接到 网络net1
docker network connect net1 ctn1

# 在启动容器ctn1时，连接到网络net1上
docker run -itd --network=net1 ctn1

# 运行container6容器，并给container6添加一个scoped-app的别名
docker run --net=mynet -itd --name=container6 --net-alias app busybox
docker network connect --alias scoped-app local_alias container6
```
