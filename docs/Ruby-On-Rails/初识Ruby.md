## ruby简介
Ruby 是一种开源的面向对象程序设计的服务器端脚本语言
- ruby安装（mac）
~~~
$ brew install ruby
$ ruby -v    # 查看ruby版本
~~~
我们可以在终端中测试ruby
~~~shell
#打开终端，输入irb
$ irb 
 > puts(1+2)
3
 => nil 
> # 可以在这里做一些简单的ruby测试，建议还是创建.rb文件，便于代码保存
~~~

#### Ruby 数据类型
> Ruby支持的数据类型包括基本的Number、String、Ranges、Symbols，以及true、false和nil这几个特殊值，同时还有两种重要的数据结构——Array和Hash。

- Array: 数组字面量通过[]中以逗号分隔定义，且支持range定义。
（1）数组通过[]索引访问
（2）通过赋值操作插入、删除、替换元素
（3）通过+，－号进行合并和删除元素，且集合做为新集合出现
（4）通过<<号向原数据追加元素
（5）通过*号重复数组元素
（6）通过｜和&符号做并集和交集操作（注意顺序）
- Hash:  哈希是在大括号内放置一系列键/值对，键和值之间使用逗号和序列 => 分隔。尾部的逗号会被忽略。
-  Ranges: 一个范围表示一个区间，是通过设置一个开始值和一个结束值来表示。范围可使用 s..e 和 s...e 来构造，或者通过 Range.new 来构造。
使用 .. 构造的范围从开始值运行到结束值（包含结束值）。使用 ... 构造的范围从开始值运行到结束值（不包含结束值）。当作为一个迭代器使用时，范围会返回序列中的每个值。
范围 (1..5) 意味着它包含值 1, 2, 3, 4, 5，范围 (1...5) 意味着它包含值 1, 2, 3, 4 。

### Ruby判断真值时
> 只有 false 和 nil 当做假值。其余一切皆为真（包括 0、 0.0、 "" 和 []）

### Ruby 方法
~~~ruby
#方法定义
# 1
def func
end
# 2 
def func2(var1, var2)
end
# 3. 可变参数
def func3(*args)
end
# 方法调用
func1
func2(v1, v2) 
func2 v1, v2
~~~
> - 方法名有?通常意味着方法返回boolean型结果
- 方法名后有！，则此方法的接收者将被修改
- 方法名后加＝，通常意味着赋值
- 如果方法的最后一个参数值有&，Ruby假定这是一个Proc对象，Ruby会把该Proc 对象转换为一个block，然后在方法中使用。

### Ruby 中的==和equals
> Ruby 对 == 和 equals() 的处理方式与 Java 不一样。测试相等性使用 ==（Java 中是 equals()）。测试是否为同一对象使用 equals?()（Java 中是 ==）

### Ruby 中定义类
~~~ruby
# 定义
class Customer
    # 实例方法
   def getX
      @width * @height
   end
end
~~~
- 一般小写字母、下划线开头：变量（Variable）。
- $开头：全局变量（Global variable）。
- @开头：实例变量（Instance variable）。
- @@开头：类变量（Class variable）类变量被共享在整个继承链中
- 大写字母开头：常数（Constant）

### new 方法创建对象
对象是类的实例。现在您将学习如何在 Ruby 中创建类的对象。在 Ruby 中可以使用类方法 new 创建对象。
方法 new 是一种独特的方法，在 Ruby 库中预定义。new 方法属于类方法。
### 自定义方法来创建 Ruby 对象
还可以给方法 new 传递参数，这些参数可用于初始化类变量。
当您想要声明带参数的 new 方法时，您需要在创建类的同时声明方法 initialize。
initialize 方法是一种特殊类型的方法，将在调用带参数的类的 new 方法时执行。
下面的实例创建了 initialize 方法：
~~~
class Customer
   @@no_of_customers=0
   def initialize(id, name, addr)
      @cust_id=id
      @cust_name=name
      @cust_addr=addr
   end
end
~~~
### self
> 方法前的self在class内部，所以它表示类A，这样，x方法的调用者是类A自身（而不是它的实例），根据之前的原则，在def关键字内部，self表示的是这个方法的调用者，在这里就是类A自己

### 类的继承
~~~
class Customer < Parent # 使用 <表示继承关系
end
~~~
> 访问控制
Ruby 为您提供了三个级别的实例方法保护，分别是 public、private 或 protected。Ruby 不在实例和类变量上应用任何访问控制。
- Public 方法： Public 方法可被任意对象调用。默认情况下，方法都是 public 的，除了 initialize 方法总是 private 的。
- Private 方法： Private 方法不能从类外部访问或查看。只有类方法可以访问私有成员。
- Protected 方法： Protected 方法只能被类及其子类的对象调用。访问也只能在类及其子类内部进行。

### Ruby 模块（Module）
模块（Module）是一种把方法、类和常量组合在一起的方式。模块（Module）为您提供了两大好处。
- 模块提供了一个命名空间和避免名字冲突。
- 模块实现了 mixin 装置。

模块（Module）定义了一个命名空间，相当于一个沙盒，在里边您的方法和常量不会与其他地方的方法常量冲突。
模块类似与类，但有一下不同：
- 模块不能实例化
- 模块没有子类
- 模块只能被另一个模块定义

~~~
## 基本结构
module Identifier
   statement1
   statement2
   ...........
end
~~~

在类中嵌入模块使用 include 语句：
~~~
#demo 定义一个module
module Week
   FIRST_DAY = "Sunday"
   def Week.weeks_in_month
      puts "You have four weeks in a month"
   end
   def Week.weeks_in_year
      puts "You have 52 weeks in a year"
   end
end

#在类中引用上面定义的module
class Decade
   include Week # 使用include引用module
   def no_of_months
      puts Week::FIRST_DAY
      number=10*12
      puts number
   end
end

d1=Decade.new  # 创建一个对象
puts Week::FIRST_DAY # 调用模块中的常量
Week.weeks_in_month
Week.weeks_in_year
d1.no_of_months # ok
d1.weeks_in_month # ok
~~~
>  class是使用<作为继承的关键字，只支持单继承；module是使用include来做实例继承（实例化的时候动态功能插入），extend做类继承（可以理解为static继承）

|         | Class          | Module  |
| ------------- |:-------------:| -----:|
| 能否初始化     | 能 | 不能 |
| 用途     | 定义和新建对象      | namespace，mixin |
| 父类 | Module     |    Object |
| 继承 | 能继承与被继承     | 不能 |
| inclusion | 不能      | 可以用 include 在class和module里 |
| extension | 不能     |  可以用 extend 在class和module里 |
| 包含函数 | Class函数和实例函数 |  Module函数和实例函数 |

~~~ruby
# 使用extends
#在类中引用上面定义的module
class Decade
   extend Week # 使用extend引用module
   def no_of_months
      puts Week::FIRST_DAY
      number=10*12
      puts number
   end
end

## 调用
Decade.weeks_in_month
Decade.weeks_in_year
~~~
[参考](http://www.tuicool.com/articles/6Nzeeq6)

### Ruby 迭代器
> 迭代(iterate)指的是重复做相同的事，所以迭代器(iterator)就是用来重复多次相同的事。迭代器是集合支持的方法，在 Ruby 中，数组(Array)和哈希(Hash)可以称之为集合。

~~~
collection.each do |variable|
   code
end

# demo 遍历数组
ary = [1,2,3,4,5]
ary.each do |i|
   puts i
end
~~~

### Ruby 块
您已经知道 Ruby 如何定义方法以及您如何调用方法。类似地，Ruby 有一个块的概念。
- 块由大量的代码组成。
- 您需要给块取个名称。
- 块中的代码总是包含在大括号 {} 内。
- 块总是从与其具有相同名称的函数调用。这意味着如果您的块名称为 test，那么您要使用函数 test 来调用这个块。
- 您可以使用 yield 语句来调用块。

~~~
def test
   puts "在 test 方法内"
   yield
   puts "你又回到了 test 方法内"
   yield
end
test {puts "你在块内"}
~~~
> 关于block，这里不再详细介绍，下一节将详细分析Proc、lambda

> 以上为ruby的部分基本概念，详细介绍待续。。。
