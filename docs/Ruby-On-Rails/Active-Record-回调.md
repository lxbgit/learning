create:

```ruby
before_validation

after_validation

before_save

around_save

before_create

around_create

after_create

after_save

after_commit/after_rollback
```

- 条件回调

使用`:if` 或者 `:unless`选项,
例如:

```ruby
before_update :del_team_activities, if: Proc.new { |a| a.team_ids.present? }
```
