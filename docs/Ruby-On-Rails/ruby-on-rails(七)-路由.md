- member，添加成员路由，只需在 resource 块中添加 member 块：
~~~ruby
resources :nodes do
    member do
      post :block
    end
  end
~~~
> Rails 路由能够识别 /nodes/1/block 路径上的 POST 请求

- collection
~~~ruby
resource :uers, controller: 'uers' do
    collection do
      get :login
    end
 end
~~~
> Rails 路由能够识别 /uers/login 路径上的 GET 请求


- namespace 与 scope
`namespace`声明路由信息：
~~~
namespace :blog do
   resources :contexts
end
~~~
生成的路由信息如下：
~~~
 blog_contexts GET    /blog/contexts(.:format)          {:action=>"index", :controller=>"blog/contexts"}
                  POST   /blog/contexts(.:format)          {:action=>"create", :controller=>"blog/contexts"}
 new_blog_context GET    /blog/contexts/new(.:format)      {:action=>"new", :controller=>"blog/contexts"}
edit_blog_context GET    /blog/contexts/:id/edit(.:format) {:action=>"edit", :controller=>"blog/contexts"}
     blog_context GET    /blog/contexts/:id(.:format)      {:action=>"show", :controller=>"blog/contexts"}
                  PUT    /blog/contexts/:id(.:format)      {:action=>"update", :controller=>"blog/contexts"}
                  DELETE /blog/contexts/:id(.:format)      {:action=>"destroy", :controller=>"blog/contexts"}
~~~
而scope则如下：
~~~
scope :module => 'blog' do
   resources :contexts
end
~~~
生成的路由如下：
~~~
 contexts GET    /contexts(.:format)           {:action=>"index", :controller=>"blog/contexts"}
              POST   /contexts(.:format)           {:action=>"create", :controller=>"blog/contexts"}
  new_context GET    /contexts/new(.:format)       {:action=>"new", :controller=>"blog/contexts"}
 edit_context GET    /contexts/:id/edit(.:format)  {:action=>"edit", :controller=>"blog/contexts"}
      context GET    /contexts/:id(.:format)       {:action=>"show", :controller=>"blog/contexts"}
              PUT    /contexts/:id(.:format)       {:action=>"update", :controller=>"blog/contexts"}
              DELETE /contexts/:id(.:format)       {:action=>"destroy", :controller=>"blog/contexts"}
~~~
