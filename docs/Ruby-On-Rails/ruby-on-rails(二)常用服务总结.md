需要启动的服务：
- 启动redis服务（缓存服务器）
~~~shell
# 启动redis服务
# 默认端口号6379
redis-server /usr/local/etc/redis.conf
~~~
- memcached （缓存服务器）
~~~shell
# 默认端口号11211
memcached -d
~~~
- 启动ElasticSearch服务 （搜索服务）
~~~
# 默认端口号9200
~~~

- [social-share-button 社会化分享 ](https://github.com/huacnlee/social-share-button)
