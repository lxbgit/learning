使用`rails new appname`生成Rails应用后，我们可以通过`tree`命令来查看Rails应用的目录结构：
### 目录结构
应用程序目录下会有app、bin、config、db、lib、log、public、test、tmp和vendor等11个目录和config.ru、Gemfile、Gemfile.lock、Rakefile、README.md等5个文件。
目录在稍后会一一解释，先看一下app目录下的文件：
- config.ru 用来启动Rails程序的Rack设置文件
~~~
require ::File.expand_path('../config/environment', __FILE__)
run Myapps::Application
~~~
- Gemfile设置Rails程序所依赖的Gems (一旦用bundle install安装后，会生成Gemfile.lock)
~~~
source 'https://ruby.taobao.org/'
gem 'rails', '3.2.1'
gem 'sqlite3'
group :assets do
 gem 'sass-rails',  '~> 3.2.3'
 gem 'coffee-rails', '~> 3.2.1'
 gem 'uglifier', '>= 1.0.3'
end
gem 'jquery-rails'
gem ... ...
~~~
- Rakefile 用来载入可以被终端执行的Rake任务

---
下面是用tree命令查看，所显示的目录和文件结构：
~~~
.
├── app
│  ├── assets
│  │  ├── images
│  │  │  └── rails.png
│  │  ├── javascripts
│  │  │  └── application.js
│  │  └── stylesheets
│  │    └── application.css
│  ├── controllers
│  │  └── application_controller.rb
│  ├── helpers
│  │  └── application_helper.rb
│  ├── mailers
│  ├── models
│  └── views
│    └── layouts
│      └── application.html.erb
├── config
│  ├── application.rb
│  ├── boot.rb
│  ├── database.yml
│  ├── environment.rb
│  ├── environments
│  │  ├── development.rb
│  │  ├── production.rb
│  │  └── test.rb
│  ├── initializers
│  │  ├── backtrace_silencers.rb
│  │  ├── inflections.rb
│  │  ├── mime_types.rb
│  │  ├── secret_token.rb
│  │  ├── session_store.rb
│  │  └── wrap_parameters.rb
│  ├── locales
│  │  └── en.yml
│  └── routes.rb
├── config.ru
├── db
│  └── seeds.rb
├── doc
│  └── README_FOR_APP
├── Gemfile
├── lib
│  ├── assets
│  └── tasks
├── log
├── public
│  ├── 404.html
│  ├── 422.html
│  ├── 500.html
│  ├── favicon.ico
│  ├── index.html
│  └── robots.txt
├── Rakefile
├── README.rdoc
├── script
│  └── rails
├── test
│  ├── fixtures
│  ├── functional
│  ├── integration
│  ├── performance
│  │  └── browsing_test.rb
│  ├── test_helper.rb
│  └── unit
├── tmp
│  └── cache
│    └── assets
└── vendor
  ├── assets
  │  ├── javascripts
  │  └── stylesheets
  └── plugins
~~~

## 应用目录(app/)
app目录是Rails程序的主目录，不同子目录分别存放了模型 models (M)、控制器 controller (C)、视图 views (V)及mailers、helpers、channels、 jobs和assets等文档。
#### 模型－控制器－视图
分别存放模型、控制器和视图。其中，模型统一存放在app/models目录下，控制器统一存放在app/controllers目录下(可以使用目录进一步组织控制器，例如admin目录下用于存放管理后台相关的控制器)，视图存放在app/views目录下，视图模型存放在app/view/layouts目录下，默认为applicaiton.html.erb。
#### assets静态文件
assets静态文件存放在app/assets目录下，分别为app/assets/images、app/assets/stylesheets、app/assets/javascripts目录。
#### helpers
helpers是一些在视图中可以使用的小方法，用来产生较复杂的HTML。预设的Helper文件名称对应控制器，但不强制要求，在任意一个Helper文件中定义的方法，都可以在任何视图中使用。
......
## 配置文件目录(config/)
虽然Rails遵循“约定优于配置”的原则，但仍有一些需要设定的地方。在配置文件目录下，会存放应用程序设置文件application.rb、数据库设置文件database.yml、路由设置文件routes.rb、多重环境设置config/environments目录、其它初始设置文件config/initializers。
#### Rails启动应用程序设置
启动Rails程序(例如rails console或rails server)，会执行以下三个文档
boot.rb 载入Bundler环境，这个文件由Rails自动产生，不需要修改；
~~~
require 'rubygems'

# Set up gems listed in the Gemfile.
ENV['BUNDLE_GEMFILE'] ||= File.expand_path('../../Gemfile', __FILE__)

require 'bundler/setup' if File.exists?(ENV['BUNDLE_GEMFILE'])
~~~
application.rb 载入Rails gems和依赖的其它gems，接着设定Rails程序；
~~~
require File.expand_path('../boot', __FILE__)
require 'rails/all'

if defined?(Bundler)
 # If you precompile assets before deploying to production, use this line
 Bundler.require(*Rails.groups(:assets => %w(development test)))
 # If you want your assets lazily compiled in production, use this line
 # Bundler.require(:default, :assets, Rails.env)
end

module Myapps
 class Application < Rails::Application
  # Settings in config/environments/* take precedence over those specified here.
  # Application configuration should go into files in config/initializers
  # ... ...

  # Configure the default encoding used in templates for Ruby 1.9.
  config.encoding = "utf-8"

  # Configure sensitive parameters which will be filtered from the log file.
  config.filter_parameters += [:password]

  # ... ...

  # Enable the asset pipeline
  config.assets.enabled = true

  # Version of your assets, change this if you want to expire all your assets
  config.assets.version = '1.0'
 end
end
environment.rb 执行所有启动程序(initializers)，这个文件同样由Rails产生，不需要修改。
# Load the rails application
require File.expand_path('../application', __FILE__)

# Initialize the rails application
Myapps::Application.initialize!
~~~
#### 初始设置文件(initializers)
由environment.rb调用，系统默认的初始设置文件有backtrace_silencers.rb、inflections.rb、mime_types.rb、secret_token.rb、session_store.rb和wrap_parameters.rb等6个，分别对应的用途是：选择性移动异常追踪、单复数转换、mime_types、加密cookies信息的token、默认session储存以及参数封装等。
## 数据库存储目录(db/)
- migrate文件夹 
数据库迁移文件
- schema.rb 
- seeds.rb 初始化数据库

## 共享类或模块文件(lib/)
一些共享的类或模块可以存放在该目录。另外，Rake的任务，可存放在lib/tasks目录下。

##日志目录(log/)
- development.log 
开发环境下日志打印

## 公共文件目录(public/)
对于web服务器来说，可以直接访问的文件目录。可以用于存放通用的images、stylesheets和javascripts (Rails 2.x)。

## 测试文件目录(test/)
用于存放单元测试、功能测试及整合测试文件。
## 临时文件目录(tmp/)
存放一些临时文件
## 第三方插件目录(vendor/)
在使用bundler安装gems插件时，也可以选择安装在该目录下。
例如`bundle install --path vendor/bundle`
