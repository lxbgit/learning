yield
 是你指明您的内容将如何放在一个布局中。你可能会有这样的事情：
~~~
<div>
  <h1> This is the wrapper!</h1>
  <%= yield :my_content %
</div>
~~~

content_for你指定的内容将被渲染到内容区域。你可能会有这样的事情：
~~~
<% content_for :my_content do %>
  This is the content.
<% end %>
~~~

The result would be：
~~~
<div>
  <h1> This is the wrapper!</h1>
  This is the content.
</div>
~~~

他们在渲染过程的两端，与`yield`规定内容去哪里，和`content_for`指定实际的内容是什么。

最好的方法是将`yield`用在layout中，`content_for`用在view中；
`content_for`有另外的一种用法，当不传`block`给它时，它将返回之前呈现的内容；这主要用在helper中，`yeild`不能工作；
在view中，`content_for`最好用于呈现内容，`yield`最好用于调取内容。

-----

status 是enum类型
~~~erb
<%= f.input :status, collection: Post.statuses, label_method: :first, value_method: :first, as: :radio_buttons, class: 'with-gap' %>
~~~
