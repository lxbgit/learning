# https://github.com/ruby-china/homeland/wiki/Ubuntu-12.04-%E4%B8%8A%E4%BD%BF%E7%94%A8-Nginx-Passenger-%E9%83%A8%E7%BD%B2-Ruby-on-Rails

- 先安装 RVM和Ruby

1.1 更新 apt，并安装 curl：
`$ sudo apt-get update`
`$ sudo apt-get install curl`
1.2 然后安装 RVM：（按照[rvm官网](https://rvm.io/)来）
`$ \curl -sSL https://get.rvm.io | bash`
> 安装ruby还可以使用一下方式：
`sudo apt-get install ruby2.4`

- [配置Nginx和Passenger](https://www.phusionpassenger.com/library/install/nginx/install/oss/trusty/)

> 按照步骤来
搭建环境： Ubuntu 14.04 LTS + APT（不了解[百度](http://baike.baidu.com/link?url=3dU921ORjE9jXd6R628FUWFd9t6jwriZjFhcyRcVgR6uq6OQwiOjTAZt3OgjIT9lAWig7aBIoXEGsfIzi49ytq)）

  [Step 1: install Passenger packages](https://www.phusionpassenger.com/library/install/nginx/install/oss/trusty/#step-1:-install-passenger-packages)步骤一

```shell
# Install our PGP key and add HTTPS support for APT
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 561F9B9CAC40B2F7
sudo apt-get install -y apt-transport-https ca-certificates
# Add our APT repository
sudo sh -c 'echo deb https://oss-binaries.phusionpassenger.com/apt/passenger trusty main > /etc/apt/sources.list.d/passenger.list'
sudo apt-get update

# Install Passenger + Nginx
sudo apt-get install -y nginx-extras passenger
```

  [Step 2: enable the Passenger Nginx module and restart Nginx](https://www.phusionpassenger.com/library/install/nginx/install/oss/trusty/#step-2:-enable-the-passenger-nginx-module-and-restart-nginx)

>编辑 `/etc/nginx/nginx.conf` 文件，反注释 `include /etc/nginx/passenger.conf` 这句；（原先是注释的，现在讲注释去掉）

```shell

# 1.打开 `/etc/nginx/nginx.conf` 找到
     # include /etc/nginx/passenger.conf;
# 2. 将注释去掉
# 其实`/etc/nginx/passenger.conf `这个文件就是配置 ：
# passenger_root /usr/lib/ruby/vendor_ruby/phusion_passenger/locations.ini;
# passenger_ruby /usr/local/rvm/wrappers/ruby-2.4.1/ruby;

# 可以使用vim打开查看

include /etc/nginx/passenger.conf;

# 3 重启nginx
sudo service nginx restart
```

  [Step 3: check installation](https://www.phusionpassenger.com/library/install/nginx/install/oss/trusty/#step-3:-check-installation)

> 这一步就是检查是否安装成功
如果未成功，那么打开 `vim /var/log/nginx/error.log` 查看错误信息

```shell
sudo /usr/bin/passenger-config validate-install
sudo /usr/sbin/passenger-memory-stats
```

  [Step 4: update regularly](https://www.phusionpassenger.com/library/install/nginx/install/oss/trusty/#step-4:-update-regularly)

> 更新 （英文水平有限）

```shell
sudo apt-get update
sudo apt-get upgrade
```

- [上传文件]
(https://github.com/ruby-china/homeland/wiki/Ubuntu-12.04-%E4%B8%8A%E4%BD%BF%E7%94%A8-Nginx-Passenger-%E9%83%A8%E7%BD%B2-Ruby-on-Rails#%E4%B8%8A%E4%BC%A0%E6%96%87%E4%BB%B6)
按照[Ubuntu 12.04 上使用 Nginx Passenger 部署 Ruby on Rails](https://github.com/ruby-china/homeland/wiki/Ubuntu-12.04-%E4%B8%8A%E4%BD%BF%E7%94%A8-Nginx-Passenger-%E9%83%A8%E7%BD%B2-Ruby-on-Rails)来

- 修改Nginx配置

删除原有的默认网站配置：
`$ rm /etc/nginx/sites-enabled/default`
新建网站配置：
`$ touch /etc/nginx/sites-enabled/example.com.conf`
编辑 `/etc/nginx/sites-enabled/example.com.conf`，写入以下内容：

```config
server {
    listen 80 default;
    server_name example.com; # 这里填写你真实域名
    root /var/www/example.com/current/public;
    passenger_enabled on;
}
```

修改 `/etc/nginx/passenger.conf` 文件

```config
passenger_root /usr/lib/ruby/vendor_ruby/phusion_passenger/locations.ini;
passenger_ruby /usr/local/rvm/wrappers/ruby-2.4.1/ruby;
```

最后重启 nginx：
`$ sudo service nginx restart`

> 一定记着，如果有错误，一定要查看 `/var/log/nginx/error.log` 错误日志（非常重要）
