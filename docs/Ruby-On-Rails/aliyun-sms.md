# aliyun-sms

本文借鉴[aliyun-sms](https://github.com/VICTOR-LUO-F/aliyun-sms)
使用该库时，发现总是返回`code = InvalidDayuStatus.Malformed`，于是我对比了`aliyun-sms`的java版本，发现了几个问题：

- Version版本

[aliyun-sms](https://github.com/VICTOR-LUO-F/aliyun-sms)使用的是sms_version为  `2016-09-27`，而Java版本使用sms_version的是`2017-05-25`

- Action

[aliyun-sms](https://github.com/VICTOR-LUO-F/aliyun-sms)使用的是action=  `SingleSendSms`，而Java版本action=`SendSms`

- 最重要的签名算法错误

按照[官网](https://help.aliyun.com/document_detail/28761.html)，重新计算签名，其实主要就是将参数排序:

```ruby
 'AccessKeyId' => configuration.access_key_id,
 'Action' => configuration.action,
 'Format' => configuration.format,
 'PhoneNumbers' => mobile_num,
  'RegionId' => configuration.region_id,
  'SignName' => configuration.sign_name,
  'SignatureMethod' => configuration.signature_method,
  'SignatureNonce' => seed_signature_nonce,
  'SignatureVersion' => configuration.signature_version,
  'TemplateCode' => template_code,
  'TemplateParam' => message_param,
  'Timestamp' => seed_timestamp,
  'Version' => configuration.sms_version,
```

> 后面的签名的计算与该排序有关，见[官网](https://help.aliyun.com/document_detail/28761.html)

完整代码如下：

```ruby
module Aliyun
  module Sms
    class Configuration
      attr_accessor :access_key_secret, :access_key_id, :action, :format, :region_id,
                    :sign_name, :signature_method, :signature_version, :sms_version
      def initialize
        @access_key_secret = ''
        @access_key_id = ''
        @action = ''
        @format = ''
        @region_id = ''
        @sign_name = ''
        @signature_method = ''
        @signature_version = ''
        @sms_version = ''
      end
    end

    class << self
      attr_writer :configuration

      def configuration
        @configuration ||= Configuration.new
      end

      def configure
        yield(configuration)
      end

      def create_params(mobile_num, template_code, message_param)
        sms_params ={
          'AccessKeyId' => configuration.access_key_id,
          'Action' => configuration.action,
          'Format' => configuration.format,
          'PhoneNumbers' => mobile_num,
          'RegionId' => configuration.region_id,
          'SignName' => configuration.sign_name,
          'SignatureMethod' => configuration.signature_method,
          'SignatureNonce' => seed_signature_nonce,
          'SignatureVersion' => configuration.signature_version,
          'TemplateCode' => template_code,
          'TemplateParam' => message_param,
          'Timestamp' => seed_timestamp,
          'Version' => configuration.sms_version,
        }
      end

      # 发送请求
      def send(mobile_num, template_code, message_param)
        sms_params = create_params(mobile_num, template_code, message_param)
        body = post_body_data(configuration.access_key_secret, sms_params)
        puts body
        # Typhoeus.post("https://sms.aliyuncs.com/",
        #          headers: {'Content-Type'=> "application/x-www-form-urlencoded"},
        #          body: post_body_data(configuration.access_key_secret, sms_params))
      end

      # 原生参数拼接成请求字符串
      def query_string(params)
        qstring = ''
        params.each do |key, value|
          if qstring.empty?
            qstring += "#{encode(key)}=#{encode(value)}"
          else
            qstring += "&#{encode(key)}=#{encode(value)}"
          end
        end
        return qstring
      end

      # 原生参数经过2次编码拼接成标准字符串
      def canonicalized_query_string(params)
        cqstring = ''
        params.each do |key, value|
          if cqstring.empty?
            cqstring += "#{encode(key)}=#{encode(value)}"
          else
            cqstring += "&#{encode(key)}=#{encode(value)}"
          end
        end
        return encode(cqstring)
      end

      # 生成数字签名
      def sign(key_secret, params)
        key = key_secret + '&'
        signature = 'POST' + '&' + encode('/') + '&' + canonicalized_query_string(params)
        sign = Base64.encode64("#{OpenSSL::HMAC.digest('sha1',key, signature)}")
        encode(sign.chomp)  # 通过chomp去掉最后的换行符 LF
      end
      # 组成附带签名的 POST 方法的 BODY 请求字符串
      def post_body_data(key_secret, params)
        body_data = 'Signature=' + sign(key_secret, params) + '&' + query_string(params)
      end
      # 对字符串进行 PERCENT 编码
      def encode(input)
        output = url_encode(input)
      end
      # 生成短信时间戳
      def seed_timestamp
        Time.now.utc.strftime("%FT%TZ")
      end
      # 生成短信唯一标识码，采用到微秒的时间戳
      def seed_signature_nonce
        # Time.now.utc.strftime("%Y%m%d%H%M%S%L")
        UUID.generate
      end
    end
  end
end
```
