- ruby 路径不对
 对于这个错误，我是排查了好久，我们知道ruby的路径是通过`/etc/nginx/passenger.conf` 下的`passenger_ruby`来设置的，最初我是从网上直接拷贝下来，在执行的过程，页面一直出不来，去`/var/log/nginx/error.log` 查看错误，如下：
~~~
*** ERROR ***: Cannot execute /usr/local/rvm/wrappers/ruby-2.2.1/ruby: No such file or directory
~~~
看到上面就知道了，原来是ruby路径不对，然后就是找到ruby的路径，重新设置，然后重启服务，OK

- Gemfile文件下的`git_source`
~~~ruby
 Message from application: Undefined local variable or method `git_source' for Gemfile
        from /root/ruby-demo-01/Gemfile:3 (Bundler::GemfileError)
~~~
关于这个错误，我不知道为什么，但是解决办法就是直接将Gemfile下的
~~~ruby
git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end
~~~
注释掉

- 安装nio4r时，老是报错
~~~
Gem::Ext::BuildError: ERROR: Failed to build gem native extension.
    current directory: /var/lib/gems/2.4.0/gems/nio4r-2.1.0/ext/nio4r
/usr/bin/ruby2.4 -r ./siteconf20170607-31342-1waw6cx.rb extconf.rb
--with-cflags=
mkmf.rb can't find header files for ruby at /usr/lib/ruby/include/ruby.h
extconf failed, exit code 1
Gem files will remain installed in /var/lib/gems/2.4.0/gems/nio4r-2.1.0 for
inspection.
Results logged to
/var/lib/gems/2.4.0/extensions/x86_64-linux/2.4.0/nio4r-2.1.0/gem_make.out
An error occurred while installing nio4r (2.1.0), and Bundler cannot
continue.
Make sure that `gem install nio4r -v '2.1.0'` succeeds before bundling.
~~~
解决办法：
[点击这](https://stackoverflow.com/questions/42652189/how-to-fix-a-bundle-install-nio4r-error-on-rails-5-0-0)
~~~
# 安装Ruby 对应版本的ruby-dev
sudo apt-get install ruby2.4-dev
~~~

- 报如下错误时
~~~
Bundler::GemspecError: Could not read gem at
/var/lib/gems/2.4.0/cache/elasticsearch-rails-5.0.1.gem. It may be corrupted.
An error occurred while installing elasticsearch-rails (5.0.1), and
Bundler cannot continue.
Make sure that `gem install elasticsearch-rails -v '5.0.1'` succeeds before
bundling.
~~~
解决办法：找到`/var/lib/gems/2.4.0/cache/`该文件夹，删除`elasticsearch-rails-5.0.1.gem`，重新安装；
一般就是在`/var/lib/gems/2.4.0/cache/`文件下找到相应的包，然后删除，重新`gem install`
