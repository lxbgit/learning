# 进程管理（supervisor）

[参考文档](https://blog.csdn.net/xc_123/article/details/93971706)

> Supervisor是用Python开发的一个client/server服务，是Linux/Unix系统下的一个进程管理工具，不支持Windows系统。它可以很方便的监听、启动、停止、重启一个或多个进程。用Supervisor管理的进程，当一个进程意外被杀死，supervisort监听到进程死后，会自动将它重新拉起，很方便的做到进程自动恢复的功能，不再需要自己写shell脚本来控制。

## 查看所有子进程的状态

```shell
supervisorctl status
```

第一列是服务名；
第二列是运行状态，RUNNING表示运行中，FATAL 表示运行失败，STARTING表示正在启动,STOPED表示任务已停止；　
第三/四列是进程号,最后是任务已经运行的时间。

## 查看单个任务状态

```shell
supervisorctl status 服务名
```

## 关闭任务

```shell
supervisorctl stop 服务名
```

## 关闭所有进程

```shell
supervisorctl stop all
```

## 启动任务

```shell
supervisorctl start 服务名
```

## 重启任务

```shell
supervisorctl restart 服务名
```
