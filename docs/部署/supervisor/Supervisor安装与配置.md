# Supervisor安装与配置

[参考文档](https://www.jianshu.com/p/39b476e808d8)

## 安装Python包管理工具 [easy_install](https://pypi.org/project/setuptools/)

```shell
yum install python-setuptools
```

## 安装Supervisor

```shell
easy_install supervisor
```

## 配置Supervisor应用守护

- 通过运行echo_supervisord_conf程序生成supervisor的初始化配置文件，如下所示：

```shell
mkdir /etc/supervisor
echo_supervisord_conf > /etc/supervisor/supervisord.conf
```

然后查看路径下的supervisord.conf。在文件尾部添加如下配置

```shell
...

;[include]
;files = relative/directory/*.ini

;conf.d 为配置表目录的文件夹，需要手动创建
[include]
files = conf.d/*.conf
```

- 为你的程序创建一个.conf文件，放在目录"/etc/supervisor/conf.d/"下

```shell
[program:MGToastServer] ;程序名称，终端控制时需要的标识
command=dotnet MGToastServer.dll ; 运行程序的命令
directory=/root/文档/toastServer/ ; 命令执行的目录
autorestart=true ; 程序意外退出是否自动重启
stderr_logfile=/var/log/MGToastServer.err.log ; 错误日志文件
stdout_logfile=/var/log/MGToastServer.out.log ; 输出日志文件
environment=ASPNETCORE_ENVIRONMENT=Production ; 进程环境变量
user=root ; 进程执行的用户身份
stopsignal=INT
```

- 运行supervisord，查看是否生效

```shell
supervisord -c /etc/supervisor/supervisord.conf
ps -ef | grep MGToastServer
```

> ps 如果服务已启动，修改配置文件可用“supervisorctl reload”命令来使其生效

## 配置Supervisor开机启动

- 新建一个“supervisord.service”文件

```shell
# dservice for systemd (CentOS 7.0+)
# by ET-CS (https://github.com/ET-CS)
[Unit]
Description=Supervisor daemon

[Service]
Type=forking
ExecStart=/usr/bin/supervisord -c /etc/supervisor/supervisord.conf
ExecStop=/usr/bin/supervisorctl shutdown
ExecReload=/usr/bin/supervisorctl reload
KillMode=process
Restart=on-failure
RestartSec=42s

[Install]
WantedBy=multi-user.target
```

- 将文件拷贝至"/usr/lib/systemd/system/supervisord.service"

- 执行命令

```shell
systemctl enable supervisord
```

- 执行命令来验证是否为开机启动

```shell
systemctl is-enabled supervisord
```

## 常用的相关管理命令

```shell
supervisorctl restart <application name> ;重启指定应用
supervisorctl stop <application name> ;停止指定应用
supervisorctl start <application name> ;启动指定应用
supervisorctl restart all ;重启所有应用
supervisorctl stop all ;停止所有应用
supervisorctl start all ;启动所有应用
```
