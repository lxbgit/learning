# docker 中使用apollo

[参考1](https://blog.csdn.net/miss1181248983/article/details/108717531)

[参考2](https://blog.csdn.net/cc_want/article/details/85873657)

- 搜索apollo

```shell
docker search apollo
```

## 安装 apollo-configservice

```shell
docker pull apolloconfig/apollo-configservice
```

Config Service:

```shell
docker run -d \
    --name apollo-configservice \
    --net=host \
    -v /tmp/logs:/opt/logs \
    -e SPRING_DATASOURCE_URL="jdbc:mysql://114.116.237.106:3306/apolloconfigdb?characterEncoding=utf8" \
    -e SPRING_DATASOURCE_USERNAME=root \
    -e SPRING_DATASOURCE_PASSWORD=CSCEC@JHTec2021 \
    apolloconfig/apollo-configservice
```

说明：

```shell
SPRING_DATASOURCE_URL: 对应环境ApolloConfigDB的地址
SPRING_DATASOURCE_USERNAME: 对应环境ApolloConfigDB的用户名
SPRING_DATASOURCE_PASSWORD: 对应环境ApolloConfigDB的密码
```

默认端口：8080

## 安装 apollo-adminservice

```shell
docker pull apolloconfig/apollo-adminservice
```

Admin Service

```shell
docker run -d \
    --name apollo-adminservice \
    --net=host \
    -v /tmp/logs:/opt/logs \
    -e SPRING_DATASOURCE_URL="jdbc:mysql://114.116.237.106:3306/apolloconfigdb?characterEncoding=utf8" \
    -e SPRING_DATASOURCE_USERNAME=root \
    -e SPRING_DATASOURCE_PASSWORD=CSCEC@JHTec2021 \
    apolloconfig/apollo-adminservice
```

说明：

```text
SPRING_DATASOURCE_URL: 对应环境ApolloConfigDB的地址
SPRING_DATASOURCE_USERNAME: 对应环境ApolloConfigDB的用户名
SPRING_DATASOURCE_PASSWORD: 对应环境ApolloConfigDB的密码
```

默认端口：

## 安装 apollo-portal

```shell
docker pull apolloconfig/apollo-portal
```

- Portal Server

```shell
docker run -d \
    --name apollo-portal \
    --net=host \
    -v /tmp/logs:/opt/logs \
    -e SPRING_DATASOURCE_URL="jdbc:mysql://114.116.237.106:3306/ApolloPortalDB?characterEncoding=utf8" \
    -e SPRING_DATASOURCE_USERNAME=root \
    -e SPRING_DATASOURCE_PASSWORD=CSCEC@JHTec2021 \
    -e APOLLO_PORTAL_ENVS=dev \
    -e DEV_META=http://192.168.30.131:8080 \
    apolloconfig/apollo-portal
```

- demo1(测试环境配置)

```shell
docker run -d \
    --name apollo-portal \
    --net=host \
    -v /tmp/logs:/opt/logs \
    -e SPRING_DATASOURCE_URL="jdbc:mysql://114.116.237.106:3306/apolloportaldb?characterEncoding=utf8" \
    -e SPRING_DATASOURCE_USERNAME=root \
    -e SPRING_DATASOURCE_PASSWORD=CSCEC@JHTec2021 \
    apolloconfig/apollo-portal
```

- 说明：

```text
SPRING_DATASOURCE_URL: 对应环境ApolloPortalDB的地址
SPRING_DATASOURCE_USERNAME: 对应环境ApolloPortalDB的用户名
SPRING_DATASOURCE_PASSWORD: 对应环境ApolloPortalDB的密码
APOLLO_PORTAL_ENVS(可选): 对应ApolloPortalDB中的apollo.portal.envs配置项，如果没有在数据库中配置的话，可以通过此环境参数配置
DEV_META/PRO_META(可选): 配置对应环境的Meta Service地址，以${ENV}_META命名，如果ApolloPortalDB中配置了apollo.portal.meta.servers，则以apollo.portal.meta.servers中的配置为准
```

## 注意点1

运行顺序： apollo-configservice -> apollo-adminservice  -> apollo-portal

## 安装过程中遇到的问题

> 部署完成但是这里有两个地方需要修改，否则会报错

- 部署完成后需要修改两个地方

1.进入apollo-portal容器 `docker exec -it apollo-portal bin/bash`
修改apollo-env环境变量：

```shell
# 这里配置 apollo-configservice 的ip:port
local.meta=http://localhost:9180
dev.meta=http://localhost:9180
fat.meta=http://localhost:9180
uat.meta=http://localhost:9180
lpt.meta=${lpt_meta}
pro.meta=http://localhost:9180
```

2.修改数据库`ApolloConfigDB`的`ServerConfig`表中的`eureka.service.url`字段

```shell
# 修改为eureka 的server
http://ip:port/eureka
```

3.默认账号密码  apollo/admin
