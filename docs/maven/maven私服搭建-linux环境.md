# Nexus 在CentOS下安装

[下载](https://www.sonatype.com/nexus/repository-oss-download)

[参考1](http://www.zzvips.com/article/127087.html)

[参考2](https://blog.csdn.net/lazycheerup/article/details/85255799)

## nexus 下载并解压

- 下载

```shell
wget https://download.sonatype.com/nexus/3/latest-unix.tar.gz
```

- 解压

```shell
tar zxvf latest-unix.tar.gz
```

## 检查环境

- java

- 安装jdk

## 配置 nexus 环境变量

打开 `etc/` 目录下的 `profile` 文件，命令如下

```shell
vim /etc/profile
```

把 nexus 环境添加到 profile 尾部，环境代码如下：

```shell
export NEXUS_HOME=/usr/local/nexus #nexus目录path
export PATH=$PATH:$NEXUS_HOME/bin
```

然后，保存退出。重新加载配置文件，让配置生效

```shell
source /etc/profile
```

## 启动nexus

- 更改端口

nexus 默认端口是 8081 , 如果我们的端口被占用了，则需要重新为 nexus 指定端口，端口的配置文件在安装目录下的 etc 目录，如下所示

```shell
[root@izwz9id0dphnuy2q3l6rdoz nexus-3.7.1-02]# ls
bin  deploy  etc  lib  LICENSE.txt  NOTICE.txt  public  system
```

进入 etc 目录，找到 nexus-default.properties 文件。如下所示：

```shell
[root@izwz9id0dphnuy2q3l6rdoz nexus-3.7.1-02]# cd etc
[root@izwz9id0dphnuy2q3l6rdoz etc]# ls
fabric  jetty  karaf  logback  nexus-default.properties  ssl
```

用 vim 打开 nexus-default.properties 文件:

把

```shell
# Jetty section
application-port=8081
application-host=0.0.0.0
...
```

改成

```shell
# Jetty section
application-port=8085
application-host=0.0.0.0
...
```

这样就把 nexus 的端口从 8081 改为 8085 , 然后重启 nexus服务

- 重启服务

```shell
nexus restart
```

## 其他命令

```shell
# 启动
nexus start
 
# 停止
nexus stop
 
# 重启
nexus restart
 
# 查看状态
nexus status
```

## 默认

```shell
默认账号：admin 

默认密码：admin123 
```

## 问题

> 在解决问题之前，必须保证你下载的nexus是完整的

- 安装过程中出现如下错误

```shell
[root@localhost bin]# ./nexus start
WARNING: ************************************************************
WARNING: Detected execution as "root" user.  This is NOT recommended!
WARNING: ************************************************************
Starting nexus
```

说明不建议root用户执行，那么我们换一个用户：

```shell
useradd nexus #新建一个用户
chown -R nexus:nexus /usr/local/nexus #给权限
chown -R nexus:nexus /usr/local/sonatype-work #给权限
```

然后编辑`/usr/local/nexus/bin/nexus.rc`，将 `run_as_user=""` 改为 `run_as_user=nexus`
保存,

重新运行:

```shell
/usr/local/nexus/bin/nexus start

#查看状态
/usr/local/nexus/bin/nexus status
```
