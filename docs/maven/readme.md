# maven 常用命令

## maven项目install时忽略执行test的几种方法

- 在项目所在文件夹根目录使用maven命令打包时：

```shell script
<!-- 不执行单元测试，也不编译测试类 -->
mvn install -Dmaven.test.skip=true
```

或者

```shell script
<!-- 不执行单元测试，但会编译测试类，并在target/test-classes目录下生成相应的class -->
mvn install -DskipTests=true
```

## springboot项目中，在pom.xml文件的中添加如下配置：

```xml
<!-- 不执行单元测试，但会编译测试类，并在target/test-classes目录下生成相应的class -->
<skipTests>true</skipTests> 
```

或者

```xml
<!-- 不执行单元测试，也不编译测试类 -->
<maven.test.skip>true</maven.test.skip>
```

## maven项目的pom.xml文件的中添加如下配置：

```xml
<!-- 不执行单元测试，但会编译测试类并在target/test-classes目录下生成相应的class -->
<plugin>  
	<groupId>org.apache.maven.plugins</groupId>  
	<artifactId>maven-surefire-plugin</artifactId>  
	<version>2.5</version>  
	<configuration>  
		<skipTests>true</skipTests>  
	</configuration>  
</plugin>
```

## maven项目install时忽略执行test的几种方法

- 在项目所在文件夹根目录使用maven命令打包时：

```shell script
<!-- 不执行单元测试，也不编译测试类 -->
mvn install -Dmaven.test.skip=true
```

或者

```shell script
<!-- 不执行单元测试，但会编译测试类，并在target/test-classes目录下生成相应的class -->
mvn install -DskipTests=true
```

## springboot项目中，在pom.xml文件的中添加如下配置：

```shell script
<!-- 不执行单元测试，但会编译测试类，并在target/test-classes目录下生成相应的class -->
<skipTests>true</skipTests> 
```

或者

```shell script
<!-- 不执行单元测试，也不编译测试类 -->
<maven.test.skip>true</maven.test.skip>
```

## maven项目的pom.xml文件的中添加如下配置：

```xml
<!-- 不执行单元测试，但会编译测试类并在target/test-classes目录下生成相应的class -->
<plugin>  
	<groupId>org.apache.maven.plugins</groupId>  
	<artifactId>maven-surefire-plugin</artifactId>  
	<version>2.5</version>  
	<configuration>  
		<skipTests>true</skipTests>  
	</configuration>  
</plugin>
```

## 打包时忽略某些文件

```xml
<build>
    <finalName>JHWebMs</finalName>
    <plugins>
        <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-war-plugin</artifactId>
            <version>3.2.3</version>
            <configuration>
                <!-- 注意：这是打包后的路径 -->
                <!-- **:可以删除当前文件夹和文件夹里的内容 -->
                <packagingExcludes>
                    WEB-INF/classes/com/simple/business/**,
                    WEB-INF/jsp/business/**
                </packagingExcludes>
            </configuration>
        </plugin>
        <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-compiler-plugin</artifactId>
            <version>3.8.0</version>
            <configuration>
                <source>${java.version}</source>
                <target>${java.version}</target>
            </configuration>
        </plugin>
    </plugins>
</build>
```

## SpringBoot 使用maven打包-引入外部jar包

- 引入外部jar包

```xml
  <dependency>
      <groupId>com.xx.xxx</groupId> //组织，随便命名
      <artifactId>***</artifactId> //包的名字，随便命名
      <version>1.1.2</version> //版本，随便命名
      <scope>system</scope>  //scope为system时，自动添加lib依赖包
      <systemPath>${basedir}/src/main/resources/lib/**.jar</systemPath> //路径，这里我jar在resources目录的lib文件夹下，也可以放在跟目标，路径按需修改
 </dependency>
```

- 项目打包

> 是由于项目打包时，不识别外部jar，又没有进行配置，继而导致出现问题。

```xml
  <plugin>
       <groupId>org.springframework.boot</groupId>
       <artifactId>spring-boot-maven-plugin</artifactId>
       <configuration>
             <includeSystemScope>true</includeSystemScope>
       </configuration>
  </plugin>

```
