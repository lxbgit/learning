# maven父子工程---子模块相互依赖打包时所遇到的问题:依赖的程序包找不到

> 在对`Module A`进行打包时,出现问题:`Module A`中所依赖的`base工程`的`util程序包`不存在。即使能打包成功,用`java -jar`启动`jar`包也会报`Class Not Found`,依赖的`base工程`的类找不到

## 打war包时

>只在有main方法的springboot项目的pom.xml中添加

```xml
<build>  
    <plugins>  
        <plugin>  
            <groupId>org.springframework.boot</groupId>  
            <artifactId>spring-boot-maven-plugin</artifactId>  
        </plugin>  
    </plugins>  
</build>
```

> 其他依赖不需要添加,普通打成jar包的项目不需要添加`boot-plugin`

## 如果是以jar包形式提供那么pom修改为

```xml
 <build>  
    <plugins>  
        <plugin>  
            <groupId>org.springframework.boot</groupId>  
            <artifactId>spring-boot-maven-plugin</artifactId>  
            <configuration>  
                <classifier>exec</classifier>  
            </configuration>  
        </plugin>  
    </plugins>  
</build>
```

[https://blog.csdn.net/DamonREN/article/details/85091900](参考1)
[https://docs.spring.io/spring-boot/docs/2.3.0.RELEASE/maven-plugin/reference/html/#repackage-example-custom-classifier](参考2)

## 原因

```
parent (pom)
   |
   |---- module A (jar)
   |---- module B (jar)
   |---- admin (war)
            |
            |---- A
            |---- B

```
