# centos7 下安装 oracle11g

- [下载](www.oracle.com)

- [参考1](https://blog.csdn.net/qq_24058757/article/details/88170905)

## 设置数据库自启动

- 修改`dbstart`、`dbshut`文件

```shell
# $ORACLE_HOME/bin/dbshut是一样的，这里不重复了
[oracle@node1 ~]# vi $ORACLE_HOME/bin/dbstart
ORACLE_HOME_LISTNER=$1
修改为：
ORACLE_HOME_LISTNER=$ORACLE_HOME
```

- 修改`/etc/oratab`文件

```shell
#/u01/oracle/product/11.2.0/db_1/这个是自己安装路径，只需要将N改为Y
[oracle@node1 ~]# vi /etc/oratab
orcl:/u01/oracle/product/11.2.0/db_1/:N
修改为：
orcl:/u01/oracle/product/11.2.0/db_1/:Y
```

- 把lsnrctl start和dbstart添加到rc.local文件中

```shell
#将下面两句加入到rc.local文件中，路径换成自己的；oracle用户下如果没有权限可以切换到root用户
[root@node1 ~]# vi /etc/rc.d/rc.local
su - oracle -lc "/u01/oracle/product/11.2.0/db_1/bin/lsnrctl start"
su - oracle -lc "/u01/oracle/product/11.2.0/db_1/bin/dbstart"
```

- 添加执行权限

```shell
[oracle@node1 ~]$ su root
[root@node1 ~]# chmod +x /etc/rc.d/rc.local
```
