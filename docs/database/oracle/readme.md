# Oracle 问题

## 使用`sys`登录时

- sys用数据库的超级用户，数据库内很多重要的东西（数据字典表、内置包、静态数据字典视图等）都属于这个用户，sys用户必须以sysdba身份登录

- system是数据库内置的一个普通管理员，你手工创建的任何用户在被授予dba角色后都跟这个用户差不多

## LRM-00109: could not open parameter file '/data/oracle/product/11.2.0/db_1/dbs/initorcl.ora'

使用`startup` 开启数据库时

> cp init.ora.1119201915113 /data/oracle/product/11.2.0/db_1/dbs/initorcl.ora

## ORA-01102: cannot mount database in EXCLUSIVE mode

[解决](https://blog.csdn.net/lingxiao1126/article/details/7720375)

- `cd $ORACLE_HOME/dbs`
- `ls sgadef*`
- `ls lk*`
- `rm lk*`
- `ipcs -map`
- `ipcs -s`
- `ipcrm -s semid`
- ` ipcs -m`

## ORA-00845: MEMORY_TARGET not supported on this system

[解决](https://blog.csdn.net/kangkanggegeg/article/details/78958177)

## ora-01950:对表空间XXX无权限

> 在创建表的时候，插入数据提示无权限

```sql
#//username 换成没有权限的用户
grant resource to username
```

