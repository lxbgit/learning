# 数据库字段自动更新时间

```sql
alter table t_article
    modify update_time datetime not null default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '更新时间';

alter table t_article modify create_time datetime not null default CURRENT_TIMESTAMP;

```
