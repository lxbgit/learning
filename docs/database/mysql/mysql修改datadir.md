
# Mysql 修改datadir目录

[参考](https://www.cnblogs.com/perfei/p/16210396.html)

```sql
mysql> show variables like "%datadir%";
+---------------+-----------------+
| Variable_name | Value           |
+---------------+-----------------+
| datadir       | /var/lib/mysql/ |
+---------------+-----------------+
1 row in set (0.01 sec)
```

```sql
mysql> shutdown;
Query OK, 0 rows affected (0.00 sec)

```

```shell
cp -r /var/lib/mysql ./db1

chown -R mysql.mysql db1


vim /etc/mysql/my.cnf

datadir=/source/data/mysql/db1

/etc/init.d/mysql start
```
