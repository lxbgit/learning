# Centos7 安装MySQL8

## 添加MySQL8的本地源

- 执行以下命令获取安装MySQL源

`wget https://repo.mysql.com//mysql80-community-release-el7-1.noarch.rpm`

`sudo yum localinstall mysql80-community-release-el7-1.noarch.rpm`

- 可以用下面命令检测源是否添加成功
`yum repolist enabled | grep "mysql.*-community.*"`

## 安装MySQL服务器

- 执行以下命令进行安装

`sudo yum install mysql-community-server`

## 启动MySQL

- 用下面命令启动MySQL
`sudo service mysqld start`
- 你可以用下面的命令检查MySQL的运行状态
`sudo service mysqld status`

- 重启MySQL

`sudo service mysqld restart`

- 停止MySQL

`sudo service mysqld stop`

> 注意：如果你的服务器内存是500M或者更小，可能会因为内存不够导致无法启动成功。
> 修改`/etc/my.cnf`中的`innodb_buffer_pool_size=50M`或者更小即可

## MySQL ROOT账号权限配置和远程链接

### 登陆MySQL本地Shell客户端，并修改root初始密码

- 软件安装好之后，会在错误日志中生成一个超级用户的初始密码，用下面的命令可以查看这个初始密码
`sudo grep 'temporary password' /var/log/mysqld.log`

- 登陆mysql shell客户端，用ALTER USER指令修改初始密码

```shell
[root@you ~]# mysql -u root -p
Enter password: 
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 19
Server version: 8.0.11 MySQL Community Server - GPL

Copyright (c) 2000, 2018, Oracle and/or its affiliates. All rights reserved.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> ALTER USER 'root'@'localhost' IDENTIFIED BY 'MyNewPass4!';
Query OK, 0 rows affected (0.08 sec)
```

> 注意：MySQL对密码复杂度有一定的要求（新版本允许修改规则），默认密码规则如下：
> 1.长度不得小于8位
> 2.必须包含至少一个数字，一个小写字母，一个大写字母和一个特殊字符

### 修改root账号远程访问权限

- 执行以下命令进行修改

```shell
mysql> use mysql;
mysql> update user set host="%" where user='root';
mysql> GRANT ALL ON *.* TO 'root'@'%';
mysql> flush privileges;
```

> 执行完之后用exit命令退出shell客户端，重启MySQL。
> 然后就可以在客户端中测试一下链接是否正常了

### 需要注意的几个问题

- 上面的GRANT语句可能和之前的版本不同，我在用之前版本的写法时一直报错。网上其他人写的旧的教程中这个语句不太适合用在MySQL8中。引用中有最新的官网文档可以拿来参考

- 有些系统会因为服务器防火墙导致即使配置成功，也无法远程链接MySQL。如果无法链接，可以先暂时关闭防火墙测试一下是否时因为防火墙的原因(不同版本的Centos系统防火墙配置可能不一样，具体可能需要另查资料)
防火墙命令:

```shell
[root@you ~]# service firewalld stop
```

或者

```shell
[root@you ~]# systemctl stop firewalld.service
```

- 如果你的客户端出现下面这个错误，Client does not support authentication protocol requested by server。是因为MySQL8服务器版本中使用了新的密码验证机制，这需要客户端的支持，如果是旧的客户端（比如Navicat for mysql 11.1.13），可能不会很好的支持，需要你换到比较新的版本。暂时没有找到能让旧版本支持的方法。引用链接中方法我试过了，不管用。

### 引用

[grant](https://dev.mysql.com/doc/refman/8.0/en/grant.html)
