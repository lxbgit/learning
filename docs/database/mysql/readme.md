# mysql

## mysql 导入sql

```shell
 mysql> source home\数据库名\数据库名.sql
```

## v5.7 修改root默认密码

- 修改`/etc/my.cnf`, 在`[mysqld]`小节下添加一行 `skip-grant-tables=1`

> 这一行配置让 mysqld 启动时不对密码进行验证

- 重启MySQL 服务 `service mysql start`

- 使用 `root`用户登录mysql `mysql -u root`

- 切换到mysql数据库 `use mysql;`，更新`user`表

> `update user set authentication_string = password('密码') where user = 'root';`
> `flush privileges;`

- 退出mysql `quit`

- 编辑 `/etc/my.cnf`文件，注释掉 `skip-grant-tables`

- 重启mysql服务 `service mysql restart`,然后重新登录

- 登录后，使用`alter user 'root'@'localhost' identified by '密码'` 修改密码

## 问题1

- mysql 连接报错， `java.sql.SQLException:null,message from server:"Host 'x.x.x.x' is not allowed to connect` 异常

> 解决办法：修改访问权限即可

- 打开终端

`mysql -u root -p` 登录

```mysql
use mysql;
select user, host from user;
update user set host = '%' where user = 'root';
flush privileges;
```

- 重启服务

## mysql 使用过程中问题2

- MYSQL “Access denied; you need (at least one of) the SUPER privilege(s) for this operation” 问题解决

> MySQL Access denied; you need (at least one of) the SUPER or SET_USER_ID privilege

导入MySQL执行sql文件导入数据库的操作时出现了 “[Err] 1227 - Access denied; you need (at least one of) the SUPER privilege(s) for this operation” 的报错提示，经过仔细检查发现在sql文件中的存储过程增加了 DEFINER=`root`@`localhost` 如图所示，

```sql
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `pub_org_count_view` AS select `a`.`orgId` AS `org_id`,`b`.`pid` AS `org_pid`,`a`.`childNum` AS `child_num` from ((select `pub_org`.`pid` AS `orgId`,count(`pub_org`.`pid`) AS `childNum` from `pub_org` group by `pub_org`.`pid`) `a` left join `pub_org` `b` on((`a`.`orgId` = `b`.`id`))) */;
```

解决方法有三种：

一是在服务器上使用MySQL登录数据库并且IP地址用localhost；

二是把DEFINER=`root`@`localhost`的localhost改为你的服务器IP，把root改成你的用户名；

三是在你的sql文件中删除DEFINER=`root`@`localhost`这个限制。

## 表名忽略大小写

用root帐号登录后，在`/etc/my.cnf` 中的`[mysqld]`后添加添加`lower_case_table_names=1`，重启MYSQL服务，这时已设置成功：不区分表名的大小写；
`lower_case_table_names`参数详解：
`lower_case_table_names = 0`

其中0：区分大小写，1：不区分大小写
