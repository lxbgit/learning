# CentOS7下安装mysql5.x

## 下载

`https://downloads.mysql.com/archives/get/p/23/file/mysql-5.7.32-linux-glibc2.12-i686.tar.gz`

## 安装

```shell
yum install mysql-community-server
```

## 命令

启动mysql：

```shell
systemctl start mysqld
```

查看mysql状态：

```shell
systemctl status mysqld
```
