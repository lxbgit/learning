# mysql

- `show databases;`

> 查询所有的数据库

- 创建视图

```sql
# 创建视图
create view pub_dictionary_count_view as
select a.dictId as dict_id, b.dict_pid as dict_pid, a.childNum as child_num
from (select dict_pid as "dictId", count(dict_pid) as "childNum" from pub_dictionary group by dict_pid) a
         left join pub_dictionary b on a.dictId = b.id;
```

- 修改字段

```sql
alter table t_article
    modify update_time datetime not null default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '更新时间';

alter table t_article modify create_time datetime not null default CURRENT_TIMESTAMP;

```

- 添加字段

```sql
# 添加字段
alter table pub_user_role add column create_time datetime not null default CURRENT_TIMESTAMP comment '创建时间';
#
alter table pub_user_role add column update_time datetime not null default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '更新时间';

```

- 字段自动更新时间

```sql
alter table t_article
    modify update_time datetime not null default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '更新时间';

alter table t_article modify create_time datetime not null default CURRENT_TIMESTAMP;

```

- ALTER USER 用法

  - 基本使用 `ALTER USER testuser IDENTIFIED BY '123456';`

  - 修改当前登录用户 `ALTER USER USER() IDENTIFIED BY '123456';`

  - 使密码过期 `ALTER USER testuser IDENTIFIED BY '123456' PASSWORD EXPIRE;`

  - 使密码从不过期 `ALTER USER testuser IDENTIFIED BY '123456' PASSWORD EXPIRE NEVER;`

## 数据库设计规范

### 库名

- 见名知意

- 越短越好

- 禁止拼音

### 字段

> 同 “库名”
