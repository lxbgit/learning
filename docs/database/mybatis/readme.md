# Mybatis配置

[简介](http://www.mybatis.org/mybatis-3/zh/getting-started.html)
[配置](http://www.mybatis.org/mybatis-3/zh/configuration.html#properties)

> mybatis 介绍

## mybatis别名

| Alias | Mapped Type |
| ----- | --------- |
|_byte	| byte|
|_long|	long|
|_short|	short|
|_int|	int|
|_integer|	int|
|_double|	double|
|_float|	float|
|_boolean|	boolean|
|string|	String|
|byte|	Byte|
|long|	Long|
|short|	Short|
|int|	Integer|
|integer|	Integer|
|double|	Double|
|float|	Float|
|boolean|	Boolean|
|date|	Date|
|decimal|	BigDecimal|
|bigdecimal|	BigDecimal|
|object|	Object|
|map|	Map|
|hashmap|	HashMap|
|list|	List|
|arraylist|	ArrayList|
|collection|	Collection|
|iterator|	Iterator|

## 类型处理器(TypeHandlers)

|Type Handler(类型处理器)| Java Types(Java 类型)| JDBC Types(JDBC 类型)|
| ----- | --------- | ------ |
|`BooleanTypeHandler` | `java.lang.Boolean, boolean` | Any compatible BOOLEAN|
|`ByteTypeHandler`|	`java.lang.Byte, byte` | Any compatible NUMERIC or BYTE|
|`ShortTypeHandler`| `java.lang.Short, short`|	Any compatible NUMERIC or SHORT INTEGER|
|`IntegerTypeHandler`| `java.lang.Integer, int`| Any compatible NUMERIC or INTEGER|
|`LongTypeHandler`|	`java.lang.Long, long`|	Any compatible NUMERIC or LONG INTEGER|
|`FloatTypeHandler`| `java.lang.Float, float`| Any compatible NUMERIC or FLOAT|
|`DoubleTypeHandler`|	`java.lang.Double, double`|	Any compatible NUMERIC or DOUBLE|
|`BigDecimalTypeHandler`|	`java.math.BigDecimal`|	Any compatible NUMERIC or DECIMAL|
|`StringTypeHandler`|	`java.lang.String`|	CHAR, VARCHAR|
|`ClobReaderTypeHandler`|	`java.io.Reader`|	-|
|`ClobTypeHandler`|	`java.lang.String`|	CLOB, LONGVARCHAR|
|`NStringTypeHandler`|	`java.lang.String`|	NVARCHAR, NCHAR|
|`NClobTypeHandler`|	`java.lang.String`|	NCLOB|
|`BlobInputStreamTypeHandler`|	`java.io.InputStream`|	-|
|`ByteArrayTypeHandler`|	`byte[]`|	Any compatible byte stream type|
|`BlobTypeHandler`|	`byte[]`|	BLOB, LONGVARBINARY|
|`DateTypeHandler`|	`java.util.Date`|	TIMESTAMP|
|`DateOnlyTypeHandler`|	`java.util.Date`|	DATE|
|`TimeOnlyTypeHandler`|	`java.util.Date`|	TIME|
|`SqlTimestampTypeHandler`|	`java.sql.Timestamp`|	TIMESTAMP|
|`SqlDateTypeHandler`|`java.sql.Date`|	DATE|
|`SqlTimeTypeHandler`|`java.sql.Time`|	TIME|
|`ObjectTypeHandler`| Any |	OTHER, or unspecified|
|`EnumTypeHandler`|	Enumeration Type | VARCHAR any string compatible type, as the code is stored (not index). VARCHAR 或任何兼容的字符串类型，用以存储枚举的名称（而不是索引值）|
|`EnumOrdinalTypeHandler`|	Enumeration Type | Any compatible NUMERIC or DOUBLE, as the position is stored (not the code itself).任何兼容的 `NUMERIC` 或 `DOUBLE` 类型，存储枚举的序数值（而不是名称）|
|`SqlxmlTypeHandler` | `java.lang.String` | `SQLXML`|
|`InstantTypeHandler`	|`java.time.Instant`|	`TIMESTAMP`|
|`LocalDateTimeTypeHandler`|	`java.time.LocalDateTime`|	`TIMESTAMP`|
|`LocalDateTypeHandler`| `java.time.LocalDate`|	`DATE`|
|`LocalTimeTypeHandler`|	`java.time.LocalTime`|	`TIME`|
|`OffsetDateTimeTypeHandler`|	`java.time.OffsetDateTime`|	`TIMESTAMP`|
|`OffsetTimeTypeHandler`|	`java.time.OffsetTime`|	`TIME`|
|`ZonedDateTimeTypeHandler`|	`java.time.ZonedDateTime`|	`TIMESTAMP`|
|`YearTypeHandler`|	`java.time.Year`|	`INTEGER`|
|`MonthTypeHandler`|	`java.time.Month`|	`INTEGER`|
|`YearMonthTypeHandler`|	`java.time.YearMonth`|	`VARCHAR` 或 `LONGVARCHAR`|
|`JapaneseDateTypeHandler`|	`java.time.chrono.JapaneseDate`|	`DATE`|

## mybatis注解

> [参考](http://www.mybatis.org/mybatis-3/zh/java-api.html)


| 注解 | 使用对象 | 对应xml | 描述 |
| ----- | ------ | ------ | ------ |
| `@Results` | 方法 | `<resultMap>` | 结果映射的列表，包含了一个特别结果列如何被映射到属性或字段的详情。属性有：value, id。value 属性是 Result 注解的数组。这个 id 的属性是结果映射的名称 |
| `@Result` | N/A | `<result>`或`<id>` | 在列和属性或字段之间的单独结果映射。属性有：`id`, `column`, `javaType`, `jdbcType`, `typeHandler`, `one`, `many`。`id` 属性是一个布尔值，来标识应该被用于比较（和在 XML 映射中的`<id>`相似）的属性。`one` 属性是单独的联系，和 `<association>` 相似，而 `many` 属性是对集合而言的，和`<collection>`相似。它们这样命名是为了避免名称冲突 |
| `@One` | N/A | `<association>` | 复杂类型的单独属性值映射。属性有：select，已映射语句（也就是映射器方法）的全限定名，它可以加载合适类型的实例。fetchType会覆盖全局的配置参数 lazyLoadingEnabled。注意 联合映射在注解 API中是不支持的。这是因为 Java 注解的限制,不允许循环引用 |
| `@ResultMap` | 方法 |	N/A | 这个注解给 `@Select` 或者`@SelectProvider` 提供在 XML 映射中的 `<resultMap>` 的id。这使得注解的 select 可以复用那些定义在 XML 中的 ResultMap。如果同一 select 注解中还存在 `@Results` 或者 `@ConstructorArgs`，那么这两个注解将被此注解覆盖。|


- `@Select` 对应xml中 `<select>`
- `@Delete` 对应xml中 `<delete>`
- `@Update` 对应xml中 `<update>`
- `@Insert` 对应xml中 `<insert>`

## 设置(settings)

> 这是 MyBatis 中极为重要的调整设置，它们会改变 MyBatis 的运行时行为。 下表描述了设置中各项的意图、默认值等。

| 设置名	| 描述 |	有效值 | 默认值 |
| ----- | ---- | ----- | ----|
| `cacheEnabled` |	全局地开启或关闭配置文件中的所有映射器已经配置的任何缓存。| 	`true / false` |	`true` |
|`lazyLoadingEnabled` |	延迟加载的全局开关。当开启时，所有关联对象都会延迟加载。 特定关联关系中可通过设置 fetchType 属性来覆盖该项的开关状态。 |	`true / false` |	`false` |
|`aggressiveLazyLoading`| 当开启时，任何方法的调用都会加载该对象的所有属性。 否则，每个属性会按需加载（参考 lazyLoadTriggerMethods)。| `true / false`	| `false` （在 3.4.1 及之前的版本默认值为 `true`）|
|`multipleResultSetsEnabled` |	是否允许单一语句返回多结果集（需要驱动支持）。|	`true / false` | `true` |
|`useColumnLabel`|	使用列标签代替列名。不同的驱动在这方面会有不同的表现，具体可参考相关驱动文档或通过测试这两种不同的模式来观察所用驱动的结果。| `true / false` | `true`|
|`useGeneratedKeys`| 允许 JDBC 支持自动生成主键，需要驱动支持。 如果设置为 true 则这个设置强制使用自动生成主键，尽管一些驱动不能支持但仍可正常工作（比如 Derby）。|	`true / false`	| `false`|
|`autoMappingBehavior`|	指定 MyBatis 应如何自动映射列到字段或属性。 NONE 表示取消自动映射；PARTIAL 只会自动映射没有定义嵌套结果集映射的结果集。 FULL 会自动映射任意复杂的结果集（无论是否嵌套）。|	`NONE, PARTIAL, FULL` |	`PARTIAL`|
|`autoMappingUnknownColumnBehavior`| 指定发现自动映射目标未知列（或者未知属性类型）的行为。| `NONE`: 不做任何反应; `WARNING`: 输出提醒日志 (`'org.apache.ibatis.session.AutoMappingUnknownColumnBehavior'` 的日志等级必须设置为 WARN);`FAILING`: 映射失败 (抛出 `SqlSessionException`) | `NONE`, `WARNING`, `FAILING`, `NONE`|
|`defaultExecutorType`|	配置默认的执行器。`SIMPLE` 就是普通的执行器；`REUSE` 执行器会重用预处理语句（prepared statements）； `BATCH` 执行器将重用语句并执行批量更新。|	`SIMPLE`, `REUSE` ,`BATCH` |	`SIMPLE` |
|`defaultStatementTimeout`|	设置超时时间，它决定驱动等待数据库响应的秒数。|	任意正整数 |	未设置 (null)|
|`defaultFetchSize`| 为驱动的结果集获取数量（fetchSize）设置一个提示值。此参数只可以在查询设置中被覆盖。|	任意正整数 | 未设置 (null)|
|`safeRowBoundsEnabled`| 允许在嵌套语句中使用分页（RowBounds）。如果允许使用则设置为 false。|	`true`, `false` | `False` |
|`safeResultHandlerEnabled`| 允许在嵌套语句中使用分页（ResultHandler）。如果允许使用则设置为 `false`。| `true`, `false` |	`True` |
|`mapUnderscoreToCamelCase`| 是否开启自动驼峰命名规则（camel case）映射，即从经典数据库列名 A_COLUMN 到经典 Java 属性名 aColumn 的类似映射。|	`true` , `false` | `False`|
|`localCacheScope`|	MyBatis 利用本地缓存机制（Local Cache）防止循环引用（circular references）和加速重复嵌套查询。 默认值为 SESSION，这种情况下会缓存一个会话中执行的所有查询。 若设置值为 STATEMENT，本地会话仅用在语句执行上，对相同 SqlSession 的不同调用将不会共享数据。|	`SESSION`, `STATEMENT` |	`SESSION` |
|`jdbcTypeForNull`|	当没有为参数提供特定的 JDBC 类型时，为空值指定 JDBC 类型。 某些驱动需要指定列的 JDBC 类型，多数情况直接用一般类型即可，比如 `NULL`, `VARCHAR`, `OTHER`|	JdbcType 常量，常用值：`NULL`, `VARCHAR`, `OTHER`|`OTHER`|
|`lazyLoadTriggerMethods`|	指定哪个对象的方法触发一次延迟加载。|	用逗号分隔的方法列表。 |	`equals`,`clone`,`hashCode`,`toString` |
|`defaultScriptingLanguage`|	指定动态 SQL 生成的默认语言。|	一个类型别名或完全限定类名。|	`org.apache.ibatis.scripting.xmltags.XMLLanguageDriver`|
|`defaultEnumTypeHandler`|	指定 Enum 使用的默认 TypeHandler 。（新增于 3.4.5）|	一个类型别名或完全限定类名。 | `org.apache.ibatis.type.EnumTypeHandler`|
|`callSettersOnNulls`|	指定当结果集中值为 null 的时候是否调用映射对象的 setter（map 对象时为 put）方法，这在依赖于 Map.keySet() 或 null 值初始化的时候比较有用。注意基本类型（int、boolean 等）是不能设置成 null 的。 | `true` , `false` |	`false`|
|`returnInstanceForEmptyRow`|	当返回行的所有列都是空时，MyBatis默认返回 null。 当开启这个设置时，MyBatis会返回一个空实例。 请注意，它也适用于嵌套的结果集 （如集合或关联）。（新增于 3.4.2）	|` true` , `false`	| `false` |
|`logPrefix`|	指定 MyBatis 增加到日志名称的前缀。|	任何字符串 | 未设置 |
|`logImpl`|	指定 MyBatis 所用日志的具体实现，未指定时将自动查找。|	`SLF4J` ,` LOG4J` , `LOG4J2` , `JDK_LOGGING`, ` COMMONS_LOGGING` ,` STDOUT_LOGGING` ,` NO_LOGGING` |	未设置 |
|`proxyFactory`| 指定 Mybatis 创建具有延迟加载能力的对象所用到的代理工具。| `CGLIB`, `JAVASSIST` | `JAVASSIST` （MyBatis 3.3 以上）|
|`vfsImpl`|	指定 VFS 的实现 | 自定义 VFS 的实现的类全限定名，以逗号分隔。| 未设置 |
|`useActualParamName`|	允许使用方法签名中的名称作为语句参数名称。 为了使用该特性，你的项目必须采用 Java 8 编译，并且加上 -parameters 选项。（新增于 3.4.1）| `true`, `false` | `true` |
|`configurationFactory`| 指定一个提供 Configuration 实例的类。 这个被返回的 Configuration 实例用来加载被反序列化对象的延迟加载属性值。 这个类必须包含一个签名为static Configuration getConfiguration() 的方法。（新增于 3.2.3）| 未设置 |

### 一个配置例子

```xml
<settings>
  <setting name="cacheEnabled" value="true"/>
  <setting name="lazyLoadingEnabled" value="true"/>
  <setting name="multipleResultSetsEnabled" value="true"/>
  <setting name="useColumnLabel" value="true"/>
  <setting name="useGeneratedKeys" value="false"/>
  <setting name="autoMappingBehavior" value="PARTIAL"/>
  <setting name="autoMappingUnknownColumnBehavior" value="WARNING"/>
  <setting name="defaultExecutorType" value="SIMPLE"/>
  <setting name="defaultStatementTimeout" value="25"/>
  <setting name="defaultFetchSize" value="100"/>
  <setting name="safeRowBoundsEnabled" value="false"/>
  <setting name="mapUnderscoreToCamelCase" value="false"/>
  <setting name="localCacheScope" value="SESSION"/>
  <setting name="jdbcTypeForNull" value="OTHER"/>
  <setting name="lazyLoadTriggerMethods" value="equals,clone,hashCode,toString"/>
</settings>
```
