# netty 学习笔记

[github](https://github.com/netty/netty)
[gitee](https://gitee.com/mirrors/netty.git)

```gradle
 implementation("io.netty", "netty-all","4.1.66.Final")
```

[demo](https://gitee.com/dreamerlxb/netty-demo.git)
