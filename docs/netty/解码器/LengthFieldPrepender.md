# `LengthFieldPrepender`

> 预置消息长度的编码器

```java
 // 往 Pipeline 链中添加一个解码器
 ch.pipeline().addLast(new LengthFieldPrepender(4, false));
```

预置消息长度的编码器。 长度值以二进制形式预置。
例如， LengthFieldPrepender (2)将编码以下 12 字节的字符串：
+----------------+
| "HELLO, WORLD" |
+----------------+

进入以下内容：
+--------+----------------+
+ 0x000C | "HELLO, WORLD" |
+--------+----------------+

如果您在构造函数中打开lengthIncludesLengthFieldLength标志，则编码数据将如下所示（12（原始数据）+ 2（前置数据）= 14 (0xE)）：
+--------+----------------+
+ 0x000E | "HELLO, WORLD" |
+--------+----------------+