# `LengthFieldBasedFrameDecoder`

`io.netty.handler.codec.LengthFieldBasedFrameDecoder`

## 重要参数

* `maxFrameLength`：最大帧长度。也就是可以接收的数据的最大长度。如果超过，此次数据会被丢弃。

* `lengthFieldOffset`：长度域偏移。就是说数据开始的几个字节可能不是表示数据长度，需要后移几个字节才是长度域。

* `lengthFieldLength`：长度域字节数。用几个字节来表示数据长度。

* `lengthAdjustment`：数据长度修正。因为长度域指定的长度可以是`header+body`的整个长度，也可以只是`body`的长度。如果表示`header+body`的整个长度，那么我们需要修正数据长度。

* `initialBytesToStrip`：跳过的字节数。如果你需要接收`header+body`的所有数据，此值就是0，如果你只想接收body数据，那么需要跳过header所占用的字节数。

## `lengthFieldLength` 长度域字节数

> 偏移量 0 处的 2 字节长度字段，不剥离头部

```javadoc
本例中长度字段的值为12（0x0C） ，代表“HELLO, WORLD”的长度。 默认情况下，解码器假定长度字段表示长度字段后面的字节数。 因此，它可以用简单的参数组合来解码

   lengthFieldOffset   = 0  长度域不偏移
   lengthFieldLength   = 2  长度域占两个字节
   lengthAdjustment    = 0  
   initialBytesToStrip = 0 (= do not strip header)
  
   BEFORE DECODE (14 bytes)         AFTER DECODE (14 bytes)
   +--------+----------------+      +--------+----------------+
   | Length | Actual Content |----->| Length | Actual Content |
   | 0x000C | "HELLO, WORLD" |      | 0x000C | "HELLO, WORLD" |
   +--------+----------------+      +--------+----------------+
```

## `initialBytesToStrip`

> 偏移量 0 处的 2 字节长度字段，带头

```javadoc
因为我们可以通过调用ByteBuf.readableBytes()来获取内容的长度，所以您可能希望通过指定initialBytesToStrip来去除长度字段。
 在这个例子中，我们指定了2 ，与长度字段的长度相同，以剥离前两个字节

   lengthFieldOffset   = 0   长度域不偏移
   lengthFieldLength   = 2   长度域占两个字节
   lengthAdjustment    = 0
   initialBytesToStrip = 2 (= the length of the Length field)
  
   BEFORE DECODE (14 bytes)         AFTER DECODE (12 bytes)
   +--------+----------------+      +----------------+
   | Length | Actual Content |----->| Actual Content |
   | 0x000C | "HELLO, WORLD" |      | "HELLO, WORLD" |
   +--------+----------------+      +----------------+
```

## `lengthAdjustment`

> 偏移量0处的2字节长度字段，不剥离头部，长度字段代表整个消息的长度

```javadoc

在大多数情况下，长度字段仅表示消息正文的长度，如前面的示例所示。 但是，在某些协议中，长度字段表示整个消息的长度，包括消息头。 在这种情况下，我们指定一个非零的lengthAdjustment 。 由于本示例消息中的长度值始终比正文长度大2 ，因此我们指定-2作为lengthAdjustment进行补偿。

lengthFieldOffset   =  0
   lengthFieldLength   =  2
   lengthAdjustment    = -2 (= the length of the Length field)
   initialBytesToStrip =  0
  
   BEFORE DECODE (14 bytes)         AFTER DECODE (14 bytes)
   +--------+----------------+      +--------+----------------+
   | Length | Actual Content |----->| Length | Actual Content |
   | 0x000E | "HELLO, WORLD" |      | 0x000E | "HELLO, WORLD" |
   +--------+----------------+      +--------+----------------+
```

## 其他情况

参考 io.netty.handler.codec.LengthFieldBasedFrameDecoder
