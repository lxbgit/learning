# freemarker 模板引擎

> [github](https://github.com/apache/freemarker)

```maven
<dependency>
    <groupId>org.freemarker</groupId>
    <artifactId>freemarker</artifactId>
    <version>{version}</version>
</dependency>
```

```gradle
implementation("org.freemarker:freemarker:${version}")
```
