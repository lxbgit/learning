# thymeleaf 内联

 [参考1](https://www.thymeleaf.org/doc/tutorials/3.0/usingthymeleaf.html#text-inlining)
 [参考2](https://www.thymeleaf.org/doc/tutorials/3.0/usingthymeleaf.html#textual-syntax)

- text

```text
// th:inline="text"  时，使用以下
    // [# th:insert="forum/articles/_md_toolbar :: mdScript('99999999')" /]
```

- javascript

> 输出变量时会json序列化，

例如：

```html
 这段代码说明：1如何在js中使用权限；2如何在js中引入其他js片段（里面的每个字符都有含义）

<script th:inline="javascript">
/*[# sec:authorize="isFullyAuthenticated()"]*/
    /*[# th:insert="forum/articles/_md_toolbar :: mdScript('99999999')" /]*/
/*[/]*/
```

- css

> 暂未使用

```
<style th:inline="css">
</style>
```
    
- none

> 不使用内联，`[[]]`和`[()]`不会解析
    
