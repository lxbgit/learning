#  thymeleaf 片段（fragment）

[参考](https://www.thymeleaf.org/doc/tutorials/3.0/usingthymeleaf.html#difference-between-thinsert-and-threplace-and-thinclude)

- `th:fragment` 定义一个片段

    - 无参数
    ```text
      <div class="frag" th:fragment="f">fragment 内容</div>
    ```

    - 有参数
    ```html
      <div class="frag" th:fragment="f(param1, param1 ... )">fragment 内容</div>
    ```

- `th:insert`

> 保留自己的主标签，保留th:fragment的主标签

```html
<div class="main" th:insert="~{footer :: f}"></div>
```

结果

```html
<div class="main">
    <div class="frag">fragment 内容</div>
</div>
```

- `th:inlude`

> 不建议使用，和`th:insert` 类似，但会保留自己的主标签，并将`th:fragment`的内容插入主标签

```html
<div th:include="~{footer :: f}"></div>
```

结果

```html
<div class="main">
    fragment 内容
</div>
```

- `th:replace`

> 不要自己的主标签，保留th:fragment的主标签 

```html
<div class="main" th:insert="~{footer :: f}"></div>
```

结果

```html
<div class="frag">
fragment 内容
</div>
```
