# thymeleaf

[官网](https://www.thymeleaf.org/)

[github](https://github.com/thymeleaf/thymeleaf)

```maven
<dependency>
  <groupId>org.thymeleaf</groupId>
  <artifactId>thymeleaf</artifactId>
  <version>3.0.12.RELEASE</version>
</dependency>
```

```gradle
implemention("org.thymeleaf:thymeleaf:3.0.12.RELEASE")
```

[文档](https://www.thymeleaf.org/documentation.html)
