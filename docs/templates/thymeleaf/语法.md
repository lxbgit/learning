# thymeleaf 语法

- `th:fragment`

```text
th:insert  ：保留自己的主标签，保留th:fragment的主标签。
th:replace ：不要自己的主标签，保留th:fragment的主标签。
th:include ：保留自己的主标签，不要th:fragment的主标签。（官方3.0后不推荐）
```

例子(定义`fragment`)：

```html
<div xmlns:th="http://www.thymeleaf.org" th:fragment="header(title)">
```

使用 `<th:block th:insert="forum/taglib :: adminHead('项目管理|Issue')"/>`

- 文本替换 |...|

```html
<li th:class="|layui-nav-item ${index == 2 ? 'layui-this' : ''}|">
    <a th:href="@{/projects}">项目</a>
</li>
```

- `th:each="e,eState : ${emps}"`

> 其中e为循环的每一项，eState是下标属性(可省略)，eState属性包括

```
index：列表状态的序号，从0开始；
count：列表状态的序号，从1开始；
size：列表状态，列表数据条数；
current：列表状态，当前数据对象
even：列表状态，是否为奇数，boolean类型
odd：列表状态，是否为偶数，boolean类型
first：列表状态，是否为第一条，boolean类型
last：列表状态，是否为最后一条，boolean类型
```

- `th:if`

```
<!--th:if 条件判断，类似的有th:switch，th:case，优先级仅次于th:each,-->
<p th:text="${map.Boss.name}" th:if="${map.Boss.age gt 20}"></p>
```

运算关系

```
gt：great than（大于）
ge：great equal（大于等于）
eq：equal（等于）
lt：less than（小于）
le：less equal（小于等于）
ne：not equal（不等于）
```

```
[[…]]
表示th:text
会转义特殊字符

[(…)]
表示th:utext
不会转义特殊字符
```

## thymeleaf tags

`th:remove`的值如下:

- `all`:删除包含标签和所有的孩子。
- `body`:不包含标记删除,但删除其所有的孩子。
- `tag`:包含标记的删除,但不删除它的孩子。
- `all-but-first`:删除所有包含标签的孩子,除了第一个。
- `none`:什么也不做。这个值是有用的动态评估。

`th:insert`  ：保留自己的主标签，保留`th:fragment`的主标签。
`th:replace` ：不要自己的主标签，保留`th:fragment`的主标签。
`th:include` ：保留自己的主标签，不要`th:fragment`的主标签。（官方3.0后不推荐）

- `#dates`:`java.util.Date` 对象的实用方法。
- `#calendars`:和`dates`类似, 但是`java.util.Calendar`对象.
- `#numbers`: 格式化数字对象的实用方法。
- `#strings`: 字符创对象的实用方法： `contains`, `startsWith`, `prepending`/`appending`等.
- `#objects`: 对`objects`操作的实用方法。
- `#bools`: 对布尔值求值的实用方法。
- `#arrays`: 数组的实用方法。
- `#lists`:`list`的实用方法。
- `#sets`: `set`的实用方法。
- `#maps`: `map`的实用方法。
- `#aggregates`: 对数组或集合创建聚合的实用方法。
- `#messages`: 在表达式中获取外部信息的实用方法。
- `#ids`: 处理可能重复的id属性的实用方法 (比如：迭代的结果)。

## thymeleaf 日期格式化

```
/*
 * Format date with the standard locale format
 * Also works with arrays, lists or sets
 */
${#temporals.format(temporal)}
${#temporals.arrayFormat(temporalsArray)}
${#temporals.listFormat(temporalsList)}
${#temporals.setFormat(temporalsSet)}

/*
 * Format date with the standard format for the provided locale
 * Also works with arrays, lists or sets
 */
${#temporals.format(temporal, locale)}
${#temporals.arrayFormat(temporalsArray, locale)}
${#temporals.listFormat(temporalsList, locale)}
${#temporals.setFormat(temporalsSet, locale)}

/*
 * Format date with the specified pattern
 * SHORT, MEDIUM, LONG and FULL can also be specified to used the default java.time.format.FormatStyle patterns
 * Also works with arrays, lists or sets
 */
${#temporals.format(temporal, 'dd/MMM/yyyy HH:mm')}
${#temporals.arrayFormat(temporalsArray, 'dd/MMM/yyyy HH:mm')}
${#temporals.listFormat(temporalsList, 'dd/MMM/yyyy HH:mm')}
${#temporals.setFormat(temporalsSet, 'dd/MMM/yyyy HH:mm')}

/*
 * Format date with the specified pattern and locale
 * Also works with arrays, lists or sets
 */
${#temporals.format(temporal, 'dd/MMM/yyyy HH:mm', locale)}
${#temporals.arrayFormat(temporalsArray, 'dd/MMM/yyyy HH:mm', locale)}
${#temporals.listFormat(temporalsList, 'dd/MMM/yyyy HH:mm', locale)}
${#temporals.setFormat(temporalsSet, 'dd/MMM/yyyy HH:mm', locale)}

/*
 * Format date with ISO-8601 format
 * Also works with arrays, lists or sets
 */
${#temporals.formatISO(temporal)}
${#temporals.arrayFormatISO(temporalsArray)}
${#temporals.listFormatISO(temporalsList)}
${#temporals.setFormatISO(temporalsSet)}

/*
 * Obtain date properties
 * Also works with arrays, lists or sets
 */
${#temporals.day(temporal)}                    // also arrayDay(...), listDay(...), etc.
${#temporals.month(temporal)}                  // also arrayMonth(...), listMonth(...), etc.
${#temporals.monthName(temporal)}              // also arrayMonthName(...), listMonthName(...), etc.
${#temporals.monthNameShort(temporal)}         // also arrayMonthNameShort(...), listMonthNameShort(...), etc.
${#temporals.year(temporal)}                   // also arrayYear(...), listYear(...), etc.
${#temporals.dayOfWeek(temporal)}              // also arrayDayOfWeek(...), listDayOfWeek(...), etc.
${#temporals.dayOfWeekName(temporal)}          // also arrayDayOfWeekName(...), listDayOfWeekName(...), etc.
${#temporals.dayOfWeekNameShort(temporal)}     // also arrayDayOfWeekNameShort(...), listDayOfWeekNameShort(...), etc.
${#temporals.hour(temporal)}                   // also arrayHour(...), listHour(...), etc.
${#temporals.minute(temporal)}                 // also arrayMinute(...), listMinute(...), etc.
${#temporals.second(temporal)}                 // also arraySecond(...), listSecond(...), etc.
${#temporals.nanosecond(temporal)}             // also arrayNanosecond(...), listNanosecond(...), etc.

/*
 * Create temporal (java.time.Temporal) objects from its components
 */
${#temporals.create(year,month,day)}                                // return a instance of java.time.LocalDate
${#temporals.create(year,month,day,hour,minute)}                    // return a instance of java.time.LocalDateTime
${#temporals.create(year,month,day,hour,minute,second)}             // return a instance of java.time.LocalDateTime
${#temporals.create(year,month,day,hour,minute,second,nanosecond)}  // return a instance of java.time.LocalDateTime

/*
 * Create a temporal (java.time.Temporal) object for the current date and time
 */
${#temporals.createNow()}                      // return a instance of java.time.LocalDateTime
${#temporals.createNowForTimeZone(zoneId)}     // return a instance of java.time.ZonedDateTime
${#temporals.createToday()}                    // return a instance of java.time.LocalDate
${#temporals.createTodayForTimeZone(zoneId)}   // return a instance of java.time.LocalDate

/*
 * Create a temporal (java.time.Temporal) object for the provided date
 */
${#temporals.createDate(isoDate)}              // return a instance of java.time.LocalDate
${#temporals.createDateTime(isoDate)}          // return a instance of java.time.LocalDateTime
${#temporals.createDate(isoDate, pattern)}     // return a instance of java.time.LocalDate
${#temporals.createDateTime(isoDate, pattern)} // return a instance of java.time.LocalDateTime
```
