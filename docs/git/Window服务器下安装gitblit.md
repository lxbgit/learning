# Window服务器下安装git

一、安装Java（不啰嗦了）

二、检查环境

三、下载gitblit

- [下载地址](http://www.gitblit.com/)

- 解压缩下载的压缩包即可，无需安装

- 创建用于存储资料的文件夹。（`G:\git\gitrepo`）

- 修改配置文件配置`gitblit.properties` 文件
4.1、找到Git目录下的data文件下的`gitblit.properties`文件，“记事本”打开
4.2、找到`git.repositoriesFolder`(资料库路径)，赋值为第3步创建好的文件目录
4.3、找到`server.httpPort`，设定http协议的端口号
4.4、找到`server.httpBindInterface`，设定服务器的IP地址。这里就设定你的服务器IP
4.5、找到`server.httpsBindInterface`，设定为localhost
4.6、保存，关闭文件

- 运行`gitblit.cmd` 批处理文件
- 在浏览器中打开就可以访问了 （ip:port, 默认账户密码admin）
