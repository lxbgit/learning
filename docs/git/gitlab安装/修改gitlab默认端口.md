# 修改gitlab默认端口

- 编辑 `/etc/gitlab/gitlab.rb`

```shell
#外部访问url（经过编译后，自动将这个配置编译到nginx配置，nginx就无需配置了）
external_url 'http://gitlab.test.domain.com:8888'
#默认值就是8080。如果端口被占用，可将8080修改为其它（例如：9090）
unicorn['port'] = 8080
```

> 注意需要在防火墙中开放端口，具体防火墙的使用，参见[防火墙](../../Linux/centos/防火墙.md)
