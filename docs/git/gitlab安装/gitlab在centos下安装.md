# git 安装

[gitlab镜像](https://mirrors.tuna.tsinghua.edu.cn/gitlab-ce/yum/)

[参考1](https://blog.csdn.net/duyusean/article/details/80011540)

[参考2](https://segmentfault.com/a/1190000021278114)

[参考3](https://www.jianshu.com/p/b04356e014fa)

## gitlab

- yum安装

### 新建 `/etc/yum.repos.d/gitlab_gitlab-ce.repo`，内容为

```shell
[gitlab-ce]
name=Gitlab CE Repository
baseurl=https://mirrors.tuna.tsinghua.edu.cn/gitlab-ce/yum/el$releasever/
gpgcheck=0
enabled=1
```

### 安装依赖

```shell
sudo yum install curl openssh-server openssh-clients postfix cronie
sudo service postfix start
sudo chkconfig postfix on
#这句是用来做防火墙的，避免用户通过ssh方式和http来访问。
sudo lokkit -s http -s ssh
```

### GitLab使用postfix发送邮件

- 启动postfix 服务

```shell
service postfix start
```

- 设置postfix开机自启动

```shell
chkconfig postfix on
```

### 下载

```shell
wget https://mirrors.tuna.tsinghua.edu.cn/gitlab-ce/yum/el6/gitlab-ce-11.10.0-ce.0.el6.x86_64.rpm
```

### 安装

```shell
rpm -i gitlab-ce-11.10.0-ce.0.el6.x86_64.rpm
```

### 执行

```shell
sudo yum makecache
sudo yum install gitlab-ce
sudo gitlab-ctl reconfigure  #Configure and start GitLab
```

### 修改密码

```shell
> gitlab-rails console production
> user=User.where(id:1).first
> user.password='123456'
> user.save!
> quit
```

### 修改gitlab配置文件指定服务器ip和自定义端口

编辑：

```shell
vim  /etc/gitlab/gitlab.rb
```

#### gitlab基本配置

```shell
#外部访问url（经过编译后，自动将这个配置编译到nginx配置，nginx就无需配置了）
external_url 'http://gitlab.test.domain.com:8888'
#默认值就是8080。如果端口被占用，可将8080修改为其它（例如：9090）
unicorn['port'] = 8080
```

#### gitlab发送邮件配置

```shell
gitlab_rails['smtp_enable'] = true
gitlab_rails['smtp_address'] = "smtp.exmail.qq.com"
gitlab_rails['smtp_port'] = 25
gitlab_rails['smtp_user_name'] = "huangdc@domain.com"
gitlab_rails['smtp_password'] = "smtp password"
gitlab_rails['smtp_authentication']= "plain"
gitlab_rails['smtp_enable_starttls_auto']= true
gitlab_rails['gitlab_email_from']= 'huangdc@domain.com'
gitlab_rails['gitlab_email_reply_to']= 'noreply@domain.com'
```

### 使配置生效

```shell
gitlab-ctl reconfigure
#重新启动GitLab
gitlab-ctl restart
```

### gitlab命令

- 查看gitlab版本号

```shell
cat /opt/gitlab/embedded/service/gitlab-rails/VERSION
```

- 启动所有 gitlab 组件

```shell
gitlab-ctl start    # 启动所有 gitlab 组件
```

- 停止所有 gitlab 组件

```shell
gitlab-ctl stop
```

- 重启所有 gitlab 组件

```shell
gitlab-ctl restart
```

- 查看服务状态

```shell
gitlab-ctl status
```

- 查看日志

```shell
gitlab-ctl tail
```

- 修改默认的配置文件

```shell
vim /etc/gitlab/gitlab.rb
```

> 修改完配置完成后，`gitlab-ctl reconfigure` 重新启动服务；

- 检查gitlab

```shell
gitlab-rake gitlab:check SANITIZE=true --trace
```

### 问题

- 错误1 policycoreutils-python依赖检测失败

```shell
[root@cscec82-01 ~]# rpm -i gitlab-ce-11.10.0-ce.0.el6.x86_64.rpm
警告：gitlab-ce-11.10.0-ce.0.el6.x86_64.rpm: 头V4 RSA/SHA1 Signature, 密钥 ID f27eab47: NOKEY
错误：依赖检测失败：
	policycoreutils-python 被 gitlab-ce-11.10.0-ce.0.el6.x86_64 需要
```

解决办法：

```shell
# 安装依赖（centos 7）
yum install policycoreutils-python

# 如果是centos8,换一个gitlab版本
```
