# Git 常用命令2

## `git fetch`

1.第一种

- 将远程仓库的master分支下载到本地当前branch中
`git fetch orgin master`

- 比较本地的master分支和origin/master分支的差别
`git log -p master  ..origin/master`

- 进行合并
`git merge origin/master`

2.第二种

- 从远程仓库master分支获取最新，在本地建立临时tmp分支
`git fetch origin master:tmp`

- 将当前分支和tmp对比
`git diff tmp`

- 合并tmp分支到当前分支
`git merge tmp`

- 重新提交本地主分支到远程仓库
`git push -u origin master`

- 删除临时分支
`git branch -d tmp`

## `git pull`

> 相当于是从远程获取最新版本并merge到本地

## 回退版本

### `git reset`

`git reset[--soft | --mixed | --hard] [HEAD]`

> 命令用于回退版本，可以指定退回某一次提交的版本。

- `--soft`

用于回退到某个版本

```git
git reset --soft HEAD~3  # 回退上上上一个版本
```

- `--mixed`

为默认，可以不用带该参数，用于重置暂存区的文件与上一次的提交(commit)保持一致，工作区文件内容保持不变

```git
 git reset HEAD^            # 回退所有内容到上一个版本  
 git reset HEAD^ hello.php  # 回退 hello.php 文件的版本到上一个版本  
 git reset  052e           # 回退到指定版本
```

- `--hard`

参数撤销工作区中所有未提交的修改内容，将暂存区与工作区都回到上一次版本，并删除之前的所有信息提交：

```git
 git reset –hard HEAD~3  # 回退上上上一个版本  
 git reset –hard bae128  # 回退到某个版本回退点之前的所有信息。 
 git reset --hard origin/master    # 将本地的状态回退到和远程的一样 
```

### 注意：谨慎使用 –hard 参数，它会删除回退点之前的所有信息。

HEAD 说明：

HEAD 表示当前版本

HEAD^ 上一个版本

HEAD^^ 上上一个版本

HEAD^^^ 上上上一个版本

以此类推...

可以使用 ～数字表示
HEAD~0 表示当前版本

HEAD~1 上一个版本

HEAD^2 上上一个版本

HEAD^3 上上上一个版本

以此类推...

### `git revert`

> 保留每次记录, git revert是用一次新的commit来回滚之前的commit，git reset是直接删除指定的commit

使用 `git revert -n  hds882378234`

https://www.git-scm.com/docs/git-revert
