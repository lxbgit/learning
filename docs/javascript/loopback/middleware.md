# [middleware配置属性](http://loopback.io/doc/en/lb3/Defining-middleware.html#middleware-configuration-properties)

- `name`  String类型

- `enabled` boolean类型

- `params` Object/Array

传递给中间件`function`的参数，大多数中间件 `constructors` 都会有一个参数`options`,即为`params`的值

- `methods` String[]
- `paths` String[]
- `optional` Boolean

> 翻译的不是很准确，参见[中间件配置属性](http://loopback.io/doc/en/lb3/Defining-middleware.html#middleware-configuration-properties)

## [中间件的定义](http://loopback.io/doc/en/lb3/Defining-middleware.html)

```js
var LoopBackContext = require('loopback-context');
// 此处的`options`即为上述定义的`params`中的参数
module.exports = function(options) {
  'use strict';

  let UserModel = null;

  return function storeCurrentUser(req, res, next) {
    var app = req.app;
    var registry = app.registry;
    if (!UserModel) {
      UserModel = registry.getModel('Account');
    }

    if (!req.accessToken) {
      return next();
    }

    UserModel.findById(req.accessToken.userId, function(err, user) {
      if (err) {
        return next(err);
      }
      if (!user) {
        return next(new Error('No user with this access token was found.'));
      }
      var loopbackContext = LoopBackContext.getCurrentContext();
      if (loopbackContext) {
        loopbackContext.set('currentUser', user);
      }
      next();
    });
  };
};
```

## demo

```json
{
  "initial:before": {
    "loopback#favicon": {}
  },
  "initial": {
    "compression": {},
    "cors": {
      "params": {
        "origin": true,
        "credentials": true,
        "maxAge": 86400
      }
    },
    "helmet#xssFilter": {},
    "helmet#frameguard": {
      "params": [
        "deny"
      ]
    },
    "helmet#hsts": {
      "params": {
        "maxAge": 0,
        "includeSubdomains": true
      }
    },
    "helmet#hidePoweredBy": {},
    "helmet#ieNoOpen": {},
    "helmet#noSniff": {},
    "helmet#noCache": {
      "enabled": false
    },
    "loopback-context#per-request": {
    }
  },
  "session": {},
  "auth": {
    "loopback#token": {
// 可以查看token源码
// 这的意思是指定AccessToken对应的model
      "params": {
        "model": "accessToken"
      }
    }
  },
  "auth:after": {
    "./middleware/store-current-user": {}
  },
  "parse": {},
  "routes": {
    "loopback#rest": {
      "paths": [
        "${restApiRoot}"
      ]
    }
  },
  "files": {
    "loopback#static": {
      "params": "$!../client/app"
    }
  },
  "final": {
    "loopback#urlNotFound": {}
  },
  "final:after": {
    "strong-error-handler": {
      "params": {
        "debug": true,
        "log": true
      }
    }
  }
}
```
