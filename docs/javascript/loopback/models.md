# 在`server/models`或者`common/models`下定义model

- 定义`model` 例子：

```json
{
  "name": "Account",
  "base": "User",
  "idInjection": true,
  "options": {
    "validateUpsert": true
  },
  "properties": {
    "name": {
      "type": "string",
      "length": "32"
    },
    "username": {
      "type": "string",
      "default": "",
      "length": "32"
    },
    "masterId": {
      "type": "number"
    },
    "email": {
      "type": "string",
      "default": "",
      "length": "32"
    },
    "password": {
      "type": "string",
      "length": "128"
    },
    "status": {
      "type": "string",
      "length": "16"
    },
    "created": {
      "type": "date"
    },
    "lastUpdated": {
      "type": "date"
    }
  },
  "validations": [],
  "relations": {
    "accessTokens": {
      "type": "hasMany",
      "model": "accessToken",
      "foreignKey": "userId"
    },
    "master": {
      "type": "belongsTo",
      "model": "Account",
      "foreignKey": "masterId"
    },
    "underlings": {
      "type": "hasMany",
      "model": "Account",
      "foreignKey": "masterId"
    },
    "allocations": {
      "type": "hasMany",
      "model": "Allocation",
      "foreignKey": "userId"
    }
  },
  "acls": [{
    "principalType": "ROLE",
    "principalId": "$owner",
    "permission": "ALLOW"
  }],
  "methods": {}
}
```


- 定义model

```json
{
  "name": "XXXX",
  "plural": "XXXXs",
  "base": "parent",
  "idInjection": true,
  "options": {
  },
  "properties": {},
  "validations": [],
  "relations": {},
  "acls": [],
  "methods": {}
}
```

