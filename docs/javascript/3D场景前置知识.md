# 3D 场景前置知识

[参考](https://www.zhihu.com/question/36367846)

## 基础

1.场景(Scene)：是物体、光源等元素的容器，可以配合 chrome 插件使用，抛出 window.scene即可实时调整 obj 的信息和材质信息。
2.相机（Camera）：场景中的相机，代替人眼去观察，场景中只能添加一个，一般常用的是透视相机（PerspectiveCamera）
3.物体对象（Mesh）：包括二维物体（点、线、面）、三维物体，模型等等
4.光源（Light）：场景中的光照，如果不添加光照场景将会是一片漆黑，包括全局光、平行光、点光源等
5.渲染器（Renderer）:场景的渲染方式，如webGL\canvas2D\Css3D。
6.控制器(Control): 可通过键盘、鼠标控制相机的移动

## 相机

`Three.js`中我们常用的有两种类型的相机：`正交（orthographic）相机`、`透视（perspective）相机`。一般情况下为了模拟人眼我们都是使用透视相机；

- 正交镜头的特点是，物品的渲染尺寸与它距离镜头的远近无关。也就是说在场景中移动一个物体，其大小不会变化。正交镜头适合2D游戏。

- 透视镜头则是模拟人眼的视觉特点，距离远的物体显得更小。透视镜头通常更适合3D渲染。

### `THREE.PerspectiveCamera(fov,aspect,near,far)`

> 参数描述 fov视野角度，从镜头可以看到的场景的部分。通常3D游戏的FOV取值在60-90度之间较好的默认值为60 aspect渲染区域的纵横比。较好的默认值为window.innerWidth/window.innerHeight near最近离镜头的距离 far远离镜头的距离

创建摄像机以后还要对其进行移动、然后对准物体积聚的场景中心位置，分别是设置其 position和调用 lookAt 方法，参数均是一个 xyz向量(new THREE.Vector3(x,y,z))

```
camera.position：控制相机在整个3D环境中的位置（取值为3维坐标对象-THREE.Vector3(x,y,z)）
camera.lookAt：控制相机的焦点位置，决定相机的朝向（取值为3维坐标对象-THREE.Vector3(x,y,z)）
```

## 灯光

> 在Three.js中光源是必须的，如果一个场景你不设置灯光那么世界将会是一片漆黑。Three.js内置了多种光源以满足特定场景的需要。大家可以根据自己的项目需要来选择何种灯光。

### 光源分类

|    光源    |   说明    |
|-----------|-----------|
|AmbientLight|环境光，其颜色均匀的应用到场景及其所有对象上，这种光源为场景添加全局的环境光|
|PointLight|3D空间中的一个点光源，向所有方向发出光线|
|SpotLight|产生圆锥光柱的聚灯光，台灯，天花板射灯通常都属于这类光源|
|DirectionalLight|无线光，光线是平行的。典型的例子是日光，用于模拟遥远的，类似太阳那样的光源|
|HemisphereLight|特殊光源，用于创建户外自然的光线效果|
|AreaLight|面光源，指定一个发光的区域|
|LensFlare|不是光源，用于给光源添加镜头光晕效果|

## `Mesh`

> 在计算机的世界里，一条弧线是由有限个点构成的有限条线段连接得到的。当线段数量越多，长度就越短，当达到你无法察觉这是线段时，一条平滑的弧线就出现了。 计算机的三维模型也是类似的。只不过线段变成了平面，普遍用三角形组成的网格来描述。我们把这种模型称之为 `Mesh` 模型。 在 threeJs 的世界中，材质(`Material`)+几何体(`Geometry`)就是一个 `mesh`。设置其name属性可以通过`scene.getObjectByName(name)`获取该物体对象;`Geometry`就好像是骨架，材质则类似于皮肤，对于材质和几何体的分类见下表格

### 材质分类

|    材质    |   说明    |
|-----------|-----------|
|MeshBasicMaterial|基本的材质，显示为简单的颜色或者显示为线框，不考虑光线的影响|
|MeshDepthMaterial|使用简单的颜色，但是颜色深度和距离相机的远近有关|
|MeshNormalMaterial|基于面Geometry的法线（normals）数组来给面着色|
|MeshFaceMaterial|容器，允许Geometry的每一个面指定一个材质|
|MeshLambertMaterial|考虑光线的影响，哑光材质|
|MeshPhongMaterial|考虑光线的影响，光泽材质|
|ShaderMaterial|允许使用自己的着色器来控制顶点如何被放置，像素如何被着色|
|LineBasicMaterial|用于THREE.Line对象，创建彩色线条|
|LineDashMaterial|用于THREE.Line对象，创建虚线条|
|RawShaderMaterial|仅和用于THREE.BufferedGeometry联用，优化静态Geometry(顶点、面不变)的渲染|
|SpriteCanvasMaterial|在针对单独的点进行渲染时用到|
|SpriteMaterial|在针对单独的点进行渲染时用到|
|PointCloudMaterial|在针对单独的点进行渲染时用到|

### 几何图形

#### 2D

|    图形    |   说明    |
|-----------|-----------|
|矩形 `THREE.PlaneGeometry`|外观上是一个矩形|
|`THREE.CircleGeometry`|外观上是一个圆形或者扇形|
|`THREE.RingGeometry`|外观上是一个圆环或者扇环|
|`THREE.ShapeGeometry`|改形状允许你创建自定义的二维图形，其操作方式类似于SVG/Canvas的画布|

#### 3D

|    图形    |   说明    |
|-----------|-----------|
|矩形 `THREE.BoxGeometry`|这是一个具有长宽高的盒子|
|`THREE.SphereGeometry`|这是一个三维球体/不完整球体|
|`THREE.CylinderGeometry`|可绘制圆柱、圆筒。圆锥或者截锥|

## 加载外部模型

一般来讲我们的场景中不可能都是一些奇奇怪怪的形状，或多或少项目中都会用到一些外部的模型资源，不如动物啊，装饰物啊什么的，再加上一些动画，这样整个场景更加显得生动，那么 threejs 中我们可以通过哪些方式来加载外部的模型资源呢？

加载外部模型，是通过Three.js加载器（Loader）实现的。加载器把文本/二进制的模型文件转化为Three.js对象结构。 每个加载器理解某种特定的文件格式。

需要注意的是，由于贴图的尺寸必须是(2的幂数)X (2的幂数)，如：1024X512，所以为了防止贴图变形，平面的宽度比例需要与贴图的比例一致

### 支持的格式

|    格式    |   说明    |
|-----------|-----------|
|JSON|Three.js自定义的、基于JSON的格式。可以声明式的定义一个Geometry或者Scence。利用改格式，你可以方便的重用复杂的Gepmetry或者Scene|
|OBJ/MTL|obj是Wavefront开发的一种简单的3D格式，|
|PLY| 常用于存储来自3D扫描仪的信息 |

## 粒子

THREE.Sprite

> 在`WebGlRenderer`渲染器中使用`THREE.Sprite`创建的粒子可以直接添加到`scene`中。创建出来的精灵总是面向镜头的。即不会有倾斜变形之类透视变化，只有近大远小的变化。

## 场景交互

`Three.js`中并没有直接提供“点击”功能，一开始使用的时候我也觉得一脸懵逼，后来才发现我们可以基于`THREE.Raycaster`来判断鼠标当前对应到哪个物体,用来进行碰撞检测.

## 动画

场景中如果我们添加了各种 `mesh` 和模型并给他加入了一些 tweend动画会发现他并不会运动，因为你的场景并没有实时渲染，所以要让场景真的动起来，我们需要用到`requestAnimationFrame`；
