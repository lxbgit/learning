# 录屏

<!--	google 实现 录屏注意：
需要在 chrome://flags/  开启 Experimental Web Platform features
 -->

## 重要的api

- `navigator.mediaDevices`

- `navigator.mediaDevices.getUserMedia`

> 火狐中使用

- `navigator.mediaDevices.getDisplayMedia`

> chrome 中使用

- `MediaRecorder`

> 本地保存

- 重要代码

```js
if (navigator.userAgent.indexOf("Firefox") != -1) {
    navigator.mediaDevices.getUserMedia({
        video: { mediaSource: 'screen' },
    }).then(gotMediaStream)
        .catch(function (err) {
            console.log('navigator.mediaDevices.getUserMedia error:', err);
        });
}else if (navigator.getDisplayMedia) {
    navigator.getDisplayMedia({video: true,})
        .then(gotMediaStream)
        .catch(function (err) {
            console.log('navigator.getDisplayMedia error:', err);
        });
} else if(navigator.mediaDevices.getDisplayMedia) {
    navigator.mediaDevices.getDisplayMedia({video: true,})
        .then(gotMediaStream)
        .catch(function (err) {
            console.log('navigator.mediaDevices.getDisplayMedia error:', err);
        });
} else {
    navigator.mediaDevices.getUserMedia({
        video: { mediaSource: 'screen' },
    }).then(gotMediaStream)
        .catch(function (err) {
            console.log('navigator.mediaDevices.getUserMedia error:', err);
        });
}
```

- 保存

```js
var options = {
    mimeType: 'video/webm;codecs=vp8'
}

if(!MediaRecorder.isTypeSupported(options.mimeType)){
    console.error(`${options.mimeType} is not supported!`);
    return;	
}

try{
    mediaRecorder = new MediaRecorder(window.stream, options);
}catch(e){
    console.error('Failed to create MediaRecorder:', e);
    return;
}

mediaRecorder.ondataavailable = handleDataAvailable;
mediaRecorder.start(10);

```

## 注意

判断浏览器是否为火狐

```js
navigator.userAgent.indexOf("Firefox") != -1
```
