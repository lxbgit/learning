# openlayer4

- `ol.Coordinate`

> An array of numbers representing an xy coordinate. Example: [16, 48].
表示XY坐标的数字数组。例如：[16, 48]

- `ol.geom. LineString`

> Linestring geometry. 线的几何形状; 代表是一条线

- `ol.proj.fromLonLat(coordinate, opt_projection)`

 参数 | 类型 | 解释
------ | ------- | --------
 `coordinate` | `ol.Coordinate` | 坐标
 `opt_projection` |  `ol.ProjectionLike` | 例如： "EPSG：4326"

> Transforms a coordinate from longitude/latitude to a different projection.
将坐标由经度/纬度变为不同的投影。

- `ol.proj. Projection`

## `ol.layer.Tile`

### 事件

- `chang`
- `postcompose`
- `precompose`

### 方法

- `once`   监听一个

> `once(type, listener, opt_this)`
`return ol.EventsKey |Array.<[ol.EventsKey]>`

## 展示GIF 图片 案例

[GitHUB](https://github.com/openlayers/openlayers/issues/11412)

[demo](https://deploy-preview-11565--ol-site.netlify.app/examples/animated-gif.html)
