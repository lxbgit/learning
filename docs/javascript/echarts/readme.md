# ECharts color属性设置（RGB，透明度，线性渐变，径向渐变，纹理填充）

1 颜色英文名
示例：

```js
color:'red'
```

2 十六进制格式
有两种表示方式。

第一种为#+6位十六进制数，前2位表示红色，中间2位表示绿色，后2位表示蓝色。

第二种为#+3位十六进制数，第1位表示红色，第2位表示绿色，第3位表示蓝色。

示例：

```js
color:"#ccc",
color:"#cccccc"
```

3 rgb
rgb()方法的第2个参数表示红色，第2个参数表示绿色，第3个参数表示蓝色。

```js
color:'rgb(128, 128, 128)'
```

4 rgba（透明度）
rgb()方法的第2个参数表示红色，第2个参数表示绿色，第3个参数表示蓝色，第4个参数表示透明度，1为不透明，0表示透明。

```js
color:'rgba(128, 128, 128,1)'
```

5 LinearGradient（线性渐变）

```js
color: [  //仪表盘背景颜色渐变
    [1,new echarts.graphic.LinearGradient(0, 0, 1, 0, 
        [
            {
                offset: 0.1,
                color: "#fd2100"
            },
            {
                offset: 0.6,
                color: "#d09f00"
            },
            {
                offset: 1,
                color: "#26fd00"
            }
        ]);
    ]
],
```

6 linear（线性渐变）
// 线性渐变，前四个参数分别是 x0, y0, x2, y2, 范围从 0 - 1，相当于在图形包围盒中的百分比，如果 globalCoord 为 `true`，则该四个值是绝对的像素位置

```js
color: {
    type: 'linear',
    x: 0,
    y: 0,
    x2: 0,
    y2: 1,
    colorStops: [{
        offset: 0, color: 'red' // 0% 处的颜色
    }, {
        offset: 1, color: 'blue' // 100% 处的颜色
    }],
    global: false // 缺省为 false
}
```

7 radial（径向渐变）
// 径向渐变，前三个参数分别是圆心 x, y 和半径，取值同线性渐变

```js
color: {
    type: 'radial',
    x: 0.5,
    y: 0.5,
    r: 0.5,
    colorStops: [{
        offset: 0, color: 'red' // 0% 处的颜色
    }, {
        offset: 1, color: 'blue' // 100% 处的颜色
    }],
    global: false // 缺省为 false
}
```

8 纹理填充
// 纹理填充

```js
color: {
    image: imageDom, // 支持为 HTMLImageElement, HTMLCanvasElement，不支持路径字符串
    repeat: 'repeat' // 是否平铺，可以是 'repeat-x', 'repeat-y', 'no-repeat'
}
```
