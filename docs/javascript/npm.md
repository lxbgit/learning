# npm 命令

[参考](https://hacpai.com/article/1558105860489)

```shell
➜  ~ npm get registry #查看当前npm源
https://registry.npmjs.org/
➜  ~ npm config set registry https://registry.npm.taobao.org #持久换源
➜  ~ npm get registry
https://registry.npm.taobao.org/
```

npm允许在package.json文件中配置，使用scripts字段定义脚本命令

```shell
npm init //生成package.json文件
npm install //安装文件插件
```

解决国内NPM安装依赖速度慢问题：

```shell
npm install --registry=http://registry.npm.taobao.org
```

只需要使用–registry参数指定镜像服务器地址，为了避免每次安装都需要--registry参数，可以使用如下命令进行永久设置：

`npm config set registry http://registry.npm.taobao.org`

## 通过改变地址来使用淘宝镜像

- npm的默认地址是`https://registry.npmjs.org/`
- 可以使用`npm config get registry`查看npm的仓库地址
- 可以使用`npm config set registry https://registry.npm.taobao.org`来改变默认下载地址，达到可以不安装cnpm就能采用淘宝镜像的目的，然后使用上面的get命令查看是否成功

## 开发的npm registry 管理工具 nrm, 能够查看和切换当前使用的registry;

- 安装 ： npm install -g nrm
- 使用：
$nrm ls  #查看当前的registry

```shell
$ nrm ls  # 查看当前的registry

* npm ---- https://registry.npmjs.org/
  cnpm --- http://r.cnpmjs.org/
  taobao - https://registry.npm.taobao.org/
  nj ----- https://registry.nodejitsu.com/
  rednpm - http://registry.mirror.cqupt.edu.cn/
  npmMirror  https://skimdb.npmjs.com/registry/
  edunpm - http://registry.enpmjs.org/
```

```shell
nrm use taobao # 切换registry 到taobao (国内更快一些)
```

## 其他命令

```shell
nrm help     # show help
nrm list      # show all registries
nrm use cnpm # switch to cnpm
nrm home  # go to a registry home page
```

## npm下载缓慢解决方法

npm的服务器在国外，拉取npm包的列表、下载包这个过程会比较缓慢。凡是包管理工具基本都有这个问题，例如maven、pip等，这些问题都可以通过配置镜像来解决。阿里巴巴提供了maven库，清华大学有pip源（还有其它多种源，如ubuntu）。淘宝部门提供了npm镜像，是国内最常使用的npm镜像。

---

## 方法1

- 如果只在本次安装中使用镜像，直接给npm指定registry参数即可

`npm install -gd express --registry=http://registry.npm.taobao.org`

- 如果一劳永逸，让镜像永远生效

 `npm config set registry http://registry.npm.taobao.org`

> 这个命令相当于直接修改~/.npmrc文件

## 方法2 安装`cnpm`

> 单单配置镜像并不能很好地解决npm安装缓慢的问题，要让整个过程直接访问国内服务器就需要安装cnpm，它会自动使用国内镜像

`npm install -g cnpm`

## n模块

> node有一个模块叫n（这名字可够短的。。。），是专门用来管理node.js的版本的。
首先安装n模块：

```shell
npm install -g n
```

第二步：
升级node.js到最新稳定版

```shell
n stable
```

是不是很简单？！
n后面也可以跟随版本号比如：

```shell
n v0.10.26
```

或

```shell
n 0.10.26
```

就这么简单，这可怎么办？？！！
另外分享几个npm的常用命令:

```shell
npm -v          #显示版本，检查npm 是否正确安装。

npm install express   #安装express模块

npm install -g express  #全局安装express模块

npm list         #列出已安装模块

npm show express     #显示模块详情

npm update        #升级当前目录下的项目的所有模块

npm update express    #升级当前目录下的项目的指定模块

npm update -g express  #升级全局安装的express模块

npm uninstall express  #删除指定的模块
```
