# underscore

[参考](https://www.liaoxuefeng.com/wiki/001434446689867b27157e896e74d51a89c25cc8b43bdb3000/001450372452505599881a3debd4becbe1591a94950fbb8000)

## collections

- map/filter

> map()和filter()可以作用于Object

```javascript

// 作用于对象
var upper = _.map(obj, function (value, key) {
    return xxxx;
});
```

- every / some

> 当集合的所有元素都满足条件时，`_.every()`函数返回`true`，当集合的至少一个元素满足条件时，`_.some()`函数返回`true`
> 当集合是`Object`时，我们可以同时获得`value`和`key`

- max / min

> 这两个函数直接返回集合中最大和最小的数; 注意，如果集合是`Object`，`max()`和`min()`只作用于`value`，忽略掉`key`

- groupBy

> `groupBy()`把集合的元素按照`key`归类，`key`由传入的函数返回

```javascript
var scores = [20, 81, 75, 40, 91, 59, 77, 66, 72, 88, 99];
var groups = _.groupBy(scores, function (x) {
    if (x < 60) {
        return 'C';
    } else if (x < 80) {
        return 'B';
    } else {
        return 'A';
    }
});
```

- shuffle / sample

> `shuffle()`用洗牌算法随机打乱一个集合：

```javascript
_.shuffle([10,32, 33, 24, 57, 6]); // [32, 57, 24, 6, 24, 10]
```

> `sample()` 则是随机选择一个或多个元素

```javascript
_.sample([10,32, 33, 24, 57, 6]); // [6, 24, 10]
```

## Arrays

- first / last

> 顾名思义，这两个函数分别取第一个和最后一个元素：

```javascript
'use strict';
var arr = [2, 4, 6, 8];
_.first(arr); // 2
_.last(arr); // 8
```

- flatten

> `flatten()`接收一个`Array`，无论这个`Array`里面嵌套了多少个`Array`，`flatten()`最后都把它们变成一个一维数组

```javascript
'use strict';
_.flatten([1, [2], [3, [[4], [5]]]]); // [1, 2, 3, 4, 5]
```

- zip / unzip

> `zip()`把两个或多个数组的所有元素按索引对齐，然后按索引合并成新数组。例如，你有一个`Array`保存了名字，另一个`Array`保存了分数，现在，要把名字和分数给对上，用`zip()`轻松实现

```javascript
'use strict';
var names = ['Adam', 'Lisa', 'Bart'];
var scores = [85, 92, 59];
_.zip(names, scores);
// [['Adam', 85], ['Lisa', 92], ['Bart', 59]]
```

> `unzip()`则是反过来：

```js
'use strict';
var namesAndScores = [['Adam', 85], ['Lisa', 92], ['Bart', 59]];
_.unzip(namesAndScores);
// [['Adam', 'Lisa', 'Bart'], [85, 92, 59]]
```

- object

> 有时候你会想，与其用`zip()`，为啥不把名字和分数直接对应成`Object`呢？别急，`object()`函数就是干这个的

```javascript
'use strict';

var names = ['Adam', 'Lisa', 'Bart'];
var scores = [85, 92, 59];
_.object(names, scores);
// {Adam: 85, Lisa: 92, Bart: 59}
```

- range

> `range()`让你快速生成一个序列，不再需要用`for`循环实现了

```javascript
'use strict';

// 从0开始小于10:
_.range(10); // [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

// 从1开始小于11：
_.range(1, 11); // [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

// 从0开始小于30，步长5:
_.range(0, 30, 5); // [0, 5, 10, 15, 20, 25]

// 从0开始大于-10，步长-1:
_.range(0, -10, -1); // [0, -1, -2, -3, -4, -5, -6, -7, -8, -9]
```

- `_.flattenDeep(array)`

> 递归遍历数据，返回一个新数组

```js
a = _.flattenDeep([[1,2,3],[4,5,6]]); // [1,2,3,4,5,6]
a = _.flattenDeep([[1,2,3],[4,5,[7,8,9]]]); // [1,2,3,4,5,6,7,8,9]
```
