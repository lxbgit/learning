# git clone 克隆或下载一个仓库单个文件夹

> 现在有一个test仓库https://github.com/mygithub/test
    你要gitclone里面的tt子目录：
    在本地的硬盘位置打开Git Bash

```shell
git init test && cd test     //新建仓库并进入文件夹
git config core.sparsecheckout true //设置允许克隆子目录

echo 'tt*' >> .git/info/sparse-checkout //设置要克隆的仓库的子目录路径   //空格别漏

git remote add origin git@github.com:mygithub/test.git  //这里换成你要克隆的项目和库

git pull origin master    //下载
```
