# git常见问题

- 在执行 git pull origin master 命令时，发生报错无法pull的情况。

`fatal: refusing to merge unrelated histories`

查了原因是说在git 2.9版本后，需要加上这句 ```--allow-unrelated-histories```

```git pull origin master --allow-unrelated-histories```

- `git` 添加远程库

```shell
git remote add origin https://github.com/dreamerlxb/springboot-websocket-demo.git
```

- [git问题](https://blog.csdn.net/TUTGG/article/details/87875675?utm_source=app)

- git commit 忽略eslint 校验

```shell
git commit --no-verify -m "commit"   #就可以跳过代码检查
```

