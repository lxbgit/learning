# Git 常用命令

## 常用命令

- 本地库初始化 `git init`
- `git add`

> 把本地的修改加到stage中;
>`git add .` 把所有的文件加到stage中

- `git commit -m “信息”`

> （把stage中的修改提交到本地库）

- `git push`（把本地库的修改提交到远程库中）
- `git pull`（把远程库的代码更新到工作台）
- `git log`（查看当前分支上面的日志信息）
- `git status`（查看当前分支有哪些修改）
- `git remote` （操作远程库信息）
  - `git remote add <源名称> <源地址>`
  - `git remote remove <源名称>`
  - `git remote set-url <源名称> <源地址>`

## 分支常用命令

- `git branch -v`（查看本地库中的所有分支）
  - `git branch` //查看本地所有分支
  - `git branch -r` //查看远程所有分支

- `git branch dev` (创建一个新的分支dev)
- `git checkout dev` （切换分支）

- 分支合并
  - 切换到接收修改的分支 （`git checkout master`）
  - 执行`merge`命令  （`git merge dev`）
（注：切换分支后，在dev分支中做出的修改需要合并到被合并的分支master上)

- 冲突解决
  - 编辑文件，删除特殊符号
  - 将文件修改完毕后，保存退出
  - `git add [文件名]`
  - `git commit –m “日志信息”`

- 删除已经在git中的文件 `git rm -r --cached .idea`

> `-r 表示删除文件`
