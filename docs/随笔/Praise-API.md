# Praises

- 获取praises信息
`GET  /api/praises`

参数名 | 是否必需 | 描述
----- | --------| -------

-  获取praise信息
`GET /api/praises/:id`

参数名 | 是否必需 | 描述
----- | --------| -------
id   |   是     | praise id

## 响应
~~~json
{
}
~~~

-  创建Praise
 `POST /api/praises`

参数名 | 是否必需 | 描述
----- | --------| -------
praised_id | 是 | 被表扬的用户id
team_id | 是 | 被表扬用户所在的小组
user_id | 是 | user id

