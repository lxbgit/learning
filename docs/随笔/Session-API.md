# sessions

- 创建一个session(登陆)
`GET  /api/sessions`

参数名 | 是否必需 | 描述
----- | --------| -------
phone | true | 手机号
password | true | 密码

- 创建一个session(退出)
`DELETE  /api/sessions`


参数名 | 是否必需 | 描述
----- | -------- | --------
