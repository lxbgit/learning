# teams

- 获取teams信息
`GET  /api/teams`

参数名 | 是否必需 | 描述
----- | --------| -------

-  获取team信息
`GET /api/teams/:id`

参数名 | 是否必需 | 描述
----- | --------| -------
id   |   是     | 用户 id

## 响应
~~~json
{
}
~~~

- 修改teams信息
`PUT  /api/teams/:id`

参数名 | 是否必需 | 描述
----- | --------| -------
name | 是 | team name
description | 是 | team description

-  创建team
 `POST /api/teams`

参数名 | 是否必需 | 描述
----- | --------| -------
name | 是 | team name
description | 是 | team description
owner_id | 是 | user id

-  删除team
 `DELETE /api/teams/:id`

参数名 | 是否必需 | 描述
----- | --------| -------

-  小组所在的用户
`GET /api/teams/:id/users`

参数名 | 是否必需 | 描述
----- | --------| -------
id | 是 | team id

## 响应
~~~json
~~~
