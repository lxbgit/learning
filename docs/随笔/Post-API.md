# posts

- 获取Posts信息  `GET  /api/posts`

    参数名 | 是否必需 | 描述
    ----- | --------| -------

- 修改Posts信息  `PUT  /api/posts/:id`

    参数名 | 是否必需 | 描述
    ----- | --------| -------
    title | 是 | post title
    content | 是 | post content

- 创建Post `POST /api/posts`

    参数名 | 是否必需 | 描述
    ----- | --------| -------
    title | 是 | post title
    content | 是 | post content
    user_id | 是 | user id

- 删除Post `DELETE /api/posts/:id`

    参数名 | 是否必需 | 描述
    ----- | --------| -------

- 点赞 post `POST /api/posts/:id/like`

    参数名 | 是否必需 | 描述
    ----- | --------| -------
    id | 是 | 用户id

- 取消点赞 post  `POST /api/posts/:id/unlike`

    参数名 | 是否必需 | 描述
    ----- | --------| -------
    id | 是 | 用户id

- 收藏 post  `POST /api/posts/:id/favorite`

    参数名 | 是否必需 | 描述
    ----- | --------| -------
    id | 是 | 用户id

- 取消收藏 post  `POST /api/posts/:id/unfavorite`

    参数名 | 是否必需 | 描述
    ----- | --------| -------
    id | 是 | 用户id

    **响应**

    ```json
    {
        id: 12,
        name: ""
    }
    ```

- 获取用户信息   `GET /api/posts/:id`

    参数名 | 是否必需 | 描述
    ----- | --------| -------
    id   |   是     | 用户 id

    **响应**

    ```json
    {
        id: 90,
        name: ""
    }
    ```
