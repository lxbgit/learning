# checklist_items

- 获取checklist_items信息
`GET  /api/checklist_items`

参数名 | 是否必需 | 描述
----- | --------| -------
week | false | 第几周
 month | false | 几月
year | false | 年份

- 修改checklist_items信息
`PUT  /api/checklist_items/:id`

参数名 | 是否必需 | 描述
----- | --------| -------
content | false | checklist_item content
deadline | false | checklist_item deadline

- 创建checklist_items
 `POST /api/checklist_items`

参数名 | 是否必需 | 描述
----- | --------| -------
content | 是 | checklist_item content
deadline | 是 | checklist_item deadline
user_id | 是 | user id
next_week | false | 是否为下一周

- 删除checklist_item
 `DELETE /api/checklist_items/:id`

参数名 | 是否必需 | 描述
----- | --------| -------
