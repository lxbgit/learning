# users

- 修改用户信息
`PUT  /api/users/:id`

参数名 | 是否必需 | 描述
----- | --------| -------
name | 是 | 用户name
email | 是 | 用户email

-  创建用户
     `POST /api/users`

参数名 | 是否必需 | 描述
----- | --------| -------
name | 是 | 用户name
password | 是 | 用户password
phone | 是 | 用户phone
code | 是 | 验证码

- 上传图片
 `POST users/:id/avatar`
    
参数名 | 是否必需 | 描述
----- | --------| -------
avatar | 是 | 用户avatar


- 用户的 posts
`GET /api/users/:id/posts`
     
参数名 | 是否必需 | 描述
----- | --------| -------
id | 是 | 用户id


- 用户的 activities
 ` GET /api/users/:id/activities`
      
参数名 | 是否必需 | 描述
----- | --------| -------
id | 是 | 用户id


 

-  用户所在的小组
`GET /api/users/:id/teams`

参数名 | 是否必需 | 描述
----- | --------| -------
id | 是 | 用户id

## 响应
~~~json
[ {}, {}]
~~~

-  用户发送的links
`GET /api/users/:id/links`

参数名 | 是否必需 | 描述
----- | --------| -------
id | 是 | 用户id

## 响应
~~~json
[ {}, {}]
~~~
     
-  重置密码
`POST /api/users/:id/reset_pwd`

参数名 | 是否必需 | 描述
----- | --------| -------
id | 是 | 用户id
  old_password  |   是     | 旧密码
new_password | 是 | 新密码

## 响应
~~~json
{"ok": true, error: "错误信息"}
~~~

-  获取用户信息
`GET /api/users/:id`

参数名 | 是否必需 | 描述
----- | --------| -------
id   |   是     | 用户 id

## 响应
~~~json
{
"id": 1,
"email": "test-user-00@mail.com",
"name": "newbie",
"admin": false,
"avatar": "[http://placehold.it/60x60?text=L](http://placehold.it/60x60?text=L)",
"created_at": "2017-07-03T02:48:32.000Z",
"updated_at": "2017-07-11T06:17:43.000Z"}
~~~
