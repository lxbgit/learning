# mentions

- 创建mentions
 `POST /api/mentions`

参数名 | 是否必需 | 描述
----- | --------| -------
message | 是 | mention message
mentioned_ids | 是 | 1,2,3
user_id | 是 | user id

- 获取mentions信息
`GET  /api/mentions`

参数名 | 是否必需 | 描述
----- | --------| -------

- 修改mention信息
`PUT  /api/mentions/:id`

参数名 | 是否必需 | 描述
----- | --------| -------
message | 是 | mention message
mentioned_ids | 是 |提及到的 mentioned_ids

- 删除mentions
 `DELETE /api/mentions/:id`

参数名 | 是否必需 | 描述
----- | --------| -------
id   |   是     | mention id

- 获取用户信息

`GET /api/mentions/:id`

参数名 | 是否必需 | 描述
----- | --------| -------
id   |   是     | mention id

## 响应

~~~json
{
}
~~~
