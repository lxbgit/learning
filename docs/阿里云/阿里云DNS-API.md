# Aliyun

## DNS 配置API

[官方文档](https://help.aliyun.com/document_detail/262200.html)

[参考API](https://next.api.aliyun.com/api-tools/sdk/Httpdns?version=&language=java)

[参考API2](https://next.api.aliyun.com/api/Httpdns/2016-02-01/AddDomain?spm=api-workbench.SDK%20Document.0.0.18c41e0fyfwj0c)

[文档中心](https://help.aliyun.com/document_detail/39863.html)
