# Ubuntu

> 使用`apt-get` 管理包

- `apt-get install package` 安装

## apt-get安装jdk8

- 安装python-software-properties

```shell
sudo apt-get install python-software-properties
sudo apt-get install software-properties-common
```

- 添加ppa

```shell
sudo add-apt-repository ppa:webupd8team/java
```

- 然后更新系统

```shell
sudo apt-get update
```

- 最后开始安装

```shell
sudo apt-get install oracle-java8-installer
java -version
```

--------

java版本切换

```shell
sudo update-java-alternatives -s java-8-oracle
```

## 蓝灯

- 下载[lantern](https://github.com/getlantern/lantern/releases/tag/latest)
- 使用gdebi安装 `sudo gdebi lantern-installer-64-bit.deb`
- 然后运行，在终端输入 `lantern`

> 可能会遇到如下错误：

`/.lantern/bin/lantern: error while loading shared libraries: libappindicator3.so.1: cannot open shared object file: No such file or directory`

解决办法：
`sudo apt-get install libappindicator3-0.1`
