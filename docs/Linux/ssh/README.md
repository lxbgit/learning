# 常用SSH命令

## 连接远程服务器

- `ssh root@211.87.227.215`

> `ssh username@ip`

## 从服务器下载整个目录

- `scp -r root@39.107.80.119:/var/www/test  /var/www/`

> 将服务器39.107.80.119下文件夹`/var/www/test`下载到本地`/var/www/`

## 上传目录到服务器

- `scp -r test root@39.107.80.119:/var/www/`

> 把当前目录下的test目录上传到服务器的`/var/www/` 目录

## 上传本地文件到服务器

- `scp /var/www/test.js root@39.107.80.119:/var/www/`

> 把本机`/var/www/`目录下的test.js文件上传到39.107.80.119这台服务器上的`/var/www/`目录中
