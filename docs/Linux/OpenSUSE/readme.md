# OpenSUSE

> 使用`zypper`管理依赖库

- `zypper install package` 安装

- `zypper -h` 查找帮助


## OpenSUSE 源

> [阿里云镜像](https://opsx.alibaba.com/mirror)
> [北京交通大学镜像](https://mirror.bjtu.edu.cn/)
> [中科大镜像](https://mirrors.ustc.edu.cn/)

```bash
zypper addrepo -f http://mirrors.aliyun.com/opensuse/update/leap/42.1/oss aliyun1-update

zypper addrepo -f http://mirrors.aliyun.com/opensuse/update/leap/42.1/non-oss aliyun1-update-non-oss

zypper ar -f https://mirror.bjtu.edu.cn/opensuse/tumbleweed/repo/non-oss/ bjtu-tumbleweed-non-oss

zypper ar -f https://mirrors.ustc.edu.cn/opensuse/tumbleweed/repo/non-oss/ ustc-tumbleweed-non-oss
```
