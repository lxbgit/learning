# Linux端口占用及kill相关进程

## 方式一：`lsof`命令

- 查看占用端口进程的PID

```shell
lsof -i:{端口号}
```

- 根据PID kill掉相关进程

```shell
kill -9 {PID}
```

## 方式二：`netstat`命令

- 查看占用端口进程的PID

```shell
netstat -tunlp|grep {port}
```

- `kill`方法如上

```shell
kill -9 {PID}
```

## 补充：根据程序名查看对应的PID

- 用ps命令（zb专用）

```shell
ps -ef | grep {programName}
kill -9 {PID}
```

- 用`pgrep`命令

> pgrep命令的p表明了这个命令是专门用于进程查询的grep

```shell
pgrep {programName}
kill -9 {PID}
```

- 查看指定的端口
`lsof -i:port`

## netstat

> Netstat 命令用于显示各种网络相关信息，如网络连接，路由表，接口状态 (Interface Statistics)，masquerade 连接，多播成员 (Multicast Memberships) 等等。

```shell
-a (all)显示所有选项，netstat默认不显示LISTEN相关
-t (tcp)仅显示tcp相关选项
-u (udp)仅显示udp相关选项
-n 拒绝显示别名，能显示数字的全部转化成数字。(重要)
-l 仅列出有在 Listen (监听) 的服務状态

-p 显示建立相关链接的程序名(macOS中表示协议 -p protocol)
-r 显示路由信息，路由表
-e 显示扩展信息，例如uid等
-s 按各个协议进行统计 (重要)
-c 每隔一个固定时间，执行该netstat命令。

提示：LISTEN和LISTENING的状态只有用-a或者-l才能看到
```

- 查看所有端口

```shell
netstat -aptn
```

- 使用Netstat命令列出所有网络端口

```shell
netstat -lntu
```

- 显示网络接口列表

```shell
netstat -i

OR

netstat -ie
```

## 其他

- 查看在不同的应用程序和端口/协议组合列表/etc/services在Linux中使用文件cat命令 ：

```shell
$ cat /etc/services 
OR
$ cat /etc/services | less
```
