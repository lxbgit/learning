# linux查看外网访问ip

- 对连接的IP按连接数量进行排序

```shell
netstat -ntu | awk '{print $5}' | cut -d: -f1 | sort | uniq -c | sort -n 
```

- 统计每个IP的连接数

```
统计每个IP的连接数：netstat -n | awk '/^tcp/ {print $5}' | awk -F: '{print $1}' | sort | uniq -c| sort -rn
                      ss -n | awk '/^tcp/ {print $5}' | awk -F: '{print $1}' | sort | uniq -c| sort -rn

---------------------------------------------------(netstat -n in CentOS6)----------------------------------------------------
Proto Recv-Q Send-Q Local Address               Foreign Address             State
tcp        0      0 10.0.2.15:22                10.0.2.2:58262              ESTABLISHED
tcp        0      0 10.0.2.15:22                10.0.2.2:58263              ESTABLISHED

---------------------------------------------------(ss -n in CentOS7)---------------------------------------------------------
Netid State      Recv-Q Send-Q                       Local Address:Port                                      Peer Address:Port
tcp   ESTAB      0      0                                10.0.2.15:22                                            10.0.2.2:58352
tcp   ESTAB      0      0                                10.0.2.15:22                                            10.0.2.2:57127


netstat -n                  :以数字形式显示地址信息
awk '/^tcp/ {print $5}'     :筛选以tcp开头的行，并打印5th filed.
awk -F: '{print $1}'        :以冒号为字段分隔符，打印第一个字段(-F fs 指定行中分隔数据字段的字段分隔符)
uniq -c                     :去除重复项目，-c 进行计数
sort -rn                    :进行排序，-r 反向排序 -n 使用纯数字进行排序
```

- 查看TCP连接状态  

```shell
netstat -nat |awk '{print $6}'|sort|uniq -c|sort -rn  
netstat -n | awk '/^tcp/ {++S[$NF]};END {for(a in S) print a, S[a]}'  
netstat -n | awk '/^tcp/ {++state[$NF]}; END {for(key in state) print key,"\t",state[key]}'  
netstat -n | awk '/^tcp/ {++arr[$NF]};END {for(k in arr) print k,"\t",arr[k]}'  
netstat -n |awk '/^tcp/ {print $NF}'|sort|uniq -c|sort -rn  
netstat -ant | awk '{print $NF}' | grep -v '[a-z]' | sort | uniq -c  
```

- 查看80端口连接数最多的20个IP

```shell
netstat -anlp|grep 80|grep tcp|awk '{print $5}'|awk -F: '{print $1}'|sort|uniq -c|sort -nr|head -n20  
```

- 查找较多time_wait连接

```shell
netstat -n|grep TIME_WAIT|awk '{print $5}'|sort|uniq -c|sort -rn|head -n20
```

- 查找较多的SYN连接

```shell
netstat -an | grep SYN | awk '{print $5}' | awk -F: '{print $1}' | sort | uniq -c | sort -nr | more 
```

- 查看当前并发访问数：

```shell
netstat -an | grep ESTABLISHED | wc -l
```

- 查看所有连接请求

```shell
netstat -tn 2>/dev/null 
但是只要established的，则grep  "ESTABLISHED" 
netstat -tn | grep ESTABLISHED 2>/dev/null
```

- 查看访问某一ip的所有外部连接IP(数量从多到少)

````shell
netstat -nt | grep 121.41.30.149:80 | awk '{print $5}' | awk -F: '{print ($1>$4?$1:$4)}' | sort | uniq -c | sort -nr | head
```

- 根据端口查找进程

```
netstat -ntlp | grep 80 | awk '{print $7}' | cut -d/ -f1
```



## ngnix 相关

根据nginx的访问日志判断
查看访问记录
1.从1000行开始到3000
cat access.log |head -n 3000|tail -n 1000
2.从1000行开始，显示200行
cat access.log |tail -n +1000 |head -n 200
3.


通过查询日志记录进行分析（如果没有单独配置，access.log一般放在nginx/logs下）
awk '{print $1}' 日志地址 | sort | uniq -c | sort -n -k 1 -r | head -n 100


tail -n 1000：显示最后1000行
tail -n +1000：从1000行开始显示，显示1000行以后的
head -n 1000：显示前面1000行


1.根据访问IP统计UV


awk '{print $1}'  access.log|sort | uniq -c |wc -l


2.统计访问URL统计PV


awk '{print $7}' access.log|wc -l


3.查询访问最频繁的URL


awk '{print $7}' access.log|sort | uniq -c |sort -n -k 1 -r|more


4.查询访问最频繁的IP


awk '{print $1}' access.log|sort | uniq -c |sort -n -k 1 -r|more


5.根据时间段统计查看日志


 cat  access.log| sed -n '/14\/Mar\/2015:21/,/14\/Mar\/2015:22/p'|more


6.通过日志查看含有send的url,统计ip地址的总连接数
cat access.log | grep "send" | awk '{print $1}' | sort | uniq -c | sort -nr


7.通过日志查看当天访问次数最多的时间段
awk '{print $4}' access.log | grep "24/Mar/2011" |cut -c 14-18|sort|uniq -c|sort -nr|head


8.通过日志查看当天指定ip访问次数过的url和访问次数
cat access.log | grep "222.132.90.94" | awk '{print $7}' | sort | uniq -c | sort -nr