# CentOS

> 使用`yum` 管理依赖

- `yum install package` 安装

## CentOS命令

- 查看centOS的版本

`cat /etc/redhat-release`

- 查看内核版本等信息

`uname -a`

- 查看CPU是32位/64位

```shell
getconf LONG_BIT

echo $HOSTTYPE

uname -a
```

- 查看硬盘和分区

`df -h`

[参考1](https://www.cnblogs.com/achengmu/p/9055201.html)

## CentOS中防火墙的使用

Centos7默认安装了`firewalld`，如果没有安装的话，可以使用 `yum install firewalld firewalld-config`进行安装。

- 启动防火墙

`systemctl start firewalld`

- 禁用防火墙

`systemctl stop firewalld`

- 设置开机启动

`systemctl enable firewalld`

- 停止并禁用开机启动

`sytemctl disable firewalld`

- 重启防火墙

`firewall-cmd --reload`

- 查看状态

`systemctl status firewalld或者 firewall-cmd --state`

- 查看版本

`firewall-cmd --version`

- 查看帮助

`firewall-cmd --help`

- 查看区域信息

`firewall-cmd --get-active-zones`

- 查看指定接口所属区域信息

`firewall-cmd --get-zone-of-interface=eth0`

- 拒绝所有包

`firewall-cmd --panic-on`

- 取消拒绝状态

`firewall-cmd --panic-off`

- 查看是否拒绝

`firewall-cmd --query-panic`

- 将接口添加到区域(默认接口都在`public`)

`firewall-cmd --zone=public --add-interface=eth0`(永久生效再加上 `--permanent` 然后`reload`防火墙)

- 设置默认接口区域

`firewall-cmd --set-default-zone=public`(立即生效，无需重启)

- 更新防火墙规则

`firewall-cmd --reload`或`firewall-cmd --complete-reload`(两者的区别就是第一个无需断开连接，就是`firewalld`特性之一动态
添加规则，第二个需要断开连接，类似重启服务)

- 查看指定区域所有打开的端口

`firewall-cmd --zone=public --list-ports`

- 在指定区域打开端口（记得重启防火墙）

`firewall-cmd --zone=public --add-port=80/tcp` (永久生效再加上 `--permanent`)

> 说明：
`–zone` 作用域
`–add-port=8080/tcp` 添加端口，格式为：端口/通讯协议
`–permanent` #永久生效，没有此参数重启后失效

### 问题

- `Unable to round-trip http request to upstream: dial tcpxxx.xxx.xx.xx:xxxx: connect: connection refused`
出现这种报错信息一般是因为防火墙问题，需要为端口开启防火墙，并重启:

> `firewall-cmd  --zone=public --add-port=你的端口/tcp --permanent` #将端口加入防火墙
> `systemctl restart firewalld.service` #重启防火墙
