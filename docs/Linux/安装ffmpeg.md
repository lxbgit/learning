# ffmpeg

[官网](https://ffmpeg.org/)

1. 下载ffmpeg  `wget https://ffmpeg.org/releases/ffmpeg-4.1.4.tar.bz2`
2. 解压  `tar -jxvf 文件名 -C 存放的目录`
3. 配置  `./configure`
4. `make`
5. `make install`
6. 测试 `ffmpeg -h`
