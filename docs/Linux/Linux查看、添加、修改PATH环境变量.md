# Linux 查看、添加、修改`PATH` 环境变量

## 添加环境变量

（Bash shell中用export，C shell中用setenv）

1.直接在终端修改：

```shell
export PATH=$PATH:software_installation_path/bin
```

该修改只对本次进程有效

2.修改用户级

在`home/用户/.profile`中添加：

```shell
export PATH=$PATH:software_installation_path/bin
```

保存文件，重启即可（有的系统执行`./.profile`即可，不需重启；有的系统必须重启）

3.修改系统级

```shell
cd /etc/profile

vi profile

export PATH=$PATH:software_installation_path/bin
```

保存文件，重启即可（有的系统执行`./profile`即可，不需重启；有的系统必须重启）

重启后可查看PATH环境变量看是否更改：

```shell
echo $PATH
```

## 环境变量改名

```shell
echo ${path/old_name/new_name}       #变更一个目录名old_name

echo ${path//old_name/new_name}      #变更所有目录名old_name
```

## 环境变量删除

```shell
echo ${path#/deletion_name:}
```

查看PATH：`echo $PATH`

## 例子

以添加`mongodb server`为列

- 修改方法一：

```shell
export PATH=/usr/local/mongodb/bin:$PATH
```

//配置完后可以通过`echo $PATH`查看配置结果。
生效方法：立即生效
有效期限：临时改变，只能在当前的终端窗口中有效，当前窗口关闭后就会恢复原有的path配置
用户局限：仅对当前用户

- 修改方法二：
通过修改`.bashrc`文件:

```shell
vim ~/.bashrc 
# //在最后一行添上：
export PATH=/usr/local/mongodb/bin:$PATH
```

生效方法：（有以下两种）
1、关闭当前终端窗口，重新打开一个新终端窗口就能生效
2、输入`source ~/.bashrc`命令，立即生效
有效期限：永久有效
用户局限：仅对当前用户

- 修改方法三:
通过修改`profile`文件:

```shell
vim /etc/profile
/export PATH //找到设置PATH的行，添加
export PATH=/usr/local/mongodb/bin:$PATH
```

生效方法：系统重启
有效期限：永久有效
用户局限：对所有用户

- 修改方法四:
通过修改`environment`文件:

```shell
vim /etc/environment
```

在`PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games"` 中加入 `:/usr/local/mongodb/bin`

生效方法：系统重启
有效期限：永久有效
用户局限：对所有用户
