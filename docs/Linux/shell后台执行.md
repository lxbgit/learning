# shell后台执行

## 后台执行

```shell
nohup sh run.sh >log.txt 2>&1 &
```

解释

这个命令分为五个部分，分别是`nohup`、`sh run.sh`、`>log.txt`、`2>&1`、`&`。

- `nohup`和最后的`&`：表示命令在后台执行
- `sh run.sh`: 执行run.sh脚本
- `>log.txt`是将信息输出到`log.txt`日志中
- `2>&1` 是将标准错误信息转变成标准输出，这样就可以将错误信息输出到`log.txt`日志里。其中0、1、2分别代表如下含义：
  - 0：stdin(标准输入)
  - 1：stdout(标准输出)
  - 2：stderr(标准错误)

## 后台查看

```shell
ps -ef | grep 13549
```

- `ps` 表示将某个进程显示出来
- `-e` 表示显示所有程序、`-f`表示显示`UID`,`PPIP`,`C`与`STIME`栏位
- `grep`表示查找和其后面内容相关的信息，即和13549相关的信息
- `|`是管道命令 是指`ps`命令与`grep`同时执行

输出的信息分别是：`UID` 、 `PID` 、`PPID` 、 `C STIME` 、 `TTY`、 `TIME` 、 `CMD`。

- `UID` 程序被该 UID 所拥有
- `PID` 就是这个程序的 ID
- `PPID` 则是其上级父程序的ID
- `C CPU` 使用的资源百分比
- `STIME` 系统启动时间
- `TTY` 登入者的终端机位置
- `TIME` 使用掉的 CPU 时间

## 终止进程

```shell
kill -9 pid
```
