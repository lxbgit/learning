# deepin命令

```shell
sudo dpkg -i 'atom-amd64.deb'
```

> `deepin`下应用卡死了：`Alt+F2`出运行命令：`xkill`
鼠标变成一个小x，点击你想要枪毙的程序窗口，小心使用！不要乱点

## deepin 下安装mysql

- [下载](https://dev.mysql.com/downloads/mysql/)mysql

- 安装

- 修改密码 [参考](../../database/mysql/readme.md)

  - 修该`/etc/mysql/mysql.conf.d/mysqld.cnf`, 在下面添加`skip-grant-tables=1`,免密码登录

  - `alter user 'root'@'localhost' identified WITH mysql_native_password by '12345678';`

- 重启服务

## Deepin Linux 下安装jdk

[参考1](https://wiki.deepin.org/index.php?title=Deepin%E4%B8%8Bjava%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83%E9%83%A8%E7%BD%B2&oldid=923)

[参考2](http://tech.it168.com/a2018/0731/3216/000003216634.shtml)

- [下载JDK](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)

- 解压jdk

```shell
sudo tar -zxvf ~/Downloads/jdk-8u45-linux-i586.tar.gz -C /usr/lib
```

> 上述中参数-C后面的路径是解压缩的目标

- 执行以下命令

```
sudo update-alternatives --install /usr/bin/java java /opt/jdk1.8.0_66/bin/java 1000 
sudo update-alternatives --install /usr/bin/javac javac /opt/jdk1.8.0_66/bin/javac 1000
```

> 参考1

- `update-alternatives --display java` 查看JAVA的版本和优先级

- `update-alternatives --config java` 执行命令选择JAVA版本
