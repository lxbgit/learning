# linux下常用的工具

## ls命令

- `ls`

> ls命令显示最基本的文件信息，只有文件名

- `ls -F`

> `-F`参数将显示列表中的文件区分开文件和文件夹，文件夹后面带有/

- `ls -a`

> `-a`参数是显示隐藏文件作用，默认情况下ls命令是不会显示隐藏文件的，隐藏文件显示时候前面带有"."

- `ls -R`

> `-R`命令将递归显示该文件下的所有文件夹和文件（包括子文件和文件夹）

- `ls -l`

> `-l`命令将显示文件和文件夹的详细信息（和windows中以详细信息显示文件一样的效果）


## linux 下一些命令1

- `fdisk -l`

> 使用`fdisk`工具查看待扩展的数据盘分区

- `zip -r jhwebfile.zip jhwebfile`

> 打包，将jhwebfile文件夹下文件压缩成`jhwebfile.zip`

- `scp root@39.104.78.165:/mnt/bak1/JHSoftware/jfwebfile.zip /Users/tiger/Desktop`

`scp 用户名@IP:文件位置 目标位置`

> 将39.104.78.165服务器下`/mnt/bak1/JHSoftware/jfwebfile.zip` 下载到 `/Users/tiger/Desktop`

- 上传目录到服务器
`scp -r test root@39.107.80.119:/var/www/`

> 把当前目录下的`test`目录上传到服务器的`/var/www/`目录下

- 从服务器下载整个目录
`scp -r root@39.107.80.119:/var/www/test  /var/www/`

> 把服务器`/var/www/test`目录下载到本地的`/var/www/`目录下

- ssh 连接远程服务器  `ssh 用户名@ip`

`ssh root@39.104.78.165`

## linux 常用命令2

- 显示DEB包信息

```shell
dpkg -I xx.deb
```

- 显示DEB包文件列表

```shell
dpkg -c xx.deb
```

- 安装DEB包

```shell
dpkg -i xx.deb
```

- 显示所有已安装软件

```shell
dpkg -l
```

- 启动一个服务

```shell
sudo -i service elasticsearch start #以elasticsearch为例
```

- 查看端口是否被占用

```shell
netstat -an | grep port
```

## Ubuntu

- 使用`gdebi`安装deb文件

安装`gdebi` :

```shell
sudo apt-get install gdebi-core
```

-----------

解压文件
> 对于 `.tar.bz2` 使用参数`-jxvf`

```shell
tar -jxvf sublime_text_3_build_3126_x64.tar.bz2
```

## sdkman

[官网](https://sdkman.io/install)

```shell
$ curl -s "https://get.sdkman.io" | bash

$ source "$HOME/.sdkman/bin/sdkman-init.sh"
# 测试
$ sdk version
```

## linux下安装 `node`

- 使用 n 模块安装

- 提示`/usr/local/bin/node: 权限不够`时；

> 使用命令 `sudo chmod +x /usr/local/bin/node` 赋予权限

- 使用 n 切换 node 版本时，使用 `sudo n`  ,上下键选择node版本，回车切换
