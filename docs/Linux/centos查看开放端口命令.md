# centos查看开放端口命令

> CentOS7的开放关闭查看端口都是用防火墙来控制的

## 查看已经开放的端口

```shell
firewall-cmd --list-ports
```

## 查询端口

```sh
firewall-cmd --zone=public --query-port=80/tcp #查看
```

## 开启端口

```sh
firewall-cmd --zone=public --add-port=80/tcp --permanent  
```

## 关闭端口

```sh
firewall-cmd --zone= public --remove-port=80/tcp --permanent # 删除
```

- `–zone` #作用域

- `–add-port=80/tcp` #添加端口，格式为：端口/通讯协议

- `–permanent` #永久生效，没有此参数重启后失效

## 重启防火墙

```shell
#重启firewall  
firewall-cmd --reload  
#停止firewall  
systemctl stop firewalld.service  
#禁止firewall开机启动  
systemctl disable firewalld.service
```
