# 国际化

## Spring Security 国际化配置

### 重要的几个类

- `org.springframework.context.support.AbstractApplicationContext`
- `org.springframework.context.support.ReloadableResourceBundleMessageSource`
- `org.springframework.context.support.MessageSourceSupport`
- `org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider`

### 逐个解释

- `ReloadableResourceBundleMessageSource` 加载资源，bean name 应该为 `messageSource`

> 原因参考 `AbstractApplicationContext.initMessageSource`

- `AbstractUserDetailsAuthenticationProvider`

> 因为 类`AbstractUserDetailsAuthenticationProvider` 实现了 `org.springframework.context.MessageSourceAware`接口
所以会回调 `public void setMessageSource(MessageSource messageSource)`，设置`MessageSource`

- `AbstractApplicationContext`

> 具体操作见`initMessageSource`方法，该方法初始化i18n资源
