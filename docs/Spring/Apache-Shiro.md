# [shiro内置过滤器研究](http://www.cnblogs.com/koal/p/5152671.html)

**anon**
`org.apache.shiro.web.filter.authc.AnonymousFilter`

> 例子`/admins/**=anon`
没有参数，表示可以匿名使用。

**authc**
`org.apache.shiro.web.filter.authc.FormAuthenticationFilter`

> 例如:`/admins/user/**=authc`
表示需要认证才能使用，没有参数

**authcBasic**
`org.apache.shiro.web.filter.authc.BasicHttpAuthenticationFilter`

> 例如:`/admins/user/**=authcBasic` 没有参数表示httpBasic认证

**perms**
`org.apache.shiro.web.filter.authz.PermissionsAuthorizationFilter`

> 例子 `/admins/user/**=perms[user:add:*]`
perms参数可以写多个，多个时必须加上引号，并且参数之间用逗号分割，例如  `/admins/user/**=perms["user:add:*,user:modify:*"]`，
当有多个参数时必须每个参数都通过才通过，想当于`isPermitedAll()`方法。

**port**
`org.apache.shiro.web.filter.authz.PortFilter`

> 例子: `/admins/user/**=port[8081]`, 当请求的`url`的端口不是`8081`是跳转到
`schemal://serverName:8081?queryString` 其中schmal是协议http或https等，serverName是你访问的host,8081是url配置里port的端口，queryString
是你访问的url里的？后面的参数。

**rest**
`org.apache.shiro.web.filter.authz.HttpMethodPermissionFilter`

> 例子: `/admins/user/**=rest[user]` => 根据请求的方法，相当于 `/admins/user/**=perms[user:method]`  其中`method为post，get，delete` 等。

**roles**
`org.apache.shiro.web.filter.authz.RolesAuthorizationFilter`

> 例子: `/admins/user/**=roles[admin]`,
参数可以写多个，多个时必须加上引号，并且参数之间用逗号分割，当有多个参数时，例如`/admins/user/**=roles["admin,guest"]`
每个参数通过才算通过，相当于`hasAllRoles()`方法。

**ssl**
`org.apache.shiro.web.filter.authz.SslFilter`

> 例子`/admins/user/**=ssl` 没有参数，表示安全的url请求，协议为https

**user**
`org.apache.shiro.web.filter.authc.UserFilter`

> 例如`/admins/user/**=user` 没有参数表示必须存在用户，当登入操作时不做检查

这些过滤器分为两组:

1. 组是认证过滤器 (anon，authcBasic，auchc，user)

2. 组是授权过滤器 (perms，roles，ssl，rest，port)
