# Spring SPI

SPI 全称为 Service Provider Interface，是一种服务发现机制。SPI 的本质是将接口实现类的全限定名配置在文件中，并由服务加载器读取配置文件，加载实现类。这样可以在运行时，动态为接口替换实现类。正因此特性，我们可以很容易的通过 SPI 机制为我们的程序提供拓展功能。

引入了 SPI 机制后，服务接口与服务实现就会达成分离的状态，可以实现解耦以及程序可扩展机制

- `main/resources/META-INF/spring.factories`
- `org.springframework.core.io.support.SpringFactoriesLoader`
- 原理都是使用了Java的反射机制

```java

//获取所有factories文件中配置的LoggingSystemFactory
List<LoggingSystemFactory>> factories = 
    SpringFactoriesLoader.loadFactories(LoggingSystemFactory.class, classLoader);
```
