# Jackson处理Optional时遇到的问题

```xml
<dependency>
    <groupId>com.fasterxml.jackson.datatype</groupId>
    <artifactId>jackson-datatype-jdk8</artifactId>
    <version>2.12.5</version>
</dependency>
```

```java
User user = new User("jalon", Optional.of("xiaowang"));
ObjectMapper objectMapper = new ObjectMapper();
objectMapper.registerModule(new Jdk8Module());
String str = objectMapper.writeValueAsString(user);
System.out.println(str);
```
