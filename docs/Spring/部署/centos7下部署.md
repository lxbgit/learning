# SpringBoot 项目在centos 下部署

[参考1](https://www.cnblogs.com/aligege/p/9058652.html)
[参考2](https://blog.csdn.net/weixin_36413887/article/details/93492376)

## 打jar包，不啰嗦

## 创建linux下服务

- `cd /etc/systemd/system` 在该文件下创建服务

```
[Unit]
Description=weikeadmin service
After=syslog.target

[Service]
Type=simple
ExecStart= /usr/bin/java -jar /home/downloads/weikeadmin.jar

[Install]
WantedBy=multi-user.target
```

- Description 服务描述

- `/usr/bin/java` java路径（我这里是绝对路径，可以使用其他可执行java的路径）
- `/home/downloads/weike-admin.jar` 可执行jar包的路径
然后将文本文件保存成后缀名为.service
上面的文件保存之后 可以是`weikeadmin.service`

> 注意此处的java路径需要绝对路径，（我用相对路径错了），可以使用`whereis java` 查看

## 在部署服务器上执行以下命令

- 刷新服务配置文件
　　`systemctl daemon-reload`
　　
- 服务设置为开机启动(可以不用设)
　　`systemctl enable weikeadmin.service`
　　
- 启动服务
　　`systemctl start  weikeadmin.service`

- 停止服务
　　`systemctl stop weikeadmin.service`

- 查看服务状态
　　`systemctl status  weikeadmin.service`

- 查看日志
`journalctl -u weikeadmin.service`
