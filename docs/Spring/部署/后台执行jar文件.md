# java jar后台启动的方式

```shell
nohup java -jar xxxx.jar &
```

关于`nohup`命令， [详情](https://www.runoob.com/linux/linux-comm-nohup.html)

`nohup` 命令，在默认情况下（非重定向时），会输出一个名叫 `nohup.out` 的文件到当前目录下，如果当前目录的 `nohup.out` 文件不可写，输出重定向到 `$HOME/nohup.out` 文件中。
