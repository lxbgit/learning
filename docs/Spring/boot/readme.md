# Spring Boot

## `@Configuration`

### 注解的配置类有如下要求

- `@Configuration`不可以是`final`类型；
- `@Configuration`不可以是匿名类；
- 嵌套的`configuration`必须是静态类。

### `@Configuation` 总结

- `@Configuation`等价于`<Beans></Beans>`
- `@Bean`等价于`<Bean></Bean>`
- `@ComponentScan`等价于`<context:component-scan />`

## GlobalMethodSecurity

- `@PreAuthorize`

> 注解适合进入方法前的权限验证， @PreAuthorize可以将登录用户的roles/permissions参数传到方法中

- `@PostAuthorize`

> 注解使用并不多，在方法执行后再进行权限验证

- `@Secured`

> @Secured 源于 Spring之前版本.它有一个局限就是不支持Spring EL表达式

- `-`

## 异常处理

在日常开发中程序发生了异常，往往需要通过一个统一的异常处理，来保证客户端能够收到友好的提示。
通常情况下我们用`try..catch..`对异常进行捕捉处理，但是在实际项目中对业务模块进行异常捕捉，会造成代码重复和繁杂， 我们希望代码中只有业务相关的操作，所有的异常我们单独设立一个类来处理它。全局异常就是对框架所有异常进行统一管理 而这就表示在框架需要一个机制，将程序的异常转换为用户可读的异常。而且最重要的，是要将这个机制统一，提供统一的异常处理。 我们在可能发生异常的方法，全部`throw`抛给前端控制器；然后由前端控制器调用 全局异常处理器 对异常进行统一处理。 如此，我们现在的`Controller`中的方法就可以很简洁了。

- 基于`@ControllerAdvice`注解的`Controller`层的全局异常统一处理
- 返回统一格式，例如 本例中的`ActionResult`

> 如果全部异常处理返回`json`，那么可以使用`@RestControllerAdvice`代替`@ControllerAdvice`，这样在方法上就可以不需要添加`@ResponseBody`


## SpringBoot 项目在centos 下部署

> 打jar包，不啰嗦
>
`nohup java -jar  weike-admin-1.0.4.jar &`

[参考1](https://www.cnblogs.com/aligege/p/9058652.html)
[参考2](https://blog.csdn.net/weixin_36413887/article/details/93492376)

1. 创建linux下服务

- `cd /etc/systemd/system` 在该文件下创建服务

```
[Unit]
Description=weikeadmin service
After=syslog.target

[Service]
Type=simple
ExecStart= /usr/bin/java -jar /home/downloads/weikeadmin.jar

[Install]
WantedBy=multi-user.target
```

- Description 服务描述

- /usr/bin/java java路径（我这里是绝对路径，可以使用其他可执行java的路径）
- /home/downloads/weike-admin.jar 可执行jar包的路径
然后将文本文件保存成后缀名为.service
上面的文件保存之后 可以是`weikeadmin.service`

> 注意此处的java路径需要绝对路径，（我用相对路径错了），可以使用`whereis java` 查看

2. 在部署服务器上执行以下命令

- 刷新服务配置文件
　　`systemctl daemon-reload`
　　
- 服务设置为开机启动(可以不用设)
　　`systemctl enable weikeadmin.service`
　　
- 启动服务
　　`systemctl start  weikeadmin.service`

- 停止服务
　　`systemctl stop weikeadmin.service`

- 查看服务状态
　　`systemctl status  weikeadmin.service`

- 查看日志
`journalctl -u weikeadmin.service`

3. SpringBoot 部署https (可选)

```properties

server.port=443
server.ssl.key-store=classpath:4151992_dreamaerd.top.pfx
server.ssl.key-store-password=c17berLk
server.ssl.key-store-type=PKCS12
```
