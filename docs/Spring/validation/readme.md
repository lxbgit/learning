# java validation api

## api

- `@Email`

- `@Min`

- `@Max`

- `@NotNull`

- `NotEmpty`

- `......`

> 参考 `javax.validation.constraints.*`

## 使用

```java_holder_method_tree
@NotEmpty(groups = {ValidatedGroup.Update.class})
private String authorityId;
```

> groups 分组，建议继承 `javax.validation.groups.Default`
> groups默认是defaults分组，一旦指定其他groups,则就不属于defaults分组了；
> 所以需要继承default

## SpringMVC中使用
