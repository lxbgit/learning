# Spring Security

[参考](https://docs.spring.io/spring-security/site/docs/current/reference/htmlsingle/)

## `Core - spring-security-core.jar`

> 包含核心身份验证和访问控制类和接口，远程处理支持和基本配置API

```java
org.springframework.security.core
org.springframework.security.access
org.springframework.security.authentication
org.springframework.security.provisioning
```

## `Remoting - spring-security-remoting.jar`

> 提供与`Spring Remoting`的集成

`org.springframework.security.remoting`

## `Web - spring-security-web.jar`

> 含过滤器和相关的网络安全基础设施代码。任何与servlet API依赖关系的东西

`org.springframework.security.web`

## `Config - spring-security-config.jar`

> 包含安全命名空间解析代码和Java配置代码

`org.springframework.security.config`

## `LDAP - spring-security-ldap.jar`

> LDAP认证和供应代码

`org.springframework.security.ldap`

## `OAuth 2.0 Core - spring-security-oauth2-core.jar`

> `spring-security-oauth2-core.jar`包含为`OAuth 2.0`授权框架和`OpenID Connect Core 1.0`提供支持的核心类和接口。使用`OAuth 2.0`或`OpenID Connect Core 1.0`的应用程序（例如，客户端，资源服务器和授权服务器）需要此功能
  
`org.springframework.security.oauth2.core`

## `OAuth 2.0 Client - spring-security-oauth2-client.jar`

> `spring-security-oauth2-client.jar`是Spring Security对`OAuth 2.0`授权框架和`OpenID Connect Core 1.0`的客户端支持。由利用`OAuth 2.0`登录和/或OAuth客户端支持的应用程序所需

`org.springframework.security.oauth2.client`

## `OAuth 2.0 JOSE - spring-security-oauth2-jose.jar`

> 包含Spring Security支持JOSE (Javascript对象签名和加密)框架。JOSE框架的目的是提供一种方法来安全的请求

- JSON Web Token (JWT)
- JSON Web Signature (JWS)
- JSON Web Encryption (JWE)
- JSON Web Key (JWK)

`org.springframework.security.oauth2.jwt`

`org.springframework.security.oauth2.jose`

## `ACL - spring-security-acl.jar`

> 专门的domain对象ACL实现。使用安全应用于应用程序中特定的domain对象实例

`org.springframework.security.acls`

> Acl的全称是Access Control List

## `CAS - spring-security-cas.jar`

> Spring Security的CAS客户端集成。如果你想用CAS单点登录服务器来使用Spring Security Web认证

`org.springframework.security.cas`

## `OpenID - spring-security-openid.jar`

> OpenID Web认证支持。用于对外部OpenID服务器进行身份验证

`org.springframework.security.openid`

## `Test - spring-security-test.jar`

> 支持使用Spring Security进行测试

## Standard Filter Aliases and Ordering

- 首先需要了解spring security内置的各种filter：

| Alias | Filter Class | Namespace Element or Attribute|
| ----- | ------------ | ----------------------------- |
| CHANNEL_FILTER | `ChannelProcessingFilter`| `http/intercept-url@requires-channel` |
| SECURITY_CONTEXT_FILTER | `SecurityContextPersistenceFilter` | `http` |
| CONCURRENT_SESSION_FILTER | `ConcurrentSessionFilter` | `session-management/concurrency-control` |
| HEADERS_FILTER | `HeaderWriterFilter` | `http/headers` |
| CSRF_FILTER |	`CsrfFilter` | `http/csrf` |
| LOGOUT_FILTER | `LogoutFilter` | `http/logout` |
| X509_FILTER |	`X509AuthenticationFilter` | `http/x509` |
| PRE_AUTH_FILTER | `AbstractPreAuthenticatedProcessingFilter` Subclasses |	N/A |
| CAS_FILTER | `CasAuthenticationFilter` | N/A |
| FORM_LOGIN_FILTER | `UsernamePasswordAuthenticationFilter` | `http/form-login` |
| BASIC_AUTH_FILTER | `BasicAuthenticationFilter` | `http/http-basic` |
| SERVLET_API_SUPPORT_FILTER | `SecurityContextHolderAwareRequestFilter` | `http/@servlet-api-provision` |
| JAAS_API_SUPPORT_FILTER |	`JaasApiIntegrationFilter` | `http/@jaas-api-provision` |
| REMEMBER_ME_FILTER |	`RememberMeAuthenticationFilter` | `http/remember-me` |
| ANONYMOUS_FILTER |	`AnonymousAuthenticationFilter` | `http/anonymous` |
| SESSION_MANAGEMENT_FILTER | `SessionManagementFilter`	| `session-management` |
| EXCEPTION_TRANSLATION_FILTER|	`ExceptionTranslationFilter` |	`http` |
| FILTER_SECURITY_INTERCEPTOR |	`FilterSecurityInterceptor` |	`http` |
| SWITCH_USER_FILTER | `SwitchUserFilter` |	N/A |

## `filters`

 - `org.springframework.security.web.context.SecurityContextPersistenceFilter`
 - `org.springframework.security.web.session.ConcurrentSessionFilter`
 - `org.springframework.security.web.context.request.async.WebAsyncManagerIntegrationFilter`
 - `org.springframework.security.web.header.HeaderWriterFilter`
 - `org.springframework.security.web.authentication.logout.LogoutFilter`
 - `org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter`
 - `org.springframework.security.web.authentication.www.BasicAuthenticationFilter`
 - `org.springframework.security.web.savedrequest.RequestCacheAwareFilter`
 - `org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestFilter`
 - `org.springframework.security.web.authentication.AnonymousAuthenticationFilter`
 - `org.springframework.security.web.session.SessionManagementFilter`
 - `org.springframework.security.web.access.ExceptionTranslationFilter`
 - `org.springframework.security.web.access.intercept.FilterSecurityInterceptor`
 - `org.springframework.security.web.access.intercept.FilterSecurityInterceptor`

---

- `ChannelProcessingFilter`

- `ConcurrentSessionFilter`

- `WebAsyncManagerIntegrationFilter`

- `SecurityContextPersistenceFilter`

- `HeaderWriterFilter`

- `CorsFilter`

- `CsrfFilter`

- `LogoutFilter`

- `OAuth2AuthorizationRequestRedirectFilter`

- `Saml2WebSsoAuthenticationRequestFilter`

- `X509AuthenticationFilter`

- AbstractPreAuthenticatedProcessingFilter

- CasAuthenticationFilter

- OAuth2LoginAuthenticationFilter

- Saml2WebSsoAuthenticationFilter

- UsernamePasswordAuthenticationFilter

- ConcurrentSessionFilter

- OpenIDAuthenticationFilter

- DefaultLoginPageGeneratingFilter

- DefaultLogoutPageGeneratingFilter

- DigestAuthenticationFilter

- BearerTokenAuthenticationFilter

- BasicAuthenticationFilter

- RequestCacheAwareFilter

- SecurityContextHolderAwareRequestFilter

- JaasApiIntegrationFilter

- RememberMeAuthenticationFilter

- AnonymousAuthenticationFilter

- OAuth2AuthorizationCodeGrantFilter

- SessionManagementFilter

- ExceptionTranslationFilter

- FilterSecurityInterceptor

- SwitchUserFilter

## 添加 `filter` 流程

## Spring Security中，安全构建器HttpSecurity和WebSecurity的区别是 :

- WebSecurity不仅通过HttpSecurity定义某些请求的安全控制，也通过其他方式定义其他某些请求可以忽略安全控制;
- HttpSecurity仅用于定义需要安全控制的请求(当然HttpSecurity也可以指定某些请求不需要安全控制);
- 可以认为HttpSecurity是WebSecurity的一部分，WebSecurity是包含HttpSecurity的更大的一个概念;

构建目标不同

- WebSecurity构建目标是整个Spring Security安全过滤器FilterChainProxy,
- 而HttpSecurity的构建目标仅仅是FilterChainProxy中的一个SecurityFilterChain

## 其他

```
AuthenticationEntryPoint 用来解决匿名用户访问无权限资源时的异常

AccessDeineHandler 用来解决认证过的用户访问无权限资源时的异常
```