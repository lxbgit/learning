# `FilterSecurityInterceptor`

- `SecurityMetadataSource`
- `AccessDecisionManager`
- `AuthenticationManager`

## 调用顺序

- 先将正在请求调用的受保护对象传递给`beforeInvocation()`方法进行权限鉴定。
  
- 权限鉴定失败就直接抛出异常`AccessDeniedException`了。
  
- 鉴定成功将尝试调用受保护对象，调用完成后，不管是成功调用，还是抛出异常，都将执行`finallyInvocation()`。
  
- 如果在调用受保护对象后没有抛出异常，则调用`afterInvocation()`
