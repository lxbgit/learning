# GlobalMethodSecurity

Spring Security对应方法级的注解主要又可以分为两类

- `@Secured` 注解 - `secured-annotations`

- `@PreAuthorize`, `@PreFilter`, `@PostAuthorize` and `@PostFilter` - `pre-post-annotations`

- `@PreAuthorize`

> 注解适合进入方法前的权限验证， @PreAuthorize可以将登录用户的roles/permissions参数传到方法中

- `@PostAuthorize`

> 注解使用并不多，在方法执行后再进行权限验证

- `@Secured`

> @Secured 源于 Spring之前版本.它有一个局限就是不支持Spring EL表达式

- ``

## 使用

> `@EnableGlobalMethodSecurity(prePostEnabled = true,securedEnabled = true)`
