# Standard Filter Aliases and Ordering

- 首先需要了解spring security内置的各种filter：

| Alias | Filter Class | Namespace Element or Attribute|
| ----- | ------------ | ----------------------------- |
| CHANNEL_FILTER | `ChannelProcessingFilter`| `http/intercept-url@requires-channel` |
| SECURITY_CONTEXT_FILTER | `SecurityContextPersistenceFilter` | `http` |
| CONCURRENT_SESSION_FILTER | `ConcurrentSessionFilter` | `session-management/concurrency-control` |
| HEADERS_FILTER | `HeaderWriterFilter` | `http/headers` |
| CSRF_FILTER |	`CsrfFilter` | `http/csrf` |
| LOGOUT_FILTER | `LogoutFilter` | `http/logout` |
| X509_FILTER |	`X509AuthenticationFilter` | `http/x509` |
| PRE_AUTH_FILTER | `AbstractPreAuthenticatedProcessingFilter` Subclasses |	N/A |
| CAS_FILTER | `CasAuthenticationFilter` | N/A |
| FORM_LOGIN_FILTER | `UsernamePasswordAuthenticationFilter` | `http/form-login` |
| BASIC_AUTH_FILTER | `BasicAuthenticationFilter` | `http/http-basic` |
| SERVLET_API_SUPPORT_FILTER | `SecurityContextHolderAwareRequestFilter` | `http/@servlet-api-provision` |
| JAAS_API_SUPPORT_FILTER |	`JaasApiIntegrationFilter` | `http/@jaas-api-provision` |
| REMEMBER_ME_FILTER |	`RememberMeAuthenticationFilter` | `http/remember-me` |
| ANONYMOUS_FILTER |	`AnonymousAuthenticationFilter` | `http/anonymous` |
| SESSION_MANAGEMENT_FILTER | `SessionManagementFilter`	| `session-management` |
| EXCEPTION_TRANSLATION_FILTER|	`ExceptionTranslationFilter` |	`http` |
| FILTER_SECURITY_INTERCEPTOR |	`FilterSecurityInterceptor` |	`http` |
| SWITCH_USER_FILTER | `SwitchUserFilter` |	N/A |

## `filters`

 - `org.springframework.security.web.context.SecurityContextPersistenceFilter`
 - `org.springframework.security.web.session.ConcurrentSessionFilter`
 - `org.springframework.security.web.context.request.async.WebAsyncManagerIntegrationFilter`
 - `org.springframework.security.web.header.HeaderWriterFilter`
 - `org.springframework.security.web.authentication.logout.LogoutFilter`
 - `org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter`
 - `org.springframework.security.web.authentication.www.BasicAuthenticationFilter`
 - `org.springframework.security.web.savedrequest.RequestCacheAwareFilter`
 - `org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestFilter`
 - `org.springframework.security.web.authentication.AnonymousAuthenticationFilter`
 - `org.springframework.security.web.session.SessionManagementFilter`
 - `org.springframework.security.web.access.ExceptionTranslationFilter`
 - `org.springframework.security.web.access.intercept.FilterSecurityInterceptor`
 - `org.springframework.security.web.access.intercept.FilterSecurityInterceptor`

---

- ChannelProcessingFilter

- ConcurrentSessionFilter

- WebAsyncManagerIntegrationFilter

- SecurityContextPersistenceFilter

- HeaderWriterFilter

- CorsFilter

- CsrfFilter

- LogoutFilter

- OAuth2AuthorizationRequestRedirectFilter

- Saml2WebSsoAuthenticationRequestFilter

- X509AuthenticationFilter

- AbstractPreAuthenticatedProcessingFilter

- CasAuthenticationFilter

- OAuth2LoginAuthenticationFilter

- Saml2WebSsoAuthenticationFilter

- UsernamePasswordAuthenticationFilter

- ConcurrentSessionFilter

- OpenIDAuthenticationFilter

- DefaultLoginPageGeneratingFilter

- DefaultLogoutPageGeneratingFilter

- DigestAuthenticationFilter

- BearerTokenAuthenticationFilter

- BasicAuthenticationFilter

- RequestCacheAwareFilter

- SecurityContextHolderAwareRequestFilter

- JaasApiIntegrationFilter

- RememberMeAuthenticationFilter

- AnonymousAuthenticationFilter

- OAuth2AuthorizationCodeGrantFilter

- SessionManagementFilter

- ExceptionTranslationFilter

- FilterSecurityInterceptor

- SwitchUserFilter

## 添加 `filter` 流程
