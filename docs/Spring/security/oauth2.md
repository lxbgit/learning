# OAuth2

[参考1](https://juejin.im/post/5a3cbce05188252582279467)

## 基本概念

- `client`：第三方应用（即App或向外提供接口）

https://github.com/spring-projects/spring-security/tree/main/oauth2/oauth2-client

```gradle
// https://mvnrepository.com/artifact/org.springframework.security/spring-security-oauth2-client
implementation group: 'org.springframework.security', name: 'spring-security-oauth2-client', version: '5.5.0'
```

- `Resource Owner`:资源所有者（即用户）

- `Authentication Server`:授权认证服务（发配Access Token）

- `Resource Server`：资源服务器（存储用户资源信息等资源）

https://github.com/spring-projects/spring-security/tree/main/oauth2/oauth2-resource-server

```gradle
// https://mvnrepository.com/artifact/org.springframework.security/spring-security-oauth2-resource-server
implementation group: 'org.springframework.security', name: 'spring-security-oauth2-resource-server', version: '5.5.0'
```

## 授权认证服务

主要包括三个主要类

- `ClientDetailsServiceConfigurer`：用来配置客户端详情服务

- `AuthorizationServerSecurityConfigurer`：用来配置令牌端点(Token Endpoint)的安全约束

- `AuthorizationServerEndpointsConfigurer`：来配置授权（authorization）以及令牌（token）的访问端点和令牌服务(token services)

### 端点接入-endpoints
  
> 授权认证是使用AuthorizationEndpoint这个端点来进行控制，一般使用AuthorizationServerEndpointsConfigurer 来进行配置


- 1).端点（endpoints）的相关属性配置：
    - `authenticationManager`：认证管理器。若我们上面的Grant Type设置为password，则需设置一个AuthenticationManager对象
    - `userDetailsService`：若是我们实现了UserDetailsService,来管理用户信息，那么得设我们的userDetailsService对象
    - `authorizationCodeServices`：授权码服务。若我们上面的Grant Type设置为authorization_code，那么得设一个AuthorizationCodeServices对象
    - `tokenStore`：这个就是我们上面说到，把我们想要是实现的Access Token类型设置
    - `accessTokenConverter`：Access Token的编码器。也就是JwtAccessTokenConverter
    - `tokenEnhancer`:token的拓展。当使用jwt时候，可以实现TokenEnhancer来进行jwt对包含信息的拓展
    - `tokenGranter`：当默认的Grant Type已经不够我们业务逻辑，实现TokenGranter 接口，授权将会由我们控制，并且忽略Grant Type的几个属性。

- 2).端点（endpoints）的授权url：要授权认证，肯定得由url请求，才可以传输。因此OAuth2提供了配置授权端点的URL。

`AuthorizationServerEndpointsConfigurer` ，还是这个配置对象进行配置，其中由一个`pathMapping()`方法进行配置授权端点URL路径，默认提供了两个参数`defaultPath`和`customPath`：

```
public AuthorizationServerEndpointsConfigurer pathMapping(String defaultPath, String customPath) {
		this.patternMap.put(defaultPath, customPath);
		return this;
}
```

pathMapping的defaultPath有：

```
/oauth/authorize：授权端点
/oauth/token：令牌端点
/oauth/confirm_access：用户确认授权提交端点
/oauth/error：授权服务错误信息端点
/oauth/check_token：用于资源服务访问的令牌解析端点
/oauth/token_key：提供公有密匙的端点，如果使用JWT令牌的话
```

> 注：pathMapping的两个参数都将以 "/" 字符为开始的字符串
