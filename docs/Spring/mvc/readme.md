# Spring MVC

## 过滤器、拦截器、控制器

 filter -> interceptor -> controller ->  interceptor -> filter

## @RequestMapping

- `name`: 请求名

- `value`：指定请求的实际地址， 比如 /action/info之类

- `method`： 指定请求的method类型， GET、POST、PUT、DELETE等

- `consumes`： 指定处理请求的提交内容类型（Content-Type），例如application/json, text/html;

- `produces`: 指定返回的内容类型，仅当request请求头中的(Accept)类型中包含该指定类型才返回

- `params`： 指定request中必须包含某些参数值是，才让该方法处理

- `headers`： 指定request中必须包含某些指定的header值，才能让该方法处理请求

## `@ModelAttribute`

> [参考](https://www.cnblogs.com/canger/p/10241576.html)

### `@ModelAttribute`用于注解方法

#### 方法返回类型为`void`

> 这种情况，`@ModelAttribute`只是单纯的作为请求路由的第一站，使用者可在方法内部操作`Model`和`Request`等参数实现功能

#### 方法返回类型不为void

> 这种情况，`@ModelAttribute`会将返回值放到`Model`中，并将该类型名称的首字母小写作为model中的属性名。
  请求地址和参数不变

>对于返回类型为void的方法，@ModelAttribute也会在model中添加一对键值对，“void”->"null"

#### 方法返回类型不为void，且`@ModelAttribute`指定属性名称

> 这种情况下，`@ModelAttribute`会将返回值放到`Model`中，且对应的key值为`@ModelAttribute`设置的属性名

#### `@ModelAttribute`和`@RequestMapping`注解同一个方法

- 在`controller`处理其他请求时，不会再首先进入被`@ModelAttribute`和`@RequestMapping`同时注解的方法；

- 该方法的返回值不再是视图的逻辑名称，而是按照`@ModelAttribute`的规则被加入到`Model`中

- `@RequestMapping`注解的`value`值具有两个作用

  - 作为URI，实现请求的路由

  - 作为此次请求的逻辑视图名（严格来说此时视图的逻辑视图名是：controller的RequestMapping值+method的RequestMapping值）

### `@ModelAttribute`注解方法入参

> @ModelAttribute("attrName")用在方法入参上时，作用为:
  
- 从当前的隐式model对象中取key值attrName所对应的attrValue值，并将attrValue赋给被注解的参数。
- 而且自动暴露为模型数据用于视图页面展示时使用
