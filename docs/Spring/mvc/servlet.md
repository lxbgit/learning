# Servlet 和 Filter

## 调用流程

```java
filter1前 -> filter2前 -> ... -> servlet -> ... -> filter2后  -> filter1后

```

分割线为是否调用了 `chain?.doFilter(request, response)`

```kotlin
println("filter 前")
chain?.doFilter(request, response)
println("filter 后")
```

## 注意点

如果两个`filter`都返回了数据，

- 如果`filter1`中调用了`response?.writer?`，那么`filter2`中也必须使用`writer`，否则会报错
- 如果`filter1`中调用了`response?.outputStream?`，那么`filter2`中也必须使用`outputStream`，否则会报错

返回数据后，要关闭流，即 `response?.writer?.close()` 或者 `response?.outputStream?.close()`, 如果不关闭后面的响应流还有写入的话，也会写入响应流中，造成数据错误

> 总之，一旦返回了数据，就要将响应流关闭
