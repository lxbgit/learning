# SpringMVC 拾遗1

## 文件上传

- `MultipartFile`

> Spring 提供的支持

- `javax.servlet.http.Part`

> servlet 3.0，才可以使用

## "多部分处理"

在 `RESTful` 服务场景中，也可以从非浏览器客户端提交多部分请求。以下示例显示了一个包含 JSON 的文件
[参考着](https://docs.spring.io/spring-framework/docs/current/reference/html/web.html#mvc-multipart-forms)

多部分处理数据时，使用`org.springframework.web.bind.annotation.RequestPart`

例如：

```openapi
POST /someUrl
Content-Type: multipart/mixed

--edt7Tfrdusa7r3lNQc79vXuhIIMlatb7PQg7Vp
Content-Disposition: form-data; name="meta-data"
Content-Type: application/json; charset=UTF-8
Content-Transfer-Encoding: 8bit

{
    "name": "value"
}
--edt7Tfrdusa7r3lNQc79vXuhIIMlatb7PQg7Vp
Content-Disposition: form-data; name="file-data"; filename="file.properties"
Content-Type: text/xml
Content-Transfer-Encoding: 8bit
... File Data ...
```

可以使用 `@RequestParam` 作为字符串访问“元数据”部分，但可能希望它从 JSON 反序列化（类似于 `@RequestBody`）。在使用 HttpMessageConverter 转换多部分后，使用 `@RequestPart` 批注访问它

```java
@PostMapping("/")
public String handle(@RequestPart("meta-data") MetaData metadata,
        @RequestPart("file-data") MultipartFile file) {
    // ...
}
```
