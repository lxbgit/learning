# Spring 学习笔记

## spring 介绍

- spring是轻量级的
- IoC 控制反转
- AOP 面向切面的编程
- spring 包含并管理应用中对象的生命周期和配置
- MVC 模式
- 事务管理
- 异常处置

## IoC

- 构造器
- Setter
- 静态注入

## spring bean的作用域

- singleton
- prototype
- request
- session
- global-session

## `BeanFactory` 和 `FactoryBean`

- `BeanFactory`

    1、`BeanFactory`: 以`Factory`结尾，表示它是一个工厂类(接口)，用于管理`Bean`的一个工厂。在`Spring`中，`BeanFactory`是`IOC`容器的核心接口，它的职责包括：实例化、定位、配置应用程序中的对象及建立这些对象间的依赖。

    2、`Spring`为我们提供了许多易用的`BeanFactory`实现，`XmlBeanFactory`就是常用的一个，该实现将以`XML`方式描述组成应用的对象及对象间的依赖关系。`XmlBeanFactory`类将持有此`XML`配置元数据，并用它来构建一个完全可配置的系统或应用。

- `FactoryBean`

    1、以`Bean`结尾，表示它是一个`Bean`，不同于普通`Bean`的是：它是实现了`FactoryBean<T>`接口的`Bean`，根据该`Bean`的`ID`从`BeanFactory`中获取的实际上是`FactoryBean`的`getObject()`返回的对象，而不是`FactoryBean`本身，如果要获取`FactoryBean`对象，请在`id`前面加一个`&`符号来获取。

- `@Controller`

`Controller`: 不同包下的同名Controller，在注解时，为了避免冲突，建议为每个`Controller`都添加一个名字，例如：

```java
// com.lxb.mobile.ctrl
@Controller("testCtrl1")
public class TestController {
}

// com.lxb.ctrl
@Controller("testCtrl2")
public class TestController {
}
```

## spring jdbc

### `LIKE`

- 在sql语句中设置（%）

> `select * from pub_user WHERE 1 = 1 AND username LIKE '%' :username '%'`
`:username` 两边一定要有空格  否则查不出数据

- 在参数的值里设置（%），查询sql语句就只是个命名参数

> `select * from user WHERE 1=1 AND username LIKE :username`

`"%" + username + "%"`

## spring mvc 文件上传

- 需要的jar包

`commons-io.jar` 和 `commons-fileupload.jar`

- 配置

```xml
<bean id="multipartResolver" class="org.springframework.web.multipart.commons.CommonsMultipartResolver">
  <property name="maxUploadSize" value="100000000000000"/>
  <property name="defaultEncoding" value="utf-8"></property>
</bean>
```

> 在Spring mvc的配置文件中配置

- java 代码

```java
@RequestMapping(value = "/file/fileUpLoad", method = RequestMethod.POST)
@ResponseBody
public void fileUpLoad(@RequestParam(value = "uploadFile", required = false) MultipartFile[] files,
		HttpServletRequest request, HttpServletResponse response) {
	response.setCharacterEncoding("utf-8");
	response.setContentType("text/html");
	try {
		String resultFileId = fileService.saveFileUpLoad(files);
		if(resultFileId != null && !"".equals(resultFileId)){
			try {
				response.getWriter().write(resultFileId);// "文件上传成功"
			} catch (IOException e) {
				// TODO Auto-generated catch block
				if(logger.isDebugEnabled()){
					logger.debug("上传出错",e);
				}
			}
		}else{
			try {
				response.getWriter().write("-1");
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	} catch (Exception e) {
		if(logger.isDebugEnabled()){
			logger.debug("上传出错",e);
		}
		try {
			response.getWriter().write("-1");
		} catch (IOException e1) {
			if(logger.isDebugEnabled()){
				logger.debug("上传出错",e);
			}
		}
	}
	// model.addAttribute("fileResult", "文件上传成功");
	
}
```

## Jackson

### 序列化

- `JsonInclude.Include.ALWAYS`      默认
- `JsonInclude.Include.NON_DEFAULT` 属性为默认值不序列化
- `JsonInclude.Include.NON_EMPTY`   属性为 空（””） 或者为 NULL 都不序列化
- `JsonInclude.Include.NON_NULL`    属性为NULL   不序列化

### Jackson注解:

2、`@JsonIgnoreProperties`

此注解是类注解，作用是json序列化时将java bean中的一些属性忽略掉，序列化和反序列化都受影响。

3、`@JsonIgnore`

此注解用于属性或者方法上（最好是属性上），作用和上面的@JsonIgnoreProperties一样。

4、`@JsonFormat`

此注解用于属性或者方法上（最好是属性上），可以方便的把Date类型直接转化为我们想要的模式，比如`@JsonFormat(pattern = "yyyy-MM-dd HH-mm-ss")`

5、`@JsonSerialize`

此注解用于属性或者getter方法上，用于在序列化时嵌入我们自定义的代码，比如序列化一个double时在其后面限制两位小数点。

6、`@JsonDeserialize`

此注解用于属性或者 `setter` 方法上，用于在反序列化时可以嵌入我们自定义的代码，类似于上面的`@JsonSerialize`

## 获取SpringMVC中所有RequestMapping映射URL信息

```java

@Autowired
private ApplicationContext applicationContext;

Set<String> noLoginUrlSet = new HashSet<>();
RequestMappingHandlerMapping mapping = applicationContext.getBean(RequestMappingHandlerMapping.class);
Map<RequestMappingInfo, HandlerMethod> handlerMethods = mapping.getHandlerMethods();// 就是这个
for (RequestMappingInfo rmi : handlerMethods.keySet()) {
   HandlerMethod handlerMethod = handlerMethods.get(rmi);
   if (handlerMethod.hasMethodAnnotation(NoLogin.class)) {
      PatternsRequestCondition prc = rmi.getPatternsCondition();
      Set<String> patterns = prc.getPatterns();
      noLoginUrlSet.addAll(patterns);
   }
}
```
