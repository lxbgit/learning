# Mac 工具

## Homebrew

### [Homebrew官网](https://brew.sh/)

> 安装官网教程一步步来

brew使用例子：

- `brew install`

```shell
# brew安装postgresql
brew install postgresql

# brew link 出错
```

- `brew list`

> 列出所有brew安装的package

- `brew link`出错，解决办法：

```shell
sudo chown -R $USER /usr/local/lib /usr/local/include /usr/local/bin /usr/local/Cellar
```

[详细](https://superuser.com/questions/913069/homebrew-error-could-not-symlink-lib-gcc-4-9)

------

### 使用`brew`安装`wget`

```shell
brew install wget
```

> 测试 `wget http://www.arefly.com/`

## MacPort

> Mac下使用MacPort安装软件，类似ubuntu下的apt-get一样，可以快速安装软件

安装：
[官方网站](http://www.macports.org/)下载最新版，然后安装。

使用：
安装完之后，打开终端，输入port，提示找不到port命令，是因为port命令没在path路径下，解决方法：
将`/opt/local/bin`和`/opt/local/sbin`添加到$PATH搜索路径中:
在`/etc/profile`或`~/.bash_profile`中添加如下语句：

```shell
 export PATH="/opt/local/bin:$PATH"
 export PATH="/opt/local/sbin:$PATH"
```

然后，重新打开终端，再输入port，就可以使用了！！

------

以下为常用的命令：
更新ports tree和MacPorts版本：`sudo port -v selfupdate`
搜索索引中的软件：`port search name`
安装新软件：`port install name`
卸载软件：`sudo port uninstall name`
查看有更新的软件以及版本：`port outdated`
升级可以更新的软件：`sudo port upgrade outdated`

------

另外mac还有另一种类似的安装工具叫[brew](https://brew.sh/)，具体用法请查看官网

## SDKMAN

[官网](https://sdkman.io/usage)

> 管理SDK

### 安装

[参考](https://sdkman.io/install)

### 使用

[Usage](https://sdkman.io/usage)

## Xcode

> 为了免下载安装Xcode，安装时使用了别人提供的Xcode.dmg安装，而非使用自己账号在AppStore下载的。

这样的安装模式会出现一个问题，更新Xcode时AppStroe会提示让你输入
 **下载该Xcode的账号及密码** ，该账号不能直接修改。下面的方法可以删除原有账号信息，而后可以使用新的账号信息更新。

做如下步骤:

- 打开引用程序目录
- 找到`Xcode`，右键“显示包内容”
- 找到`_MASReceipt`文件夹
- 删除它，然后退出重启`AppStroe`，更新`Xcode`即可。

## mac 配置JAVA_HOME

打开终端：

- `cd` 根目录下

- `vi .bash_profile`

```shell
vi .bash_profile

# 1、打开终端，输入
vim ~/.bash_profile
```

- 添加下面代码

```shell
export JAVA_HOME=$(/usr/libexec/java_home)
export PATH=$JAVA_HOME/bin:$PATH
export CLASS_PATH=$JAVA_HOME/lib
```

> 添加完后点按`esc`(确认输入无效)后输入`:wq`(没引号)

- 使配置生效

```shell
source .bash_profile
```

------

> `ls -l /usr/libexec/java_home` 查看java_home

```shell
cd /Library/Java/JavaVirtualMachines
```

## 终端命令行用`sublime`打开文件或者目录

- 进入终端

```shell
cd
```

- `ls -al`

> 找到`.zshrc`，这个是zsh的配置文件

- `sudo vi .zshrc`

> `alias atom='/Applications/Atom.app/Contents/MacOS/Atom'`
> `alias subl='/Applications/SublimeText.app/Contents/SharedSupport/bin/subl'`
> `alias code='/Applications/Visual\ Studio\ Code.app/Contents/Resources/app/bin/code`

```shell
# 使编辑生效
source ~/.zshrc
```

- 保存

## Mac下使用brew安装redis

使用Homebrew安装[Redis](http://lib.csdn.net/base/redis)可以减少大量的安装和配置的工作量。
安装命令 `brew install redis` 安装完成后的提示信息

```shell
To have launchd start redis at login: ln -sfv /usr/local/opt/redis/*.plist ~/Library/LaunchAgents Then to load redis now: launchctl load ~/Library/LaunchAgents/homebrew.mxcl.redis.plist Or, if you don’t want/need launchctl, you can just run: redis-server /usr/local/etc/redis.conf
````

开机启动redis命令:
`$ ln -sfv /usr/local/opt/redis/*.plist ~/Library/LaunchAgents`
使用launchctl启动redis server:
`$ launchctl load ~/Library/LaunchAgents/homebrew.mxcl.redis.plist`
使用配置文件启动redis server:
`$ redis-server /usr/local/etc/redis.conf`
停止redis server的自启动:
`$ launchctl unload ~/Library/LaunchAgents/homebrew.mxcl.redis.plist`
redis 配置文件的位置 `/usr/local/etc/redis.conf`

卸载redis和它的文件:

```shell
 brew uninstall redis
 rm ~/Library/LaunchAgents/homebrew.mxcl.redis.plist
```

测试redis server是否启动 $ redis-cli ping

## 其他

### `brew` 开启服务

- `brew services restart mosquitto` 开启mosquitto服务`

### Mac 常用快捷键

- `Command+Shift+.` 可以显示隐藏文件、文件夹，再按一次，恢复隐藏；
- `Command+shift+.` 可以显示/隐藏以"."开头的文件或者文件夹`

- `finder` 下使用`Command+Shift+G` 可以前往任何文件夹，包括隐藏文件夹。
