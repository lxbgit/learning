# Nginx 在mac中使用

## 安装

```shell
brew search nginx
brew install nginx
```

## 安装日志

```shell
Docroot is: /usr/local/var/www

The default port has been set in /usr/local/etc/nginx/nginx.conf to 8080 so that
nginx can run without sudo.

nginx will load all files in /usr/local/etc/nginx/servers/.

To have launchd start nginx now and restart at login:
  brew services start nginx
Or, if you don't want/need a background service you can just run:
  nginx
```

## 配置

```nginx
worker_processes  4;  #Nginx运行时使用的CPU核数

events {
    worker_connections  1024;  #一个woeker进程的最大连接数
}

http {  #Nginx用作虚拟主机时使用。每一个server模块生成一个虚拟主机。
    include       mime.types;  #定义MIME类型和后缀名关联的文件的位置
    default_type  application/octet-stream;  #指定mime.types文件中没有记述到的后缀名的处理方法

    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '  #定义日志的格式。可以选择main或者ltsv，后面定义要输出的内容。
　　　　　　　　　　　　　'$status $body_bytes_sent "$http_referer" ' 　　　　　　　　　　　　　'"$http_user_agent" "$http_x_forwarded_for"';      1.$remote_addr 与$http_x_forwarded_for 用以记录客户端的ip地址； 2.$remote_user ：用来记录客户端用户名称； 3.$time_local ：用来记录访问时间与时区； 4.$request ：用来记录请求的url与http协议； 5.$status ：用来记录请求状态；  6.$body_bytes_s ent ：记录发送给客户端文件主体内容大小； 7.$http_referer ：用来记录从那个页面链接访问过来的； 8.$http_user_agent ：记录客户端浏览器的相关信息；。
    access_log  /usr/local/var/log/nginx/access.log  main;  #连接日志的路径，上面指定的日志格式放在最后

    sendfile        on;  #是否使用OS的sendfile函数来传输文件
    keepalive_timeout  65;  #HTTP连接的持续时间。设的太长会使无用的线程变的太多


    server { 
        listen       80;  #监听端口
        server_name  localhost;  #服务地址

        charset utf-8;   #编码方式

        access_log  /usr/local/var/log/nginx/localhost.access.log  main;  #nginx请求日志地址

        root /var/www; #你的网站根目录
        location / { # 所有的请求都会走的路径
            index  index.html index.htm index.php;
        　　 try_files $uri /$uri index.php?$args;  #从左边开始找指定文件是否存在
        }
     

        error_page  404              /404.html;
        error_page   500 502 503 504  /50x.html;
        location = /50x.html {
            root   html;
        }

        location ~ \.php$ {  # 正则表达式: .php文件走的路径
            fastcgi_pass   127.0.0.1:9000; #走fastcgi 路径
            fastcgi_index  index.php;
            fastcgi_param  SCRIPT_FILENAME  /var/www$fastcgi_script_name; #定义的根目录 以及 请求的脚本名
            include        fastcgi_params; # 请求参数
        }
   
        location ~ /\.ht { # 当前项目的根路径
            deny  all;
        }
    }

    include sites-enabled/nginx-*.conf;   # 添加的文件其他虚拟主机配置
}
```

## 使用

> tomcat和nginx配合使用，nginx作为服务转发
