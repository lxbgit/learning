# mosquitto 用户配置

> [参考](https://mosquitto.org/man/mosquitto_passwd-1.html)

> mosquitto中可以添加多个用户，只有使用用户名和密码登陆服务器才允许用户进行订阅与发布操作。可以说用户机制是mosquitto重要的安全机制，增强服务器的安全性

- 修改配置文件 `mosquitto.conf`

## 首先我们来新增两个用户 1： admin/admin 2: mosquitto/mosquitto 具体步骤

- 打开mosquitto.conf文件，找到allow_anonymous节点，这个节点作用是，是否开启匿名用户登录，默认是true。打开此项配置（将前面的 # 号去掉）之后将其值改为true

```conf
修改前：#allow_anonymous
修改后：allow_anonymous false
```

- 找到`password_file`节点，这个节点是告诉服务器你要配置的用户将存放在哪里。打开此配置并指定`pwfile.example`文件路劲（注意是绝对路劲）

> 保存用户名与密码, pwfile.example (根据需要自己定义)

```conf
修改前：#password_file
修改后：password_file /etc/mosquitto/pwfile.example （这里的地址根据自己文件实际位置填写）
```

- 创建用户名和密码、打开命令窗口 键入如下命令

> [mosquitto_passwd](https://mosquitto.org/man/mosquitto_passwd-1.html)

```bash
mosquitto_passwd -c /etc/mosquitto/pwfile.example admin
```

> 提示连续两次输入密码、创建成功。命令解释： -c 创建一个用户、/etc/mosquitto/pwfile.example 是将用户创建到 `pwfile.example`  文件中、admin 是用户名

- 创建mosquitto用户。在命令窗口键入如下命令

```bash
mosquitto_passwd /etc/mosquitto/pwfile.example mosquitto
```

> 同样连续会提示连续输入两次密码。注意第二次创建用户时不用加 -c 如果加 -c 会把第一次创建的用户覆盖

> 注意,mosquitto_passwd -c命令每次都只会生成只包含一个用户的文件,如果你想在passwd.conf中存放多个用户， 可以使用mosquitto_passwd -b 命令

`mosquitto_passwd -b [最终生成的password_file文件]  [用户名]  [密码]`

> mosquitto_passwd -b命令必须在控制台输入明文的密码，且每次只是在passwd.conf中新增一个用户，不会覆盖之前已生成的用户

- 使用`mosquitto_passwd -D [用户名]`命令删除一个用户

`mosquitto_passwd -D lxb`

## 增加权限配置

> 保存权限配置 aclfile.example (根据需要自己定义)
> 打开配置文件 aclfile.example 在其中添加如下配置信息

```conf
# admin 设置为订阅权限，并且只能访问的主题为"root/topic/#"
user admin
topic read root/topic/#

# mosquitto 设置为发布权限，并且只能访问的主题为"root/topic/#"
user mosquitto
topic write root/topic#
```

- 打开mosquitto.conf文件，找到acl_file节点。打开配置做如下修改：

```conf
修改前：#acl_file
修改后：acl_file /etc/mosquitto/aclfile.example (根据自己文件实际位置填写)
```
