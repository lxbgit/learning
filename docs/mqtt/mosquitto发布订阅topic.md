# 发布订阅

## `mosquitto_pub`

[参考](https://mosquitto.org/man/mosquitto_pub-1.html)

```shell
mosquitto_pub { [-h hostname] [-p port-number] [-u username] [-P password] -t message-topic... | -L URL } [-A bind-address] [-c] [-d] [-D command identifier value] [-i client-id] [-I client-id-prefix] [-k keepalive-time] [-q message-QoS] [--quiet] [-r] [--repeat count] [--repeat-delay seconds] [-S] { -f file | -l | -m message | -n | -s } [ --will-topic topic [--will-payload payload] [--will-qos qos] [--will-retain] ] [[ { --cafile file | --capath dir } [--cert file] [--key file] [--ciphers ciphers] [--tls-version version] [--tls-alpn protocol] [--tls-engine engine] [--keyform { pem | engine }] [--tls-engine-kpass-sha1 kpass-sha1] [--insecure] ] | [ --psk hex-key --psk-identity identity [--ciphers ciphers] [--tls-version version] ]] [--proxy socks-url] [-V protocol-version]

mosquitto_pub [--help]
```

## `mosquitto_sub`

```shell
mosquitto_sub { [-h hostname] [-p port-number] [-u username] [-P password] -t message-topic... | -L URL [-t message-topic...] } [-A bind-address] [-c] [-C msg-count] [-d] [-D command identifier value] [-E] [-i client-id] [-I client-id-prefix] [-k keepalive-time] [-N] [-q message-QoS] [--remove-retained] [ -R | --retained-only ] [--retain-as-published] [-S] [-T filter-out...] [-U unsub-topic...] [-v] [-V protocol-version] [-W message-processing-timeout] [--proxy socks-url] [--quiet] [ --will-topic topic [--will-payload payload] [--will-qos qos] [--will-retain] ] [[ { --cafile file | --capath dir } [--cert file] [--key file] [--tls-version version] [--tls-alpn protocol] [--tls-engine engine] [--keyform { pem | engine }] [--tls-engine-kpass-sha1 kpass-sha1] [--insecure] ] | [ --psk hex-key --psk-identity identity [--tls-version version] ]]

mosquitto_sub [--help]
```
