# MQTT

> MQTT现在主要用于即时通讯，物联网M2M，物联网采集等

## 服务器搭建

### [mosquitto](https://github.com/eclipse/mosquitto)

> [参考](https://mosquitto.org/blog/)

- 安装（Mac）

`brew install mosquitto`

安装成功

```bash
mosquitto has been installed with a default configuration file.
You can make changes to the configuration by editing:
    /usr/local/etc/mosquitto/mosquitto.conf

To have launchd start mosquitto now and restart at login:
  brew services start mosquitto
Or, if you don't want/need a background service you can just run:
  mosquitto -c /usr/local/etc/mosquitto/mosquitto.conf
```

- 测试

  - 启动服务
  ``brew services restart mosquitto``
  - 再打开一个终端订阅主题
  ``mosquitto_sub  -h 127.0.0.1 -p 1883 -v -t testtopic``
  - 此时在第一个终端发布topic
  ``mosquitto_pub  -h 127.0.0.1 -p 1883 -t testtopic -m helloworld``
  > 结果：``testtopic helloworld``
  
### `mosquitto_sub`命令

| 命令|描述|
|---|---|
|-h | 服务器主机，默认localhost |
|-t | 指定主题 |
|-u | 用户名 |
|-P | 密码 |
|-i | 客户端id，唯一 |
|-m | 发布的消息内容 |

### 其他
