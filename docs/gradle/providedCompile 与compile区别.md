# gradle providedCompile 与compile区别

> `maven:provided`
    provided:在编译和测试的过程有效，最后生成war包时不会加入，诸如：servlet-api，
    因为servlet-api，tomcat等web服务器已经存在了，如果再打包会冲突
> `gradle:provided`和`compile`的区别:
    如果你的jar包/依赖代码 仅在编译的时候需要，但是在运行时不需要依赖,就用`providedCompile`
    此属性 相当于maven中的`provided`

## Gradle compile

> 如果你的jar包/依赖代码 在编译的时候需要依赖，在运行的时候也需要，那么就用compile
   例如 ：

```groovy
compile group: 'org.springframework', name: 'spring-context', version: '5.0.2.RELEASE'
```

> gradle 5.x 使用 `api` 或者 `implementation`

## Gradle providedCompile

> 如果你的jar包/依赖代码 仅在编译的时候需要，但是在运行时不需要依赖,就用providedCompile

```groovy
compileOnly group: 'javax.servlet', name: 'servlet-api', version:'3.1.0'
compileOnly group: 'javax.servlet', name: 'jsp-api', version: '2.0'
```
