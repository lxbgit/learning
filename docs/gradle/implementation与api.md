# `implementation` 和 `api` 区别

https://docs.gradle.org/current/userguide/upgrading_version_6.html#sec:configuration_removal

## `implementation`

应该用于声明依赖项，这些依赖项是库的实现细节：它们在编译期间对库的使用者不可见

## `api`

仅当应用使用了 `java-library` 插件时才可用，应该用于声明作为库 API 一部分的依赖项，这些依赖项需要在编译时暴露给使用者

> 在 Gradle 7 中，`compile`和`runtime`配置都被删除了。因此，必须迁移到上面的`implementation`和`api`配置。如果您仍在为 Java 库使用 `java` 插件，则需要改为应用 `java-library` 插件。

- build.gradle.kts

```gradle
plugins {
    `java-library`
}

dependencies {
    api('....')
}
```
