# ELK

[参考文档](https://blog.csdn.net/xllove2008/article/details/107041269/)

统一的日志服务,在微服务框架中，所有的系统调用边界、请求接入接出边界，都存在统一的日志埋点，这些日志埋点和监控系统对接，可以方便的查看系统的运行各项指标，同时也可以根据日志跟踪到一个服务从前到后的整个调用链路。

elastic官网地址：https://www.elastic.co/cn/

elastic产品地址：https://www.elastic.co/cn/elastic-stack

yum源地址：https://mirrors.tuna.tsinghua.edu.cn/elasticstack/yum

ELK主要由ElasticSearch、Logstash和Kibana三个开源工具组成，还有其他专门由于收集数据的轻量型数据采集器Beats。

## ELK简介

- Elasticsearch: 分布式搜索引擎

- Logstash: 数据收集处理引擎

- Kibana: 可视化化平台

- Filebeat: 轻量级数据收集引擎

## 版本说明

Elasticsearch、Logstash、Kibana、Filebeat安装的版本号必须全部一致,不然会出现kibana无法显示web页面。

![图片](./20200702094415648.png)

## 安装

### 环境检查

- yum

- java

- ELK yum 源

```shell
[ELK]
name=ELK-Elasticstack
baseurl=https://mirrors.tuna.tsinghua.edu.cn/elasticstack/yum/elastic-7.x/
gpgcheck=0
enabled=1
```

### 部署安装
