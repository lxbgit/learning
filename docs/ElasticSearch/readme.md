# Elasticsearch

[这篇文章不错](https://www.sitepoint.com/full-text-search-rails-elasticsearch/)

| 关系型数据库 | Elasticsearch|
|:--------------: | :------------: |
|Databases(数据库)| Indices(索引)|
|Tables(表) |Types(类型)|
|Rows(行) |Documents(文档)|
|Columns(列) |Fields(字段)|

## ES与Mysql对应关系

- `index` –> `DB`

- `type` –> `Table`

- `Document` –> `row`

## `@Field`

- `analyzer`：指定分词器，es中默认使用的标准分词器，比如我们需要指定中文IK分词器，可以指定值为ik_max_word
- `type`： 指定该属性在es中的类型，其中的值是FileType类型的值，比如FileType.Text类型对应es中的text类型
- `index`：指定该词是否需要索引，默认为true
- `store`：指定该属性内容是否需要存储，默认为
- `fielddata` ：指定该属性能否进行排序，因为es中的text类型是不能进行排序（已经分词了）
- `searchAnalyzer` ： 指定搜索使用的分词器

## `@Document`

- `indexName`： 索引名称
- `type`：索引类型
- `shards`：分片的数量
- `replicas`：副本的数量
- `refreshInterval`： 刷新间隔
- `indexStoreType`：索引文件存储类型

## Text和Keyword

- text类型:
    1. 支持分词，全文检索,支持模糊、精确查询,不支持聚合,排序操作;
    2. text类型的最大支持的字符长度无限制,适合大字段存储；
> 使用场景：
        存储全文搜索数据, 例如: 邮箱内容、地址、代码块、博客文章内容等。
        默认结合standard analyzer(标准解析器)对文本进行分词、倒排索引。
        默认结合标准分析器进行词命中、词频相关度打分。

- keyword类型
    1. 不进行分词，直接索引,支持模糊、支持精确匹配，支持聚合、排序操作。
    2. keyword类型的最大支持的长度为——32766个UTF-8类型的字符,可以通过设置ignore_above指定自持字符长度，超过给定长度后的数据将不被索引，无法通过term精确匹配检索返回结果。
> 使用场景：
     存储邮箱号码、url、name、title，手机号码、主机名、状态码、邮政编码、标签、年龄、性别等数据。
     用于筛选数据(例如: select * from x where status='open')、排序、聚合(统计)。
     直接将完整的文本保存到倒排索引中。

## 搜索与过滤

### 合并查询

> 即`boolQuery`，可以设置多个条件的查询方式。它的作用是用来组合多个Query，有四种方式来组合，must，mustnot，filter，should。

- `must`代表返回的文档必须满足must子句的条件，会参与计算分值；
- `filter`代表返回的文档必须满足filter子句的条件，但不会参与计算分值；
- `should`代表返回的文档可能满足should子句的条件，也可能不满足，有多个should时满足任何一个就可以，通过minimum_should_match设置至少满足几个。
- `mustnot`代表必须不满足子句的条件。

譬如我想查询title包含“XXX”，且userId=“1”，且weight最好小于5的结果。那么就可以使用boolQuery来组合

### `Query`和`Filter`的区别

从代码上就能看出来，`query`和`Filter`都是`QueryBuilder`，也就是说在使用时，你把Filter的条件放到withQuery里也行，反过来也行。那么它们两个区别在哪？
查询在Query查询上下文和Filter过滤器上下文中，执行的操作是不一样的：

1. 查询：是在使用query进行查询时的执行环境，比如使用search的时候。
在查询上下文中，查询会回答这个问题——“这个文档是否匹配这个查询，它的相关度高么？”
ES中索引的数据都会存储一个_score分值，分值越高就代表越匹配。即使lucene使用倒排索引，对于某个搜索的分值计算还是需要一定的时间消耗。
2. 过滤器：在使用filter参数时候的执行环境，比如在bool查询中使用Must_not或者filter
在过滤器上下文中，查询会回答这个问题——“这个文档是否匹配？”
它不会去计算任何分值，也不会关心返回的排序问题，因此效率会高一点。
另外，经常使用过滤器，ES会自动的缓存过滤器的内容，这对于查询来说，会提高很多性能。

总而言之：
1 查询上下文：查询操作不仅仅会进行查询，还会计算分值，用于确定相关度；
2 过滤器上下文：查询操作仅判断是否满足查询条件，不会计算得分，查询的结果可以被缓存。
所以，根据实际的需求是否需要获取得分，考虑性能因素，选择不同的查询子句。

- `term`

不分词检索, 把检索串当作一个整体来执行检索, 即不会对检索串分词.

> term是完全匹配检索, 要用在不分词的字段上, 如果某个field在映射中被分词了, term检索将不起作用.
  所以, 不分词的field, 要在mapping中设置为不分词.

> 可以使用keyword字段进行term检索

- `terms` ，相当于多个`term`检索, 类似于SQL中`in`关键字的用法, 即在某些给定的数据中检索:

- `prefix`，就是前缀检索. 比如商品name中有多个以"Java"开头的document, 检索前缀"Java"时就能检索到所有以"Java"开头的文档.

> 扫描所有倒排索引, 性能较差

- `wildcard` 通配符检索

> 扫描所有倒排索引, 性能较差

```shell script
GET shop/_search
{
    "query": {
        "wildcard": { "name": "ja*" }
    }
}
```

- `regexp` 正则检索

> 扫描所有倒排索引, 性能较差

```shell script
GET shop/_search
{
    "query": {
        "regexp": { "name": "jav[a-z]*" }
    }
}
```

- `fuzzy` 模糊查询

> 说明：fuzzy才是实现真正的模糊查询，我们输入的字符可以是个大概，他可以根据我们输入的文字大概进行匹配查询，具体可看文章中的解释和代码，注意与wildcard模糊查询的区别


## API

- 搜索

```
http://localhost:9200/{索引名称}/_search
```

- `_cat`

```
http://localhost:9200/_cat
```

### 修改索引映射

> 假设已经有了一个test的索引，需要修改他的mapping，那么步骤如下

- 新建一个test_new的索引，为其创建新的mapping(修改后的test mapping)
```
PUT /test_new/
{
    "settings": {
        "number_of_shards": 3,
        "number_of_replicas": 1
    },
    "mappings": {
        "properties": {
            ...
        }
    }
}
```

- 向中间索引(test_new) 备份源索引(test)的数据 ,重建索引
```
 POST _reindex
 {
   "source": {
     "index": "test"
   },
   "dest": {
     "index": "test_new"
   }
 }
```

- 查询确认数据是否copy过去

```
 GET /test/_search
 
 GET /test_new/_search
```

- 删除有问题的索引
```
DELETE test
```

- 重新创建同名的索引(test)
```
PUT test/
```
- 创建Mapping(修改后的)
```
POST test/_mapping
```

- 从中间索引还原到源索引的数据
```
 # 重建索引
 POST _reindex
 {
   "source": {
     "index": "test_new"
   },
   "dest": {
     "index": "test"
   }
 }
```
- 删除中间索引

```
DELETE test_new
```

## ES数据导入导出

- 安装

```shell script
npm install elasticdump -g
```

- 将索引中的数据导出到本地

```
elasticdump  --input=http://localhost:9200/op_log --output=/Users/apple/doc/es/es_op_log_data_2020_08_11.json
```

- 将本地数据导入es中

```shell script
elasticdump  --input=/Users/apple/doc/es/es_op_log_data_2020_08_11.json --output=http://localhost:9200/op_log
```

- 将es导入另一个es

```shell script
elasticdump --input=http://ip:9200/demo --output=http://127.0.0.1:9200/demo
```


[参考](https://www.cnblogs.com/JimShi/p/11244126.html)
